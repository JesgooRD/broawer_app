package com.briliasm.browser.fragment;

import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.briliasm.browser.ControllerBarLayout;
import com.briliasm.browser.base.BasePageFragmentInterface;
import com.briliasm.browser.bean.MutiwindowData;
import com.briliasm.browser.subfragment.BookMarkFragment;
import com.briliasm.browser.subfragment.EntertainTableFragment;
import com.briliasm.browser.subfragment.NewsTableFragment;
import com.briliasm.browser.subfragment.ShortcutFragment;
import com.briliasm.browser.util.Log;
import com.briliasm.browser.util.Util;
import com.jesgoo.browser.R;

import java.util.ArrayList;


public class MainPageFragment extends Fragment implements BasePageFragmentInterface {
    FragmentActivity context = null;
    Handler handler;
    ViewPager pager = null;
    private int index;

    TextView t1,t2,t3,t4;

    private int offset = 0;// 动画图片偏移量
    private int currIndex = 0;// 当前页卡编号
    private int bmpW;// 动画图片宽度
    private ImageView cursor;// 动画图片
    private ControllerBarLayout controllerBar;
    EditText urlEt;
    Button goBtn;
    static String[] posterWhiteList = new String[]{".com",".cn",".org",".tv"};
    View view;
    public static MainPageFragment newInstance(FragmentActivity activity,Handler handler,int index) {

        MainPageFragment fragment = new MainPageFragment();
        fragment.handler = handler;
        fragment.context = activity;
        fragment.index = index;
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);



        return fragment;
    }

    public MainPageFragment() {
        // Required empty public constructor
    }
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Log.d("hidden_changed:" + this + hidden);
        if(hidden){
            this.onPause();
        }else{
            this.onResume();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = getLayoutInflater(savedInstanceState).inflate(R.layout.fragment_main_page, null);

        initUrlNavigation(view);
        initPagerViewer(view);
        initControllerBar(view);
    }
    public void setEditTextUrl(String content){
        urlEt.setText(content);
    }


    @Override
    public void onResume() {
        super.onResume();

        BrowserPageFragment browserPageFragment = MutiwindowData.mulWindowBrowserList.get(index);
        Log.d("onResume:" + this + " " + browserPageFragment);
        if(browserPageFragment != null && browserPageFragment.getUrl() != null){
            controllerBar.updateMainControllbarStatus(true);
        }
    }

    private void initUrlNavigation(View view) {
        urlEt = (EditText)view.findViewById(R.id.urlEt);
        urlEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId,KeyEvent event)  {
                if (actionId== EditorInfo.IME_ACTION_SEND ||(event!=null&&event.getKeyCode()== KeyEvent.KEYCODE_ENTER))
                {
                    goBtn.performClick();
                    return true;
                }
                return false;
            }
        });
        goBtn = (Button)view.findViewById(R.id.goBtn);
        goBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String keyword = urlEt.getText().toString();
                if(keyword.startsWith("http://") || keyword.startsWith("https://")){
                    Util.startUrl(context,keyword);
                    return;
                }
                for(int i=0;i<posterWhiteList.length;i++){
                    if(keyword.toLowerCase().endsWith(posterWhiteList[i])){
                        if(!keyword.startsWith("http://")){
                            keyword = "http://"+keyword;
                        }
                        Util.startUrl(context,keyword);
                        return;
                    }
                }
                Util.startUrl(context, "http://www.baidu.com/s?wd=" + keyword);

            }
        });
    }

    /**
     * 初始化PageViewer
     */
    private void initPagerViewer(View view) {


        cursor = (ImageView)view.findViewById(R.id.cursor);
        bmpW = BitmapFactory.decodeResource(getResources(), R.drawable.scroller).getWidth();
        DisplayMetrics dm = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int screenW = dm.widthPixels;
        offset = (screenW/4 - bmpW)/2;
        //imgageview设置平移，使下划线平移到初始位置（平移一个offset）
        Matrix matrix = new Matrix();
        matrix.postTranslate(offset, 0);
        cursor.setImageMatrix(matrix);


        t1 = (TextView) view.findViewById(R.id.text1);
        t2 = (TextView) view.findViewById(R.id.text2);
        t3 = (TextView) view.findViewById(R.id.text3);
        t4 = (TextView) view.findViewById(R.id.text4);
        t1.setOnClickListener(new MyOnClickListener(0));
        t2.setOnClickListener(new MyOnClickListener(1));
        t3.setOnClickListener(new MyOnClickListener(2));
        t4.setOnClickListener(new MyOnClickListener(3));



        pager = (ViewPager) view.findViewById(R.id.viewpage);
        pager.requestFocus();
        pager.setOffscreenPageLimit(4);
        final ArrayList<android.support.v4.app.Fragment> fragmentList = new ArrayList<android.support.v4.app.Fragment>();

        Fragment firstFragment = new NewsTableFragment();
        Fragment secondFragment = new EntertainTableFragment();
        Fragment thirdFragment = new ShortcutFragment();
        Fragment btFragment= new BookMarkFragment();
        fragmentList.add(firstFragment);
        fragmentList.add(secondFragment);
        fragmentList.add(thirdFragment);
        fragmentList.add(btFragment);


        pager.setAdapter(new MyPagerAdapter(this.getChildFragmentManager(), fragmentList));
        pager.setCurrentItem(0);
        pager.setOnPageChangeListener(new MyOnPageChangeListener());
    }
    private void initControllerBar(View view){
        controllerBar = (ControllerBarLayout)view.findViewById(R.id.controllbar);
        controllerBar.setHandler(handler);

        controllerBar.setFragment(this);
        controllerBar.updateMainControllbarStatus(false);
    }
    /**
     * 页卡切换监听
     */
    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {

        int one = offset * 2 + bmpW;// 页卡1 -> 页卡2 偏移量
        int two = one * 2;// 页卡1 -> 页卡3 偏移量

        @Override
        public void onPageSelected(int arg0) {
            Animation animation = new TranslateAnimation(currIndex*one,arg0*one,0,0);//平移动画
            currIndex = arg0;
            animation.setFillAfter(true);//动画终止时停留在最后一帧，不然会回到没有执行前的状态
            animation.setDuration(200);//动画持续时间0.2秒
            cursor.startAnimation(animation);//是用ImageView来显示动画的
//			int i = currIndex + 1;
//			Toast.makeText(MainActivity.this, "您选择了第"+i+"个页卡", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        return view;
    }

    /**
     * 头标点击监听
     */
    public class MyOnClickListener implements View.OnClickListener {
        private int index = 0;

        public MyOnClickListener(int i) {
            index = i;
        }

        @Override
        public void onClick(View v) {
            pager.setCurrentItem(index);
        }
    };


    public class MyPagerAdapter extends FragmentPagerAdapter {
        ArrayList<android.support.v4.app.Fragment> list;
        public MyPagerAdapter(FragmentManager fm,ArrayList<android.support.v4.app.Fragment> list) {
            super(fm);
            this.list = list;

        }


        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public android.support.v4.app.Fragment getItem(int arg0) {
            return list.get(arg0);
        }




    }

    @Override
    public boolean handleControllerBar() {
        return controllerBar.handleBackPressed();
    }


}
