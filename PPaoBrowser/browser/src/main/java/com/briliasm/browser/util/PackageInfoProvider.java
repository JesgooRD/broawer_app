package com.briliasm.browser.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

import com.briliasm.browser.bean.FavoriteData;

import java.util.ArrayList;
import java.util.List;
public class PackageInfoProvider {

    public static List<FavoriteData> getAppInfo(Context context) {
        List<FavoriteData> list = new ArrayList<>();


        PackageManager pm = context.getPackageManager();
        List<PackageInfo> pakageinfos = pm.getInstalledPackages(PackageManager.GET_UNINSTALLED_PACKAGES);

        for (PackageInfo packageInfo : pakageinfos) {

            if(!filterApp(packageInfo.applicationInfo)){
                continue;
            }
            FavoriteData data = new FavoriteData();
            String str_name = packageInfo.applicationInfo.loadLabel(pm).toString();
            data.setTitle(str_name);
            String version = packageInfo.versionName;
            Drawable drawable = packageInfo.applicationInfo.loadIcon(pm);
            data.setPkg(packageInfo.packageName);
            data.setDrawable(drawable);
            data.setChecked(false);
            list.add(data);

        }

        return list;
    }

    /**
     * 三方应用程序的过滤器
     *
     * @param info
     * @return true 三方应用 false 系统应用
     */
    public static  boolean filterApp(ApplicationInfo info) {
//        if ((info.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0) {
//// 代表的是系统的应用,但是被用户升级了. 用户应用
//            return true;
//        } else
         if ((info.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
// 代表的用户的应用
            return true;
        }
        return false;
    }
}