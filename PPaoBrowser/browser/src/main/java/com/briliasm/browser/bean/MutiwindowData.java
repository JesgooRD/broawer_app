package com.briliasm.browser.bean;

import android.graphics.drawable.Drawable;

import com.briliasm.browser.fragment.BrowserPageFragment;
import com.briliasm.browser.fragment.MainPageFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangliang on 15/7/23.
 */
public class MutiwindowData {

    public  static List<BrowserPageFragment> mulWindowBrowserList = new ArrayList(9);
    public  static List<MainPageFragment> mulWindowMainList = new ArrayList(9);
    public  static List<MutiwindowData> mulWindowDataList = new ArrayList(9);
    static{
        for(int i=0;i<9;i++){
            MutiwindowData.mulWindowMainList.add(null);
        }

        for(int i=0;i<9;i++){
            MutiwindowData.mulWindowBrowserList.add(null);
        }

        for(int i=0;i<9;i++){
            MutiwindowData.mulWindowDataList.add(new MutiwindowData());
        }


    }
    private Drawable drawable;


    private String title;
    private String url;
    private int id;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }
}
