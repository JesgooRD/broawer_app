package com.briliasm.browser.dl;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.briliasm.browser.util.Log;
import com.briliasm.browser.util.Util;

import java.util.ArrayList;
import java.util.List;
/**
 * 
 * 一个业务类
 */
public class Dao {
	private static DBHelper dbHelper;
	private Context context;

	public Dao(Context context) {
		this.context = context;
		if (dbHelper == null) {
			dbHelper = new DBHelper(context, "jesgoo_lenovo_download.db");
		}

	}

	private boolean testDB() {
		try {

			SQLiteDatabase database = dbHelper.getReadableDatabase();// 如果与app的db名冲突，此处会抛出异常
			String sql = "select count(*)  from download_info where url =?";

			Cursor cursor = database.rawQuery(sql, new String[] { "test" });
			cursor.moveToFirst();
			cursor.getInt(0);
			cursor.close();

			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static DownloadData getDownloadData(String urlstr) {
		String fileName = Util.getNameFromUrl(urlstr);
		SQLiteDatabase database = dbHelper.getReadableDatabase();
		String sql = "select thread_id,start_pos, end_pos,compelete_size,url from download_info where filename=?";
		Cursor cursor = database.rawQuery(sql, new String[] { fileName });
		DownloadData data = null;
		while (cursor.moveToNext()) {
			if(data == null){
				data = new DownloadData();
			}
//			String url = cursor.getString(4);
			String url = urlstr;//当url变化时，用新的url下载，但是数据库里面的url字段不变
			long fSize = data.getFileSize() + cursor.getInt(2) - cursor.getInt(1) + 1;
			long cmpSize = data.getComplete() + cursor.getInt(3);
			data.setComplete(cmpSize);
			data.setFileSize(fSize);
			data.setUrlstring(url);
		}
		return data;

	}

	//
	// public List<DownloadData> getDownloadData(){
	//
	// SQLiteDatabase database = dbHelper.getReadableDatabase();
	// String sql =
	// "select thread_id,start_pos, end_pos,compelete_size,url from download_info;";
	// Cursor cursor = database.rawQuery(sql, new String[] { });
	// HashMap<String,DownloadData> dmap = new HashMap<String,DownloadData>();
	// List<DownloadData> dataList = new ArrayList<DownloadData>();
	// while (cursor.moveToNext()) {
	// String url = cursor.getString(4);
	// DownloadData data = dmap.get(url);
	//
	// long fSize = 0;
	// long cmpSize = 0;
	// if(data != null){
	// fSize = data.getFileSize()+cursor.getInt(2)-cursor.getInt(1)+1;
	// cmpSize = data.getComplete()+cursor.getInt(3);
	// }else{
	// fSize = cursor.getInt(2)-cursor.getInt(1)+1;
	// cmpSize = cursor.getInt(3);
	// data = new DownloadData();
	// }
	// data.setComplete(cmpSize);
	// data.setFileSize(fSize);
	// data.setUrlstring(cursor.getString(4));
	// dmap.put(url, data);
	// }
	// cursor.close();
	// Set<String> keys = dmap.keySet();
	// Iterator<String> it =keys.iterator();
	// while(it.hasNext()){
	// String key = it.next();
	// DownloadData data = dmap.get(key);
	//
	// dataList.add(data);
	// }
	//
	//
	// return dataList;
	// }

	/**
	 * 查看数据库中是否有数据
	 */
	public boolean isHasInfors(String urlstr) {
		String fileName = Util.getNameFromUrl(urlstr);
		SQLiteDatabase database = dbHelper.getReadableDatabase();
		String sql = "select count(*)  from download_info where filename=?";
		Cursor cursor = database.rawQuery(sql, new String[] { fileName });
		cursor.moveToFirst();
		int count = cursor.getInt(0);
		cursor.close();
		Log.d("isHasInfors count:" + count);
		return count == 0;
	}

	/**
	 * 保存 下载的具体信息
	 */
	public void saveInfos(List<PartInfo> infos) {
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		try {
			for (PartInfo info : infos) {
				String sql = "insert into download_info(thread_id,start_pos, end_pos,compelete_size,url,filename) values (?,?,?,?,?,?)";
				Object[] bindArgs = { info.getThreadId(), info.getStartPos(), info.getEndPos(),
						info.getCompeleteSize(), info.getUrl(),Util.getNameFromUrl(info.getUrl()) };
				database.execSQL(sql, bindArgs);
				
				Log.d("saveInfos:"+bindArgs);
			}
			
		} catch (Exception e) {
			Log.d("saveInfos:"+e);
		}
	}

	/**
	 * 得到下载具体信息
	 */
	public List<PartInfo> getInfos(String filename) {
		List<PartInfo> list = new ArrayList<PartInfo>();
		SQLiteDatabase database = dbHelper.getReadableDatabase();
		String sql = "select thread_id, start_pos, end_pos,compelete_size,url,filename from download_info where filename=?";
		Cursor cursor = database.rawQuery(sql, new String[] { filename });
		while (cursor.moveToNext()) {
			PartInfo info = new PartInfo(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3),
					cursor.getString(4));
			list.add(info);
		}
		cursor.close();
		return list;
	}

	/**
	 * 更新数据库中的下载信息
	 */
	public void updataInfos(int threadId, long compeleteSize, String filename) {
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		String sql = "update download_info set compelete_size=? where thread_id=? and filename=?";
		Object[] bindArgs = { compeleteSize, threadId, filename };
		try {
			database.execSQL(sql, bindArgs);

		} catch (Exception e) {

		}
	}

	/**
	 * 关闭数据库
	 */
	public void closeDb() {
		try {
			dbHelper.close();
		} catch (Exception e) {

		}
	}

	public void deleteAll() {
		try {
			SQLiteDatabase database = dbHelper.getWritableDatabase();
			database.delete("download_info", null, null);
			database.close();
		} catch (Exception e) {

		}

	}

	/**
	 * 下载完成后删除数据库中的数据
	 */
	public void delete(String url) {
		String fileName = Util.getNameFromUrl(url);
		try {
			SQLiteDatabase database = dbHelper.getWritableDatabase();
			database.delete("download_info", "filename=?", new String[] { fileName });
			database.close();
		} catch (Exception e) {

		}
	}

	class DBHelper extends SQLiteOpenHelper {
		public DBHelper(Context context, String dbName) {
			super(context, dbName, null, 1);
		}

		/**
		 * 在download.db数据库下创建一个download_info表存储下载信息
		 */
		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("create table download_info(_id integer PRIMARY KEY AUTOINCREMENT, thread_id integer, "
					+ "start_pos integer, end_pos integer, compelete_size integer,url char,filename char)");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		}

	}
}