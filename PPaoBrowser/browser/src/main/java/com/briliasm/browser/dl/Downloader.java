package com.briliasm.browser.dl;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Message;

import com.briliasm.browser.util.Log;
import com.briliasm.browser.util.Util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

class Downloader {
	private Context context;
	private String urlstr;// 下载的地址
	protected String localfile;// 保存路径
	protected String fileName;
	private static final int threadcount = 1;// 线程数

	private Handler mHandler;// 消息处理器
	private Dao dao;// 工具类
	protected long cmpSize = 0;
	protected long fileSize = 0;// 所要下载的文件的大小
	protected long lastCmpSize=0;
	private List<PartInfo> infos;// 存放下载信息类的集合
	private static final int INIT = 1;// 定义三种下载的状态：初始化状态，正在下载状态，暂停状态
	private static final int DOWNLOADING = 2;
	private static final int PAUSE = 3;
	private int state = INIT;
	protected long lastData = System.currentTimeMillis();

	Map<Long,MyThread> threads= new HashMap<Long,MyThread>();
	public Downloader(String urlstr, long contentLength, String fileName,Context context,
			Handler mHandler) {
		this.urlstr = urlstr;
		this.fileName = fileName;
		this.localfile = Util.DOWNLOAD_PATH+fileName;
		this.mHandler = mHandler;
		this.fileSize = (int) contentLength;
		this.context = context;
		dao = new Dao(context);
		File f = new File(Util.DOWNLOAD_PATH);
		if(!f.exists() || !f.isDirectory()){
			f.mkdirs();
		}
		boolean isFirst = isFirst(urlstr);
		if (isFirst) {
			init();
			cmpSize = 0;
			long range = fileSize / threadcount;
			infos = new ArrayList<PartInfo>();
			for (int i = 0; i < threadcount - 1; i++) {
				PartInfo info = new PartInfo(i, i * range, (i + 1) * range - 1, 0, urlstr);
				infos.add(info);
				Log.d("Downloader.getDownloaderInfors  create parti= "+i+ "   " + info.getCompeleteSize()+" / "+(info.getEndPos()-info.getStartPos()+1));
			}
			PartInfo info = new PartInfo(threadcount - 1, (threadcount - 1) * range, fileSize - 1, 0, urlstr);
			infos.add(info);
			Log.d("Downloader.getDownloaderInfors create parti= " + (threadcount - 1) + "   " + info.getCompeleteSize() + " / " + (info.getEndPos() - info.getStartPos() + 1));
			// 保存infos中的数据到数据库
			dao.saveInfos(infos);

		}
	}


	/**
	 * 初始化
	 */
	private void init() {
		Log.d("Downloader.init() threadid:"+Thread.currentThread()+" fileSize="+fileSize+"  ts:"+System.currentTimeMillis());
		if (fileSize <= 0) {
			// -1是因为长度未知，0是因为没有初始化
			try {
				URL url = new URL(urlstr);
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				connection.setConnectTimeout(5000);
				connection.setRequestMethod("GET");
				fileSize = connection.getContentLength();

				connection.disconnect();
			} catch (Exception e) {
				Log.d("Downloader", " 获取fileSize失败");
			}
			Log.d("Downloader.init():"+"  http获取文件长度后 ts:"+System.currentTimeMillis());
		}
		Log.d("Downloader.init():"+"  开始建立random文件 ts:"+System.currentTimeMillis());
		File file = new File(localfile);
		if (!file.exists()) {
			try {
				file.createNewFile();
				// 本地访问文件
				RandomAccessFile accessFile = new RandomAccessFile(file, "rwd");
				accessFile.setLength(fileSize);
				accessFile.close();
			} catch (Exception e) {
				Log.d("Downloader", " 建立文件失败:" + localfile);
			}
		}
		Log.d("Downloader.init():"+"  建立完random文件 ts:"+System.currentTimeMillis());
	}

	/**
	 * 判断是否是第一次 下载
	 */
	private boolean isFirst(String urlstr) {
		return dao.isHasInfors(urlstr);
	}

	/**
	 * 利用线程开始下载数据
	 */
	public void download() {
		
		if (state == DOWNLOADING)
			return;
		// 得到数据库中已有的urlstr的下载器的具体信息
		infos = dao.getInfos(fileName);

		int compeleteSize = 0;
		for (PartInfo info : infos) {
			compeleteSize += info.getCompeleteSize();
			Log.d("Downloader.getDownloaderInfors download parti= "+info.getThreadId()+ "   " + info.getCompeleteSize()+" / "+(info.getEndPos()-info.getStartPos()+1));
		}
		cmpSize = compeleteSize;
		state = DOWNLOADING;
		for (PartInfo info : infos) {
			threads= new HashMap<Long,MyThread>();
			MyThread mth = new MyThread(info.getThreadId(), info.getStartPos(), info.getEndPos(), info.getCompeleteSize(),
					info.getUrl());
			mth.start();
			threads.put(mth.getId(), mth);
		}
	}

	public class MyThread extends Thread {
		private int threadId;
		private long startPos;
		private long endPos;
		private long compeleteSize;
		private String urlstr;
		InputStream is = null;
		
		private void myDestory(){
			try{
				if(is != null){
					is.close();
					is = null;
					Log.d("MyThread.myDestroy()");
				}
			}catch(Exception e){}
		}
		public MyThread(int threadId, long startPos, long endPos, long compeleteSize, String urlstr) {
			this.threadId = threadId;
			this.startPos = startPos;
			this.endPos = endPos;
			this.compeleteSize = compeleteSize;
			this.urlstr = urlstr;
		}

		@Override
		public void run() {
			HttpURLConnection connection = null;
			RandomAccessFile randomAccessFile = null;
			
			try {
				randomAccessFile = new RandomAccessFile(localfile, "rwd");
				randomAccessFile.seek(startPos + compeleteSize);
				int sdkVersion = Build.VERSION.SDK_INT;
				if(sdkVersion >= 21 ){

					HttpClient client = new DefaultHttpClient();
					HttpGet httpget = new HttpGet(urlstr);
					HttpResponse response = client.execute(httpget);

					HttpEntity entity = response.getEntity();
					is = new BufferedInputStream(entity.getContent());//buffer size is 8192

				}else{
					URL url = new URL(urlstr);
					connection = (HttpURLConnection) url.openConnection();
					connection.setConnectTimeout(5000);
					connection.setRequestMethod("GET");
					// 设置范围，格式为Range：bytes x-y;
					connection.setRequestProperty("Range", "bytes=" + (startPos + compeleteSize) + "-" + endPos);
//						 将要下载的文件写到保存在保存路径下的文件中
					is = new BufferedInputStream(connection.getInputStream());////buffer size <= 2048 when sdk>=21, not limited  when sdk <21

				}

				byte[] buffer = new byte[1024 * 1024 ];
				int length = -1;
				long c1 = System.currentTimeMillis();
				while ((length = is.read(buffer)) != -1) {
					randomAccessFile.write(buffer, 0, length);

					//分段大小
					compeleteSize += length;
					//总大小
					cmpSize += length;


					// 用消息将下载信息传给进度条，对进度条进行更新，30k更新一次
					if(fileSize - cmpSize <2 || cmpSize - lastCmpSize >1024*30){
						lastCmpSize = cmpSize;
						Message message = Message.obtain();
						message.what = 1;
						message.obj = urlstr;
						mHandler.sendMessage(message);
						// 更新数据库中的下载信息
						dao.updataInfos(threadId, compeleteSize, fileName);
					}
					Log.d("read.length: "+length+"   "+compeleteSize/1024/1024 +" / "+fileSize/1024/1024);
//					Log.d("MyThread.read() state="+state+" progress="+cmpSize);
					if (state == PAUSE || state == INIT) {
						return;
					}
					sendDoneMsg();
				}

				long c2 = System.currentTimeMillis();
				Log.d("read.time:"+(c2-c1));
			} catch (Exception e) {
				Log.d("MyThread.read() exception:", e.getMessage());
			} finally {
				try {
					if (randomAccessFile != null) {
						randomAccessFile.close();
					}
					if (connection != null) {
						connection.disconnect();
					}
					if (is != null) {
						is.close();
					}
					if (dao != null) {
						dao.closeDb();
					}
					threads.remove(this.getId());
				} catch (Exception e) {
					Log.d("DownloadThread", e.getMessage());
				}
			}

		}
	}
	boolean doneMsgSent = false;
	synchronized void sendDoneMsg(){
		if(cmpSize == fileSize){

			Message message = Message.obtain();
			message.what = 2;
			message.obj = urlstr;
			mHandler.sendMessage(message);
			doneMsgSent = true;

			Log.d("download_progress:" + localfile+"  "+Thread.currentThread());
		}
	}

	// 删除数据库中urlstr对应的下载器信息
	public void delete(String urlstr) {
		dao.delete(urlstr);
		dao.closeDb();
	}

	// 设置暂停
	public void pause() {
		state = PAUSE;
		dao.closeDb();
	}

	// 重置下载状态
	public void reset() {
		state = INIT;
		try{
			if(threads != null){
				Log.d("MyThread.threads.size="+threads.size());
				Set<Long> keySets = threads.keySet();
				Iterator<Long> it = keySets.iterator();
				while(it.hasNext()){
					MyThread myThread = threads.get(it.next()); 
					myThread.myDestory();
				}
				threads.clear();
				threads = null;
			}
		}catch(Exception e){
			Log.d("Downloader.reset:"+e);
		}
	}

}