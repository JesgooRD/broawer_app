package com.briliasm.browser;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.briliasm.browser.base.BasePageFragmentInterface;
import com.briliasm.browser.bean.ActivityMsg;
import com.briliasm.browser.bean.MutiwindowData;
import com.briliasm.browser.fragment.BrowserPageFragment;
import com.briliasm.browser.fragment.MainPageFragment;
import com.briliasm.browser.util.Log;
import com.briliasm.browser.util.UpdateManager;
import com.briliasm.browser.util.Util;
import com.jesgoo.browser.R;
import com.jesgoo.sdk.AdSize;
import com.jesgoo.sdk.AdView;

public class MainActivity extends FragmentActivity {
    private static boolean isExit = false;
    BasePageFragmentInterface curFragment;
    int index = 0;

    //    private static boolean isShowAd = true;
    private static boolean isShowAd = false;


    private MainPageFragment setAndGetMainFragment(int index) {
        MutiwindowData.mulWindowDataList.get(index).setUrl(null);
        MutiwindowData.mulWindowDataList.get(index).setTitle("首页");
        MutiwindowData.mulWindowDataList.get(index).setDrawable(getResources().getDrawable(R.drawable.main_page_icon));

        MainPageFragment fragment = MutiwindowData.mulWindowMainList.get(index);
        if (fragment == null) {
            fragment = MainPageFragment.newInstance(this, handler, index);
            MutiwindowData.mulWindowMainList.set(index, fragment);
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE |
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//		transaction.setCustomAnimations(R.anim.back_left_in,R.anim.back_right_out);
//		transaction.setCustomAnimations(R.animator.slide_left_in, R.animator.slide_left_out);//(R.animator.slide_right_in, R.animator.slide_left_out,R.animator.slide_left_in, R.animator.slide_right_out);
//		transaction.replace(R.id.main, fragment, "aa");
        hideAllFragment(transaction, fragment);
        if (fragment.isAdded()) {
            transaction.show(fragment);

            Log.d("showfragment:" + fragment);
        } else {
            transaction.add(R.id.main, fragment, fragment.getClass().getName() + index);
            Log.d("addfragment:" + fragment);
        }
        transaction.commit();
        this.curFragment = fragment;
        return fragment;
    }

    private BrowserPageFragment setAndGetBrowserFragment(int index, String url) {
        BrowserPageFragment fragment = MutiwindowData.mulWindowBrowserList.get(index);
        if (fragment == null) {
            fragment = BrowserPageFragment.newInstance(this, handler, url);
            MutiwindowData.mulWindowBrowserList.set(index, fragment);
        } else {
            fragment.updateUrl(url);
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE |
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//		transaction.replace(R.id.main, fragment, "aa");
        transaction.commit();
        hideAllFragment(transaction, fragment);
        if (fragment.isAdded()) {
            transaction.show(fragment);
            Log.d("showfragment:" + fragment);
        } else {
            transaction.add(R.id.main, fragment, fragment.getClass().getName() + index);
            Log.d("addfragment:" + fragment);
        }


        this.curFragment = fragment;
        return fragment;
    }

    private void hideAllFragment(FragmentTransaction transaction, Fragment fragment) {
        for (int i = 0; i < MutiwindowData.mulWindowBrowserList.size(); i++) {
            Fragment f = MutiwindowData.mulWindowBrowserList.get(i);
            if (f != null && f.isAdded() && !f.isHidden() && f != fragment) {
                transaction.hide(f);
                Log.d("hidefragment:" + f);
            }

            Fragment f2 = MutiwindowData.mulWindowMainList.get(i);
            if (f2 != null && f2.isAdded() && !f2.isHidden() && f2 != fragment) {
                transaction.hide(f2);
                Log.d("hidefragment:" + f2);
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UpdateManager.checkNeedsUpdate(this);
        Util.updateConfig(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        Intent it = getIntent();
        String urlString = it.getDataString();

        if (urlString != null) {
            setAndGetBrowserFragment(index, urlString);
        } else {
            setAndGetMainFragment(index);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                Util.httpPing(MainActivity.this, Util.STATISTICS_URL + "andid=" + Secure.getString(MainActivity.this.getContentResolver(), Secure.ANDROID_ID) + "&brd=" + Build.BRAND + "&model=" + Build.MODEL + "&osv=" + Build.VERSION.RELEASE);
            }
        }).start();


        if (isShowAd) {
            configInterstitialAD();
        }

//		PackageInfoProvider.getAppInfo(this);

//		Util.isExistShortcut(this, pkgs);
    }


//	private AdView interstitialAd;

    private void configInterstitialAD() {


        AdView.preLoad(this);

        AdView interstitialAd = new AdView(this, AdSize.Interstitial, "s10d87d8");


        interstitialAd.setListener(new AdView.AdViewListener() {
            @Override
            public void onAdReady(AdView adView) {

                Log.e("vinsen", "onAdReady");

                isShowAd = false;

                adView.showInterstialAd();
            }

            @Override
            public void onAdShow() {
                Log.e("vinsen", "onAdShow");


            }

            @Override
            public void onAdClick() {
                Log.e("vinsen", "onAdClick");


            }

            @Override
            public void onAdFailed(String s) {
                Log.e("vinsen", "onAdFailed");

            }

            @Override
            public void onEvent(String s) {
                Log.e("vinsen", "onEvent");


            }
        });

        interstitialAd.showInterstialAd();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String urlString = intent.getDataString();
        if (urlString != null) {

            setAndGetBrowserFragment(index, urlString);
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (!curFragment.handleControllerBar()) {
                if (curFragment instanceof MainPageFragment) {
                    exit();
                } else {
                    setAndGetMainFragment(index);
                }
            }
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (ActivityMsg.getActivityMsg(msg.what)) {
                case BACK:
                    isExit = false;
                    break;
                case BROWSER2MAIN:
                    if (!(curFragment instanceof MainPageFragment)) {
                        setAndGetMainFragment(index);
                    }

                    break;
                case SELECT_WINDOW:
                    if (msg.arg1 != index) {
                        index = msg.arg1;
                        MutiwindowData data = (MutiwindowData) msg.obj;
                        if (MutiwindowData.mulWindowBrowserList.get(index) != null) {
                            setAndGetBrowserFragment(index, data.getUrl());
                        } else {
                            setAndGetMainFragment(index);
                        }
                    }

                    break;
                case MAIN2BROWSER:
                    Util.startUrl(MainActivity.this, MutiwindowData.mulWindowBrowserList.get(index).getUrl());
                    break;
                default:
                    break;
            }

        }
    };

    private void exit() {
        if (!isExit) {
            isExit = true;
            Toast.makeText(getApplicationContext(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
            Message msg = Message.obtain();
            msg.what = ActivityMsg.BACK.getValue();
            handler.sendMessageDelayed(msg, 2000);
        } else {
            finish();
            System.exit(0);
        }
    }

    @Override
    public void finish() {
        ViewGroup view = (ViewGroup) getWindow().getDecorView();
        view.removeAllViews();
        super.finish();
    }
}
