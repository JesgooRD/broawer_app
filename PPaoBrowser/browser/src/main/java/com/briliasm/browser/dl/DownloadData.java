package com.briliasm.browser.dl;

/**
  *自定义的一个记载下载器详细信息的类 
  */
public class DownloadData {
    public static final int STATUS_DONE = 3;
    public static final int STATUS_PAUSE = 2;
    public static final int STATUS_DOWNLOADING = 1;
//    public static final int STATUS_NONE = 0;
     private long fileSize;//文件大小
     private long complete;//完成度
     private String urlstring;//下载器标识
//    private int status = STATUS_PAUSE;
     public DownloadData(long fileSize, long complete, String urlstring) {
         this.fileSize = fileSize;
         this.complete = complete;
         this.urlstring = urlstring;
     }
     public DownloadData() {
     }
     public long getFileSize() {
         return fileSize;
     }
     public void setFileSize(long fileSize) {
         this.fileSize = fileSize;
     }
     public long getComplete() {
         return complete;
     }
     public void setComplete(long complete) {
         this.complete = complete;
     }
     public String getUrlstring() {
         return urlstring;
     }
     public void setUrlstring(String urlstring) {
         this.urlstring = urlstring;
     }
     @Override
     public String toString() {
         return "[fileSize=" + fileSize + ", complete=" + complete
                 + ", urlstring=" + urlstring + "]";
     }
//     public int getStatus(){
//
//         return status;
//     }
//    public void setStatus(int status){
//        this.status = status;
//    }


 }