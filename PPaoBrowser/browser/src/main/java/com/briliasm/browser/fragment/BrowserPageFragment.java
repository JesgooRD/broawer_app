package com.briliasm.browser.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.briliasm.browser.ControllerBarLayout;
import com.briliasm.browser.MyWebView;
import com.jesgoo.browser.R;
import com.briliasm.browser.base.BasePageFragmentInterface;
import com.briliasm.browser.bean.FavoriteData;
import com.briliasm.browser.bean.MutiwindowData;
import com.briliasm.browser.subfragment.BookMarkFragment;
import com.briliasm.browser.util.FavoriteDao;
import com.briliasm.browser.util.Log;

import java.util.ArrayList;
import java.util.List;


public class BrowserPageFragment extends Fragment implements BasePageFragmentInterface {


    private String url;
    private Handler handler;
    ControllerBarLayout controllbar;
    MyWebView myWebView;
    private int index = -1;
    public int getIndex(){
        if(index != -1){
            return index;
        }
        for(int i=0;i< MutiwindowData.mulWindowBrowserList.size();i++){
            if(MutiwindowData.mulWindowBrowserList.get(i) == this){
                index = i;
                return index;
            }
        }
        return index;
    }

    public static BrowserPageFragment newInstance(FragmentActivity activity,Handler handler,String url) {
        BrowserPageFragment fragment = new BrowserPageFragment();
        fragment.handler = handler;
        fragment.url= url;

        return fragment;
    }


    public String getUrl(){
        return url;
    }
    public void updateUrl(String url){
        if(this.url != url ){
//            Log.e("updateUrl:" + url);
            myWebView.clearHistory();
            myWebView.setClear(true);
            myWebView.clearCache(true);
//            myWebView.loadUrl("javascript:document.open();document.close();");
            myWebView.loadUrl(url);

        }
        this.url = url;
    }
    public BrowserPageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("hidden_changed:onPause");
        if(myWebView != null){
            myWebView.onPause();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d("hidden_changed:onResume");
        if(myWebView != null){
            myWebView.onResume();
        }
    }

//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
//        Log.d("hidden_changed:" +this +hidden);
//        if(hidden){
//            this.onPause();
//            if(myWebView != null){
//                myWebView.onPause();
//            }
//        }else{
//            this.onResume();
//            if(myWebView != null){
//                myWebView.onResume();
//            }
//        }
//    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_browser_page,null);
        initMyWebView(view);
        return view;
    }
    private void initMyWebView(View view){

        controllbar = (ControllerBarLayout)view.findViewById(R.id.controllbar);
        controllbar.setHandler(handler);


        myWebView = (MyWebView)view.findViewById(R.id.myWebView);
        myWebView.setHandler(handler);
        myWebView.setControllerBar(controllbar);
        myWebView.setId(getIndex());

        controllbar.setFragment(this);
        controllbar.updateBrowserControllbarStatus();
        myWebView.loadUrl(url);
    }


    @Override
    public boolean handleControllerBar() {
        return controllbar.handleBackPressed();
    }
    public void saveBookMark(){
        if(myWebView!=null && myWebView.getUrl() != null){
            FavoriteDao favoriteDao = new FavoriteDao(getActivity());
            List<FavoriteData> list = new ArrayList<>();
            FavoriteData data = new FavoriteData();
            data.setTitle(myWebView.getTitle());
            data.setUrl(myWebView.getUrl());
            list.add(data);
            favoriteDao.saveBookmark(list);
            favoriteDao.closeDb();
            BookMarkFragment.update(data);
        }else{
            Toast.makeText(getActivity(),"无法保存书签",Toast.LENGTH_SHORT).show();
        }

    }
    public void refresh(){
        if(myWebView!=null && myWebView.getUrl() != null){
            myWebView.reload();
        }
    }

}
