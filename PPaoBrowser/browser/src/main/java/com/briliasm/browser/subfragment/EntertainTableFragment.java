package com.briliasm.browser.subfragment;

import com.briliasm.browser.base.BaseTableFragment;
import com.briliasm.browser.bean.FavoriteData;
import com.briliasm.browser.util.Util;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;


public class EntertainTableFragment extends BaseTableFragment {


    public EntertainTableFragment(){
    }
    public List<FavoriteData>  getData(){
        List<FavoriteData> list =  new ArrayList<>();
//        String newsStr = Util.getFromAssets(getActivity(),"entertain_shortcut_list.json");
        String newsStr = Util.readPrivateFileData(getActivity(),Util.ENTERTAIN_FILE);
        if("".equals(newsStr)){
            newsStr = "[\n" +
                    "  {\"title\":\"微信热文\",\"img\":\"weixin\",\"url\":\"http://weixin.sogou.com/wap\"},\n" +
                    "  {\"title\":\"今日头条\",\"img\":\"toutiao\",\"url\":\"http://m.toutiao.com/\"},\n" +
                    "  {\"title\":\"爱笑不笑\",\"img\":\"axbx\",\"url\":\"http://aixiaobuxiao.com/\"},\n" +
                    "  {\"title\":\"H5游戏\",\"img\":\"youxi\",\"url\":\"http://h5.m.gamedog.cn/\"},\n" +
                    "  {\"title\":\"乐视\",\"img\":\"leshi\",\"url\":\"http://m.letv.com/\"},\n" +
                    "  {\"title\":\"爱奇艺\",\"img\":\"iqiyi\",\"url\":\"http://m.iqiyi.com/\"},\n" +
                    "  {\"title\":\"优酷\",\"img\":\"youku\",\"url\":\"http://www.youku.com/\"},\n" +
                    "  {\"title\":\"宜搜\",\"img\":\"yisou\",\"url\":\"http://book.easou.com/\"},\n" +
                    "  {\"title\":\"糗事百科\",\"img\":\"qsbk\",\"url\":\"http://www.qiushibaike.com/\"},\n" +
                    "  {\"title\":\"暴走漫画\",\"img\":\"baozoumanhua\",\"url\":\"http://m.baozoumanhua.com/\"},\n" +
                    "  {\"title\":\"冷笑话\",\"img\":\"lengxiaohua\",\"url\":\"http://m.lengxiaohua.cn/\"},\n" +
                    "  {\"title\":\"漫画大全\",\"img\":\"manhua\",\"url\":\"http://book.2345.com/m/manhua/\"}\n" +
                    "]";
        }
        try {
            JSONArray newsList = new JSONArray(newsStr);
            for(int i=0;i<newsList.length();i++){
                FavoriteData data = new FavoriteData();
                data.setTitle(newsList.getJSONObject(i).optString("title"));
                data.setUrl(newsList.getJSONObject(i).optString("url"));
                data.setPkg(null);
                data.setDrawable(getResources().getDrawable(Util.getDrawableId(getActivity(), newsList.getJSONObject(i).optString("img"))));
                list.add(data);
            }
        }catch (Exception e){
        }

        return list;
    }

}  