package com.briliasm.browser.util;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.briliasm.browser.MainActivity;

import org.apache.http.util.EncodingUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class Util {


    private static DisplayMetrics displayMetrics = null;

    public static String DOWNLOAD_PATH = "/sdcard/minibrowser/";
    public static String UPDATE_URL = "http://cdn.jesgoo.com/app/minibrowser/version.txt";
    public static String APK_URL = "http://cdn.jesgoo.com/app/minibrowser/minibrowser.apk";
    public static String CONFIG_URL = "http://cdn.jesgoo.com/app/minibrowser/config";
    public static String STATISTICS_URL = "http://rcv.moogos.com/rtsdk?type=a0&";
    public static String NEWS_FILE = "news_shortcut_list.json";
    public static String ENTERTAIN_FILE = "entertain_shortcut_list.json";
    public static DisplayMetrics getMetrics(Context context) {
        if (displayMetrics == null) {
            displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getApplicationContext().getSystemService(Context.WINDOW_SERVICE))
                    .getDefaultDisplay().getMetrics(displayMetrics);
        }
        return displayMetrics;
    }
    public static boolean isWifi(Context context) {

        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo net = cm.getActiveNetworkInfo();
            if (net != null && net.isConnectedOrConnecting()) {
                if (net.getType() == ConnectivityManager.TYPE_WIFI) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            Log.d(e);
        }

        return false;
    }
    public static void openFile(Context context,String localFile){
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction(android.content.Intent.ACTION_VIEW);
        String mimeType = "text/plain";
        if(localFile.endsWith(".apk")){
            mimeType ="application/vnd.android.package-archive";
        }else if(localFile.endsWith(".jpg") || localFile.endsWith(".png")||localFile.endsWith(".gif")||localFile.endsWith(".jpeg")){
            mimeType = "image/*";
        }else if(localFile.endsWith(".pdf")){
            mimeType = "application/pdf";
        }else if(localFile.endsWith(".ppt")||localFile.endsWith(".pptx")){
            mimeType = "application/vnd.ms-powerpoint";
        }else if(localFile.endsWith(".xls")||localFile.endsWith(".xlsx")){
            mimeType = "application/vnd.ms-excel";
        }else if(localFile.endsWith(".doc")||localFile.endsWith(".docx")){
            mimeType = "application/msword";
        }else if(localFile.endsWith(".mp3")||localFile.endsWith(".wav")||localFile.endsWith(".ogg")||localFile.endsWith(".midi")){
            mimeType = "audio/*";
        }else if(localFile.endsWith(".mp4")||localFile.endsWith(".rmvb")||localFile.endsWith(".avi")||localFile.endsWith(".flv")){
            mimeType = "video/*";
        }else if(localFile.endsWith(".jar")||localFile.endsWith(".zip")||localFile.endsWith(".rar")||localFile.endsWith(".gz")){
            mimeType = "application/vnd.android.package-archive";
        }else if(localFile.endsWith(".htm")||localFile.endsWith(".html")){
            mimeType = "text/html";
        }

        intent.setDataAndType(Uri.fromFile(new File(localFile)), mimeType);
        context.startActivity(intent);
    }


    public static void startUrl(Context context,String url){
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");

        Uri content_url = Uri.parse(url);
        intent.setData(content_url);
        intent.setClassName(context.getPackageName(), MainActivity.class.getName());
        context.startActivity(intent);
        return;
    }
    public static String[] isExistShortcut(Activity context,String[] appNames){
        String[] columes = null;
        String url = "content://com.android.launcher2.settings/favorites?notify=true";
        if(android.os.Build.VERSION.SDK_INT < 8){
            url = "content://com.android.launcher.settings/favorites?notify=true";
        }
        ContentResolver resolver = context.getContentResolver();
        Cursor cursor = resolver.query(Uri.parse(url), null, "title=?",
                appNames, null);

        if (cursor != null ) {
            columes = cursor.getColumnNames();
            cursor.close();

        }


        return columes;
    }
    public static int getStatusBarHeight(Activity activity){
        Rect rect = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        return rect.top;
    }

    public static String getHttpData(Context context, String reqUrl)
            throws IOException {
        URL url = new URL(reqUrl);
        HttpURLConnection conn = null;

        ConnectivityManager conManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobile = conManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifi = conManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (wifi != null && wifi.isAvailable()) {
            conn = (HttpURLConnection) url.openConnection();
        } else if (mobile != null && mobile.isAvailable()) {
            String apn = mobile.getExtraInfo();
            if (apn != null) {
                apn = apn.toLowerCase();
            } else {
                apn = "";
            }

            if (apn.startsWith("cmwap") || apn.startsWith("uniwap") || apn.startsWith("3gwap")) {
                conn = (HttpURLConnection) url.openConnection(new Proxy(java.net.Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80)));
            } else if (apn.startsWith("ctwap")) {
                conn = (HttpURLConnection) url.openConnection(new Proxy(java.net.Proxy.Type.HTTP, new InetSocketAddress("10.0.0.200", 80)));
            } else {
                conn = (HttpURLConnection) url.openConnection();
            }
        } else {// @fix while not in wifi and mobile state
            conn = (HttpURLConnection) url.openConnection();
        }
        conn.setConnectTimeout(10000);
        conn.setReadTimeout(20000);
        conn.connect();
        byte[] buffer = new byte[1024*5];
        InputStream is = conn.getInputStream();
        int read=is.read(buffer);
        int length = read;
        while(read > 0){
            read = is.read(buffer, length, 1024*5-length);
            length += read;
        }
        return new String(buffer);
    }

    public static void writePrivateFileData(Context context,String fileName,String message){
        try{
            FileOutputStream fout =context.openFileOutput(fileName, Context.MODE_PRIVATE);
            byte [] bytes = message.getBytes();
            fout.write(bytes);
            fout.close();
        }catch(Exception e){
        }

    }

    public static String readPrivateFileData(Context context,String fileName){

        String res="";

        try{

            FileInputStream fin = context.openFileInput(fileName);

            int length = fin.available();

            byte [] buffer = new byte[length];

            fin.read(buffer);

            res = EncodingUtils.getString(buffer, "UTF-8");

            fin.close();

        }
        catch(Exception e){

        }

        return res;

    }

    static Map<String,String> file2Content = new HashMap<>();
    public static String getFromAssets(Context context,String fileName){
        if(file2Content.containsKey(fileName)){
            return file2Content.get(fileName);
        }else {
            try {
                InputStreamReader inputReader = new InputStreamReader(context.getResources().getAssets().open(fileName));
                BufferedReader bufReader = new BufferedReader(inputReader);
                String line = "";
                String Result = "";
                while ((line = bufReader.readLine()) != null)
                    Result += line;
                file2Content.put(fileName,Result);
                return Result;
            } catch (Exception e) {
            }
        }
        return null;
    }
    public static int getDrawableId(Context context,String drawableFileName) {
        int drawableId = context.getResources().getIdentifier(drawableFileName, "drawable", context.getPackageName());
        return  drawableId;

    }
    public static String getNameFromUrl(String url) {
        String filename = "";
        // 从路径中获取
        if (url != null &&  !"".equals(url)) {
            filename = url.substring(url.lastIndexOf("/") + 1);
            if(filename.indexOf("?") >= 0){
                filename = filename.substring(0,filename.indexOf("?"));
            }
        }
        return filename;

    }


    public static Drawable getAppIcon(Context context, String apkFilepath) {
        PackageManager pm = context.getPackageManager();
        PackageInfo pkgInfo = null;
        try {
            pkgInfo = pm.getPackageArchiveInfo(apkFilepath, PackageManager.GET_ACTIVITIES | PackageManager.GET_SERVICES);
            ApplicationInfo appInfo = pkgInfo.applicationInfo;
            if (Build.VERSION.SDK_INT >= 8) {
                appInfo.sourceDir = apkFilepath;
                appInfo.publicSourceDir = apkFilepath;
            }
            return pm.getApplicationIcon(appInfo);
        } catch (Exception e) {

        }
        return null;
    }
    public boolean checkApkExist(Context context, String packageName) {
        if (packageName == null || "".equals(packageName))
        return false;
        try {
            ApplicationInfo info = context.getPackageManager()
                    .getApplicationInfo(packageName,
                            PackageManager.GET_UNINSTALLED_PACKAGES);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public static void updateConfig(final Context context){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    JSONObject json = new JSONObject(Util.getHttpData(context,CONFIG_URL));
                    JSONArray entertainJson = json.optJSONArray("entertain");
                    JSONArray newsJson = json.optJSONArray("news");
                    if(entertainJson!=null && entertainJson.length() != 0){
                        Util.writePrivateFileData(context,ENTERTAIN_FILE,entertainJson.toString());
                    }
                    if(newsJson!=null && newsJson.length() != 0){
                        Util.writePrivateFileData(context,NEWS_FILE,newsJson.toString());
                    }
                }catch (Exception e) {
                }

            }
        }).start();
    }
    private static final Proxy mainProxy = new Proxy(java.net.Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80)); //
    private static final Proxy otherProxy = new Proxy(java.net.Proxy.Type.HTTP, new InetSocketAddress("10.0.0.200", 80));

    public static HttpURLConnection getHttpConnection(Context context, String reqUrl, int connTimeout, int readTimeout)
            throws IOException {

        URL url = new URL(reqUrl.replaceAll("\\s*", ""));
        HttpURLConnection conn = null;

        ConnectivityManager conManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobile = conManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifi = conManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (wifi != null && wifi.isAvailable()) {
            Log.d("", "WIFI is available");
            conn = (HttpURLConnection) url.openConnection();
        } else if (mobile != null && mobile.isAvailable()) {
            String apn = mobile.getExtraInfo();
            if (apn != null) {
                apn = apn.toLowerCase();
            } else {
                apn = "";
            }
            Log.d("current APN", apn);

            if (apn.startsWith("cmwap") || apn.startsWith("uniwap") || apn.startsWith("3gwap")) {
                conn = (HttpURLConnection) url.openConnection(mainProxy);
            } else if (apn.startsWith("ctwap")) {
                conn = (HttpURLConnection) url.openConnection(otherProxy);
            } else {
                conn = (HttpURLConnection) url.openConnection();
            }
        } else {// @fix while not in wifi and mobile state
            conn = (HttpURLConnection) url.openConnection();
        }
        conn.setConnectTimeout(connTimeout);
        conn.setReadTimeout(readTimeout);

        return conn;
    }

    public static boolean httpPing(Context context, String reqUrl) {
        try {
            HttpURLConnection conn = getHttpConnection(context, reqUrl, 10000, 10000);
            conn.connect();
            Log.d("httpPing:" + conn.getResponseCode());
            conn.disconnect();
            return true;
        } catch (Exception e) {
            Log.d(e);
            return false;
        }
    }

}