package com.briliasm.browser;

import android.app.AlertDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.briliasm.browser.base.BasePageFragmentInterface;
import com.briliasm.browser.bean.ActivityMsg;
import com.briliasm.browser.bean.MutiwindowData;

import com.briliasm.browser.fragment.BrowserPageFragment;
import com.briliasm.browser.fragment.MainPageFragment;
import com.briliasm.browser.util.Util;
import com.jesgoo.browser.R;

import java.util.List;

/**
 * Created by zhangliang on 15/7/28.
 */
public class ControllerBarLayout extends LinearLayout implements View.OnClickListener {
    private FragmentActivity activity;
    private BasePageFragmentInterface fragment;
    private MyWebView myWebView;
    List<MutiwindowData> mData;
    private Handler handler;
    private int curSelect;
    private ImageButton backBtn, forwardBtn, menuBtn, mutiWinBtn;
    private GridAdapter adapter;

    class ViewHolder {
        public ImageView bgimg;
        public ImageView contimg;
        public ImageView closeimg;
        public TextView title;
//        public TextView info;
    }

    //only for BrowserPageFragment
    public void setWebView(MyWebView webView) {
        this.myWebView = webView;
    }

    FrameLayout mgrView;

    class GridAdapter extends BaseAdapter {
        private LayoutInflater mInflater;


        public GridAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Object getItem(int arg0) {
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            if (convertView == null) {

                holder = new ViewHolder();

                convertView = mInflater.inflate(R.layout.muti_window_item, null);
                holder.bgimg = (ImageView) convertView.findViewById(R.id.background);
                holder.closeimg = (ImageView) convertView.findViewById(R.id.close);
                holder.contimg = (ImageView) convertView.findViewById(R.id.preview);
                holder.title = (TextView) convertView.findViewById(R.id.shortcutTitle);
                holder.title.setSingleLine(true);
//                holder.info = (TextView)convertView.findViewById(R.id.sho);

                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            MutiwindowData data = mData.get(position);

            if (data.toString() != null) {
                holder.bgimg.setBackgroundResource(R.drawable.tab_thumbnail_selected);
                holder.title.setText(data.getTitle());
                holder.contimg.setImageDrawable(data.getDrawable());
                holder.contimg.setScaleType(ImageView.ScaleType.FIT_XY);
                holder.closeimg.setBackgroundResource(R.drawable.close_window);

                final ViewHolder h = holder;
                holder.closeimg.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (position != 0) {
                            MutiwindowData.mulWindowDataList.set(position, new MutiwindowData());
                            h.title.setText("新建窗口");
                            h.bgimg.setBackgroundResource(R.drawable.tab_thumbnail_add);
                        }
                        if (mgrView != null && mgrView.getParent() != null) {
                            ViewGroup vg = (ViewGroup) mgrView.getParent();
                            vg.removeView(mgrView);
                        }
                        Message msg = Message.obtain();
                        msg.what = ActivityMsg.SELECT_WINDOW.getValue();
                        msg.arg1 = 0;
                        msg.obj = mData.get(position);
                        handler.sendMessage(msg);

                        adapter.notifyDataSetChanged();


                    }
                });
            } else {
                holder.title.setText("新建窗口");
                holder.bgimg.setBackgroundResource(R.drawable.tab_thumbnail_add);
//                holder.closeimg.setBa
            }


            return convertView;
        }
    }


    public ControllerBarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        activity = ((MainActivity) context);
        activity.getLayoutInflater().inflate(R.layout.controller_bar_content, this);
        backBtn = (ImageButton) this.findViewById(R.id.back);
        forwardBtn = (ImageButton) this.findViewById(R.id.forward);
        menuBtn = (ImageButton) this.findViewById(R.id.menuButton);
        mutiWinBtn = (ImageButton) this.findViewById(R.id.multiWin);
        ImageButton homeBtn = (ImageButton) this.findViewById(R.id.home);

        final OnClickListener listener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                ControllerBarLayout.this.requestFocus();
                int id = v.getId();


                switch (id) {
                    case R.id.back:
                        if (mgrView != null && mgrView.getParent() != null) {
                            ViewGroup vg = (ViewGroup) mgrView.getParent();
                            vg.removeView(mgrView);
                        }
                        if (fragment instanceof BrowserPageFragment) {
                            if (myWebView.canGoBack()) {
                                myWebView.goBack();
                            } else {

                                if (handler != null) {
                                    Message msg = Message.obtain();
                                    msg.what = ActivityMsg.BROWSER2MAIN.getValue();
                                    handler.sendMessage(msg);
                                }
                            }

                        }
                        curSelect = id;
                        break;
                    case R.id.forward:
                        if (mgrView != null && mgrView.getParent() != null) {
                            ViewGroup vg = (ViewGroup) mgrView.getParent();
                            vg.removeView(mgrView);
                        }
                        if (fragment instanceof BrowserPageFragment) {
                            myWebView.goForward();
                        } else if (fragment instanceof MainPageFragment) {
                            Message msg = Message.obtain();
                            msg.what = ActivityMsg.MAIN2BROWSER.getValue();
                            handler.sendMessage(msg);
                        }

                        curSelect = id;
                        break;
                    case R.id.menuButton:
                        ViewGroup decorView0 = (ViewGroup) activity.getWindow().getDecorView();
                        if (mgrView != null && mgrView.getParent() != null) {
                            ViewGroup vg = (ViewGroup) mgrView.getParent();
                            vg.removeView(mgrView);
                            if (curSelect == id) {
                                return;
                            }
                        }

                        curSelect = id;
                        mgrView = new FrameLayout(activity);
                        FrameLayout.LayoutParams lp0 = new FrameLayout.LayoutParams(-1, Util.getMetrics(activity).heightPixels - Util.getStatusBarHeight(activity) - ControllerBarLayout.this.getHeight());
                        lp0.topMargin = Util.getStatusBarHeight(activity);
                        decorView0.addView(mgrView, lp0);

                        FrameLayout menuView = new FrameLayout(activity);
                        FrameLayout.LayoutParams lp1 = new FrameLayout.LayoutParams(-1, Util.getMetrics(activity).heightPixels / 4);
//                        lp1.bottomMargin =  ControllerBarLayout.this.getHeight();
                        lp1.gravity = Gravity.BOTTOM;
                        mgrView.addView(menuView, lp1);

                        activity.getLayoutInflater().inflate(R.layout.option_content, menuView);
                        TextView save = (TextView) menuView.findViewById(R.id.save_bookmark);
                        TextView refresh = (TextView) menuView.findViewById(R.id.refresh_page);
                        TextView download = (TextView) menuView.findViewById(R.id.manage_download);
                        TextView info = (TextView) menuView.findViewById(R.id.page_info);
                        TextView screenshot = (TextView) menuView.findViewById(R.id.game);
                        TextView copy = (TextView) menuView.findViewById(R.id.url_copy);
                        TextView paste = (TextView) menuView.findViewById(R.id.url_paste);
                        TextView settings = (TextView) menuView.findViewById(R.id.settings);
                        TextView share = (TextView) menuView.findViewById(R.id.share);
                        TextView exit = (TextView) menuView.findViewById(R.id.exit);


                        save.setOnClickListener(ControllerBarLayout.this);
                        refresh.setOnClickListener(ControllerBarLayout.this);
                        download.setOnClickListener(ControllerBarLayout.this);
                        info.setOnClickListener(ControllerBarLayout.this);
                        screenshot.setOnClickListener(ControllerBarLayout.this);
                        copy.setOnClickListener(ControllerBarLayout.this);
                        paste.setOnClickListener(ControllerBarLayout.this);
                        settings.setOnClickListener(ControllerBarLayout.this);
                        share.setOnClickListener(ControllerBarLayout.this);
                        exit.setOnClickListener(ControllerBarLayout.this);

                        mgrView.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (mgrView != null && mgrView.getParent() != null) {
                                    ViewGroup vg = (ViewGroup) mgrView.getParent();
                                    vg.removeView(mgrView);
                                }
                            }
                        });
                        mgrView.requestFocus();
                        break;
                    case R.id.multiWin:
                        if (mgrView != null && mgrView.getParent() != null) {
                            ViewGroup vg = (ViewGroup) mgrView.getParent();
                            vg.removeView(mgrView);
                            if (curSelect == id) {
                                return;
                            }
                        }
                        curSelect = id;
                        if (fragment instanceof BrowserPageFragment) {
                            MutiwindowData.mulWindowDataList.get(myWebView.getId()).setUrl(myWebView.getUrl());
                            MutiwindowData.mulWindowDataList.get(myWebView.getId()).setTitle(myWebView.getTitle());
                            MutiwindowData.mulWindowDataList.get(myWebView.getId()).setDrawable(new BitmapDrawable(myWebView.captureWebView()));
                        }
                        mData = MutiwindowData.mulWindowDataList;
                        ViewGroup decorView = (ViewGroup) activity.getWindow().getDecorView();

                        mgrView = new FrameLayout(activity);
                        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(-1, Util.getMetrics(activity).heightPixels - Util.getStatusBarHeight(activity) - ControllerBarLayout.this.getHeight());
                        lp.topMargin = Util.getStatusBarHeight(activity);
                        decorView.addView(mgrView, lp);
                        activity.getLayoutInflater().inflate(R.layout.muti_window_content, mgrView);


                        GridView gv = (GridView) mgrView.findViewById(R.id.muti_win_panel);
                        adapter = new GridAdapter(activity);
                        gv.setAdapter(adapter);
                        mgrView.setOnClickListener(null);
                        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                if (mgrView != null && mgrView.getParent() != null) {
                                    ViewGroup vg = (ViewGroup) mgrView.getParent();
                                    vg.removeView(mgrView);
                                }
                                Message msg = Message.obtain();
                                msg.what = ActivityMsg.SELECT_WINDOW.getValue();
                                msg.arg1 = position;
                                msg.obj = mData.get(position);
                                handler.sendMessage(msg);
                            }
                        });
                        mgrView.requestFocus();
                        break;
                    case R.id.home:
                        if (mgrView != null && mgrView.getParent() != null) {
                            ViewGroup vg = (ViewGroup) mgrView.getParent();
                            vg.removeView(mgrView);
                        }
                        curSelect = id;

                        if (handler != null) {
                            Message msg = Message.obtain();
                            msg.what = ActivityMsg.BROWSER2MAIN.getValue();
                            handler.sendMessage(msg);
                        }
                        break;

                }
            }
        };
        backBtn.setOnClickListener(listener);
        forwardBtn.setOnClickListener(listener);
        menuBtn.setOnClickListener(listener);
        mutiWinBtn.setOnClickListener(listener);
        homeBtn.setOnClickListener(listener);

    }


    public boolean handleBackPressed() {

        if (mgrView != null && mgrView.getParent() != null) {
            ViewGroup vg = (ViewGroup) mgrView.getParent();
            vg.removeView(mgrView);
            return true;
        }
        return false;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public void setFragment(BasePageFragmentInterface fragment) {
        this.fragment = fragment;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.save_bookmark:
                if (fragment instanceof BrowserPageFragment) {
                    ((BrowserPageFragment) fragment).saveBookMark();
                }
                break;
            case R.id.refresh_page:
                if (fragment instanceof BrowserPageFragment) {
                    ((BrowserPageFragment) fragment).refresh();
                }
                break;
            case R.id.manage_download:
//                Intent downloadIntent = new Intent(activity,DownloadMgrActivity.class);
//                activity.startActivity(downloadIntent);
                break;
            case R.id.page_info:
                String msg = "迷你浏览器一款致力于简单易用，功能强大，保护用户隐私，提升用户体验的浏览器。";
                if (fragment instanceof BrowserPageFragment) {
                    msg = "标题：" + myWebView.getTitle() + "\n地址：" + myWebView.getUrl() + "\n备注：" + msg;
                }
                final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage(msg);
                builder.setTitle("迷你浏览器");
                builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.create().show();

                break;
            case R.id.game:

                Util.startUrl(activity, "http://51oneone.com");

                break;
            case R.id.url_copy:
                if (fragment instanceof BrowserPageFragment) {
                    ClipboardManager cmb = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                    cmb.setText(myWebView.getUrl());
                    Toast.makeText(activity, "已复制链接地址", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(activity, "当前页面无法复制", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.url_paste:
                ClipboardManager cmb = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                String url = cmb.getText().toString();
                if (fragment instanceof BrowserPageFragment) {
                    myWebView.loadUrl(url);
                } else {
                    ((MainPageFragment) fragment).setEditTextUrl(url);
                }
                break;
            case R.id.settings:
                Intent intent = new Intent(activity, SettingsActivity.class);
                activity.startActivity(intent);

                break;
            case R.id.share:
                Intent shareIntent = new Intent("android.intent.action.SEND");
                shareIntent.setType("text/plain");
                shareIntent.setPackage("com.tencent.mm");
                shareIntent.putExtra("android.intent.extra.SUBJECT", "Share");
                if (fragment instanceof MainPageFragment) {
                    shareIntent.putExtra("android.intent.extra.TEXT", "我正在使用迷你浏览器，有情怀，极速，轻捷。\n下载地址：" + Util.APK_URL);
                } else if (fragment instanceof BrowserPageFragment) {
                    shareIntent.putExtra("android.intent.extra.TEXT", "分享给你瞅瞅：" + myWebView.getTitle() + " ： " + myWebView.getUrl() + "\n迷你浏览器，有情怀，极速，轻捷。");
                }

                shareIntent.setFlags(0x10000000);
//                shareIntent.setPackage("com.tencent.mobileqq");
                activity.startActivity(shareIntent);
                break;
            case R.id.exit:
                AlertDialog.Builder builder0 = new AlertDialog.Builder(activity);
                builder0.setMessage("真的要退出？");
                builder0.setTitle("迷你浏览器");
                builder0.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                });
                builder0.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder0.create().show();
                break;
        }
        if (mgrView != null && mgrView.getParent() != null) {
            ViewGroup vg = (ViewGroup) mgrView.getParent();
            vg.removeView(mgrView);
        }
    }

    public void updateBrowserControllbarStatus() {
        boolean canGoForward = myWebView.canGoForward();
        forwardBtn.setEnabled(canGoForward);
        if (canGoForward) {
            forwardBtn.setImageResource(R.drawable.controlbar_forward_normal);

        } else {
            forwardBtn.setImageResource(R.drawable.controlbar_forward_disable);

        }
    }

    public void updateMainControllbarStatus(boolean forwardEnable) {
        if (backBtn.isEnabled()) {
            backBtn.setEnabled(false);
            backBtn.setImageResource(R.drawable.controlbar_backword_disable);
        }
        if (forwardEnable) {
            forwardBtn.setEnabled(true);
            forwardBtn.setImageResource(R.drawable.controlbar_forward_normal);
        } else {
            forwardBtn.setEnabled(false);
            forwardBtn.setImageResource(R.drawable.controlbar_forward_disable);
        }


    }
}
