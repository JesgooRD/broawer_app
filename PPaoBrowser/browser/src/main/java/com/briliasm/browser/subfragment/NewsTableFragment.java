package com.briliasm.browser.subfragment;

import com.briliasm.browser.base.BaseTableFragment;
import com.briliasm.browser.bean.FavoriteData;
import com.briliasm.browser.util.Util;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;


public class NewsTableFragment extends BaseTableFragment {


    public NewsTableFragment(){
    }
    public List<FavoriteData>  getData(){
        List<FavoriteData> list =  new ArrayList<>();
//        String newsStr = Util.getFromAssets(getActivity(),"news_shortcut_list.json");
        String newsStr =Util.readPrivateFileData(getActivity(),Util.NEWS_FILE);
        if("".equals(newsStr)){
            newsStr ="[\n" +
                    "  {\"title\":\"hao123\",\"img\":\"hao123\",\"url\":\"http://m.hao123.com/?union=1&from=1012734b&tn=ops1012734b\"},\n" +
                    "  {\"title\":\"知乎\",\"img\":\"zhihu\",\"url\":\"http://m.zhihu.com/\"},\n" +
                    "  {\"title\":\"凤凰网\",\"img\":\"fenghuang\",\"url\":\"http://i.ifeng.com/\"},\n" +
                    "  {\"title\":\"百度\",\"img\":\"baidu\",\"url\":\"http://m.baidu.com\"},\n" +
                    "  {\"title\":\"淘宝\",\"img\":\"taobao\",\"url\":\"http://m.taobao.com\"},\n" +
                    "  {\"title\":\"新浪\",\"img\":\"sina\",\"url\":\"http://sina.cn/?from=wap\"},\n" +
                    "  {\"title\":\"网易\",\"img\":\"net163\",\"url\":\"http://3g.163.com/touch/\"},\n" +
                    "  {\"title\":\"手机腾讯\",\"img\":\"qqnews\",\"url\":\"http://info.3g.qq.com/\"},\n" +
                    "  {\"title\":\"58同城\",\"img\":\"tongcheng58\",\"url\":\"http://m.58.com/\"},\n" +
                    "  {\"title\":\"京东\",\"img\":\"jd\",\"url\":\"http://m.jd.com/\"},\n" +
                    "  {\"title\":\"携程\",\"img\":\"xiecheng\",\"url\":\"http://m.ctrip.com/\"},\n" +
                    "  {\"title\":\"汽车之家\",\"img\":\"qichezhijia\",\"url\":\"http://m.autohome.com.cn/\"},\n" +
                    "  {\"title\":\"百度市场\",\"img\":\"baiduapp\",\"url\":\"http://mobile.baidu.com/\"}\n" +
                    "]";
        }
        try {
            JSONArray newsList = new JSONArray(newsStr);
            for(int i=0;i<newsList.length();i++){
                FavoriteData data = new FavoriteData();
                data.setTitle(newsList.getJSONObject(i).optString("title"));
                data.setUrl(newsList.getJSONObject(i).optString("url"));
                data.setPkg(null);
                data.setDrawable(getResources().getDrawable(Util.getDrawableId(getActivity(), newsList.getJSONObject(i).optString("img"))));
                list.add(data);
            }
        }catch (Exception e){
        }

        return list;
    }

}  