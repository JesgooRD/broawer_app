package com.briliasm.browser.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;

/**
 * @since 2011-01-20
 * 
 *        Adapter class of android.util.Log. Extension: Writing text log in log file.
 * 
 */
final public class Log {
	public static final int VERBOSE = 2;
	public static final int DEBUG = 3;
	public static final int INFO = 4;
	public static final int WARN = 5;
	public static final int ERROR = 6;
	public static final int ASSERT = 7;
	public static final  int LOG_LEVEL= VERBOSE;

	public static final String TAG = "minibrowser";
	// public static final String AD_MANAGER = "VideoManager";
	// public static final String AD_LOADER = "AdLoader";


	private static final SimpleDateFormat LOG_TIME_FM = new SimpleDateFormat("MM-dd HH:mm:ss.SSS");
	public static final int UNLOGGABLE = -1;



	private synchronized static void debug(String tag, String msg) {

	}

	private static void debug(String tag, String msg, Throwable tr) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		tr.printStackTrace(pw);
		debug(tag, new StringBuilder(msg).append("\n").append(sw.toString()).toString());
		pw.close();

		try {
			sw.close();
		} catch (IOException e) {
			android.util.Log.w("Log.debug", "", e);
		}
	}

	// /
	// / Methods from android.util.Log
	// /
	public static boolean isLoggable(String tag, int level) {
		// return android.util.Log.isLoggable(tag, level);
		return level >= LOG_LEVEL;
	}

	public static boolean isLoggable(int level) {
		return isLoggable(TAG, level);
	}


	private static String join(Object[] msgs) {
		StringBuilder m = new StringBuilder();
		for (Object msg : msgs) {
			m.append(msg).append(' ');
		}
		return m.toString();
	}
	

	public static int d(Object... msgs) {
		if (!isLoggable(DEBUG)) {
			return UNLOGGABLE;
		}
		
		return d(join(msgs));
	}
	
	public static int d(String msg) {
		if (!isLoggable(DEBUG)) {
			return UNLOGGABLE;
		}

		debug(TAG, msg);
		return android.util.Log.d(TAG, msg);
	}

	public static int d(Throwable tr) {
		return d("", tr);
	}

	public static int d(String msg, Throwable tr) {
		if (!isLoggable(DEBUG)) {
			return UNLOGGABLE;
		}

		debug(TAG, msg, tr);
		return android.util.Log.d(TAG, msg, tr);
	}
	
	public static int w(String msg) {
		if (!isLoggable(WARN)) {
			return UNLOGGABLE;
		}

		debug(TAG, msg);
		return android.util.Log.w(TAG, msg);
	}
	
	public static int w(Object... msgs) {		
		if (!isLoggable(WARN)) {
			return UNLOGGABLE;
		}
		
		return w(join(msgs));
	}

	public static int w(String msg, Throwable tr) {
		if (!isLoggable(WARN)) {
			return UNLOGGABLE;
		}

		debug(TAG, msg, tr);
		return android.util.Log.w(TAG, msg, tr);
	}

	public static int w(Throwable tr) {
		return w("", tr);
	}

	public static int e(Object... msgs) {
		if (!isLoggable(ERROR)) {
			return UNLOGGABLE;
		}
		
		return e(join(msgs));
	}

	public static int e(String msg) {
		if (!isLoggable(ERROR)) {
			return UNLOGGABLE;
		}

		debug(TAG, msg);
		return android.util.Log.e(TAG, msg);
	}

	public static int e(Throwable tr) {
		return e("", tr);
	}

	public static int e(String msg, Throwable tr) {
		if (!isLoggable(ERROR)) {
			return UNLOGGABLE;
		}

		debug(TAG, msg, tr);
		return android.util.Log.e(TAG, msg, tr);
	}
	

	
	public static int i(String msg) {
		if (!isLoggable(INFO)) {
			return UNLOGGABLE;
		}

		debug(TAG, msg);
		return android.util.Log.i(TAG, msg);
	}
	
	public static int i(Object... msgs) {		
		if (!isLoggable(INFO)) {
			return UNLOGGABLE;
		}
		
		return i(join(msgs));
	}

	public static int i(String msg, Throwable tr) {
		if (!isLoggable(INFO)) {
			return UNLOGGABLE;
		}

		debug(TAG, msg, tr);
		return android.util.Log.i(TAG, msg, tr);
	}
}
