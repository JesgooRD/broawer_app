package com.briliasm.browser.util;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.jesgoo.browser.R;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class UpdateManager {

	private Context mContext;
	static boolean inited = false;

	private Dialog noticeDialog;

	private Dialog downloadDialog;
	/* 下载包安装路径 */
	private static final String savePath = "/sdcard/minibrowser/";

	private static double remoteVer = 1.0;
	/* 进度条与通知ui刷新的handler和msg常量 */
	private ProgressBar mProgress;

	private static final int DOWN_UPDATE = 1;

	private int progress;

	private Thread downLoadThread;

	private boolean interceptFlag = false;

	public static void checkNeedsUpdate(final Context context) { // 这个方法没有用到什么类的成员变量，加static，效率更高些
		if (!UpdateManager.inited) {
			new UpdateManager(context);
			UpdateManager.inited = true;
		}
	}

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case DOWN_UPDATE:
				mProgress.setProgress(progress);
				break;
			default:
				break;
			}
		};
	};

	public UpdateManager(Context context) {
		this.mContext = context;
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					PackageManager packageManager = mContext.getPackageManager();
					PackageInfo packInfo = packageManager.getPackageInfo(mContext.getPackageName(), 0);
					double localVer = Double.parseDouble(packInfo.versionName);
					final JSONObject json = new JSONObject(Util.getHttpData(mContext, Util.UPDATE_URL));
					remoteVer =  json.optDouble("version", 1.0);
					if (localVer < remoteVer) {
						mHandler.post(new Runnable() {
							@Override
							public void run() {
								UpdateManager.this.showNoticeDialog(json);
							}
						});
					}
				} catch (Exception e) {
					Log.d("UpdateManager", e.toString());
				}
			}
		}).start();
	}

	private static String apkUrl = "";

	private void showNoticeDialog(JSONObject json) {
		String details = json.optString("details", "");
		apkUrl = json.optString("url", "");
		if (!apkUrl.equals("")) {
			Builder builder = new Builder(mContext);
			builder.setTitle("软件版本更新");
			builder.setMessage("亲,升级新版更赞哦\n" + details);
			builder.setPositiveButton("下载", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					showDownloadDialog();
				}
			});
			builder.setNegativeButton("以后再说", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			noticeDialog = builder.create();
			noticeDialog.show();
		}
	}

	private void showDownloadDialog() {
		Builder builder = new Builder(mContext);
		builder.setTitle("正在更新");

		final LayoutInflater inflater = LayoutInflater.from(mContext);
		View v = inflater.inflate(R.layout.dl_progress, null);
		mProgress = (ProgressBar) v.findViewById(R.id.progress);

		builder.setView(v);
		builder.setNegativeButton("取消", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				interceptFlag = true;
			}
		});
		downloadDialog = builder.create();
		downloadDialog.show();

		downLoadThread = new Thread(mdownApkRunnable);
		downLoadThread.start();
	}

	private Runnable mdownApkRunnable = new Runnable() {
		@Override
		public void run() {
			try {
				URL url = new URL(apkUrl);

				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.connect();
				int length = conn.getContentLength();
				InputStream is = conn.getInputStream();

				File file = new File(savePath);
				if (!file.exists()) {
					file.mkdirs();
				}
				String apkFile = savePath+ "aixiaobuxiao_"+remoteVer+".apk";
				File ApkFile = new File(apkFile);
				FileOutputStream fos = new FileOutputStream(ApkFile);

				int count = 0;
				byte buf[] = new byte[1024];

				do {
					int numread = is.read(buf);
					count += numread;
					progress = (int) (((float) count / length) * 100);
					// 更新进度
					mHandler.sendEmptyMessage(DOWN_UPDATE);
					if (numread <= 0) {
						// 下载完成通知安装
						installApk(apkFile);
						downloadDialog.dismiss();
						break;
					}
					fos.write(buf, 0, numread);
				} while (!interceptFlag);// 点击取消就停止下载.

				fos.close();
				is.close();
			} catch (Exception e) {
				e.printStackTrace();
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(mContext, "下载出错了 %>_<%", Toast.LENGTH_LONG).show();
					}

				});
			}

		}
	};

	private void installApk(String filePath) {
		File apkfile = new File(filePath);
		if (!apkfile.exists()) {
			return;
		}
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setDataAndType(Uri.parse("file://" + apkfile.toString()), "application/vnd.android.package-archive");
		mContext.startActivity(i);

	}
}
