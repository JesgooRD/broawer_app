package com.briliasm.browser.bean;

import android.graphics.drawable.Drawable;

/**
 * Created by zhangliang on 15/7/23.
 */
public class FavoriteData {


    private Drawable drawable;


    private String title;

    //pkg比url优先级高
    private String pkg;
    private String url;
    //pkg时值，默认为false
    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public String getPkg() {
        return pkg;
    }

    public void setPkg(String pkg) {
        this.pkg = pkg;

    }

    public void setTitle(String title) {
        this.title = title;
    }

}
