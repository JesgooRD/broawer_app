package com.briliasm.browser.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.webkit.JavascriptInterface;

import com.briliasm.browser.MainActivity;

/**
 * Created by zhangliang on 15/7/22.
 */
public class JavaScriptObject {
    private Context context;

    public JavaScriptObject(Context context){
        this.context = context;
    }
    @JavascriptInterface
    public void open(String url){
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        Uri content_url = Uri.parse(url);
        intent.setData(content_url);
        intent.setClassName(context.getPackageName(), MainActivity.class.getName());
        context.startActivity(intent);
    }
}
