package com.briliasm.browser;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.net.http.SslError;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.DownloadListener;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.briliasm.browser.dl.Dao;
import com.briliasm.browser.dl.DownloadData;
import com.briliasm.browser.dl.DownloaderManager;
import com.briliasm.browser.util.Log;
import com.briliasm.browser.util.Util;

import java.io.File;
import java.io.FileInputStream;

public class MyWebView extends WebView {
	Handler handler;
	ControllerBarLayout controllerBar;
	boolean clear = false;
	public static String[] AD_LIST = new String[]{"mobads.baidu.com","pos.baidu.com","fastapi.net","gdt.qq.com","dxpmedia.com","p.inte.sogou.com","m.lu.sogou.com","yiliude.com","p.tanx.com","s.csbew.com","x.jd.com/exsites","afp.csbew.com"};
	public static String[] IMG_POST_LIST = new String[]{".png",".jpg",".bmp",".jpeg"};
	public static String[] LOCALHOST_LIST = new String[]{"localhost","127.0.0.1"};

	private static boolean localHostForbidden = true;
	private static boolean adRemove = true;
	private static boolean floatRemove = true;
	private static boolean imgRemove = false;
	private static boolean wifiDownload = true;
	public void setHandler(Handler handler){
		this.handler = handler;
	}
	public void setControllerBar(ControllerBarLayout controllerBar){
		this.controllerBar = controllerBar;
		controllerBar.setWebView(this);
	}
	public static void getAndSetPrefs(Context context){
		localHostForbidden = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SettingsActivity.LOCALHOST_FORBIDDEN, true);
		adRemove = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SettingsActivity.AD_REMOVE, true);
		floatRemove = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SettingsActivity.FLOAT_ELEM_REMOVE, true);
		imgRemove = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SettingsActivity.IMG_REMOVE, false);
		wifiDownload = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SettingsActivity.WIFI_DOWNLOAD, true);
	}
	public MyWebView(final Context context, AttributeSet attrs) {
		super(context, attrs);
		getAndSetPrefs(context);
		final ProgressBar prgBar = new ProgressBar(context, null, android.R.attr.progressBarStyleHorizontal);
		prgBar.setMax(100);
		int prgHeight = (int) (5 * Util.getMetrics(context).density);
		FrameLayout.LayoutParams progressLP = new FrameLayout.LayoutParams(-1, prgHeight);
		this.addView(prgBar, progressLP);

		this.setDownloadListener(new DownloadListener() {
			@Override
			public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
				if(wifiDownload && !Util.isWifi(MyWebView.this.getContext())){
					Toast.makeText(MyWebView.this.getContext(),"由于已经设置”仅WIFI下载“，未能下载~",Toast.LENGTH_SHORT).show();
					return;
				}

				try {
					Dao dao = new Dao(context);
					DownloadData data = dao.getDownloadData(url);

					if(data == null){
						data = new DownloadData();
						data.setComplete(0);
						data.setFileSize(contentLength);
//						data.setStatus(DownloadData.STATUS_PAUSE);
						data.setUrlstring(url);
						Log.d("DownloadData:null");
					}else{
						Log.d("DownloadData:"+data.toString());
					}
					DownloaderManager.startDownload(context, data, true);
				} catch (Exception e) {
//					AdUtil.browserOutSide(context, url,2);
					Log.d(e);
				}

//
//				Intent intent = new Intent(MyWebView.this.getContext(), DownloadMgrActivity.class);
//				intent.putExtra("url",url);
//				intent.putExtra("contentLength",contentLength);
//				MyWebView.this.getContext().startActivity(intent);
			}
		});
		WebChromeClient wvChromeClient = new WebChromeClient(){
			boolean dismissed = false;
			@Override
			public void onProgressChanged(WebView view, final int progress) {
				prgBar.setProgress(progress);
				if(progress > 50 && floatRemove && !dismissed){
					dismissed = true;
					dismissFixedElement(10);
				}
				if (progress >= 100) {
					prgBar.setVisibility(View.INVISIBLE);
				} else {
					prgBar.setVisibility(View.VISIBLE);
				}
			}  
		    @Override
		    public boolean onConsoleMessage(ConsoleMessage cm)
		    {
		    	Log.d("MyApplication", cm.message() + " -- From line " + cm.lineNumber() + " of " + cm.sourceId() );
		        return true;
		    }
		};
		WebViewClient wvClient = new WebViewClient(){
			private boolean inBlackList(String url,String[] list){
				for(int i=0;i<list.length;i++){
					if(url.contains(list[i])){
						Log.d("ignoreUrl:"+url);
						return true;
					}
				}
				return false;
			}
			private WebResourceResponse nullRes(Context context){
				try {
					String priPath = "/data/data/" + context.getPackageName() + "/files/";
					String tmpFileStr = priPath + "tmp";
					new File(priPath).mkdirs();
					new File(tmpFileStr).createNewFile();
					return new WebResourceResponse("", "", new FileInputStream(tmpFileStr));
				}catch (Exception e){

				}
				return null;
			}


			@Override 
			public WebResourceResponse  shouldInterceptRequest(WebView view, String url){

				try {
					if(localHostForbidden && inBlackList(url,LOCALHOST_LIST)){
//						Log.d("ignoreUrl:" + url);

						return nullRes(context);
	       			}
					if(adRemove && inBlackList(url,AD_LIST)){
//						Log.d("ignoreUrl:"+url);
						return nullRes(context);
	       			}
					if(imgRemove && inBlackList(url,IMG_POST_LIST)){

						return nullRes(context);
					}
				} catch (Exception e) {
					return null;
				}
				return  super.shouldInterceptRequest(view,url);
			}
			@Override 
			public void onLoadResource(WebView view, String url){
       			super.onLoadResource(view, url);
       			//仅会被通知，但无法拦截
			}
        	@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);

				controllerBar.updateBrowserControllbarStatus();
        	}
        	@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);

				controllerBar.updateBrowserControllbarStatus();
				if(floatRemove){
					dismissFixedElement(20);
					dismissFixedElement(1000);
				}
				if(clear){
					clearHistory();
					clearCache(true);
					clear = false;
				}

			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
        		return false;
			}


			@Override
			public void onReceivedError(WebView view, int errorCode,String description, String failingUrl) {
				super.onReceivedError(view, errorCode, description, failingUrl);
			}

			@Override
			public void onReceivedSslError(WebView view, SslErrorHandler sslHandler, SslError error) {
				sslHandler.proceed();// 接受证书
			}
        };
        setWebViewClient(wvClient);
        setWebChromeClient(wvChromeClient);
        setFocusable(true);
//        requestFocus();
        requestFocus(View.FOCUS_DOWN|View.FOCUS_UP);
//        addJavascriptInterface(new JsInterface(this,handler), "JsObject");

        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
		settings.setJavaScriptCanOpenWindowsAutomatically(true);
		settings.setNeedInitialFocus(true);
		settings.setSupportZoom(true);
		settings.setBuiltInZoomControls(true);
		settings.setAllowFileAccess(true);
		settings.setUseWideViewPort(true);
		settings.setDomStorageEnabled(true);
		settings.setAppCacheEnabled(true);
		settings.setDatabasePath(context.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath());
		settings.setDatabaseEnabled(true);
		settings.setAppCacheEnabled(true);
		settings.setAppCachePath(context.getApplicationContext().getDir("cache", Context.MODE_PRIVATE).getPath());
		settings.setGeolocationEnabled(true);
		settings.setGeolocationDatabasePath(context.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath());
		settings.setAppCacheMaxSize(5*1024*1024);
		settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);

	}
	/**
	 * 截取webView快照(webView加载的整个内容的大小)

	 * @return
	 */
	public Bitmap captureWebView(){
		Picture snapShot = capturePicture();

		Bitmap bmp = Bitmap.createBitmap(Util.getMetrics(getContext()).widthPixels,Util.getMetrics(getContext()).heightPixels, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bmp);
		snapShot.draw(canvas);
		return bmp;
	}
	public void setClear(boolean clear){
		this.clear = clear;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) { 
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if(canGoBack()) {  
	            goBack();  
	            return true;
	    	}		    	
	    }
		return super.onKeyDown(keyCode, event);
	}
//	;
//	(function() {
//
//		function getAbsoluteLeft(o) {
//				oLeft = o.offsetLeft
//		while (o.offsetParent != null) {
//			oParent = o.offsetParent
//			oLeft += oParent.offsetLeft
//			o = oParent
//		}
//		return oLeft
//		}
//
//		function getAbsoluteTop(o) {
//				oTop = o.offsetTop;
//		while (o.offsetParent != null) {
//			oParent = o.offsetParent
//			oTop += oParent.offsetTop // Add parent top position
//			o = oParent
//		}
//		return oTop
//		}
//		window.traverseNodes = function(node, recursive) {
//
//			if (node.nodeType == 1) {
//				dismissFixedElement(node);
//				for (var i = 0; i < node.attributes.length; i++) {
//					var attr = node.attributes.item(i);
//					if (attr.specified) {
//						dismissFixedElement(attr);
//					}
//				}
//
//				if (node.hasChildNodes) {
//					var sonnodes = node.childNodes;
//					for (var i = 0; i < sonnodes.length; i++) {
//						var sonnode = sonnodes.item(i);
//						traverseNodes(sonnode);
//					}
//				}
//			} else {
//				dismissFixedElement(node);
//			}
//		};
//
//
//
//		function dismissFixedElement(node) {
//		try {
//			var style = window.getComputedStyle(node, null);
//			if (style && style.position && style.position == "fixed" && (node.clientWidth && node.clientWidth > document.body.scrollWidth / 4)) {
//
//				if (getAbsoluteTop(node) > 80 && getAbsoluteLeft(node) < 20) {
//					node.style.display = "none";
//				}
//			}
//		} catch (e) {}
//		};
//		traverseNodes(document.documentElement);
//	})();

	@Override
	public void onResume() {
		super.onResume();

	}

	public void dismissFixedElement(long delayed){
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				String js = "eval(function(p,a,c,k,e,r){e=function(c){return c.toString(36)};if('0'.replace(0,e)==0){while(c--)r[e(c)]=k[c];k=[function(e){return r[e]||e}];e=function(){return'[235-9a-df-hj-np-y]'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p}(';(6(){6 j(o){b=o.k;l(o.9!=c){3=o.9;b+=3.k;o=3}m b}6 n(o){d=o.p;l(o.9!=c){3=o.9;d+=3.p;o=3}m d}q.f=6(2,recursive){7(2.nodeType==1){a(2);r(5 i=0;i<2.s.t;i++){5 g=2.s.u(i);7(g.specified){a(g)}}7(2.hasChildNodes){5 h=2.childNodes;r(5 i=0;i<h.t;i++){5 v=h.u(i);f(v)}}}else{a(2)}};6 a(2){try{5 8=q.getComputedStyle(2,c);7(8&&8.w&&8.w==\"fixed\"&&(2.x&&2.x>y.body.scrollWidth/4)){7(n(2)>80&&j(2)<20){2.8.display=\"none\"}}}catch(e){}};f(y.documentElement)})();',[],35,'||node|oParent||var|function|if|style|offsetParent|dismissFixedElement|oLeft|null|oTop||traverseNodes|attr|sonnodes||getAbsoluteLeft|offsetLeft|while|return|getAbsoluteTop||offsetTop|window|for|attributes|length|item|sonnode|position|clientWidth|document'.split('|'),0,{}))";
				loadUrl("javascript:" + js);
			}
		};
		handler.postDelayed(runnable, delayed);



	}
}
