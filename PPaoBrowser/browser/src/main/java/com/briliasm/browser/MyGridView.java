package com.briliasm.browser;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.GridView;

public class MyGridView extends GridView {
    OnTouchInvalidPositionListener mTouchInvalidPosListener;
    public MyGridView(Context context,AttributeSet attrs){
        super(context,attrs);
    }
    public interface OnTouchInvalidPositionListener {

        boolean onTouchInvalidPosition(int motionEvent);

    }

    public void setOnTouchInvalidPositionListener(OnTouchInvalidPositionListener listener) {

        mTouchInvalidPosListener = listener;

    }

    @Override

    public boolean onTouchEvent(MotionEvent event) {

        if (mTouchInvalidPosListener == null) {

            return super.onTouchEvent(event);

        }

        if (!isEnabled()) {

// A disabled view that is clickable still consumes the touch

// events, it just doesn't respond to them.

            return isClickable() || isLongClickable();

        }

        final int motionPosition = pointToPosition((int) event.getX(), (int) event.getY());

        if (motionPosition == INVALID_POSITION) {

            super.onTouchEvent(event);

            return mTouchInvalidPosListener.onTouchInvalidPosition(event.getActionMasked());

        }

        return super.onTouchEvent(event);

    }

}