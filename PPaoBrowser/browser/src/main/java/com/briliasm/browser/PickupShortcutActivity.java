package com.briliasm.browser;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.briliasm.browser.bean.FavoriteData;
import com.briliasm.browser.util.FavoriteDao;
import com.briliasm.browser.util.PackageInfoProvider;
import com.jesgoo.browser.R;

import java.util.ArrayList;
import java.util.List;


public class PickupShortcutActivity extends Activity {
    List<FavoriteData> mData;
    class GridAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        class ViewHolder{
            public ImageView img;
            public TextView title;
            public ImageView checkBox;
        }
        public GridAdapter(Context context){
            this.mInflater = LayoutInflater.from(context);
        }
        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Object getItem(int arg0) {
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            FavoriteData data = mData.get(position);

            ViewHolder holder = null;
            if (convertView == null) {

                holder=new ViewHolder();

                convertView = mInflater.inflate(R.layout.pickup_shortcut_item, null);
                holder.img = (ImageView)convertView.findViewById(R.id.shortcutIcon);
                holder.title = (TextView)convertView.findViewById(R.id.shortcutTitle);
                holder.checkBox = (ImageView)convertView.findViewById(R.id.checkbox);

                convertView.setTag(holder);

            }else {
                holder = (ViewHolder)convertView.getTag();
            }
            holder.title.setSingleLine(true);
            holder.checkBox.setSelected(data.isChecked());
            holder.img.setImageDrawable(mData.get(position).getDrawable());
            holder.title.setText(data.getTitle());
//            holder.info.setText(data.getTitle());

            return convertView;
        }
    }
    public List<FavoriteData>  getData(){

        List<FavoriteData> listAll = PackageInfoProvider.getAppInfo(this);
        FavoriteDao favoriteDao = new FavoriteDao(this);
        List<FavoriteData> checkedFavoriteData = favoriteDao.getShortcutPkgs();
        favoriteDao.closeDb();
        for(int i=0;i<checkedFavoriteData.size();i++){
            for(int j=0;j<listAll.size();j++){
                if(listAll.get(j).getPkg().equals(checkedFavoriteData.get(i).getPkg())){
                    listAll.get(j).setChecked(true);
                }
            }
        }

        return listAll;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);


        setContentView(R.layout.activity_pickup_shortcut);
        mData= getData();
        GridView gridView = (GridView)findViewById(R.id.pickup_shortcut_gridview);
        final GridAdapter adapter = new GridAdapter(this);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                FavoriteData data = mData.get(position);

                if(data.isChecked()){
                    data.setChecked(false);
                    FavoriteDao favoriteDao = new FavoriteDao(view.getContext());
                    favoriteDao.deleteShortCut(data.getPkg());
                    favoriteDao.closeDb();
                }else{
                    List<FavoriteData> list = new ArrayList<FavoriteData>(1);
                    list.add(data);
                    FavoriteDao favoriteDao = new FavoriteDao(view.getContext());
                    favoriteDao.saveShortCutInfos(list);
                    favoriteDao.closeDb();
                    data.setChecked(true);
                }

                mData.set(position,data);
                adapter.notifyDataSetChanged();
            }
        });
        ImageView doneBtn = (ImageView)findViewById(R.id.pickup_done);
//        ImageView cancelBtn = (ImageView)findViewById(R.id.pickup_back);
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.pickup_done:
                        List<FavoriteData> selectedData = new ArrayList<>();

                        for(int i=0;i<mData.size();i++){
                            FavoriteData data = mData.get(i);
                            if(data.isChecked()){
                                selectedData.add(data);
                            }
                        }
                        FavoriteDao favoriteDao = new FavoriteDao(v.getContext());
                        favoriteDao.saveShortCutInfos(selectedData);
                        favoriteDao.closeDb();
                        Toast.makeText(v.getContext(), "添加成功", Toast.LENGTH_SHORT).show();
                        finish();
                        break;
//                    case R.id.pickup_back:
//                        finish();
//                        break;

                }
            }
        };
        doneBtn.setOnClickListener(listener);
//        cancelBtn.setOnClickListener(listener);


    }
}
