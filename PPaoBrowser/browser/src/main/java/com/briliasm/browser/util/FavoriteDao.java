package com.briliasm.browser.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.briliasm.browser.bean.FavoriteData;

import java.util.ArrayList;
import java.util.List;

/**
  *
  * 一个业务类
  */
public class FavoriteDao {
     private DBHelper dbHelper;
     private Context context;
     public FavoriteDao(Context context) {
    	 this.context = context;
//
         dbHelper = new DBHelper(context,"__browser_favorite.db");
     }

     public List<FavoriteData> getShortcutPkgs() {
         List<FavoriteData> list = new ArrayList<FavoriteData>();
         SQLiteDatabase database = dbHelper.getReadableDatabase();
         String sql = "select pkg from shortcut;";
         Cursor cursor = database.rawQuery(sql, new String[] { });
         while (cursor.moveToNext()) {
             FavoriteData info = new FavoriteData();
             info.setPkg(cursor.getString(0));
             try {
                 PackageManager pm = context.getPackageManager();
                 PackageInfo packageInfo = pm.getPackageInfo(cursor.getString(0), 0);

                 info.setPkg(packageInfo.packageName);
                 info.setTitle(packageInfo.applicationInfo.loadLabel(pm).toString());
                 info.setDrawable(packageInfo.applicationInfo.loadIcon(pm));
                 info.setChecked(true);

                 list.add(info);
             }catch (Exception e){
                deleteShortCut(info.getPkg());
             }

         }
         cursor.close();
         return list;
     }
    public List<FavoriteData> getBookmarks() {
        List<FavoriteData> list = new ArrayList<FavoriteData>();
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        String sql = "select url,title,_id from bookmark;";
        Cursor cursor = database.rawQuery(sql, new String[] {});

        while (cursor.moveToNext()) {
            FavoriteData info = new FavoriteData();
            info.setUrl(cursor.getString(0));
            info.setTitle(cursor.getString(1));
            list.add(info);
            Log.d("bookmark_id:"+cursor.getInt(2));
        }
        if(list.size() == 0){
            String baiduTitle = "百度一下";
            String baiduUrl = "http://www.baidu.com";

            FavoriteData data = new FavoriteData();
            data.setTitle(baiduTitle);
            data.setUrl(baiduUrl);
            saveBookmark(list);

            data.setTitle(baiduTitle);
            data.setUrl(baiduUrl);

            list.add(data);
        }
        cursor.close();
        return list;
    }


//     /**
//      * 更新数据库中的下载信息
//      */
//     public void updataInfos(int threadId, int compeleteSize, String urlstr) {
//         SQLiteDatabase database = dbHelper.getReadableDatabase();
//         String sql = "update download_info set compelete_size=? where thread_id=? and url=?";
//         Object[] bindArgs = { compeleteSize, threadId, urlstr };
//         try{
//        	 database.execSQL(sql, bindArgs);

//	     }catch(Exception e){
//	    	 Log.d("updateInfos exception:"+urlstr,e);
//	     }
//     }
//     /**
//      * 关闭数据库
//      */
     public void closeDb() {
    	 try{
    		 dbHelper.close();
    	 }catch(Exception e){
    	 }
     }

    public void saveBookmark(List<FavoriteData> infos) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        for (FavoriteData info : infos) {
            String sql = "insert into bookmark(url,title) values (?,?)";
            Object[] bindArgs = { info.getUrl(),info.getTitle() };
            try{
                database.execSQL(sql, bindArgs);
                Toast.makeText(context,"收藏成功",Toast.LENGTH_SHORT).show();
            }catch(SQLiteConstraintException e){
                Toast.makeText(context,"已在收藏列表中",Toast.LENGTH_SHORT).show();
            }catch (Exception e){
            }
        }
    }
    public void saveShortCutInfos(List<FavoriteData> infos) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        for (FavoriteData info : infos) {
            String sql = "insert into shortcut(pkg) values (?)";
            Object[] bindArgs = { info.getPkg() };
            try{
                database.execSQL(sql, bindArgs);
//                Toast.makeText(context,"添加成功",Toast.LENGTH_SHORT).show();
            }catch(SQLiteConstraintException e){
//                Toast.makeText(context,"已在快捷方式列表中",Toast.LENGTH_SHORT).show();
            }catch (Exception e){
            }
        }
    }
     public void deleteShortCut(String pkg) {
         try {
             SQLiteDatabase database = dbHelper.getReadableDatabase();
             database.delete("shortcut", "pkg=?", new String[]{pkg});
             database.close();
         } catch (Exception e) {

         }
     }
    public void deleteBookmark(String url) {
        try {
            SQLiteDatabase database = dbHelper.getReadableDatabase();
            database.delete("bookmark", "url=?", new String[]{url});
            database.close();
        } catch (Exception e) {

        }
    }
    class DBHelper extends SQLiteOpenHelper
    {
        public DBHelper(Context context,String dbName) {
        super(context, dbName, null, 1);
    }
        /**
         * 在download.db数据库下创建一个download_info表存储下载信息
         */
        @Override
        public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table shortcut(_id integer PRIMARY KEY AUTOINCREMENT, pkg char UNIQUE)");
        db.execSQL("create table bookmark(_id integer PRIMARY KEY AUTOINCREMENT, title char,url char UNIQUE)");
    }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    }
 }