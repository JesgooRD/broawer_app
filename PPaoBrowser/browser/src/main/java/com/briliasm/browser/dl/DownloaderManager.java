package com.briliasm.browser.dl;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.briliasm.browser.util.Log;
import com.briliasm.browser.util.Util;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

//明示下载 管理器
public class DownloaderManager {
    static int THREAD_COUNT = 1;

    // 存放各个下载器
    private static Map<String, Downloader> downloaders = new HashMap<String, Downloader>();
    // 存放与下载器对应的进度条
    private static Map<String, PendingIntent> pendingIntents = new HashMap<String, PendingIntent>();
    private static Map<String, Notification> notifications = new HashMap<String, Notification>();
    private static Map<String, Integer> ids = new HashMap<String, Integer>();

    private static Map<String, String> titles = new HashMap<String, String>();

    protected static Context mContext;
    private static NotificationManager nm;
    private static int id = 109321;
    private static Object lock = new Object();

    public static Downloader getDownLoader(String url) {
        return downloaders.get(url);
    }

    private static Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1) {// 正在下载的消息
                String url = (String) msg.obj;
                Downloader dl = downloaders.get(url);
                if (dl != null && dl.fileSize > 0) {// 如果最后“下载完成”消息先于“下载中”的消息到达，则可能dl为空
                    dl.lastData = System.currentTimeMillis();
                    // 为了处理progress*100溢出的情况
                    long progress = dl.cmpSize;
                    try {
                        Notification ntf = notifications.get(url);
                        PendingIntent pendingIntent = pendingIntents.get(url);
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);
                        builder.setContentText(titles.get(url)).setContentTitle("正在下载   " + progress * 100 / dl.fileSize
                                + "%").setContentIntent(pendingIntent)
                                .setSmallIcon(android.R.drawable.stat_sys_download_done);
                        /*ntf.setLatestEventInfo(mContext, titles.get(url), "正在下载   " + progress * 100 / dl.fileSize
                                + "%", pendingIntent);*/
//						Log.d("DownloaderManager.handler 正在下载   " + progress * 100 / dl.fileSize + "%");
                        nm.notify(ids.get(url), builder.build());
                    } catch (Exception e) {
                        Log.d(e);
                    }
                }
            } else if (msg.what == 2) {// 已经下载的消息， 并且无db记录
                String url = (String) msg.obj;

                startToInstall(url);
                nm.cancel(ids.get(url));
                notifyDownloadedInfo(titles.get(url), Util.DOWNLOAD_PATH + Util.getNameFromUrl(url));
                remove(url);
            }
        }
    };


    private static void notifyDownloadedInfo(String title, String localfile) {
        try {
            Log.d("notifyDownloadedInfo: title=" + title + ";localfile=" + localfile);
            Notification notification = new Notification();
            notification.icon = android.R.drawable.stat_sys_download_done;
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            notification.tickerText = title + "下载完成"; // 开始时的文字状态
            Intent intent = getInstallIntent(localfile);
            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, 0);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);
            builder.setContentText(title + "下载完成").setContentTitle("点击安装").setContentIntent(pendingIntent)
                    .setSmallIcon(android.R.drawable.stat_sys_download_done);
//            notification.setLatestEventInfo(mContext, title + "下载完成", "点击安装", pendingIntent);
            nm.notify(id++, builder.build());
        } catch (Exception e) {
            Log.d(e);
        }

    }

    private static void startToInstall(String url) {
        String localFile = Util.DOWNLOAD_PATH + Util.getNameFromUrl(url);
        PackageManager pm = mContext.getPackageManager();
        PackageInfo info = pm.getPackageArchiveInfo(localFile, PackageManager.GET_ACTIVITIES);
        if (info != null) {
            Intent intent = getInstallIntent(localFile);
            mContext.startActivity(intent);
        } else {
            Toast.makeText(mContext, "文件不存在~", Toast.LENGTH_LONG).show();
            Dao dao = new Dao(mContext);
            dao.delete(url);
        }
    }

    private static Intent getInstallIntent(String localfile) {
        File file = new File(localfile);
        Uri uri = Uri.fromFile(file);
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(uri, "application/vnd.android.package-archive");
        return intent;
    }

    public static void startDownload(final Context context, final DownloadData dlObject, final boolean first) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (lock) {// 下载入口是同步的，否则可能引起读写数据库异常
                    try {
                        mContext = context.getApplicationContext();
                        long contentLength = dlObject.getFileSize();
                        final String dlUrl = dlObject.getUrlstring();
                        Downloader downloader = downloaders.get(dlUrl);
                        Notification notification = null;
                        PendingIntent pendingIntent = null;

                        if (downloader == null) {
                            downloader = new Downloader(dlUrl, contentLength, Util.getNameFromUrl(dlUrl), context,
                                    mHandler);
                            downloaders.put(dlUrl, downloader);
                            notification = new Notification();
                            notification.icon = android.R.drawable.stat_sys_download;
                            notification.tickerText = Util.getNameFromUrl(dlUrl);
                            Intent intent = new Intent();
//							Log.d("DownloaderManager.startDownload pkg:" + obj.getPkg());
                            pendingIntent = PendingIntent.getActivity(mContext, id, intent,
                                    PendingIntent.FLAG_UPDATE_CURRENT);
                            notification.contentIntent = pendingIntent;
                            pendingIntents.put(dlUrl, pendingIntent);
                            notifications.put(dlUrl, notification);
                            ids.put(dlUrl, id++);
                            titles.put(dlUrl, Util.getNameFromUrl(dlUrl));
                        }
                        if (nm == null) {
                            nm = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
                        }
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext);
                        builder.setContentText(Util.getNameFromUrl(dlUrl)).setContentTitle("正在下载   " + dlObject.getComplete() * 100 / dlObject.getFileSize()
                                + "%").setContentIntent(pendingIntent);

                        /*notification.setLatestEventInfo(mContext, Util.getNameFromUrl(dlUrl), "正在下载   " + dlObject.getComplete() * 100 / dlObject.getFileSize()
                                + "%", pendingIntent);*/
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                // 增加try，可能下载太快已经下完了
                                try {
                                    nm.notify(ids.get(dlUrl), notifications.get(dlUrl));
                                } catch (Exception e) {
                                    Log.d(e);
                                }
                            }
                        });
                        downloader.download();
                        Log.d("DownloaderManager testIsDownloaded:" + dlUrl + " : " + downloader.cmpSize + " / "
                                + downloader.fileSize);

                        if (downloader.cmpSize == downloader.fileSize) {
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Message msg = mHandler.obtainMessage();
                                    msg.what = 2;
                                    msg.obj = dlUrl;
                                    mHandler.sendMessage(msg);
                                }
                            });
                        }

                    } catch (Exception e) {
                        Log.d(e);
                    }
                }
            }
        }).start();

    }

    public static void remove(String url) {
        Log.d("DownloaderManager.remove:" + url);
        try {
            downloaders.remove(url);
            downloaders.remove(url);
            pendingIntents.remove(url);
            notifications.remove(url);
            ids.remove(url);
            titles.remove(url);
        } catch (Exception e) {
            Log.d(e);
        }
    }

    public static void pause(String url) {
        Log.d("DownloaderManager.pause");
        Downloader downloader = downloaders.get(url);
        downloader.pause();
    }

}
