package com.briliasm.browser.bean;

/**
 * Created by zhangliang on 15/7/31.
 */
public enum ActivityMsg {
    BACK(0),
    BROWSER2MAIN(1),
    SELECT_WINDOW(2),
    MAIN2BROWSER(3);
    int value;
    ActivityMsg(int value){
        this.value = value;
    }
    public int getValue(){
        return  value;
    }
    public static ActivityMsg getActivityMsg(int value){
        ActivityMsg[] values = ActivityMsg.values();
        for(int i=0;i<values.length;i++){
            if(values[i].getValue() == value){
                return values[i];
            }
        }
        return null;
    }
}
