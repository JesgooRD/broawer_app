//package com.briliasm.browser.dl;
//
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.drawable.Drawable;
//import android.os.Bundle;
//import android.os.Environment;
//import android.os.Handler;
//import android.os.Message;
//import android.os.StatFs;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.briliasm.browser.download.Dao;
//import R;
//import com.briliasm.browser.bean.DownloadData;
//import com.briliasm.browser.util.Log;
//import com.briliasm.browser.util.Util;
//
//import java.math.BigDecimal;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//
//public class DownloadMgrActivity extends Activity {
//    public final class ViewHolder{
//        public ImageView img;
//        public TextView fileName;
//        public TextView sizeInfo;
//        public TextView status;
//        public ImageView pauseContinue;
//    }
//    private TextView usageInfoTV;
//    private static Context context;
//    private static List<DownloadData> mData;
//    private static MyAdapter adapter;
//    private static Map<String, com.briliasm.browser.download.Downloader> downloaders = new HashMap<String, com.briliasm.browser.download.Downloader>();
//    static Handler handler = new Handler(){
//        @Override
//        public void handleMessage(Message msg) {
//            if (msg.what == 1) {// 正在下载的消息
//                String url = (String) msg.obj;
//                com.briliasm.browser.download.Downloader dl = downloaders.get(url);
//                Log.d("download_progress:" + dl + " " + mData);
//                if (dl != null && dl.fileSize > 0) {// 如果最后“下载完成”消息先于“下载中”的消息到达，则可能dl为空
//                    dl.lastData = System.currentTimeMillis();
//                    for(int i=0;i<mData.size();i++){
//                        DownloadData data = mData.get(i);
//
//                        if(url.equals(data.getUrlstring())){
////                            Log.d("download_progress:" + url + " " + dl.cmpSize);
//                            data.setComplete(dl.cmpSize);
//                            data.setStatus(DownloadData.STATUS_DOWNLOADING);
//                            adapter.notifyDataSetChanged();
//
//                            break;
//                        }
//                    }
//                }
//            } else if (msg.what == 2) {// 已经下载的消息， 并且无db记录
//                String url = (String) msg.obj;
//
//                for(int i=0;i<mData.size();i++){
//                    DownloadData data = mData.get(i);
//                    if(url.equals(data.getUrlstring())){
//                        data.setStatus(DownloadData.STATUS_DONE);
//                        adapter.notifyDataSetChanged();
//
//                        break;
//                    }
//                }
//                Util.openFile(context,Util.DOWNLOAD_PATH+Util.getNameFromUrl(url));
//
//                adapter.notifyDataSetChanged();
//            }
//        }
//    };
//
//    public class MyAdapter extends BaseAdapter {
//
//        private LayoutInflater mInflater;
//
//
//        public MyAdapter(Context context){
//            this.mInflater = LayoutInflater.from(context);
//        }
//        @Override
//        public int getCount() {
//            return mData.size();
//        }
//
//        @Override
//        public Object getItem(int arg0) {
//            return null;
//        }
//
//        @Override
//        public long getItemId(int arg0) {
//            return 0;
//        }
//
//        @Override
//        public View getView(final int position, View convertView, ViewGroup parent) {
//
//            ViewHolder holder = null;
//            if (convertView == null) {
//
//                holder=new ViewHolder();
//
//                convertView = mInflater.inflate(R.layout.download_mgr_item, null);
//                holder.img = (ImageView)convertView.findViewById(R.id.logo);
//                holder.fileName = (TextView)convertView.findViewById(R.id.filename);
//                holder.sizeInfo = (TextView)convertView.findViewById(R.id.sizeInfo);
//                holder.status = (TextView)convertView.findViewById(R.id.status);
//                holder.pauseContinue = (ImageView)convertView.findViewById(R.id.pause_continue);
//                convertView.setTag(holder);
//
//            }else {
//                holder = (ViewHolder)convertView.getTag();
//            }
//            final DownloadData data= mData.get(position);
//            int status = data.getStatus();
//            if(data.getFileSize() == data.getComplete()){
//                status = DownloadData.STATUS_DONE;
//            }
//
//
//
//
//            switch (status){
//                case DownloadData.STATUS_DONE:
//                    holder.pauseContinue.setImageDrawable(null);
//                    Drawable drawable = Util.getAppIcon(DownloadMgrActivity.this, Util.DOWNLOAD_PATH + Util.getNameFromUrl(data.getUrlstring()));
//                    if(drawable != null){
//                        holder.img.setImageDrawable(drawable);
//                    }
//
//                    convertView.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Util.openFile(DownloadMgrActivity.this,Util.DOWNLOAD_PATH+Util.getNameFromUrl(data.getUrlstring()));
//                        }
//                    });
//                    holder.pauseContinue.setImageResource(R.drawable.mgr_remove);
//                    holder.pauseContinue.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(final View v) {
//                            AlertDialog.Builder builder0 = new AlertDialog.Builder(v.getContext());
//                            builder0.setMessage("确认删除？");
//                            builder0.setTitle("删除");
//                            builder0.setPositiveButton("确认", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//
//                                    com.briliasm.browser.download.Dao dao = new com.briliasm.browser.download.Dao(v.getContext());
//                                    dao.delete(data.getUrlstring());
//                                    dao.closeDb();
//                                    mData.remove(data);
//                                    adapter.notifyDataSetChanged();
//                                    usageInfoTV.setText(getUsageInfo(Environment.getExternalStorageDirectory().getPath()));
//                                }
//                            });
//                            builder0.setNegativeButton("取消", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    dialog.dismiss();
//                                }
//                            });
//                            builder0.create().show();
//                        }
//                    });
//
//                    break;
//
//                case DownloadData.STATUS_DOWNLOADING:
//                    holder.img.setImageResource(R.drawable.format_unkown);
//                    holder.pauseContinue.setImageResource(R.drawable.mgr_pause_icon);
//                    holder.pauseContinue.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//
//
//                            String url = data.getUrlstring();
//                            com.briliasm.browser.download.Downloader dl = downloaders.get(url);
//                            dl.pause();
//                            data.setStatus(DownloadData.STATUS_PAUSE);
//                            mData.set(position, data);
//                            adapter.notifyDataSetChanged();
//
//                        }
//                    });
//
//                    break;
//                case DownloadData.STATUS_PAUSE:
//                    holder.img.setImageResource(R.drawable.format_unkown);
//                    holder.pauseContinue.setImageResource(R.drawable.mgr_redownload_icon);
//                    holder.pauseContinue.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            data.setStatus(DownloadData.STATUS_DOWNLOADING);
//
//                            Intent intent = new Intent(DownloadMgrActivity.this, DownloadMgrActivity.class);
//                            intent.putExtra("url", data.getUrlstring());
//                            intent.putExtra("contentLength", (long) data.getFileSize());
//                            startActivity(intent);
//
//                            adapter.notifyDataSetChanged();
//
//
//                        }
//                    });
//
//                    break;
//            }
//            holder.fileName.setText(Util.getNameFromUrl(data.getUrlstring()));
//            holder.sizeInfo.setText(new BigDecimal(data.getComplete() * 1.0 / 1024 / 1024).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() + "M/" + new BigDecimal(data.getFileSize() * 1.0 / 1024 / 1024).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() + "M");
////            holder.status.setText("未安装");
//
//            return convertView;
//        }
//    }
//    private void addAndUpdateDownloadData(String url, com.briliasm.browser.download.Downloader dl){
//        int i=0;
//        for(;i<mData.size();i++){
//            DownloadData data = mData.get(i);
//            if(data.getUrlstring().equals(url)){
//                data.setComplete(dl.cmpSize);
//                mData.set(i,data);
//                adapter.notifyDataSetChanged();
//                break;
//            }
//        }
//        if(i == mData.size()){
//            DownloadData downloadData = new DownloadData(dl.fileSize,0,url);
//            mData.add(downloadData);
//            adapter.notifyDataSetChanged();
//        }
//    }
//
//    private void createNewDownloader(Intent intent){
//
//
//        String dlUrl = intent.getStringExtra("url");
//        if(dlUrl != null ){
//            long contentLength = intent.getLongExtra("contentLength", -1);
//            com.briliasm.browser.download.Downloader downloader = downloaders.get(dlUrl);
//
//            if(downloader == null){
//                downloader = new com.briliasm.browser.download.Downloader(dlUrl,contentLength, Util.getNameFromUrl(dlUrl),this,handler);
//                downloaders.put(dlUrl, downloader);
//            }
//            addAndUpdateDownloadData(dlUrl,downloader);
//            downloader.download();
//        }
//    }
//    public static String getUsageInfo(String filePath) {
//
//        // 取得sdcard文件路径
//
//        StatFs statFs = new StatFs(filePath);
//
//        // 获取block的SIZE
//
//        long blocSize = statFs.getBlockSize();
//
//        // 获取BLOCK数量
//
//         long totalBlocks = statFs.getBlockCount();
//
//        // 可使用的Block的数量
//
//        long availaBlock = statFs.getAvailableBlocks();
//
//         long total = totalBlocks * blocSize;
//
//        long availableSpare = availaBlock * blocSize;
//
//        String totalStr =new BigDecimal(total*1.0/1024/1024/1024).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() +"G";
//        long used = total - availableSpare;
//        String usedStr = used+"B";
//        if(used>1024){
//            usedStr = new BigDecimal(used*1.0/1024).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() +"K";
//            used = used/1024;
//        }
//        if(used>1024){
//            usedStr = new BigDecimal(used*1.0/1024).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() +"M";
//            used = used/1024;
//        }
//        if(used>1024){
//            usedStr = new BigDecimal(used*1.0/1024).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() +"G";
//            used = used/1024;
//        }
//        return "SD卡："+usedStr+" / "+totalStr;
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        context = this;
//        setContentView(R.layout.activity_download_mgr);
//        mData = com.briliasm.browser.download.Downloader.getDownloaderInfors(this);
//
//        ListView listView = (ListView)findViewById(R.id.downloadListView);
//        adapter = new MyAdapter(this);
//        listView.setAdapter(adapter);
//        Intent intent = getIntent();
//        createNewDownloader(intent);
//
//        usageInfoTV = (TextView)findViewById(R.id.usageInfo);
//        TextView clearAllTV = (TextView)findViewById(R.id.clearAll);
//        usageInfoTV.setText(getUsageInfo(Environment.getExternalStorageDirectory().getPath()));
//        clearAllTV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(final View v) {
//                if (mData.size() > 0) {
//                    AlertDialog.Builder builder0 = new AlertDialog.Builder(v.getContext());
//                    builder0.setMessage("确认删除？");
//                    builder0.setTitle("删除");
//                    builder0.setPositiveButton("确认", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            Set<String> keys = downloaders.keySet();
//                            Iterator it = keys.iterator();
//                            while(it.hasNext()){
//                                com.briliasm.browser.download.Downloader dl = downloaders.get(it.next());
//                                dl.pause();
//                            }
//                            com.briliasm.browser.download.Dao dao = new Dao(v.getContext());
//                            dao.deleteAll();
//                            dao.closeDb();
//                            mData.clear();
//                            adapter.notifyDataSetChanged();
//                            usageInfoTV.setText(getUsageInfo(Environment.getExternalStorageDirectory().getPath()));
//
//                        }
//                    });
//                    builder0.setNegativeButton("取消", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.dismiss();
//                        }
//                    });
//                    builder0.create().show();
//                } else {
//                    Toast.makeText(v.getContext(), "无下载内容清除", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//    }
//
//    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//        createNewDownloader(intent);
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
////        downloaders.clear();
//    }
//}
