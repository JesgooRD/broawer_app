package com.briliasm.browser.subfragment;

import com.briliasm.browser.base.BaseTableFragment;
import com.briliasm.browser.bean.FavoriteData;
import com.briliasm.browser.util.FavoriteDao;
import com.jesgoo.browser.R;

import java.util.List;


public class ShortcutFragment extends BaseTableFragment {

    public ShortcutFragment(){
    }
    public List<FavoriteData>  getData(){

        FavoriteDao favoriteDao = new FavoriteDao(getActivity());
        List<FavoriteData> list =  favoriteDao.getShortcutPkgs();
        favoriteDao.closeDb();
        FavoriteData dataOfAdd = new FavoriteData();
        dataOfAdd.setDrawable(getResources().getDrawable(R.drawable.tab_thumbnail_add));
        dataOfAdd.setTitle(getString(R.string.add_shortcut));
        dataOfAdd.setPkg(MYPKG_ADD_SHORTCUT);
        list.add(dataOfAdd);

        return list;
    }

}  