package com.briliasm.browser.subfragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.briliasm.browser.MainActivity;
import com.jesgoo.browser.R;
import com.briliasm.browser.bean.FavoriteData;
import com.briliasm.browser.util.FavoriteDao;

import java.util.List;
import java.util.Random;

public class BookMarkFragment extends Fragment {
    private static List<FavoriteData> mData;

    private static ListView listView;
    private static Context context;
    private static MyAdapter adapter;

    public BookMarkFragment() {

    }

    public static void update(FavoriteData data) {
        mData.add(data);
        adapter.notifyDataSetChanged();
    }

    public final class ViewHolder {
        public TextView img;
        public TextView title;
        public TextView info;

    }

    public class MyAdapter extends BaseAdapter {

        private LayoutInflater mInflater;


        public MyAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Object getItem(int arg0) {
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            if (convertView == null) {

                holder = new ViewHolder();

                convertView = mInflater.inflate(R.layout.bookmark_item, null);
                holder.img = (TextView) convertView.findViewById(R.id.logo_tv);
                holder.title = (TextView) convertView.findViewById(R.id.title);
                holder.info = (TextView) convertView.findViewById(R.id.url);

                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            FavoriteData data = mData.get(position);
            String title = data.getTitle();
            if (title.length() > 0) {
                holder.img.setText(title.substring(0, 1));
            }

            holder.img.setBackgroundResource(R.drawable.tv_bg);
            GradientDrawable myGrad = (GradientDrawable) holder.img.getBackground();
            Random random = new Random();
            myGrad.setColor(Color.rgb(random.nextInt(100) + 156, random.nextInt(100) + 156, random.nextInt(100) + 156));

            holder.title.setText(title);
            holder.info.setText(data.getUrl());
            holder.info.setSingleLine(true);


            return convertView;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.sfragment_bookmark, container, false);
        context = getActivity();
        FavoriteDao favoriteDao = new FavoriteDao(context);
        mData = favoriteDao.getBookmarks();
        favoriteDao.closeDb();

        listView = (ListView) view.findViewById(R.id.bookmarkLV);
        adapter = new MyAdapter(view.getContext());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FavoriteData data = mData.get(position);
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");

                Uri content_url = Uri.parse(data.getUrl());
                intent.setData(content_url);
                intent.setClassName(context.getPackageName(), MainActivity.class.getName());
                context.startActivity(intent);
                getActivity().overridePendingTransition(R.anim.back_left_in, R.anim.back_right_out);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                final FavoriteData data = mData.get(position);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("确认删除吗？");
                builder.setTitle("提示");
                builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        FavoriteDao favoriteDao = new FavoriteDao(getActivity());
                        favoriteDao.deleteBookmark(data.getUrl());
                        favoriteDao.closeDb();
                        mData.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.create().show();


                return true;
            }
        });
        return view;
    }

}  