package com.briliasm.browser.base;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.briliasm.browser.MyGridView;
import com.briliasm.browser.PickupShortcutActivity;
import com.jesgoo.browser.R;
import com.briliasm.browser.bean.FavoriteData;
import com.briliasm.browser.util.FavoriteDao;
import com.briliasm.browser.util.Util;

import java.util.ArrayList;
import java.util.List;


public class BaseTableFragment extends Fragment {

    protected  static final String MYPKG_ADD_SHORTCUT = "add_shortcut";
    private Context context;
    GridAdapter adapter;
    MyGridView gridView;
    public BaseTableFragment(){
    }
    List<FavoriteData> mData;
    public List<FavoriteData>  getData(){
        return new ArrayList<>();
    }
    class ViewHolder{
        public ImageView img;
        public TextView title;
    }
    class GridAdapter extends BaseAdapter{
        private LayoutInflater mInflater;


        public GridAdapter(Context context){
            this.mInflater = LayoutInflater.from(context);
        }
        @Override
        public int getCount() {
            return mData.size();
        }

        @Override
        public Object getItem(int arg0) {
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            if (convertView == null) {

                holder=new ViewHolder();

                convertView = mInflater.inflate(R.layout.shortcut_grid_item, null);
                holder.img = (ImageView)convertView.findViewById(R.id.shortcutIcon);
                holder.title = (TextView)convertView.findViewById(R.id.shortcutTitle);
                holder.title.setSingleLine(true);
//                holder.info = (TextView)convertView.findViewById(R.id.sho);

                convertView.setTag(holder);

            }else {
                holder = (ViewHolder)convertView.getTag();
            }
            FavoriteData data = mData.get(position);

            holder.img.setImageDrawable(mData.get(position).getDrawable());
            holder.title.setText(data.getTitle());
//            holder.info.setText(data.getTitle());

            return convertView;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.sfragment_shortcut_grid, null);
        context = getActivity();

        mData= getData();
        gridView = (MyGridView)view.findViewById(R.id.gridview);
        adapter = new GridAdapter(context);
        gridView.setAdapter(adapter);

        gridView.setOnTouchInvalidPositionListener(new MyGridView.OnTouchInvalidPositionListener() {
            @Override
            public boolean onTouchInvalidPosition(int motionEvent) {
                return false;//不终止路由事件让父级控件处理事件
            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FavoriteData data = mData.get(position);
                String pkg = data.getPkg();
                String url = data.getUrl();
                if (MYPKG_ADD_SHORTCUT.equals(pkg)) {
                    Intent manageShortcut = new Intent();
                    manageShortcut.setClass(getActivity(), PickupShortcutActivity.class);
                    startActivity(manageShortcut);
                } else if (pkg != null && !pkg.equals("")) {

                    try {
                        PackageManager pm = context.getPackageManager();
                        Intent intent = pm.getLaunchIntentForPackage(pkg);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    } catch (Exception e) {
                        Toast.makeText(context, "无法打开该应用", Toast.LENGTH_SHORT).show();
                        FavoriteDao favoriteDao = new FavoriteDao(context);
                        favoriteDao.deleteShortCut(pkg);
                        mData.remove(position);
                        adapter.notifyDataSetChanged();

                    }
                } else if (url != null && !url.equals("")) {
                    Util.startUrl(context,data.getUrl());
                }

            }
        });



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(adapter != null){
            mData= getData();
            adapter.notifyDataSetChanged();
        }
    }
}