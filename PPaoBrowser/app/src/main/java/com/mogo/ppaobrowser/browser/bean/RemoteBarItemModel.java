package com.mogo.ppaobrowser.browser.bean;

/**
 * Created by doyer on 2017/5/17.
 */
public class RemoteBarItemModel {
    public String name;
    public String icon_url;
    public String web_url;

    public RemoteBarItemModel(String name, String icon_url, String web_url) {
        this.name = name;
        this.icon_url = icon_url;
        this.web_url = web_url;
    }
}
