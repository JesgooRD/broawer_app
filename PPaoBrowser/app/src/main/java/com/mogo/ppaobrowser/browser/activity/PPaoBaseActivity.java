package com.mogo.ppaobrowser.browser.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.update.ApkUpdateUtils;
import com.mogo.ppaobrowser.utils.DialogUtils;
import com.mogo.ppaobrowser.utils.ToastUtils;
import com.umeng.analytics.MobclickAgent;

import butterknife.ButterKnife;

/**
 * Created by doyer on 2017/7/5.
 */

public abstract class PPaoBaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(attachLayoutRes());
        ButterKnife.bind(this);
        initViews();
        updateViews(false);

    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    protected abstract void updateViews(boolean b);

    protected abstract void initViews();

    protected abstract int attachLayoutRes();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slide_out_left, R.anim.slide_out_right);
        }
        return super.onOptionsItemSelected(item);
    }

    protected void showToast(String str) {
        ToastUtils.showToast(this, str);
    }

    protected void showToast(@StringRes int resId) {
        ToastUtils.showToast(this, getString(resId));
    }

    public void showAlert(String title, String message, DialogInterface.OnClickListener yes, DialogInterface.OnClickListener no) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle(title);
        // Setting Dialog Message
        alertDialog.setMessage(message);
        // On pressing Settings button
        if (yes != null)
            alertDialog.setPositiveButton(R.string.action_ok, yes);
        if (no != null)
            alertDialog.setNegativeButton(R.string.action_cancel, no);
        // Showing Alert Message
        alertDialog.show();
    }

    public void showAlert(String title, String message, DialogInterface.OnClickListener yes_listener) {
        showAlert(title, message, yes_listener, null);
    }

    public void showAlert(String title, String message) {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        };
        showAlert(title, message, listener);
    }

    /**
     * 隐藏软键盘
     *
     * @param view
     */
    protected void closeWindowSoftInput(View view) {
        InputMethodManager imm = (InputMethodManager) getApplicationContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        boolean isSoftActive = imm.isActive();
        if (isSoftActive) {
            imm.hideSoftInputFromWindow(
                    view.getApplicationWindowToken(), 0); // 强制隐藏键盘
        }
    }

    protected void startActivityWithZoom(Intent intent) {
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_in_right);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.slide_out_left, R.anim.slide_out_right);
    }

    /**
     * 设置添加屏幕的背景透明度
     *
     * @param bgAlpha
     */
    public void backgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        getWindow().setAttributes(lp);
    }

    public void createUpdateAppDialog(final String url, final boolean flag) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.dialog_setting_update, null);
        final Dialog dialog = DialogUtils.createDialog(this, view);
        final LinearLayout ll_content = (LinearLayout) view.findViewById(R.id.ll_content);
        final TextView tv_update_next = (TextView) view.findViewById(R.id.btn_left);
        final TextView tv_update_now = (TextView) view.findViewById(R.id.btn_right);
        tv_update_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                dialog.dismiss();
            }
        });
        tv_update_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!flag) {
                    createWifiDownDialog(url);
                } else
                    ApkUpdateUtils.download(PPaoBaseActivity.this, url, "泡泡浏览器");
                dialog.cancel();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void createWifiDownDialog(final String url) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.dialog_setting_update, null);
        final Dialog dialog = DialogUtils.createDialog(this, view);
        final TextView tv_left = (TextView) view.findViewById(R.id.btn_left);
        final TextView tv_right = (TextView) view.findViewById(R.id.btn_right);
        final TextView tv_title = (TextView) view.findViewById(R.id.title);
        tv_title.setText(getString(R.string.update_wifi_warning));
        tv_left.setText(getString(R.string.action_cancel));
        tv_right.setText(getString(R.string.update_ensure_download));
        tv_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                dialog.dismiss();
            }
        });
        tv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApkUpdateUtils.download(PPaoBaseActivity.this, url, "泡泡浏览器");
                dialog.cancel();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
