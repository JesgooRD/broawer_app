package com.mogo.ppaobrowser;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.mogo.ppaobrowser.base.AppConst;
import com.mogo.ppaobrowser.gen.DaoMaster;
import com.mogo.ppaobrowser.gen.DaoSession;
import com.mogo.ppaobrowser.utils.AppUtility;
import com.mogo.ppaobrowser.utils.CrashHandler;
import com.mogo.ppaobrowser.utils.LogUtility;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.greenrobot.greendao.database.Database;

/**
 * Created by doyer on 2017/5/17.
 */
public class PPaoApplication extends Application {
    private static final String TAG = "PPaoApplication";
    private static PPaoApplication APPLICATION;
    private static DaoSession daoSession;

    public static PPaoApplication getApplication() {
        return APPLICATION;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "ppao-db");
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
        APPLICATION = this;
        Fresco.initialize(this);
        setupWXAPI();
        CrashHandler.getInstance().init(getApplication());
        LogUtility.setEnable(AppUtility.isDebug());
    }

    public static DaoSession getDaoSession() {
        return daoSession;
    }

    private void setupWXAPI() {
        IWXAPI api = WXAPIFactory.createWXAPI(this, AppConst.WX_APP_ID, false);
        api.registerApp(AppConst.WX_APP_ID);
    }

}
