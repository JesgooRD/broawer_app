package com.mogo.ppaobrowser.browser.interface_package;

/**
 * Created by doyer on 2017/6/5.
 */

public interface DataAccessListener<T> {
    void requestSuccess(T ob);

    void requestFail(String error);
}
