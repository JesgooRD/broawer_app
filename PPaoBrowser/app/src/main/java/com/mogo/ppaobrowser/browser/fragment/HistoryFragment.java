package com.mogo.ppaobrowser.browser.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.activity.BrowserActivity;
import com.mogo.ppaobrowser.browser.adapter.HBAdapter;
import com.mogo.ppaobrowser.browser.base.BrowserBaseActivity;
import com.mogo.ppaobrowser.browser.base.BaseFragment;
import com.mogo.ppaobrowser.browser.interface_package.FragmentBackListener;
import com.mogo.ppaobrowser.browser.interface_package.SaveDataListener;
import com.mogo.ppaobrowser.browser.bean.HistoryModel;
import com.mogo.ppaobrowser.widget.OwnItemDecoration;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by doyer on 2017/5/22.
 */
public class HistoryFragment extends BaseFragment implements FragmentBackListener {
    @BindView(R.id.recycler_history)
    RecyclerView recyclerHistory;
    @BindView(R.id.btn_clear)
    Button btnClear;
    @BindView(R.id.empty_layout)
    RelativeLayout emptyLayout;
    private ArrayList<Object> historyModelList;
    private List<Boolean> checkList;

    Unbinder unbinder;

    HBAdapter adapter;
    PopupWindow popupWindow;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (activity instanceof BrowserBaseActivity) {
            ((BrowserBaseActivity) activity).setBackListener(this);
            ((BrowserBaseActivity) activity).setInterception(true);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("HistoryFragment");
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("HistoryFragment");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (getActivity() instanceof BrowserBaseActivity) {
            ((BrowserBaseActivity) getActivity()).setBackListener(null);
            ((BrowserBaseActivity) getActivity()).setInterception(false);
        }
    }

    @Override
    protected void initViews() {
        if (historyModelList.size() == 0) {
            emptyLayout.setVisibility(View.VISIBLE);
            return;
        }
        //创建默认的线性LayoutManager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this.getContext());
        recyclerHistory.setLayoutManager(mLayoutManager);
        adapter = new HBAdapter(this.getContext(), historyModelList, checkList, null);
        recyclerHistory.addItemDecoration(new OwnItemDecoration());
        adapter.setOnItemListener(new HBAdapter.OnItemClickListener() {
            @Override
            public boolean setOnItemLongClick(int position) {
                if (popupWindow == null) {
                    PopupWindowOut(recyclerHistory.getRootView());
                } else if (!popupWindow.isShowing()) {
                    popupWindow.showAtLocation(recyclerHistory.getRootView(), Gravity.BOTTOM, 0, 0);
                }
                adapter.selected_show = true;//长按改变checkbox显示状态
                adapter.notifyDataSetChanged();
                return true;
            }

            @Override
            public void setOnItemCheckedChanged(int position, boolean isCheck) {
                if (adapter.selected_show)
                    checkList.set(position, isCheck);
            }

            @Override
            public void setOnItemClickListener(int position) {
                Intent intent = new Intent(getContext(), BrowserActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("urlString", ((HistoryModel) historyModelList.get(position)).getUrl());
                intent.putExtras(bundle);
                startActivity(intent);
                getActivity().finish();
            }
        });
        recyclerHistory.setAdapter(adapter);
    }

    @Override
    protected int attachLayoutRes() {
        return 0;
    }

    @Override
    protected void updateViews(boolean b) {

    }

    private void initData() {
        historyModelList = new ArrayList<>();
        getDataFromBase();
        checkList = new ArrayList<>();
        for (int i = 0; i < historyModelList.size(); i++) {
            checkList.add(false);
        }
    }

    private void getDataFromBase() {
        historyModelList.addAll(browserPresenter.getAllHistory());
        Collections.reverse(historyModelList); // 倒序排列
    }

    protected void PopupWindowOut(View view) {
        if (view != null) {
            final View contentView = LayoutInflater.from(this.getContext()).inflate(R.layout.bh_pop_bottom, null);
            popupWindow = new PopupWindow(contentView);
            contentView.findViewById(R.id.pop_clear).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int length = checkList.size();
                    for (int i = length - 1; i >= 0; i--) {
                        if (checkList.get(i)) {
                            browserPresenter.removeHistory((HistoryModel) historyModelList.get(i));
                            historyModelList.remove(i);
                            checkList.remove(i);
                            adapter.selected_show = false;
                            adapter.notifyDataSetChanged();
                        }
                    }
                    if (historyModelList.size() == 0) {
                        emptyLayout.setVisibility(View.VISIBLE);
                    }
                    popupWindow.dismiss();
                }
            });

            popupWindow.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
            popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
            popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));
            popupWindow.setFocusable(false);
            popupWindow.setOutsideTouchable(false);
            popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
        }
    }

    @Override
    public void onbackForward() {
        if ((popupWindow != null && popupWindow.isShowing()) || adapter == null ? false : adapter.selected_show) {
            popupWindow.dismiss();
            adapter.selected_show = !adapter.selected_show;
            adapter.notifyDataSetChanged();
        } else {
            getActivity().finish();
        }
    }


    @OnClick(R.id.btn_clear)
    public void onViewClicked() {
        emptyLayout.setVisibility(View.VISIBLE);
        historyModelList.removeAll(historyModelList);
        adapter.selected_show = false;
        adapter.notifyDataSetChanged();
        browserPresenter.removeAllHistory();
    }

}
