package com.mogo.ppaobrowser.utils;

import android.util.Base64;

import java.io.UnsupportedEncodingException;

/**
 * Created by doyer.du on 9/26/16.
 */
public class StringUtil {
    public static final String charsetUTF8 = "UTF-8";
    public static final String charsetUTF16 = "UTF-16";

    public static String getBase64(String source) {
        return getBase64(source, charsetUTF8);
    }

    public static String getBase64(String source, String charsetName) {
        byte[] data = null;
        try {
            data = source.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }

        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    public static boolean isNullorEmpty(String source) {
        return source == null || source.isEmpty();
    }

    public static String[] split(String base, String symbol) {
        String[] array = null;
        if (base == null || symbol == null) {
            return array;
        }

        array = base.split(symbol);

        return array;
    }
}
