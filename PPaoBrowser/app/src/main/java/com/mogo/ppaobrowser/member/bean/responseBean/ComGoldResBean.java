package com.mogo.ppaobrowser.member.bean.responseBean;

/**
 * Created by doyer on 2017/7/17.
 */

public class ComGoldResBean {

    /**
     * code : 200
     * gold : 0
     * message : shared
     */

    public int code;
    public int gold;
    public String message;

}
