package com.mogo.ppaobrowser.browser.controller;

import android.graphics.drawable.Drawable;
import android.util.Log;


import com.mogo.ppaobrowser.browser.fragment.MainFragment;
import com.mogo.ppaobrowser.browser.fragment.WebViewFragment;
import com.mogo.ppaobrowser.utils.LogUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangliang on 15/7/23.
 */
public class MultiWindowManager {

    public static List<WebViewFragment> mulWindowBrowserList = new ArrayList(9);
    public static List<MainFragment> mulWindowMainList = new ArrayList(9);
    public static List<MultiWindowManager> mulWindowDataList = new ArrayList(9);

    static {
        LogUtility.debug("doyer", "执行代static码段");
        for (int i = 0; i < 9; i++) {
            MultiWindowManager.mulWindowMainList.add(null);
        }

        for (int i = 0; i < 9; i++) {
            MultiWindowManager.mulWindowBrowserList.add(null);
        }

        for (int i = 0; i < 9; i++) {
            MultiWindowManager.mulWindowDataList.add(new MultiWindowManager());
        }

    }

    public static void releaseWindowData(int position) {
        mulWindowDataList.remove(position);
        mulWindowDataList.add(null);
    }

    private Drawable drawable;

    private String title;
    private String url;
    private int id;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }


    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }
}
