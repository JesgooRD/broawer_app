package com.mogo.ppaobrowser.member.view;

/**
 * Created by Lain on 2016/10/14.
 * 接口基础 成功失败返回监听
 */

public interface IResultListener<T> {
    void onSuccess(T t);
    void onFail(String error);
}
