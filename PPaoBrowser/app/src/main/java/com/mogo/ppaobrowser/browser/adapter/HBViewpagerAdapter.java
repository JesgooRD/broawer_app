package com.mogo.ppaobrowser.browser.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by doyer on 2017/5/22.
 * 历史、标签所在viewpager适配器
 */
public class HBViewpagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> mFragmentList;
    private List<String> titleList;

    public HBViewpagerAdapter(FragmentManager fm, List<Fragment> fragments, List<String> titleList) {
        super(fm);
        this.mFragmentList = fragments;
        this.titleList = titleList;
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleList.get(position);
    }

}
