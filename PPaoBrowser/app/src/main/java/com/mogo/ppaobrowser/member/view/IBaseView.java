package com.mogo.ppaobrowser.member.view;

/**
 * Created by Lain on 2016/10/14.
 *  基类显示view 弹出框，显示隐藏载入框
 */

public interface IBaseView {
    void showToast(String str);
    void showLoadingDialog(String string);
    void dismissDialog();
}
