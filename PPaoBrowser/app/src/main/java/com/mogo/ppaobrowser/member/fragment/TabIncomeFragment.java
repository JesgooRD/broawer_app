package com.mogo.ppaobrowser.member.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.member.bean.IncomeGoldBean;
import com.mogo.ppaobrowser.member.bean.responseBean.IncomeResbean;
import com.mogo.ppaobrowser.utils.DateTimeUtils;
import com.mogo.ppaobrowser.widget.OwnItemDecoration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by doyer on 2017/7/19.
 */

public class TabIncomeFragment extends Fragment {
    private static final String TAG = "TabIncomeFragment";
    @BindView(R.id.income_recycler)
    RecyclerView incomeRecycler;
    Unbinder unbinder;
    List<IncomeGoldBean> goldList = new ArrayList<>();
    List<IncomeGoldBean> cashList = new ArrayList<>();
    private IncomeAdapter adapter;
    String tag;
    final int VIEW_TYPE_GOLD = 0;
    final int VIEW_TYPE_CASH = 1;
    private final Map<String, String> taskMap = new HashMap<>();

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (goldList.size() > 0 | cashList.size() > 0) {
                adapter.notifyDataSetChanged();
            }
        }
    };


    public static Fragment newInstance(String title) {
        final TabIncomeFragment tabIncomeFragment = new TabIncomeFragment();
//        initTaskData(tabIncomeFragment.taskMap);
        if (title.equals("金币")) {
            tabIncomeFragment.tag = "金币";
        } else {
            tabIncomeFragment.tag = "零钱";
        }
        return tabIncomeFragment;
    }

    private static void initTaskData(Map<String, String> taskMap) {
        taskMap.put("1", "首次微信登录任务");
        taskMap.put("2", "完善个人资料任务");
        taskMap.put("3", "签到任务");
        taskMap.put("4", "分享app任务");
        taskMap.put("5", "阅读小说任务奖励");
        taskMap.put("6", "阅读漫画任务奖励");
        taskMap.put("7", "观看视频任务奖励");
        taskMap.put("8", "阅读资讯任务奖励");
        taskMap.put("9", "有效反馈任务");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.income_fragment_layout, container, false);
        unbinder = ButterKnife.bind(this, view);
        //创建默认的线性LayoutManager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this.getContext());
        incomeRecycler.setLayoutManager(mLayoutManager);
        adapter = new IncomeAdapter();
        incomeRecycler.setAdapter(adapter);
        incomeRecycler.addItemDecoration(new OwnItemDecoration());
        return view;
    }

    public void setData(List<IncomeResbean> list) {
        for (IncomeResbean income : list
                ) {
            if (income.addGoldCoin != 0) {
                IncomeGoldBean incomeGold = new IncomeGoldBean();
                incomeGold.eventName = income.taskName;
                incomeGold.gold = String.valueOf(income.addGoldCoin);
                incomeGold.event_time = DateTimeUtils.convertToDate(income.eventTime * 1000);
                goldList.add(incomeGold);
            }
            if (income.addCash != 0) {
                IncomeGoldBean incomeCash = new IncomeGoldBean();
                incomeCash.eventName = income.taskName;
                incomeCash.gold = String.valueOf(income.addCash) + "元";
                incomeCash.event_time = DateTimeUtils.convertToDate(income.eventTime * 1000);
                cashList.add(incomeCash);
            }
        }
        handler.sendEmptyMessage(0);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private class IncomeAdapter extends RecyclerView.Adapter {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == VIEW_TYPE_GOLD) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.income_gold_item, parent, false);
                CashViewHolder cashViewHolder = new CashViewHolder(view);
                return cashViewHolder;
            } else {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.income_cash_item, parent, false);
                GoldViewHolder goldViewHolder = new GoldViewHolder(view);
                return goldViewHolder;
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof CashViewHolder) {
                IncomeGoldBean incomeGoldBean = goldList.get(position);
                ((CashViewHolder) holder).name.setText(incomeGoldBean.eventName);
                ((CashViewHolder) holder).time.setText(incomeGoldBean.event_time);
                ((CashViewHolder) holder).gold.setText(incomeGoldBean.gold);
            } else {
                IncomeGoldBean incomeGoldBean = cashList.get(position);
                ((GoldViewHolder) holder).name.setText(incomeGoldBean.eventName);
                ((GoldViewHolder) holder).time.setText(incomeGoldBean.event_time);
                ((GoldViewHolder) holder).gold.setText(incomeGoldBean.gold);
            }
        }

        @Override
        public int getItemCount() {
            if (tag.equals("金币") && goldList != null)
                return goldList.size();
            else return cashList.size();
        }

        @Override
        public int getItemViewType(int position) {
            if (tag.equals("金币"))
                return VIEW_TYPE_GOLD;
            else return VIEW_TYPE_CASH;
        }

        private class CashViewHolder extends RecyclerView.ViewHolder {

            TextView name;
            TextView time;
            TextView gold;

            public CashViewHolder(View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.tv_gold_name);
                time = (TextView) itemView.findViewById(R.id.tv_gold_time);
                gold = (TextView) itemView.findViewById(R.id.tv_gold_num);
            }
        }

        private class GoldViewHolder extends RecyclerView.ViewHolder {

            TextView name;
            TextView time;
            TextView gold;

            public GoldViewHolder(View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.tv_gold_name);
                time = (TextView) itemView.findViewById(R.id.tv_gold_time);
                gold = (TextView) itemView.findViewById(R.id.tv_gold_num);
            }
        }
    }
}
