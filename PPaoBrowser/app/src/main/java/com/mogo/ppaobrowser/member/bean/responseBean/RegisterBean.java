package com.mogo.ppaobrowser.member.bean.responseBean;

/**
 * Created by Lain on 2016/10/20.
 * 登录返回 实体类
 */

public class RegisterBean<T> {

    /**
     * code : 200
     * data : {"Age":0,"Cash":0,"Gender":1,"Gold":0,"HeadUrl":"http://127.0.0.1:9091","NickName":"chenxun","OpenId":"4","SignCount":0,"UserId":31}
     * message : success
     */

    public int code;
    public T data;
    public String message;
    public int userId;
}
