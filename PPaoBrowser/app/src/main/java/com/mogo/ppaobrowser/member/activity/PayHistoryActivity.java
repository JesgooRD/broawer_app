package com.mogo.ppaobrowser.member.activity;

import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.activity.PPaoBaseActivity;
import com.mogo.ppaobrowser.browser.interface_package.DataAccessListener;
import com.mogo.ppaobrowser.member.bean.PayHistoryBean;
import com.mogo.ppaobrowser.member.bean.TransferResNote;
import com.mogo.ppaobrowser.member.biz.UserBiz;
import com.mogo.ppaobrowser.preference.MemberCache;
import com.mogo.ppaobrowser.utils.DateTimeUtils;
import com.mogo.ppaobrowser.utils.ToastUtils;
import com.mogo.ppaobrowser.widget.OwnItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by doyer on 2017/7/20.
 */

public class PayHistoryActivity extends PPaoBaseActivity {

    private static final String TAG = "PayHistoryActivity";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.pay_history_recycler)
    RecyclerView payHistoryRecycler;
    @BindView(R.id.tv_null_history)
    TextView tvNullHistory;

    private PayHistoryAdapter adapter;
    List<PayHistoryBean> payHistoryBeanList = new ArrayList<>();
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (payHistoryBeanList.size() > 0) {
                tvNullHistory.setVisibility(View.GONE);
                payHistoryRecycler.setVisibility(View.VISIBLE);
                adapter.notifyDataSetChanged();
            }
        }
    };


    @Override
    protected void updateViews(boolean b) {

    }

    @Override
    protected void initViews() {
        setSupportActionBar(toolbar);
        toolbarTitle.setText(getString(R.string.pay_history));
        //创建默认的线性LayoutManager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        payHistoryRecycler.setLayoutManager(mLayoutManager);
        adapter = new PayHistoryAdapter();
        payHistoryRecycler.addItemDecoration(new OwnItemDecoration());
        payHistoryRecycler.setAdapter(adapter);
        //API 请求
        getPayHistory();
        String s = DateTimeUtils.convertToDate(System.currentTimeMillis());
        String s1 = DateTimeUtils.convertToTime(System.currentTimeMillis());
        Log.d(TAG, "initViews: " + s);

    }

    private boolean getPayHistory() {
        //提交
        int userId = MemberCache.getMemUserId();
        if (userId == 0) {
            ToastUtils.show(this, "您尚未登陆");
            return true;
        }

        UserBiz userBiz = new UserBiz();
        userBiz.getPayRecord(new DataAccessListener<List<TransferResNote>>() {
            @Override
            public void requestSuccess(List<TransferResNote> list) {
                if (list != null && list.size() > 0) {
                    payHistoryBeanList.clear();
                    PayHistoryBean payHistoryBean = new PayHistoryBean();
                    for (TransferResNote databean : list
                            ) {
                        payHistoryBean.goods_name = "支付宝" + databean.amount + "元";
                        payHistoryBean.goods_state = databean.status;
                        payHistoryBean.goods_time0 = DateTimeUtils.convertToDate(databean.commitTime * 1000);
                        payHistoryBean.goods_time1 = DateTimeUtils.convertToTime(databean.commitTime * 1000);
                        payHistoryBeanList.add(payHistoryBean);

                    }
                    handler.sendEmptyMessage(0);
                }
            }

            @Override
            public void requestFail(String error) {

            }
        });

        return false;
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.pay_history_layout;
    }

    private class PayHistoryAdapter extends RecyclerView.Adapter<PayHistoryAdapter.PayHistoryViewHolder> {


        @Override
        public PayHistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pay_history_item, parent, false);
            PayHistoryViewHolder payHistoryViewHolder = new PayHistoryViewHolder(view);
            return payHistoryViewHolder;
        }

        @Override
        public void onBindViewHolder(PayHistoryViewHolder holder, int position) {
            PayHistoryBean payHistoryBean = null;
            if (payHistoryBean == null) {
                payHistoryBean = payHistoryBeanList.get(position);
            }
            holder.name.setText(payHistoryBean.goods_name);
            holder.time0.setText(payHistoryBean.goods_time0);
            holder.time1.setText(payHistoryBean.goods_time1);
            if (payHistoryBean.goods_state == 0)
                holder.state.setText("处理中");
            else holder.state.setText("处理成功");

        }

        @Override
        public int getItemCount() {
            if (payHistoryBeanList.size() == 0) {
                tvNullHistory.setVisibility(View.VISIBLE);
                payHistoryRecycler.setVisibility(View.GONE);
            }
            return payHistoryBeanList.size();
        }

        public class PayHistoryViewHolder extends RecyclerView.ViewHolder {
            TextView name;
            TextView time0;
            TextView time1;
            TextView state;

            public PayHistoryViewHolder(View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.tv_name);
                time0 = (TextView) itemView.findViewById(R.id.tv_time0);
                time1 = (TextView) itemView.findViewById(R.id.tv_time1);
                state = (TextView) itemView.findViewById(R.id.tv_state);
            }

        }
    }
}
