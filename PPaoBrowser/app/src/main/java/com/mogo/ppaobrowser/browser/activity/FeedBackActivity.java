package com.mogo.ppaobrowser.browser.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mogo.ppaobrowser.Constants;
import com.mogo.ppaobrowser.PPaoApplication;
import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.interface_package.DataAccessListener;
import com.mogo.ppaobrowser.member.biz.UserBiz;
import com.mogo.ppaobrowser.preference.MemberCache;
import com.mogo.ppaobrowser.utils.AppUtility;
import com.mogo.ppaobrowser.utils.HttpUtil;
import com.mogo.ppaobrowser.utils.StringUtil;
import com.mogo.ppaobrowser.utils.ToastUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.RequestBody;

/**
 * Created by doyer on 2017/5/24.
 * 吐槽反馈
 */
public class FeedBackActivity extends PPaoBaseActivity implements View.OnFocusChangeListener {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_content)
    EditText etContent;
    @BindView(R.id.et_contact)
    EditText etContact;
    @BindView(R.id.feed_commit)
    Button feedCommit;
    private int requestCode = 0x021;

    Handler handler = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
            switch (msg.what) {
                case 0:
                    ToastUtils.show(FeedBackActivity.this, " 小报告已提交！");
                    etContact.setText("");
                    etContent.setText("");
                    finish();
                    break;
                case 1:
                    String error = (String) msg.obj;
                    ToastUtils.show(FeedBackActivity.this, error);
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void updateViews(boolean b) {

    }

    @Override
    protected void initViews() {
        setSupportActionBar(toolbar);
        toolbarTitle.setText(getString(R.string.feed_back));
        etContact.setOnFocusChangeListener(this);
        etContent.setOnFocusChangeListener(this);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.feed_back_layout;
    }

    @OnClick({/*R.id.title_back, */com.mogo.ppaobrowser.R.id.feed_commit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            case R.id.title_back:
//                FeedBackActivity.this.finish();
//                break;
            case R.id.feed_commit:
                String deviceId = "";
                if (AppUtility.isMarshmallow() && !AppUtility.isGranted_(this, Manifest.permission.READ_PHONE_STATE)) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, requestCode);
                } else {
                    deviceId = AppUtility.getDeviceId();
                    feedBack(deviceId);
                }
                break;
        }
    }

    private void feedBack(String deviceId) {
        UserBiz userBiz = new UserBiz();
        int userId = MemberCache.getMemUserId();
        String android_id = Settings.System.getString(PPaoApplication.getApplication().getContentResolver(), Settings.System.ANDROID_ID);
        String pck_name = PPaoApplication.getApplication().getPackageName();
        String contact_way = etContact.getText().toString().trim();
        String content = etContent.getText().toString();
        if (StringUtil.isNullorEmpty(content)) {
            ToastUtils.show(this, getResources().getString(R.string.feed_toast));
        } else {
            try {
                content = URLEncoder.encode(content, "UTF-8");
                contact_way = URLEncoder.encode(contact_way, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String channel = getAppMetaData(this, "UMENG_CHANNEL");
            String time = System.currentTimeMillis() + "";
            String app_version = AppUtility.getAppVersion();
            String os_version = Build.VERSION.SDK_INT + "";
            final DataAccessListener requestListener = new DataAccessListener() {
                @Override
                public void requestSuccess(Object ob) {
                    handler.sendEmptyMessage(0);
                }

                @Override
                public void requestFail(String error) {
                    Message message = Message.obtain();
                    message.what = 1;
                    message.obj = error;
                    handler.sendMessage(message);
                }
            };

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("UserId", userId);
                jsonObject.put("Android_id", android_id);
                jsonObject.put("Pkg_name", pck_name);
                jsonObject.put("Contact_way", contact_way);
                jsonObject.put("Content", content);
                jsonObject.put("Channel", channel);
                jsonObject.put("App_version", app_version);
                jsonObject.put("Os_version", os_version);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=UTF-8"), jsonObject.toString());
            userBiz.complain(requestListener, body);
//            HttpUtil.HttpGet(feedBack_url, requestListener);
        }
    }

    /**
     * 获取渠道名
     *
     * @param ctx 此处习惯性的设置为activity，实际上context就可以
     * @return 如果没有获取成功，那么返回值为空
     */
    public static String getChannelName(Activity ctx) {
        if (ctx == null) {
            return null;
        }
        String channelName = null;
        try {
            PackageManager packageManager = ctx.getPackageManager();
            if (packageManager != null) {
                //注意此处为ApplicationInfo 而不是 ActivityInfo,因为友盟设置的meta-data是在application标签中，而不是某activity标签中，所以用ApplicationInfo
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(ctx.getPackageName(), PackageManager.GET_META_DATA);
                if (applicationInfo != null) {
                    if (applicationInfo.metaData != null) {
                        channelName = applicationInfo.metaData.getString("");
                    }
                }

            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return channelName;
    }


    /**
     * 获取application中指定的meta-data
     *
     * @return 如果没有获取成功(没有对应值，或者异常)，则返回值为空
     */
    public static String getAppMetaData(Context ctx, String key) {
        if (ctx == null || TextUtils.isEmpty(key)) {
            return null;
        }
        String resultData = null;
        try {
            PackageManager packageManager = ctx.getPackageManager();
            if (packageManager != null) {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(ctx.getPackageName(), PackageManager.GET_META_DATA);
                if (applicationInfo != null) {
                    if (applicationInfo.metaData != null) {
                        resultData = applicationInfo.metaData.getString(key);
                    }
                }

            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return resultData;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == this.requestCode) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                feedBack(AppUtility.getDeviceId());
            } else feedBack("");
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        String hint;
        EditText editText = (EditText) v;
        switch (v.getId()) {
            case R.id.et_content:
            case R.id.et_contact:
                if (hasFocus) {
                    hint = editText.getHint().toString();
                    editText.setTag(hint);
                    editText.setHint("");
                } else {
                    hint = v.getTag().toString();
                    editText.setHint(hint);
                }
                break;
            default:
                break;
        }
    }
}
