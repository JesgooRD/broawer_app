package com.mogo.ppaobrowser.browser.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.mogo.ppaobrowser.Constants;
import com.mogo.ppaobrowser.PPaoApplication;
import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.base.AppConst;
import com.mogo.ppaobrowser.browser.activity.BrowserActivity;
import com.mogo.ppaobrowser.browser.activity.SearchActivity;
import com.mogo.ppaobrowser.browser.adapter.WebLocalGridViewAdapter;
import com.mogo.ppaobrowser.browser.adapter.WebPagerAdapter;
import com.mogo.ppaobrowser.browser.adapter.WebsiteViewPagerAdapter;
import com.mogo.ppaobrowser.browser.base.BrowserBaseActivity;
import com.mogo.ppaobrowser.browser.bean.BarItemModel;
import com.mogo.ppaobrowser.browser.bean.RemoteBarItemModel;
import com.mogo.ppaobrowser.browser.bean.WebRemoteGridViewAdapter;
import com.mogo.ppaobrowser.browser.controller.MultiWindowManager;
import com.mogo.ppaobrowser.browser.interface_package.BarController;
import com.mogo.ppaobrowser.browser.interface_package.DataAccessListener;
import com.mogo.ppaobrowser.browser.service.MainService;
import com.mogo.ppaobrowser.gen.DaoSession;
import com.mogo.ppaobrowser.gen.SignModelDao;
import com.mogo.ppaobrowser.member.activity.MemberSystemActivity;
import com.mogo.ppaobrowser.member.bean.SignModel;
import com.mogo.ppaobrowser.member.bean.responseBean.IconResBean;
import com.mogo.ppaobrowser.member.bean.responseBean.IncomeResbean;
import com.mogo.ppaobrowser.member.bean.responseBean.MemberBean;
import com.mogo.ppaobrowser.member.bean.responseBean.SignResBean;
import com.mogo.ppaobrowser.member.bean.responseBean.TaskResbean;
import com.mogo.ppaobrowser.member.biz.UserBiz;
import com.mogo.ppaobrowser.member.event.AppCommonEvent;
import com.mogo.ppaobrowser.preference.BrowserCache;
import com.mogo.ppaobrowser.preference.MemberCache;
import com.mogo.ppaobrowser.utils.AppUtility;
import com.mogo.ppaobrowser.utils.DateTimeUtils;
import com.mogo.ppaobrowser.utils.ToastUtils;
import com.mogo.ppaobrowser.utils.UrlUtils;
import com.mogo.ppaobrowser.widget.ProgressView;
import com.mogo.ppaobrowser.widget.SearchView;
import com.mogo.ppaobrowser.widget.WebViewPager;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainFragment extends WebBottomFragment implements AppBarLayout.OnOffsetChangedListener, BarController, View.OnClickListener {
    public static final String TAG = "MainFragment";
    private static final int REQUEST_SEARCH_CODE = 01;
    public static final int RESULT_SEARCH_CODE = 02;
    public static final String SEARCH_KEY = "SEARCH_KEY";
    private static MainFragment MAIN_FRAGMENT;

    @BindView(R.id.bar_search)
    SearchView barSearch;
    @BindView(R.id.bar_viewpager)
    ViewPager barViewpager;
    @BindView(R.id.bar_dot)
    LinearLayout barDot;
    @BindView(R.id.content_tab)
    TabLayout contentTab;
    @BindView(R.id.content_pager)
    WebViewPager contentPager;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.toolbar_layout)
    public CollapsingToolbarLayout toolbarLayout;
    @BindView(R.id.content_ll)
    LinearLayout contentLl;
    @BindView(R.id.coordinatorLayout)
    public CoordinatorLayout coordinatorLayout;
    @BindView(R.id.bottom_action_back)
    FrameLayout bottomActionBack;
    @BindView(R.id.bottom_action_forward)
    FrameLayout bottomActionForward;
    @BindView(R.id.bottom_action_menu)
    FrameLayout bottomActionMenu;
    @BindView(R.id.bottom_action_home)
    FrameLayout bottomActionHome;
    @BindView(R.id.bottom_page_plus)
    FrameLayout bottomPagePlus;
    Unbinder unbinder;
    @BindView(R.id.member_photo)
    SimpleDraweeView memberPhoto;
    @BindView(R.id.tv_member_name)
    TextView tvMemberName;
    @BindView(R.id.tv_member_gold)
    TextView tvMemberGold;
    @BindView(R.id.search_container)
    LinearLayout searchContainer;
    @BindView(R.id.ll)
    RelativeLayout ll;
    @BindView(R.id.bt_sign_in)
    Button btnSignIn;

    private List<View> mPagerList;
    //webview fragment
    private ArrayList<Fragment> mWebList = new ArrayList<>();
    private LayoutInflater inflater;
    private List<BarItemModel> mDatas;
    private List<RemoteBarItemModel> mRemoteDatas;
    //总的页数
    private int pageCount;
    //每一页显示的个数
    private int pageSize = 10;
    //当前显示的是第几页
    private int curIndex = 0;
    //是否展开
    public boolean appbar_expand = false;

    private String[] web_name_list = {"好123", "小说", "百度", "腾讯视频", "B站", "A站", "糗事百科", "冷兔", "淘宝", "京东", "凤凰", "58", "赶集"};
    private String[] web_url_list = {"http://m.hao123.com/", "", "http://www.baidu.com/", "http://m.v.qq.com/", "http://m.bilibili.com/index.html", "http://m.acfun.cn/", "http://www.qiushibaike.com/", "http://m.lengxiaohua.com/new", "https://m.taobao.com/#index", "https://m.jd.com/", "http://i.ifeng.com/?ch=ifengweb_2014", "http://m.58.com", "https://3g.ganji.com/"};
    private ArrayList<String> titleList = new ArrayList<String>() {{
        add("小说");
        add("漫画");
        add("视频");
        add("游戏");
        add("资讯");
    }};
    private WebPagerAdapter adapter;
    private UserBiz userBiz = new UserBiz();
    public int userId;
    MemberBean memberBean;
    private SignModelDao signDao;
    SignModel signModel = null;
    private int today_gold = 0;
    private ProgressView progressView;
    private IWXAPI api;
    protected long exitTime = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        Log.d(TAG, "onStart: ");
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("MainFragment");
        barController = this;
        userId = MemberCache.getMemUserId();
        if (userId != 0) {
            tvMemberGold.setVisibility(View.VISIBLE);
            tvMemberName.setText(MemberCache.getMemNickName());
            memberPhoto.setImageURI(MemberCache.getMemHeadUrl());
            getUserInfo();
        } else {
            tvMemberGold.setVisibility(View.GONE);
        }
        if (today_gold > 0)
            tvMemberGold.setText(String.format(getString(R.string.member_gold), today_gold + ""));
        if (MultiWindowManager.mulWindowBrowserList.get(index) != null) {
            updateBottomBar(true);
        } else updateBottomBar(false);
        Log.d(TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("MainFragment");
        Log.d(TAG, "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        Log.d(TAG, "onStop: ");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView: ");
        if (contentPager != null)
            contentPager.removeAllViews();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
        MAIN_FRAGMENT = null;
    }

    @Override
    protected void initViews() {
        Log.d(TAG, "initViews: ");
        MAIN_FRAGMENT = this;
        if (AppUtility.isWifi(PPaoApplication.getApplication())) {
            userBiz.fetchIconList(new DataAccessListener<IconResBean>() {
                @Override
                public void requestSuccess(IconResBean iconResBean) {
                    BrowserCache browserCache = new BrowserCache(mContext);
//                    if (!iconResBean.version.equals(browserCache.getIconVersion())) {
                    if (iconResBean.data != null && iconResBean.data.size() > 0) {
                        initRemoteTopBarDatas(iconResBean.data);
                        initTopBarView(false);
                        //启动service保存文件
//                        mContext.bindService(new Intent(mContext, MainService.class));
                    } else {
                        initLocalTopBarDatas();
                        initTopBarView(true);
                    }
                }

                @Override
                public void requestFail(String error) {
                    initLocalTopBarDatas();
                    initTopBarView(true);
                }
            });
        } else {
            initLocalTopBarDatas();
            initTopBarView(true);
        }

        initContentViewData();
        initContentView();
    }

    private void initRemoteTopBarDatas(List<IconResBean.IconBean> list) {
        mRemoteDatas = new ArrayList<RemoteBarItemModel>();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            //动态获取资源ID，第一个参数是资源名，第二个参数是资源类型例如drawable，string等，第三个参数包名
            String name = list.get(i).getIcon_name();
            String icon_url = list.get(i).getImage_url();
            String web_url = list.get(i).getUrl();
            mRemoteDatas.add(new RemoteBarItemModel(name, icon_url, web_url));
        }
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_scrolling;
    }

    @Override
    protected void updateViews(boolean b) {

    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Log.d(TAG, "onViewCreated");
        if (hidden) {
            if (MultiWindowManager.mulWindowBrowserList.get(index) != null) {
                updateBottomBar(true);
            } else updateBottomBar(false);
            barController = this;
        }
    }

    private void initContentView() {
        contentTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (appbar_expand) {
                    setAppBarExpand(false);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        contentTab.addTab(contentTab.newTab().setText(titleList.get(0)));
        contentTab.addTab(contentTab.newTab().setText(titleList.get(1)));
        contentTab.addTab(contentTab.newTab().setText(titleList.get(2)));
        contentTab.addTab(contentTab.newTab().setText(titleList.get(3)));
        contentTab.addTab(contentTab.newTab().setText(titleList.get(4)));
        if (adapter == null)
            adapter = new WebPagerAdapter(getChildFragmentManager(), titleList, mWebList);
        contentPager.setAdapter(adapter);
        contentPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("type", "tab");
                map.put("quantity", "" + position);
                MobclickAgent.onEvent(MainFragment.this.getContext(), "tab_click", map);
                barController = (BarController) mWebList.get(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        contentPager.setOffscreenPageLimit(titleList.size());
        contentTab.setupWithViewPager(contentPager, true);
    }

    private void initContentViewData() {
        if (mWebList.size() == 0) {
            mWebList.add(WebPagerFragment.newInstance("http://m.aixs100.com/?f=paopao", MemberCache.TAG_NOVEL));
            mWebList.add(WebPagerFragment.newInstance("http://www.manben.com/", MemberCache.TAG_CARTOON));
            mWebList.add(WebPagerFragment.newInstance("http://open.toutiao.com/?#channel=video", MemberCache.TAG_VIDEO));
            mWebList.add(WebPagerFragment.newInstance("http://m.yxdown.com/rank/", MemberCache.TAG_DOWNLOAD));
            mWebList.add(WebPagerFragment.newInstance("http://cdn.0i-i0.com/js/content.html?code=news", MemberCache.TAG_NEWS));
        }
    }

    private void initTopBarView(boolean fromLocal) {
        memberPhoto.setOnClickListener(this);
        btnSignIn.setOnClickListener(this);
        barSearch.setOnPreFocusListener(new SearchView.PreFocusListener() {
            @Override
            public void onPreFocus() {
                barSearch.setFocusable(false);
                startActivityForResult(new Intent(mContext, SearchActivity.class), REQUEST_SEARCH_CODE);
                getActivity().overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
            }
        });
        appBar.addOnOffsetChangedListener(this);
        inflater = LayoutInflater.from(this.getContext());
        //总的页数=总数/每页数量，并取整
        if (fromLocal) {
            pageCount = (int) Math.ceil(mDatas.size() * 1.0 / pageSize);
            mPagerList = new ArrayList<View>();
            for (int i = 0; i < pageCount; i++) {
                //每个页面都是inflate出一个新实例
                GridView gridView = (GridView) inflater.inflate(R.layout.gridview, barViewpager, false);
                gridView.setAdapter(new WebLocalGridViewAdapter(this.getContext(), mDatas, i, pageSize));
                mPagerList.add(gridView);

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        int pos = position + curIndex * pageSize;
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("type", "website");
                        map.put("quantity", "" + pos);
                        MobclickAgent.onEvent(MainFragment.this.getContext(), "website_click", map);
                        if (pos == 1) {
                            setAppBarExpand(false);
                            if (contentPager != null) {
                                contentPager.setCurrentItem(0);
                            }
                        } else {
                            if (barController != null)
                                barController.stopLoading();
                            MultiWindowManager.mulWindowBrowserList.set(index, null);
                            ((BrowserBaseActivity) mContext).setBrowserFragment(web_url_list[pos], index);
                        }
                    }
                });
            }
            //设置适配器
            barViewpager.setAdapter(new WebsiteViewPagerAdapter(mPagerList));
            //设置圆点
        } else {
            pageCount = (int) Math.ceil(mRemoteDatas.size() * 1.0 / pageSize);
            mPagerList = new ArrayList<View>();
            for (int i = 0; i < pageCount; i++) {
                //每个页面都是inflate出一个新实例
                GridView gridView = (GridView) inflater.inflate(R.layout.gridview, barViewpager, false);
                gridView.setAdapter(new WebRemoteGridViewAdapter(this.getContext(), mRemoteDatas, i, pageSize));
                mPagerList.add(gridView);
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        int pos = position + curIndex * pageSize;
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("type", "website");
                        map.put("quantity", "" + pos);
                        MobclickAgent.onEvent(MainFragment.this.getContext(), "website_click", map);
                        if (pos == 1) {
                            setAppBarExpand(false);
                            if (contentPager != null) {
                                contentPager.setCurrentItem(0);
                            }
                        } else {
                            if (barController != null)
                                barController.stopLoading();
                            MultiWindowManager.mulWindowBrowserList.set(index, null);
                            ((BrowserBaseActivity) mContext).setBrowserFragment(mRemoteDatas.get(position).web_url, index);
                        }
                    }
                });
            }
            //设置适配器
            barViewpager.setAdapter(new WebsiteViewPagerAdapter(mPagerList));
        }
        setOvalLayout();

    }


    /**
     * 设置圆点
     */
    public void setOvalLayout() {
        for (int i = 0; i < pageCount; i++) {
            barDot.addView(inflater.inflate(R.layout.dot, null));
        }
        // 默认显示第一页
        barDot.getChildAt(0).findViewById(R.id.v_dot)
                .setBackgroundResource(R.drawable.dot_selected);
        barViewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageSelected(int position) {
                // 取消圆点选中
                barDot.getChildAt(curIndex)
                        .findViewById(R.id.v_dot)
                        .setBackgroundResource(R.drawable.dot_normal);
                // 圆点选中
                barDot.getChildAt(position)
                        .findViewById(R.id.v_dot)
                        .setBackgroundResource(R.drawable.dot_selected);
                curIndex = position;
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }

    /**
     * 初始化bar数据源
     */
    private void initLocalTopBarDatas() {
        mDatas = new ArrayList<BarItemModel>();
        for (int i = 0; i < web_name_list.length; i++) {
            //动态获取资源ID，第一个参数是资源名，第二个参数是资源类型例如drawable，string等，第三个参数包名
            int imageId;
            if (i < 9) {
                imageId = getResources().getIdentifier("web_site_0" + (i + 1), "drawable", this.getContext().getPackageName());
            } else {
                imageId = getResources().getIdentifier("web_site_" + (i + 1), "drawable", this.getContext().getPackageName());
            }
            mDatas.add(new BarItemModel(web_name_list[i], imageId, web_url_list[i]));
        }
    }

    private void getUserInfo() {
        if (userBiz.getDefault() != null) {
            memberBean = userBiz.getDefault();
            setData2View();
        } else
            userBiz.fetchUserInfo(new DataAccessListener<MemberBean>() {
                @Override
                public void requestSuccess(MemberBean ob) {
                    if (ob != null) {
                        memberBean = ob;
                        Log.d(TAG, "requestSuccess: login success");
//                        ToastUtils.show(mContext, "login success");
                        tvMemberGold.setVisibility(View.VISIBLE);
                        setData2View();
                        getTaskState();
                    } else tvMemberGold.setVisibility(View.GONE);

                }

                @Override
                public void requestFail(String error) {
                    ToastUtils.show(mContext, error);
                    tvMemberGold.setVisibility(View.GONE);
                }
            });

        userBiz.getIncome(new DataAccessListener<List<IncomeResbean>>() {
            @Override
            public void requestSuccess(List<IncomeResbean> list) {
                today_gold = 0;
                if (list != null && list.size() > 0) {
                    for (IncomeResbean income : list
                            ) {
                        if (!(income.addGoldCoin < 0 && income.addCash > 0)) {
                            if (DateTimeUtils.getDisNow(income.eventTime * 1000) == 0) {
                                today_gold = today_gold + income.addGoldCoin;
                            }
                        }
                    }
                    if (today_gold > 0)
                        tvMemberGold.setText(String.format(getString(R.string.member_gold), today_gold + ""));
                    else
                        tvMemberGold.setText(String.format(getString(R.string.member_gold), "0"));
                }
            }

            @Override
            public void requestFail(String error) {

            }
        });
    }

    private void getTaskState() {
        userBiz.getTaskState(new DataAccessListener<List<TaskResbean>>() {
            @Override
            public void requestSuccess(List<TaskResbean> ob) {
                for (TaskResbean taskstate : ob
                        ) {
                    if (taskstate.taskId == 3) {
                        if (taskstate.taskStatus == 0)
                            updateSignState(false);
                        else updateSignState(true);
                    }
                }
            }

            @Override
            public void requestFail(String error) {

            }
        }, true);
    }

    private void setData2View() {
        if (memberBean != null) {
            memberPhoto.setImageURI(memberBean.headUrl);
            tvMemberName.setText(memberBean.nickName);
        }
    }

    private void updateSignState(boolean signed) {
        if (signed) {
            btnSignIn.setClickable(false);
            btnSignIn.setText("今日已签到");
            btnSignIn.setBackgroundResource(R.color.secondary_color);
        } else {
            btnSignIn.setClickable(true);
            btnSignIn.setText("签到领金币");
            btnSignIn.setBackgroundResource(R.color.global_text_hint_color2);
        }
    }

    public static MainFragment newInstance() {
        if (MAIN_FRAGMENT == null) {
            return new MainFragment();
        } else
            return MAIN_FRAGMENT;
    }

    @Override
    public void forward() {
        Log.d(TAG, "forward: main");
        WebViewFragment webViewFragment = MultiWindowManager.mulWindowBrowserList.get(index);
        if (webViewFragment == null) {
            return;
        } else {
            ((BrowserBaseActivity) mContext).setBrowserFragment(webViewFragment.mUrl, index);
        }
    }

    //这种情况是当appbar显示时，前进后退作为大的页面去跳转
    @Override
    public void back() {
        Log.d(TAG, "back: main");
        if (frameLayout != null && frameLayout.getParent() != null) {
            ViewGroup vg = (ViewGroup) frameLayout.getParent();
            vg.removeView(frameLayout);
        } else {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                ToastUtils.show(mContext, "再按一次退出程序");
                exitTime = System.currentTimeMillis();
            } else {
                AppUtility.exitApp(getActivity());
            }
        }
    }

    @Override
    public void backHome() {
        /**
         *出现了白屏的情况 为什么呢 因为我把栈里面所有的fragment的pop掉了 包括默认入栈的pager里面的fragment
         getFragmentManager().popBackStack();
         ((BaseActivity) mContext).setMainFragment(index);
         */
        setAppBarExpand(true);
        return;
    }

    @Override
    public void stopLoading() {

    }

    @Override
    public void refresh() {

    }

    @Override
    public void showSearchBar() {

    }

    @Override
    public void hideSearchBar() {

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.member_photo:
                userId = MemberCache.getMemUserId();
                if (userId != 0) {
                    getActivity().startActivity(new Intent(mContext, MemberSystemActivity.class));
                    getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                } else {
                    wxLogin();
                }
                break;
            case R.id.bt_sign_in:
                //签到操作
                userBiz.signIn(new DataAccessListener<SignResBean>() {
                    @Override
                    public void requestSuccess(SignResBean ob) {
                        updateSignState(true);
                        today_gold = today_gold + ob.addGold;
                        tvMemberGold.setText(String.format(getString(R.string.member_gold), today_gold + ""));
                    }

                    @Override
                    public void requestFail(String error) {
                        Log.d(TAG, error);
                        if (error.equals("300")) {
                            updateSignState(true);
                            ToastUtils.show(mContext, "今日已签到");
                        } else {
                            ToastUtils.show(mContext, error);
                        }
                    }
                });
                break;
        }
    }

    private void getSignModel() {
        if (signModel == null) {
            QueryBuilder<SignModel> query = signDao.queryBuilder().where(SignModelDao.Properties.UserId.eq(userId));
            List<SignModel> list = query.list();
            if (list.size() > 0)
                signModel = list.get(0);
            else {
                signModel = new SignModel();
                if (userId != 0)
                    signModel.setUserId(userId);
            }
        }
    }

    public enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    private State mCurrentState = State.IDLE;

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (verticalOffset == 0) {
            if (mCurrentState != State.EXPANDED) {
                setAppBarExpand(true);
            }
            mCurrentState = State.EXPANDED;
        } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
            if (mCurrentState != State.COLLAPSED) {
                setAppBarExpand(false);
                appBar.setVisibility(View.GONE);
            }
            mCurrentState = State.COLLAPSED;
        } else {
            mCurrentState = State.IDLE;
        }
    }

    public void setAppBarExpand(boolean expand) {
        if (expand) {
            appBar.setVisibility(View.VISIBLE);
            appBar.setExpanded(true);
            barController = this;
            appbar_expand = true;
        } else {
            appBar.setExpanded(false);
            barController = (BarController) mWebList.get(curIndex);
            appbar_expand = false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: " + requestCode);
        if (resultCode == RESULT_SEARCH_CODE) {
            String search_out = data.getExtras().getString(SEARCH_KEY);
            search_out = UrlUtils.smartUrlFilter(search_out, true, Constants.BAIDU_SEARCH + search_out);
            if (barController != null)
                barController.stopLoading();
            ((BrowserBaseActivity) mContext).setBrowserFragment(search_out, index);

        }
    }

    @Subscribe
    public void onEvent(AppCommonEvent event) {
        if (event.type.equals(AppCommonEvent.TASK_NOVEL)) {
            setAppBarExpand(false);
            if (contentPager != null) {
                contentPager.setCurrentItem(0);
            }
        } else if (event.type.equals(AppCommonEvent.TASK_CARTOON)) {
            setAppBarExpand(false);
            if (contentPager != null) {
                contentPager.setCurrentItem(1);
            }
        } else if (event.type.equals(AppCommonEvent.TASK_NEWS)) {
            setAppBarExpand(false);
            if (contentPager != null) {
                contentPager.setCurrentItem(4);
            }
        } else if (event.type.equals(AppCommonEvent.TASK_VIDEO)) {
            setAppBarExpand(false);
            if (contentPager != null) {
                contentPager.setCurrentItem(2);
            }
        } else if (event.type.equals(AppCommonEvent.TASK_VIDEO)) {
            setAppBarExpand(false);
            if (contentPager != null) {
                contentPager.setCurrentItem(2);
            }
        } else if (event.type.equals(AppCommonEvent.RIGISTER)) {
            progressView.dismiss();
            progressView.hideProgress();
            getUserInfo();
            getTaskState();
            if (MemberCache.getMemUserId() != 0) {
                mContext.startActivity(new Intent(mContext, MemberSystemActivity.class));
            } else {
                ToastUtils.show(mContext, "登陆失败");
            }
        } else if (event.type.equals(AppCommonEvent.LOGOUT)) {
            userId = 0;
            memberBean = null;
            updateSignState(false);
            tvMemberName.setText("");
            tvMemberGold.setText("");
            memberPhoto.setImageURI("");
            today_gold = 0;
        } else if (event.type.equals(AppCommonEvent.TYPE_READ)) {
            String page_type = event.getStringExtra(AppCommonEvent.EVENT_KEY2, null);
            int add_gold = event.getIntExtra(AppCommonEvent.EVENT_KEY, 0);
//            ToastUtils.show(mContext, page_type + "任务完成");
            today_gold = today_gold + add_gold;
            tvMemberGold.setText(String.format(getString(R.string.member_gold), today_gold + ""));

        }
    }

    private void initDao() {
        DaoSession daoSession = PPaoApplication.getDaoSession();
        signDao = daoSession.getSignModelDao();
    }

    private void wxLogin() {
        progressView = new ProgressView(mContext);
        api = WXAPIFactory.createWXAPI(mContext, AppConst.WX_APP_ID, true);
        progressView.showProgress();
        // send oauth request
        final SendAuth.Req req = new SendAuth.Req();
        req.scope = "snsapi_userinfo";
        req.state = "wechat_ppao";
        api.sendReq(req);
    }


}
