package com.mogo.ppaobrowser.browser.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.widget.Button;
import android.widget.ImageView;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.adapter.HBViewpagerAdapter;
import com.mogo.ppaobrowser.browser.base.BrowserBaseActivity;
import com.mogo.ppaobrowser.browser.fragment.BookMarkFragment;
import com.mogo.ppaobrowser.browser.fragment.HistoryFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by doyer on 2017/5/22.
 */
public class HistoryMarkActivity extends BrowserBaseActivity {

    @BindView(R.id.history_mark_pager)
    ViewPager historyMarkPager;
    @BindView(R.id.btn_clear)
    Button btnClear;
    @BindView(R.id.history_mark_tab)
    TabLayout historyMarkTab;
    @BindView(R.id.icon_back)
    ImageView iconBack;
    private HBViewpagerAdapter hbViewpagerAdapter;
    ImageView imageView;

    private List<Fragment> mFragments = new ArrayList<>();
    private List<String> titleList = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        historyMarkTab.setupWithViewPager(historyMarkPager);
        initData();
        imageView = (ImageView) findViewById(R.id.icon_back);
        hbViewpagerAdapter = new HBViewpagerAdapter(getSupportFragmentManager(), mFragments, titleList);
        historyMarkPager.setAdapter(hbViewpagerAdapter);
    }

    @Override
    protected void updateViews(boolean b) {

    }

    @Override
    protected void initViews() {

    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_history_mark;
    }

    private void initData() {
        mFragments.add(new BookMarkFragment());
        mFragments.add(new HistoryFragment());
        titleList.add(getString(R.string.bookmark));
        titleList.add(getString(R.string.history));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @OnClick(R.id.icon_back)
    public void onViewClicked() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

}
