package com.mogo.ppaobrowser.browser.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.fragment.WebViewFragment;
import com.mogo.ppaobrowser.browser.interface_package.SearchListener;
import com.mogo.ppaobrowser.widget.Search_View;
import com.mogo.ppaobrowser.browser.fragment.MainFragment;
import com.umeng.analytics.MobclickAgent;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends AppCompatActivity {

    @BindView(com.mogo.ppaobrowser.R.id.search_layout)
    Search_View searchLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        searchLayout.setSearchListener(new SearchListener() {
            @Override
            public void search_out(String content) {
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putString(MainFragment.SEARCH_KEY, content);
                intent.putExtras(bundle);
                setResult(MainFragment.RESULT_SEARCH_CODE, intent);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchLayout.getWindowToken(), 0);
                finish();
                overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        searchLayout.removeSearchListener();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
