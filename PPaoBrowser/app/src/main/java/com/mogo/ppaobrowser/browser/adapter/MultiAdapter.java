package com.mogo.ppaobrowser.browser.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.controller.MultiWindowManager;
import com.mogo.ppaobrowser.utils.LogUtility;

import java.util.ArrayList;
import java.util.List;

import static com.mogo.ppaobrowser.browser.base.BaseFragment.index;


/**
 * Created by doyer on 2017/6/27.
 */

public class MultiAdapter extends BaseAdapter {
    public static final String TAG = "MultiAdapter";
    Context context;
    List<MultiWindowManager> multiWindowManagers = new ArrayList<>();

    public MultiAdapter(Context context, List<MultiWindowManager> multiWindowManagerList) {
        this.context = context;
        this.multiWindowManagers = multiWindowManagerList;
    }

    @Override
    public int getCount() {
        LogUtility.debug(TAG, "getCount: " + multiWindowManagers.size());
        return multiWindowManagers.size();
    }

    @Override
    public Object getItem(int position) {
        return multiWindowManagers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {

            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.muti_window_item, null);
            holder.bgimg = (ImageView) convertView.findViewById(R.id.background);
            holder.closeimg = (ImageView) convertView.findViewById(R.id.close);
            holder.contimg = (ImageView) convertView.findViewById(R.id.preview);
            holder.title = (TextView) convertView.findViewById(R.id.shortcutTitle);
            holder.title.setSingleLine(true);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        MultiWindowManager data = multiWindowManagers.get(position);
        holder.bgimg.setBackgroundResource(R.drawable.tab_thumbnail_selected);
        holder.title.setText(data.getTitle());
        holder.contimg.setImageDrawable(data.getDrawable());
        holder.contimg.setScaleType(ImageView.ScaleType.FIT_XY);
        holder.closeimg.setBackgroundResource(R.drawable.close_window);
        holder.closeimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (multiWindowManagers.size() < 2) {
                    return;
                }
                multiWindowManagers.remove(position);
                MultiWindowManager.releaseWindowData(position);
                notifyDataSetChanged();
                if (position == index) {
                    //如果是当前页面的话 点击删除页面的操作需要对应的使得当前页面也删除
                }
            }
        });
        return convertView;
    }

    class ViewHolder {
        public ImageView bgimg;
        public ImageView contimg;
        public ImageView closeimg;
        public TextView title;

    }
}
