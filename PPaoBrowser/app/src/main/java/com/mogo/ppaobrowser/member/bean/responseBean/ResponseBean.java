package com.mogo.ppaobrowser.member.bean.responseBean;

import org.json.JSONObject;

/**
 * Created by doyer on 2017/7/31.
 */

public class ResponseBean<T> {

    public int code;
    public T data;
    public String message;
}
