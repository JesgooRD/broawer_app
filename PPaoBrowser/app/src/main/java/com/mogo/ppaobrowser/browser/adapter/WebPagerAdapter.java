package com.mogo.ppaobrowser.browser.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by doyer on 2017/5/18.
 * 小说、漫画等所在viewpager适配器
 */
public class WebPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<String> titleList;
    private ArrayList<Fragment> viewList;

    public WebPagerAdapter(FragmentManager fragmentManager, ArrayList<String> titleList, ArrayList<Fragment> viewList) {
        super(fragmentManager);
        this.titleList = titleList;
        this.viewList = viewList;
    }


    @Override
    public int getCount() {
        return viewList.size();
    }

    @Override
    public Fragment getItem(int position) {
        return viewList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleList.get(position);
    }

}
