package com.mogo.ppaobrowser.utils;

import android.util.Log;


import com.mogo.ppaobrowser.browser.interface_package.DataAccessListener;
import com.mogo.ppaobrowser.browser.interface_package.RequestListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by doyer on 2017/6/9.
 */

public class HttpUtil {
    public static void HttpGet(final String urlString, final DataAccessListener requestListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection connection = null;
                try {
                    URL url = new URL(urlString);
                    connection = (HttpURLConnection) url.openConnection();
                    // 设置请求方法，默认是GET
                    connection.setRequestMethod("GET");
                    connection.setConnectTimeout(3000);
                    connection.setRequestProperty("Accept-Encoding", "identity");
                    connection.connect();
                    int status = connection.getResponseCode();
                    if (status == 200) {
                        StringBuilder res = new StringBuilder();
                        InputStream is = connection.getInputStream();
                        BufferedReader bufr = new BufferedReader(new InputStreamReader(is));
                        String inputString;
                        while ((inputString = bufr.readLine()) != null) {
                            res.append(inputString);

                        }
                        if (bufr != null) {
                            bufr.close();
                        }
                        requestListener.requestSuccess(res.toString());
                        is.close();
                    } else requestListener.requestFail("失败，状态码为" + status);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    requestListener.requestFail("失败，错误信息" + e.getMessage());
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                }
            }
        }).start();

    }

    public static void HttpGet(final String urlString, final int request_code, final RequestListener requestListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection connection = null;
                try {
                    URL url = new URL(urlString);
                    connection = (HttpURLConnection) url.openConnection();
                    // 设置请求方法，默认是GET
                    connection.setRequestMethod("GET");
                    connection.setConnectTimeout(3000);
                    connection.connect();
                    int status = connection.getResponseCode();
                    if (status == 200) {
                        StringBuilder res = new StringBuilder();
                        InputStream is = connection.getInputStream();
                        BufferedReader bufr = new BufferedReader(new InputStreamReader(is));
                        String inputString;
                        while ((inputString = bufr.readLine()) != null) {
                            res.append(inputString);

                        }
                        if (bufr != null) {
                            bufr.close();
                        }
                        if (connection.getContentLength() < 0) {// special case
                            res = new StringBuilder("{error}");
                        }
                        requestListener.requestSuccess(request_code, res.toString());
                        is.close();
                    } else requestListener.requestFail("失败，状态码为" + status);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    requestListener.requestFail("失败，错误信息" + e.getMessage());
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                }
            }
        }).start();

    }


    public static String getReallyFileName(String url) {
        String filename = "";
        URL myURL;
        HttpURLConnection conn = null;
        if (url == null || url.length() < 1) {
            return null;
        }

        try {
            myURL = new URL(url);
            conn = (HttpURLConnection) myURL.openConnection();
            conn.connect();
            conn.getResponseCode();
            URL absUrl = conn.getURL();// 获得真实Url
            Log.e("H3c", "x:" + absUrl);
            // 打印输出服务器Header信息
            // Map<String, List<String>> map = conn.getHeaderFields();
            // for (String str : map.keySet()) {
            // if (str != null) {
            // Log.e("H3c", str + map.get(str));
            // }
            // }
            filename = conn.getHeaderField("Content-Disposition");// 通过Content-Disposition获取文件名，这点跟服务器有关，需要灵活变通
            if (filename == null || filename.length() < 1) {
                filename = absUrl.getFile();
                int index = filename.lastIndexOf("&");
                filename = filename.substring(index + 1);
                index = filename.lastIndexOf("?");
                filename = filename.substring(index + 1);
                index = filename.lastIndexOf("/");
                filename = filename.substring(index + 1);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
                conn = null;
            }
        }

        return filename;
    }

}
