package com.mogo.ppaobrowser.browser.bean;

/**
 * Created by doyer on 2017/5/17.
 */
public class BarItemModel {
    public String name;
    public int iconRes;
    public String url;

    public BarItemModel(String name, int iconRes, String url) {
        this.name = name;
        this.iconRes = iconRes;
        this.url = url;
    }
}
