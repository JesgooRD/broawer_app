package com.mogo.ppaobrowser.member.activity;

import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.activity.PPaoBaseActivity;
import com.mogo.ppaobrowser.browser.interface_package.DataAccessListener;
import com.mogo.ppaobrowser.member.api.ApiService;
import com.mogo.ppaobrowser.member.bean.CommonBean;
import com.mogo.ppaobrowser.member.bean.responseBean.MemberBean;
import com.mogo.ppaobrowser.member.biz.UserBiz;
import com.mogo.ppaobrowser.member.utils.RetrofitUtils;
import com.mogo.ppaobrowser.preference.MemberCache;
import com.mogo.ppaobrowser.utils.ToastUtils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by xiaoyu on 2017/7/20.
 */

public class AlipayCashActivity extends PPaoBaseActivity {
    private static final String TAG = "AlipayCashActivity";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.ll_cash)
    LinearLayout ll_cash;
    @BindView(R.id.et_account)
    EditText et_account;
    @BindView(R.id.et_name)
    EditText et_name;
    @BindView(R.id.tv_money)
    TextView tv_money;

    String account;
    String name;
    String money;
    PopupWindow popupWindow;
    UserBiz userBiz = new UserBiz();

    @Override
    protected void updateViews(boolean b) {

    }

    @Override
    protected void initViews() {
        setSupportActionBar(toolbar);
        toolbarTitle.setText(getString(R.string.money_conversion));
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.act_alipay_cash;
    }

    @OnClick({R.id.rl_money, R.id.btn_commit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_money:
                PopupWindowOut(ll_cash);
                break;
            case R.id.btn_commit:
                account = et_account.getText().toString().trim();
                name = et_name.getText().toString().trim();
                money = tv_money.getText().toString().trim();
                if (TextUtils.isEmpty(account)) {
                    ToastUtils.show(this, "请输入支付宝账号！");
                    et_account.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(name)) {
                    ToastUtils.show(this, "请输入姓名！");
                    et_name.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(money)) {
                    ToastUtils.show(this, "请选择兑换金额！");
                    return;
                }

                //提交
                int userId = MemberCache.getMemUserId();
                if (userId == 0) {
                    ToastUtils.show(this, "您尚未登陆");
                    return;
                }

                //再次确认金额是否允许兑换
                int cash = Integer.parseInt(money);
                if (!checkMoney(cash)) {
                    return;
                }
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("UserId", userId);
                    jsonObject.put("Alipay", account);
                    jsonObject.put("Name", name);
                    jsonObject.put("Amount", Integer.parseInt(money));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=UTF-8"), jsonObject.toString());

                userBiz.transferAliPay(new DataAccessListener<String>() {
                    @Override
                    public void requestSuccess(String ob) {
                        ToastUtils.show(AlipayCashActivity.this, ob);
                    }

                    @Override
                    public void requestFail(String error) {
                        ToastUtils.show(AlipayCashActivity.this, error);
                    }
                }, body, Integer.parseInt(money));


                break;
        }
    }

    //check 金币是否超过申请金额
    private boolean checkMoney(int cash) {
        MemberBean memberBean = userBiz.getDefault();
        if (cash > memberBean.cash) {
            ToastUtils.show(this, "你的数额无法兑换此面值人名币");
            return false;
        }
        return true;
    }

    protected void PopupWindowOut(View view) {
        if (view != null) {
            closeWindowSoftInput(view);
            final View contentView = LayoutInflater.from(AlipayCashActivity.this).inflate(R.layout.cash_pop_item, null);
            popupWindow = new PopupWindow(contentView);
            contentView.findViewById(R.id.one).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (checkMoney(30)) {
                        tv_money.setText("30");
                        popupWindow.dismiss();
                    }
                }
            });

            contentView.findViewById(R.id.two).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (checkMoney(50)) {
                        tv_money.setText("50");
                        popupWindow.dismiss();
                    }
                }
            });
            contentView.findViewById(R.id.three).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (checkMoney(100)) {
                        tv_money.setText("100");
                        popupWindow.dismiss();
                    }
                }
            });
            popupWindow.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
            popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
            backgroundAlpha(0.6f);
            popupWindow.setFocusable(true);
            popupWindow.setOutsideTouchable(true);
            popupWindow.setAnimationStyle(R.style.PopupAnimation);
            popupWindow.setBackgroundDrawable(new BitmapDrawable());
            popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    backgroundAlpha(1.0f);
                }
            });
            popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);

        }
    }
}
