package com.mogo.ppaobrowser.browser.fragment;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.mogo.ppaobrowser.PPaoApplication;
import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.base.BaseFragment;
import com.mogo.ppaobrowser.browser.bean.DownloadModel;
import com.mogo.ppaobrowser.gen.DownloadModelDao;
import com.mogo.ppaobrowser.preference.BrowserCache;
import com.mogo.ppaobrowser.utils.AppUtility;
import com.mogo.ppaobrowser.utils.ToastUtils;

import java.io.File;

import static com.mogo.ppaobrowser.browser.base.BrowserBaseActivity.PERMISSION_WRITE_EXTERNAL_STORAGE;

/**
 * Created by doyer on 2017/5/25.
 */
public abstract class WebBaseFragment extends BaseFragment {
    private static final String TAG = "WebBaseFragment";
    public WebView mWebView;
    public BrowserCache preferenceManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferenceManager = new BrowserCache(getContext());

        if (mWebView != null) {
            //这个还真的有用 使得小说页面不会多出很多空间的情况
            WindowManager wm = (WindowManager) getContext()
                    .getSystemService(Context.WINDOW_SERVICE);
            mWebView.setMinimumHeight(wm.getDefaultDisplay().getHeight());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mWebView != null) {
            Log.d(TAG, "onPause: ");
            mWebView.onPause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mWebView != null) {
            mWebView.stopLoading();
            mWebView.onPause();
            mWebView.clearHistory();
            mWebView.setVisibility(View.GONE);
            mWebView.removeAllViews();
            mWebView.destroyDrawingCache();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                //this is causing the segfault occasionally below 4.2
                mWebView.destroy();
            }
            mWebView = null;
        }
    }

    /**
     * 设置添加屏幕的背景透明度
     *
     * @param bgAlpha
     */
    public void backgroundAlpha(Activity activity, float bgAlpha) {
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        activity.getWindow().setAttributes(lp);
    }

    protected void popUpDownload(View view, final String s, final String s1, final String s2, final String s3) {
        final String file_name;
        file_name = URLUtil.guessFileName(s, s2, s3);
        //自定义PopupWindow的布局
        final View contentView = LayoutInflater.from(mContext).inflate(R.layout.pop_bottom_download, null);
        //初始化PopupWindow,并为其设置布局文件
        final PopupWindow popupWindow = new PopupWindow(contentView);
        ImageView imageView = (ImageView) contentView.findViewById(R.id.download_file_icon);
        TextView textView = (TextView) contentView.findViewById(R.id.download_file_name);
        if (!TextUtils.isEmpty(file_name)) {
            Log.d(TAG, "fileName: " + file_name);
            textView.setText(file_name);
        }
        contentView.findViewById(R.id.download_ensure).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                ToastUtils.show(getContext(), "正在下载中...");
                final DownloadModel downloadModel = new DownloadModel();
                DownloadModelDao downloadModelDao = PPaoApplication.getDaoSession().getDownloadModelDao();
                //file_name = URLDecoder.decode(HttpUtil.getReallyFileName(s), "utf-8");
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(s));
                //设置在什么网络情况下进行下载
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
                //设置通知栏标题
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_ONLY_COMPLETION);
                request.setMimeType(s3);
                //获取是否有读写权限
                Uri uri = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/PPao_download", file_name));
                request.setDestinationUri(uri);
                request.setAllowedOverRoaming(false);
                //设置文件存放目录
                DownloadManager downManager = (DownloadManager) getContext().getSystemService(Context.DOWNLOAD_SERVICE);
                long download_id = downManager.enqueue(request);
                downloadModel.setId(download_id);
                downloadModel.setUrl(s);
                downloadModel.setFile_name(file_name);
                downloadModel.setPath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/PPao_download/" + file_name);
                downloadModel.setStatus(0);
                downloadModelDao.insertOrReplace(downloadModel);
                Log.d(TAG, "run: " + file_name);
            }
        });
        //设置PopupWindow的宽和高,必须设置,否则不显示内容(也可用PopupWindow的构造方法设置宽高)
        popupWindow.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));   //为PopupWindow设置透明背景，相应返回键
        popupWindow.setOutsideTouchable(true);
        //设置PopupWindow进入和退出动画
        popupWindow.setAnimationStyle(R.style.PopupAnimation);
        //设置PopupWindow显示的位置
        backgroundAlpha((Activity) mContext, 0.6f);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                backgroundAlpha((Activity) mContext, 1f);
            }
        });
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
    }


}
