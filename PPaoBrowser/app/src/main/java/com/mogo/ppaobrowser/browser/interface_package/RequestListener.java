package com.mogo.ppaobrowser.browser.interface_package;

/**
 * Created by doyer on 2017/6/5.
 */

public interface RequestListener<T> {

    void requestSuccess(int request_code, T ob);

    void requestFail(String error);
}
