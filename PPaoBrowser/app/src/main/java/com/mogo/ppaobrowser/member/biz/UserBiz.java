package com.mogo.ppaobrowser.member.biz;

import android.util.Log;

import com.mogo.ppaobrowser.browser.interface_package.DataAccessListener;
import com.mogo.ppaobrowser.member.api.ApiService;
import com.mogo.ppaobrowser.member.bean.PayHistoryBean;
import com.mogo.ppaobrowser.member.bean.TransferResNote;
import com.mogo.ppaobrowser.member.bean.responseBean.ComGoldResBean;
import com.mogo.ppaobrowser.member.bean.responseBean.IconResBean;
import com.mogo.ppaobrowser.member.bean.responseBean.IncomeResbean;
import com.mogo.ppaobrowser.member.bean.responseBean.LoginResBean;
import com.mogo.ppaobrowser.member.bean.responseBean.MemberBean;
import com.mogo.ppaobrowser.member.bean.responseBean.RegisterBean;
import com.mogo.ppaobrowser.member.bean.responseBean.ResponseBean;
import com.mogo.ppaobrowser.member.bean.responseBean.SignResBean;
import com.mogo.ppaobrowser.member.bean.responseBean.TaskResbean;
import com.mogo.ppaobrowser.member.constant.HttpConstant;
import com.mogo.ppaobrowser.member.event.AppCommonEvent;
import com.mogo.ppaobrowser.member.utils.RetrofitUtils;
import com.mogo.ppaobrowser.preference.MemberCache;
import com.mogo.ppaobrowser.utils.DateTimeUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by doyer on 2017/7/17.
 */

public class UserBiz {
    public static final String TAG = "UserBiz";
    private static MemberBean DEFAULT;
    private static int TODAY_GOLD;

    private static List<TaskResbean> TaskStates;

    public void setDefault(MemberBean dataBean) {
        DEFAULT = dataBean;
    }

    public MemberBean getDefault() {
        return DEFAULT;
    }

    public List<TaskResbean> getDefaultTasks() {
        return TaskStates;
    }

    public void setDefaultTasks(List<TaskResbean> list) {
        TaskStates = list;
    }

    //LOGIN
    public void fetchUserInfo(final DataAccessListener<MemberBean> requestListener) {
        if (DEFAULT != null) {
            requestListener.requestSuccess(getDefault());
        } else {
            String type = HttpConstant.TYPE_LOGIN;
            int userId = MemberCache.getMemUserId();
            if (userId != 0)
                RetrofitUtils.createService(ApiService.class).login(type, userId).enqueue(new Callback<LoginResBean<MemberBean>>() {
                    @Override
                    public void onResponse(Call<LoginResBean<MemberBean>> call, Response<LoginResBean<MemberBean>> response) {
                        if (response.body() != null) {
                            if (response.body().code == 200) {
                                Log.d(TAG, "onResponse: login success");
                                DEFAULT = response.body().data;
                                setLogin(response.body().data);
                                requestListener.requestSuccess(response.body().data);
                            } else if (response.body().code == 300) {
                                //需要提示用户更新成功
                                requestListener.requestFail("登录失败！");
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResBean<MemberBean>> call, Throwable t) {
                        Log.d(TAG, "onFailure: " + t.getMessage());
                        requestListener.requestFail(t.getMessage());
                    }
                });
        }
    }

    //UPADATE
    public void update(MemberBean memberBean, final DataAccessListener<ComGoldResBean> dataAccessListener) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserId", memberBean.userId);
            jsonObject.put("NickName", memberBean.nickName);
            jsonObject.put("HeadUrl", memberBean.headUrl);
            jsonObject.put("Gender", memberBean.gender);
            jsonObject.put("Age", memberBean.age);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=UTF-8"), jsonObject.toString());
        RetrofitUtils.createService(ApiService.class).updateAction(body).enqueue(new Callback<ComGoldResBean>() {
            @Override
            public void onResponse(Call<ComGoldResBean> call, Response<ComGoldResBean> response) {
                if (response != null && response.body() != null) {

                    if (response.body().code == 200 && response.body().gold != 0) {
                        update(response.body().gold);
                        dataAccessListener.requestSuccess(response.body());
                    } else if (response.body().code == 300) {
                        Log.d(TAG, "onResponse: update 用户已经更新");
                        dataAccessListener.requestFail(response.body().message);
                    }
                    updateTaskState(HttpConstant.TASK_INFO_ID);
                   /* MemberCache.setTaskInfoState(true);
                    new AppCommonEvent(AppCommonEvent.UPDATE).
                            postEvent(); */
                } else
                    dataAccessListener.requestFail(response.body().message);

            }

            @Override
            public void onFailure(Call<ComGoldResBean> call, Throwable t) {
                dataAccessListener.requestFail(t.getMessage());
            }
        });
    }

    //LOGIN
    public void removeData() {
        setLogout();
        DEFAULT = null;
        new AppCommonEvent(AppCommonEvent.LOGOUT).
                postEvent();
    }

    //SIGN_IN
    public void signIn(final DataAccessListener<SignResBean> requestListener) {
        int userId = MemberCache.getMemUserId();
        if (userId != 0)
            RetrofitUtils.createService(ApiService.class).signIn(userId).enqueue(new Callback<ResponseBean<SignResBean>>() {
                @Override
                public void onResponse(Call<ResponseBean<SignResBean>> call, Response<ResponseBean<SignResBean>> response) {
                    if (response != null && response.body() != null) {
                        if (response.body().code == 200) {
                            requestListener.requestSuccess(response.body().data);
                            update(response.body().data.addGold);
                        } else if (response.body().code == 300) {
                            requestListener.requestFail("300");
                        }
                        updateTaskState(HttpConstant.TASK_SIGN_ID);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBean<SignResBean>> call, Throwable t) {
                    requestListener.requestFail(t.getMessage());
                    Log.d(TAG, "onFailure: " + t.getMessage());
                }
            });

    }

    //Share
    public void share(final DataAccessListener<ComGoldResBean> requestListener) {

        int userId = MemberCache.getMemUserId();
        if (userId != 0)
            RetrofitUtils.createService(ApiService.class).shareAction(userId).enqueue(new Callback<ComGoldResBean>() {
                @Override
                public void onResponse(Call<ComGoldResBean> call, Response<ComGoldResBean> response) {
                    if (response != null && response.body() != null) {
                        if (response.body().code == 200 && response.body().gold != 0) {
                            update(response.body().gold);
                            requestListener.requestSuccess(response.body());
                        } else {
                            requestListener.requestFail(String.valueOf(response.body().code));
                        }
                        updateTaskState(HttpConstant.TASK_SHARE_ID);
//                    MemberCache.setTaskShareState(true);
                        Log.d(TAG, "onResponse: " + response.message());
                    }
                }

                @Override
                public void onFailure(Call<ComGoldResBean> call, Throwable t) {
                    requestListener.requestFail(t.getMessage());
                    Log.d(TAG, "onFailure: " + t.getMessage());
                }
            });

    }

    /**
     * @param dataAccessListener
     * @param pull               get the newest state from server
     */

    public void getTaskState(final DataAccessListener<List<TaskResbean>> dataAccessListener, boolean pull) {
        int userId = MemberCache.getMemUserId();
        if (userId != 0)
            if (!pull) {
                if (getDefaultTasks() != null && getDefaultTasks().size() > 0) {
                    dataAccessListener.requestSuccess(TaskStates);
                    return;
                }
            }

        RetrofitUtils.createService(ApiService.class).getTaskState(userId).enqueue(new Callback<List<TaskResbean>>() {
            @Override
            public void onResponse(Call<List<TaskResbean>> call, Response<List<TaskResbean>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    setDefaultTasks(response.body());
                    dataAccessListener.requestSuccess(TaskStates);
                }
            }

            @Override
            public void onFailure(Call<List<TaskResbean>> call, Throwable t) {
                dataAccessListener.requestFail("获取数据出错");
            }
        });
    }

    public void getIncome(final DataAccessListener<List<IncomeResbean>> dataAccessListener) {
        int userId = MemberCache.getMemUserId();
        if (userId != 0)
            RetrofitUtils.createService(ApiService.class).getIncome(userId).enqueue(new Callback<List<IncomeResbean>>() {
                @Override
                public void onResponse(Call<List<IncomeResbean>> call, Response<List<IncomeResbean>> response) {
                    Log.d(TAG, "onResponse: ");
                    List<IncomeResbean> list = response.body();
                    if (list != null && list.size() > 0) {
                        Collections.sort(list, new Comparator<IncomeResbean>() {
                            @Override
                            public int compare(IncomeResbean o1, IncomeResbean o2) {
                                if (o1.eventTime > o2.eventTime)
                                    return -1;
                                else return 1;
                            }
                        });
                        if (list != null && list.size() > 0) {
                            TODAY_GOLD = 0;
                            for (IncomeResbean income : list
                                    ) {
                                if (DateTimeUtils.getDisNow(income.eventTime * 1000) == 0) {
                                    TODAY_GOLD = TODAY_GOLD + income.addGoldCoin;
                                }
                            }

                        }
                        dataAccessListener.requestSuccess(list);
                    } else
                        dataAccessListener.requestFail("未获取到数据");

                }

                @Override
                public void onFailure(Call<List<IncomeResbean>> call, Throwable t) {
                    Log.d(TAG, "Failed: ");
                    dataAccessListener.requestFail("获取数据出错");
                }
            });
    }

    public void getPayRecord(final DataAccessListener<List<TransferResNote>> dataAccessListener) {
        int userId = MemberCache.getMemUserId();
        if (userId != 0)
            RetrofitUtils.createService(ApiService.class).transferHistory(userId).enqueue(new Callback<List<TransferResNote>>() {
                @Override
                public void onResponse(Call<List<TransferResNote>> call, Response<List<TransferResNote>> response) {
                    List<TransferResNote> list = response.body();
                    if (list != null && list.size() > 0) {
                        dataAccessListener.requestSuccess(list);
                    }
                    Log.d(TAG, "onResponse: ");

                }

                @Override
                public void onFailure(Call<List<TransferResNote>> call, Throwable t) {

                }
            });
    }

    //COMPLAIN
    public void complain(final DataAccessListener<String> requestListener, RequestBody requestBody) {
        String type = HttpConstant.TYPE_LOGIN;
        int userId = MemberCache.getMemUserId();
        if (userId != 0) {
            RetrofitUtils.createService(ApiService.class).complainApp(requestBody).enqueue(new Callback<ResponseBean>() {
                @Override
                public void onResponse(Call<ResponseBean> call, Response<ResponseBean> response) {
                    if (response.body() != null) {
                        if (response.body().code == 200) {
                            Log.d(TAG, "onResponse: login success");
                            requestListener.requestSuccess("吐槽成功");
                        } else if (response.body().code == 400) {
                            //需要提示用户更新成功
                            requestListener.requestFail("用户不存在");
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBean> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());
                    requestListener.requestFail(t.getMessage());
                }
            });
        }
    }

    //申请提现
    public void transferAliPay(final DataAccessListener<String> requestListener, final RequestBody requestBody, final int money) {
        int userId = MemberCache.getMemUserId();
        if (userId != 0) {
            RetrofitUtils.createService(ApiService.class).transferCash(requestBody).enqueue(new Callback<ResponseBean>() {
                @Override
                public void onResponse(Call<ResponseBean> call, Response<ResponseBean> response) {
                    if (response.body() != null) {
                        if (response.body().code == 200) {
                            MemberBean memberBean = getDefault();
                            memberBean.cash = memberBean.cash - money;
                            setDefault(memberBean);
                            requestListener.requestSuccess("账号提交成功，三个工作日内到账～");
                        } else if (response.body().code == 400) {
                            requestListener.requestFail("用户不存在");
                        } else if (response.body().code == 300) {
                            requestListener.requestFail("您有申请还在审核状态，暂时无法申请");
                        } else requestListener.requestFail("泡泡君开小差了，请稍后再试一试吧~");

                    }
                }

                @Override
                public void onFailure(Call<ResponseBean> call, Throwable t) {
                    requestListener.requestFail(t.getMessage());
                }
            });
        }
    }

    //更新用户金币
    public void update(int gold) {
        Log.d(TAG, "gold add " + gold);
        MemberBean memberBean = getDefault();
        if (memberBean != null) {
            int gold_update = memberBean.gold + gold;
            memberBean.gold = gold_update;
            setDefault(memberBean);
        }
        Log.d(TAG, "update: gold" + getDefault().gold);

    }

    public void updateTaskState(final int taskId) {
        boolean update = false;
        if (TaskStates != null && TaskStates.size() > 0) {
            for (int i = 0; i < TaskStates.size(); i++) {
                if (TaskStates.get(i).taskId == taskId) {
                    TaskStates.get(i).taskStatus = 1;
                    update = true;
                }
            }
            if (!update) {
                update = !update;
                getTaskState(new DataAccessListener<List<TaskResbean>>() {
                    @Override
                    public void requestSuccess(List<TaskResbean> ob) {
                        if (ob != null && ob
                                .size() > 0)
                            updateTaskState(taskId);
                    }

                    @Override
                    public void requestFail(String error) {
                    }
                }, true);
            }
        }

    }

    public void setLogout() {
        MemberCache.setMemOpenid(null);
        MemberCache.setMemNickName(null);
        MemberCache.setMemCash(0);
        MemberCache.setMemAge(null);
        MemberCache.setMemHeadUrl(null);
        MemberCache.setMemGold(0);
        MemberCache.setMemGender(0);
        MemberCache.setMemUserId(0);
        MemberCache.initTaskState(false);
    }

    public static void setLogin(MemberBean dataBean) {
        MemberCache.setMemNickName(dataBean.nickName);
        MemberCache.setMemAge(dataBean.age);
        MemberCache.setMemCash(dataBean.cash);
        MemberCache.setMemHeadUrl(dataBean.headUrl);
        MemberCache.setMemGold(dataBean.gold);
        MemberCache.setMemGender(dataBean.gender);
        MemberCache.setMemUserId(dataBean.userId);
        long cache_time = MemberCache.getTaskTime() != null ? Long.parseLong(MemberCache.getTaskTime()) : System.currentTimeMillis();
        int day = DateTimeUtils.getDisNow(cache_time);
        if (day == 0) {
            MemberCache.initTaskState(false);
        }
    }

    public void setRegister(MemberBean dataBean) {
        MemberCache.setMemNickName(dataBean.nickName);
        MemberCache.setMemAge(dataBean.age);
        MemberCache.setMemCash(dataBean.cash);
        MemberCache.setMemHeadUrl(dataBean.headUrl);
        MemberCache.setMemGold(dataBean.gold);
        MemberCache.setMemGender(dataBean.gender);
        MemberCache.setMemUserId(dataBean.userId);
        long cache_time = MemberCache.getTaskTime() != null ? Long.parseLong(MemberCache.getTaskTime()) : System.currentTimeMillis();
        int day = DateTimeUtils.getDisNow(cache_time);
        if (day == 0)
            MemberCache.initTaskState(false);
    }

    public void getReadReward(final int readId, final String tag) {
        int userId = MemberCache.getMemUserId();
        if (userId != 0)
            RetrofitUtils.createService(ApiService.class).readAward(userId, readId).enqueue(new Callback<ComGoldResBean>() {
                @Override
                public void onResponse(Call<ComGoldResBean> call, Response<ComGoldResBean> response) {
                    if (response != null && response.body() != null && response.body().gold != 0) {
                        int add_gold = response.body().gold;
                        update(add_gold);
                        updateTaskState(readId);
                        new AppCommonEvent(AppCommonEvent.TYPE_READ).putExtra(AppCommonEvent.EVENT_KEY, add_gold).putExtra(AppCommonEvent.EVENT_KEY2, tag).
                                postEvent();
                    }
                }

                @Override
                public void onFailure(Call<ComGoldResBean> call, Throwable t) {

                }
            });


    }

    public void fetchIconList(final DataAccessListener<IconResBean> requestListener) {
        RetrofitUtils.createService(ApiService.class).getIcon().enqueue(new Callback<IconResBean>() {
            @Override
            public void onResponse(Call<IconResBean> call, Response<IconResBean> response) {
                if (response != null && response.body() != null) {
                    requestListener.requestSuccess(response.body());
                } else {
                    requestListener.requestFail("");
                   /* IconResBean iconResBean = new IconResBean();
                    iconResBean.version = "2";
                    List<IconResBean.IconBean> list = new ArrayList<IconResBean.IconBean>(9);
                    for (int i = 0; i < 15; i++) {
                        IconResBean.IconBean iconBean = new IconResBean.IconBean();
                        iconBean.setIcon_name("好123");
                        iconBean.setImage_url("http://static.moogos.com/resource/hao_123_1500883433_cd57f7b0ab213e8495e8a3b0ee968c2e_eec492ee.png");
                        iconBean.setUrl("http://m.hao123.com/");
                        list.add(iconBean);
                    }
                    iconResBean.data = list;
                    requestListener.requestSuccess(iconResBean);*/
                }
            }

            @Override
            public void onFailure(Call<IconResBean> call, Throwable t) {
                requestListener.requestFail(t.getMessage());
            }
        });
    }
}
