package com.mogo.ppaobrowser.browser.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mogo.ppaobrowser.Constants;
import com.mogo.ppaobrowser.PPaoApplication;
import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.base.BrowserBaseActivity;
import com.mogo.ppaobrowser.browser.bean.BookMarkModel;
import com.mogo.ppaobrowser.browser.interface_package.DataAccessListener;
import com.mogo.ppaobrowser.browser.interface_package.SaveDataListener;
import com.mogo.ppaobrowser.browser.presenter.HBPresenter;
import com.mogo.ppaobrowser.preference.BrowserCache;
import com.mogo.ppaobrowser.update.ApkUpdateUtils;
import com.mogo.ppaobrowser.utils.AppUtility;
import com.mogo.ppaobrowser.utils.HttpUtil;
import com.mogo.ppaobrowser.utils.ToastUtils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;

import static com.mogo.ppaobrowser.browser.base.BaseFragment.index;


public class BrowserActivity<T> extends BrowserBaseActivity implements SaveDataListener {
    private static final String TAG = "BrowserActivity";
    HBPresenter hbPresenter;
    @BindView(R.id.frame_layout)
    public LinearLayout linearLayout;


    private static final int NO_NEED_UPDATE = 0x0024;
    private static final int NEED_UPDATE = 0x0025;

    Handler handler = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
            if (msg.what == NO_NEED_UPDATE) {
//                ToastUtils.show(BrowserActivity.this, "当前已是最新版本！");
            } else if (msg.what == NEED_UPDATE) {
                String update_url = (String) msg.obj;
                BrowserCache browserCache = new BrowserCache(PPaoApplication.getApplication());
                if (!browserCache.getDownloadLimitkEnabled()) {
                    createUpdateAppDialog(update_url,true);
                } else {
                    if (AppUtility.isWifi(PPaoApplication.getApplication())) {
                        ApkUpdateUtils.download(BrowserActivity.this, update_url, "泡泡浏览器");
                    } else {
                        createUpdateAppDialog(update_url,false);
                    }
                }
            }
        }
    };


    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        String urlString = null;
        if (intent.getExtras() != null)
            urlString = intent.getExtras().getString("urlString");
        if (urlString == null) {
            urlString = intent.getDataString();
        }

        if (urlString != null) {
            setBrowserFragment(urlString, index);
        }
    }

    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("doyer BrowserActivity", "onDestroy: ");
    }

    @Override
    protected void updateViews(boolean b) {

    }

    @Override
    protected void initViews() {
        hbPresenter = new HBPresenter(this);
        HttpUtil.HttpGet(Constants.GET_VERSION, new DataAccessListener() {
            @Override
            public void requestSuccess(Object ob) {
                try {
                    JSONObject jsonObject = new JSONObject(ob.toString());
                    int version = AppUtility.getVersionCode(BrowserActivity.this);
                    int update_version = jsonObject.optInt("version_code", version);
                    if (version < update_version) {
                        Message message = Message.obtain();
                        String update_url = jsonObject.optString("app_url");
                        Log.d(TAG, "requestSuccess: " + update_url);
                        if (!TextUtils.isEmpty(update_url)) {
                            message.what = NEED_UPDATE;
                            message.obj = update_url;
                            handler.sendMessage(message);
                        }
                    } else {
                        Message message = Message.obtain();
                        message.what = NO_NEED_UPDATE;
                        handler.sendMessage(message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void requestFail(String error) {

            }
        });
        Intent intent = getIntent();
        String urlString = intent.getDataString();
        if (TextUtils.isEmpty(urlString) && intent.getExtras() != null) {
            urlString = intent.getExtras().getString("urlString");
        }
        if (TextUtils.isEmpty(urlString) && intent.getExtras() != null) {
            urlString = intent.getExtras().getString("urlString");
        }
        if (urlString != null) {
            setBrowserFragment(urlString, index);
        } else {
            setMainFragment(index);
        }
        Log.d(TAG, "onResume: " + urlString);

    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_main;
    }

    protected void initInjector() {
//        DaggerBrowserComponent.builder().browserModule(new BrowserModule(this, this)).build().inject(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if (baseFragment.frameLayout != null && baseFragment.frameLayout.getParent() != null) {
                ViewGroup vg = (ViewGroup) baseFragment.frameLayout.getParent();
                vg.removeView(baseFragment.frameLayout);
                return true;
            } else {
                if (baseFragment.barController != null) {
                    baseFragment.barController.back();
                    //之前这里没有加return 导致返回失效 直接返回到home
                    return true;
                } else {
                    if ((System.currentTimeMillis() - exitTime) > 2000) {
                        ToastUtils.show(this, "再按一次退出程序");
                        exitTime = System.currentTimeMillis();
                    } else {
                        AppUtility.exitApp(BrowserActivity.this);
                    }
                    return true;
                }
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void createSuccess(Object object) {
        if (object instanceof BookMarkModel) {
            hbPresenter.createBookMark((BookMarkModel) object);
        }
    }


    @Override
    public void editSuccess(Object object) {

    }

    @Override
    public void saveCancel() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        //here we can use getIntent() to get the extra data.
    }
}
