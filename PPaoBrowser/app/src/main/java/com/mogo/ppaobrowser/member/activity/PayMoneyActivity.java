package com.mogo.ppaobrowser.member.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.activity.PPaoBaseActivity;
import com.mogo.ppaobrowser.browser.interface_package.DataAccessListener;
import com.mogo.ppaobrowser.member.bean.responseBean.MemberBean;
import com.mogo.ppaobrowser.member.biz.UserBiz;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by doyer on 2017/7/20.
 */

public class PayMoneyActivity extends PPaoBaseActivity {

    private static final String TAG = "PayMoneyActivity";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.btn_conver_cash)
    Button btnConverCash;
    @BindView(R.id.ll_lack)
    LinearLayout llLack;
    @BindView(R.id.tv_pay_cash)
    TextView tvPayCash;

    UserBiz userBiz = new UserBiz();
    MemberBean memberBean;

    @Override
    protected void updateViews(boolean b) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        memberBean = userBiz.getDefault();
        if (memberBean != null) {
            tvPayCash.setText(memberBean.cash + "元");
        } else {
            userBiz.fetchUserInfo(new DataAccessListener<MemberBean>() {
                @Override
                public void requestSuccess(MemberBean ob) {
                    memberBean = ob;
                    tvPayCash.setText(ob.cash + "元");
                }

                @Override
                public void requestFail(String error) {

                }
            });
        }
    }

    @Override
    protected void initViews() {
        setSupportActionBar(toolbar);
        toolbarTitle.setText(getString(R.string.money_conversion));
        memberBean = userBiz.getDefault();
        if (memberBean != null) {
            tvPayCash.setText(memberBean.cash + "元");
        } else {
            userBiz.fetchUserInfo(new DataAccessListener<MemberBean>() {
                @Override
                public void requestSuccess(MemberBean ob) {
                    memberBean = ob;
                    tvPayCash.setText(ob.cash + "元");
                }

                @Override
                public void requestFail(String error) {

                }
            });
        }
        updateBottom();
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.pay_layout;
    }


    @OnClick({R.id.tv_pay_history, R.id.btn_conver_cash, R.id.btn_earn_cash, R.id.tv_pay_cash})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_pay_history:
                startActivityWithZoom(new Intent(PayMoneyActivity.this, PayHistoryActivity.class));
                break;
            case R.id.btn_conver_cash:
                startActivityWithZoom(new Intent(PayMoneyActivity.this, AlipayCashActivity.class));
                break;
            case R.id.btn_earn_cash:
                startActivityWithZoom(new Intent(PayMoneyActivity.this, TaskHallActivity.class));
                finish();
                break;
            case R.id.tv_pay_cash:
                break;
        }
    }

    public void updateBottom() {
       /* Random random = new Random();
        int i = random.nextInt(2);
        if (i == 0) {
            btnConverCash.setVisibility(View.GONE);
            llLack.setVisibility(View.VISIBLE);
        } else {
            btnConverCash.setVisibility(View.VISIBLE);
            llLack.setVisibility(View.GONE);
        }*/
        if (memberBean != null && memberBean.cash >= 30) {
            btnConverCash.setVisibility(View.VISIBLE);
            llLack.setVisibility(View.GONE);
        } else {
            btnConverCash.setVisibility(View.GONE);
            llLack.setVisibility(View.VISIBLE);
        }
    }
}
