package com.mogo.ppaobrowser.member.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.activity.PPaoBaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by doyer on 2017/7/19.
 */

public class CashRuleActivity extends PPaoBaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @Override
    protected void updateViews(boolean b) {

    }

    @Override
    protected void initViews() {
        setSupportActionBar(toolbar);
        toolbarTitle.setText(getString(R.string.gold_rule));
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.cash_rule_layout;
    }

}
