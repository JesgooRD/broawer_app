/**
 * 
 */
package com.mogo.ppaobrowser.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;

import com.mogo.ppaobrowser.R;


/**
 *@author doyer.du
 *@version 2016-1-13 上午10:47:18
 *Note: 聊天界面下载提示框
 *
 */
public class ProgressView {
	private Dialog alert;
	private ProgressBar progressView;
	private Context context;

	public ProgressView(Context context) {
		this.context = context;
		alert = new Dialog(context);
		alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
	}

	public void showProgress() {

		View dialogView = LayoutInflater.from(context).inflate(R.layout.progressview_layout, null);

		alert.setContentView(dialogView);
		progressView = (ProgressBar) dialogView.findViewById(R.id.progress_view);
		progressView.setClickable(false);
		alert.show();
	}

	public void hideProgress() {
		alert.dismiss();
	}

	public void setProgress(int i) {
		progressView.setProgress(i);
	}

	public void dismiss() {
		alert.dismiss();
	}
}
