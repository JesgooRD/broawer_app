package com.mogo.ppaobrowser.browser.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.activity.BrowserActivity;
import com.mogo.ppaobrowser.browser.adapter.HBAdapter;
import com.mogo.ppaobrowser.browser.base.BaseFragment;
import com.mogo.ppaobrowser.browser.base.BrowserBaseActivity;
import com.mogo.ppaobrowser.browser.bean.BookMarkModel;
import com.mogo.ppaobrowser.browser.interface_package.FragmentBackListener;
import com.mogo.ppaobrowser.browser.interface_package.SaveDataListener;
import com.mogo.ppaobrowser.widget.OwnItemDecoration;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by doyer on 2017/5/22.
 */
public class BookMarkFragment extends BaseFragment implements FragmentBackListener, SaveDataListener {
    @BindView(R.id.recycler_bookmark)
    RecyclerView recyclerBookmark;
    Unbinder unbinder;
    @BindView(R.id.empty_layout)
    RelativeLayout emptyLayout;
    private HBAdapter adapter;

    private List<Object> bookMarkModelList;
    private List<Boolean> checkList;
    PopupWindow popupWindow;
    protected boolean isCreated = false;


    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (activity instanceof BrowserBaseActivity) {
            ((BrowserBaseActivity) activity).setBackListener(this);
            ((BrowserBaseActivity) activity).setInterception(true);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
        isCreated = true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookmark, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        if (bookMarkModelList.size() == 0) {
            emptyLayout.setVisibility(View.VISIBLE);
            return;
        }
        //创建默认的线性LayoutManager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this.getContext());
        recyclerBookmark.setLayoutManager(mLayoutManager);
        adapter = new HBAdapter(this.getContext(), bookMarkModelList, checkList, this);
        recyclerBookmark.addItemDecoration(new OwnItemDecoration());
        adapter.setOnItemListener(new HBAdapter.OnItemClickListener() {
            @Override
            public boolean setOnItemLongClick(int position) {
                adapter.selected_show = true;//长按改变checkbox显示状态
                adapter.notifyDataSetChanged();
                return true;
            }

            @Override
            public void setOnItemCheckedChanged(int position, boolean isCheck) {
                if (adapter.selected_show)
                    checkList.set(position, isCheck);
                if (isCheck) {
                    if (popupWindow == null) {
                        PopupWindowOut(recyclerBookmark.getRootView());
                    } else if (!popupWindow.isShowing()) {
                        popupWindow.showAtLocation(recyclerBookmark.getRootView(), Gravity.BOTTOM, 0, 0);
                    }
                }
            }

            @Override
            public void setOnItemClickListener(int position) {
                Intent intent = new Intent(getContext(), BrowserActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("urlString", ((BookMarkModel) bookMarkModelList.get(position)).getUrl());
                intent.putExtras(bundle);
                startActivity(intent);
                getActivity().finish();
            }
        });
        recyclerBookmark.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("BookMarkFragment");
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("BookMarkFragment");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (getActivity() instanceof BrowserBaseActivity) {
            ((BrowserBaseActivity) getActivity()).setBackListener(null);
            ((BrowserBaseActivity) getActivity()).setInterception(false);
        }
    }


    @Override
    protected void initViews() {

    }

    @Override
    protected int attachLayoutRes() {
        return 0;
    }

    @Override
    protected void updateViews(boolean b) {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isCreated) {
            return;
        }
        if (!isVisibleToUser && popupWindow != null && popupWindow.isShowing()) {
            popupWindow.dismiss();
            adapter.selected_show = false;
            adapter.notifyDataSetChanged();
        }
    }

    private void initData() {
        bookMarkModelList = new ArrayList<>();
        getDataFromBase();
        checkList = new ArrayList<>();
        for (int i = 0; i < bookMarkModelList.size(); i++) {
            checkList.add(false);
        }
    }

    //从数据库里读取书签列表
    private void getDataFromBase() {
        bookMarkModelList.addAll(browserPresenter.getAllBookMarks());
       /* if (bookMarkModelList == null || bookMarkModelList.size() == 0) {
            bookMarkModelList.add(new BookMarkModel("百度", "http://www.baidu.com/"));
            bookMarkModelList.add(new BookMarkModel("腾讯", "http://www.qq.com/"));
            bookMarkModelList.add(new BookMarkModel("搜狐", "http://www.sohu.com/"));
            bookMarkModelList.add(new BookMarkModel("新浪", "http://www.sina.com.cn/"));
            for (int i = 0; i < bookMarkModelList.size(); i++) {
                browserPresenter.createBookMark((BookMarkModel) bookMarkModelList.get(i));
            }
        }*/
    }

    protected void PopupWindowOut(View view) {
        if (popupWindow != null) {
            popupWindow.dismiss();
            popupWindow = null;
        }
        if (view != null) {
            final View contentView = LayoutInflater.from(this.getContext()).inflate(R.layout.bh_pop_bottom, null);
            popupWindow = new PopupWindow(contentView);
            //清空点击回调
            contentView.findViewById(R.id.pop_clear).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int length = checkList.size();
                    for (int i = length - 1; i >= 0; i--) {
                        if (checkList.get(i)) {
                            browserPresenter.removeBookMark((BookMarkModel) bookMarkModelList.get(i));
                            bookMarkModelList.remove(i);
                            checkList.remove(i);
                            adapter.selected_show = false;
                            adapter.notifyDataSetChanged();
                        }
                    }
                    if (bookMarkModelList.size() == 0) {
                        emptyLayout.setVisibility(View.VISIBLE);
                    }
                    popupWindow.dismiss();
                }
            });

            popupWindow.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
            popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
            popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));
            popupWindow.setFocusable(false);
            popupWindow.setOutsideTouchable(false);
            popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
        }
    }

    @Override
    public void onbackForward() {
        if ((popupWindow != null && popupWindow.isShowing()) || adapter.selected_show) {
            popupWindow.dismiss();
            adapter.selected_show = !adapter.selected_show;
            adapter.notifyDataSetChanged();
        } else {
            getActivity().finish();
        }
    }

    @Override
    public void createSuccess(Object object) {

    }

    @Override
    public void editSuccess(Object object) {
        if (object instanceof BookMarkModel)
            browserPresenter.saveBookMark((BookMarkModel) object);
        initData();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void saveCancel() {

    }

}
