package com.mogo.ppaobrowser.widget;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.interface_package.SaveDataListener;
import com.mogo.ppaobrowser.browser.bean.BookMarkModel;
import com.mogo.ppaobrowser.browser.bean.HistoryModel;
import com.mogo.ppaobrowser.utils.AppUtility;
import com.mogo.ppaobrowser.utils.ToastUtils;

/**
 *
 */
public class CustomDialog {
    public final static String TAG = "CustomDialog";

    public void showEditHistoryDialog(@NonNull final Context context,
                                      @NonNull final HistoryModel item) {
        final AlertDialog.Builder editHistoryDialog = new AlertDialog.Builder(context);
        editHistoryDialog.setTitle(R.string.title_edit_bookmark);
        final View dialogLayout = View.inflate(context, R.layout.dialog_edit_bookmark, null);
        final EditText getTitle = (EditText) dialogLayout.findViewById(R.id.bookmark_title);
        getTitle.setText(item.getName());
        final EditText getUrl = (EditText) dialogLayout.findViewById(R.id.bookmark_url);
        getUrl.setText(item.getUrl());
        editHistoryDialog.setView(dialogLayout);
        editHistoryDialog.setPositiveButton(context.getString(R.string.action_cancel),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        HistoryModel editedItem = new HistoryModel();
                        editedItem.setName(getTitle.getText().toString());
                        editedItem.setUrl(getUrl.getText().toString());
                        if (dialog != null)
                            dialog.dismiss();
                    }
                });
        editHistoryDialog.show();
    }

    public void showEditBookmarkDialog(@NonNull final Context context,
                                       @NonNull final BookMarkModel item) {
        final AlertDialog.Builder editBookmarkDialog = new AlertDialog.Builder(context);
        editBookmarkDialog.setTitle(R.string.title_edit_bookmark);
        final View dialogLayout = View.inflate(context, R.layout.dialog_edit_bookmark, null);
        final EditText getTitle = (EditText) dialogLayout.findViewById(R.id.bookmark_title);
        getTitle.setText(item.getBookmark_name());
        final EditText getUrl = (EditText) dialogLayout.findViewById(R.id.bookmark_url);
        getUrl.setText(item.getUrl());
        editBookmarkDialog.setView(dialogLayout);
        editBookmarkDialog.setPositiveButton(context.getString(R.string.action_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BookMarkModel editedItem = new BookMarkModel();
                        String mark_name = getTitle.getText().toString().trim();
                        String mark_url = getUrl.getText().toString().trim();
                        if (TextUtils.isEmpty(mark_name) || TextUtils.isEmpty(mark_url)) {
                            ToastUtils.show(context, "书签栏目不能为空！");
                        } else {
                            editedItem.setBookmark_name(mark_name);
                            editedItem.setUrl(mark_url);
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    }
                }).setNegativeButton(context.getString(R.string.action_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (dialogInterface != null)
                    dialogInterface.dismiss();
            }
        });
        editBookmarkDialog.show();
    }

    public void showEditBookmarkDialog(@NonNull final Context context,
                                       @NonNull final BookMarkModel item, final SaveDataListener saveDataListener) {
        final AlertDialog.Builder editBookmarkDialog = new AlertDialog.Builder(context);
        editBookmarkDialog.setTitle(R.string.title_edit_bookmark);
        final View dialogLayout = View.inflate(context, R.layout.dialog_edit_bookmark, null);
        final EditText getTitle = (EditText) dialogLayout.findViewById(R.id.bookmark_title);
        getTitle.setText(item.getBookmark_name());
        final EditText getUrl = (EditText) dialogLayout.findViewById(R.id.bookmark_url);
        getUrl.setText(item.getUrl());
        editBookmarkDialog.setView(dialogLayout);
        editBookmarkDialog.setPositiveButton(context.getString(R.string.action_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String mark_name = getTitle.getText().toString().trim();
                        String mark_url = getUrl.getText().toString().trim();
                        if (TextUtils.isEmpty(mark_name) || TextUtils.isEmpty(mark_url)) {
                            ToastUtils.show(context,"书签栏目不能为空！");
                        } else {
                            item.setBookmark_name(mark_name);
                            item.setUrl(mark_url);
                            saveDataListener.editSuccess(item);
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    }
                }).setNegativeButton(context.getString(R.string.action_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (dialogInterface != null)
                    dialogInterface.dismiss();
                saveDataListener.saveCancel();
            }
        });
        editBookmarkDialog.show();
    }

    public void createBookmarkDialog(@NonNull final Context context,
                                     @NonNull final BookMarkModel item, final SaveDataListener saveDataListener) {
        final AlertDialog.Builder editBookmarkDialog = new AlertDialog.Builder(context);
        editBookmarkDialog.setTitle(R.string.title_add_bookmark);
        final View dialogLayout = View.inflate(context, R.layout.dialog_edit_bookmark, null);
        final EditText getTitle = (EditText) dialogLayout.findViewById(R.id.bookmark_title);
        getTitle.setText(item.getBookmark_name());
        final EditText getUrl = (EditText) dialogLayout.findViewById(R.id.bookmark_url);
        getUrl.setText(item.getUrl());
        editBookmarkDialog.setView(dialogLayout);
        editBookmarkDialog.setPositiveButton(context.getString(R.string.action_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String mark_name = getTitle.getText().toString().trim();
                        String mark_url = getUrl.getText().toString().trim();
                        if (TextUtils.isEmpty(mark_name) || TextUtils.isEmpty(mark_url)) {
                            ToastUtils.show(context,"书签栏目不能为空！");
                        } else {
                            item.setBookmark_name(mark_name);
                            item.setUrl(mark_url);
                            saveDataListener.createSuccess(item);
                            if (dialog != null)
                                dialog.dismiss();
                        }
                    }
                }).setNegativeButton(context.getString(R.string.action_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (dialogInterface != null)
                    dialogInterface.dismiss();
                saveDataListener.saveCancel();
            }
        });
        editBookmarkDialog.show();
    }


}