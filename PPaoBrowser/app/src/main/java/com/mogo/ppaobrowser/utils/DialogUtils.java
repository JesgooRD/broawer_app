package com.mogo.ppaobrowser.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mogo.ppaobrowser.R;


/**
 * Created by doyer.du on 2016/8/18.
 */
public class DialogUtils {
    public static TextView setDialogSize(Activity context, float widthProportion, float heightProportion,
                                         View rootView) {
        DisplayMetrics dm = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int screenHeigh = dm.heightPixels;
        int screenWidth = dm.widthPixels;

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) rootView
                .getLayoutParams();
        if (heightProportion >= 0) {
            layoutParams.height = (int) (screenHeigh * heightProportion);// 高度设置为屏幕高度比
        }
        layoutParams.width = (int) (screenWidth * widthProportion);// 宽度设置为屏幕的宽度比
        layoutParams.gravity = Gravity.CENTER;
        rootView.setLayoutParams(layoutParams);

        return null;
    }

    public static Dialog createDialog(Context context, View rootView) {
        Dialog dialog = new Dialog(context, R.style.default_dialog_style);
        dialog.setContentView(rootView);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        return dialog;
    }
}
