package com.mogo.ppaobrowser.browser.base;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.mogo.ppaobrowser.Constants;
import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.base.AppConst;
import com.mogo.ppaobrowser.browser.activity.DownLoadActivity;
import com.mogo.ppaobrowser.browser.activity.FeedBackActivity;
import com.mogo.ppaobrowser.browser.activity.HistoryMarkActivity;
import com.mogo.ppaobrowser.browser.activity.SettingActivity;
import com.mogo.ppaobrowser.browser.adapter.MultiAdapter;
import com.mogo.ppaobrowser.browser.controller.MultiWindowManager;
import com.mogo.ppaobrowser.browser.fragment.WebViewFragment;
import com.mogo.ppaobrowser.browser.interface_package.BarController;
import com.mogo.ppaobrowser.browser.interface_package.SaveDataListener;
import com.mogo.ppaobrowser.browser.bean.ActivityMsg;
import com.mogo.ppaobrowser.browser.bean.BookMarkModel;
import com.mogo.ppaobrowser.browser.presenter.HBPresenter;
import com.mogo.ppaobrowser.update.ApkUpdateUtils;
import com.mogo.ppaobrowser.utils.AppUtility;
import com.mogo.ppaobrowser.utils.BitmapLoader;
import com.mogo.ppaobrowser.utils.DialogUtils;
import com.mogo.ppaobrowser.utils.ToastUtils;
import com.mogo.ppaobrowser.widget.CustomDialog;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import java.util.ArrayList;
import java.util.List;

import static com.mogo.ppaobrowser.browser.base.BaseFragment.index;


/**
 * Created by doyer on 2017/6/21.
 */

public class PPaoPresenter {
    Context context;
    BaseFragment baseFragment;
    private MultiAdapter adapter;
    private List<MultiWindowManager> multiWindowManagers = new ArrayList<>();

    public PPaoPresenter(Context context, BaseFragment baseFragment) {
        this.context = context;
        this.baseFragment = baseFragment;
    }

    public void initMenuPopupWindow(View view) {
        final BarController barController = baseFragment.barController;
        MultiWindowManager multiWindowManager = MultiWindowManager.mulWindowDataList.get(index);
        final WebViewFragment webViewFragment;
        if (TextUtils.isEmpty(multiWindowManager.getUrl())) {
            //目前位于Main界面的情况
            webViewFragment = null;
        } else {
            webViewFragment = MultiWindowManager.mulWindowBrowserList.get(index);
        }

        if (view != null) {
            //自定义PopupWindow的布局
            final View contentView = LayoutInflater.from(context).inflate(R.layout.pop_bottom, null);
            //初始化PopupWindow,并为其设置布局文件
            final PopupWindow popupWindow = new PopupWindow(contentView);
            contentView.findViewById(R.id.pop_back).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow.dismiss();
                }
            });
            contentView.findViewById(R.id.pop_setting).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow.dismiss();
                    ((Activity) context).startActivityForResult(new Intent(context, SettingActivity.class), 0);
                    ((Activity) context).overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);

                }
            });
            contentView.findViewById(R.id.pop_history).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow.dismiss();
                    context.startActivity(new Intent(context, HistoryMarkActivity.class));
                }
            });

            contentView.findViewById(R.id.pop_exit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow.dismiss();
                    AppUtility.exitApp((Activity) context);

                }
            });

            contentView.findViewById(R.id.pop_refresh).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow.dismiss();
                    if (barController != null)
                        barController.refresh();
                }
            });

            contentView.findViewById(R.id.pop_download).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                    context.startActivity(new Intent(context, DownLoadActivity.class));
                }
            });

            contentView.findViewById(R.id.pop_top).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                    context.startActivity(new Intent(context, FeedBackActivity.class));
                }
            });

            contentView.findViewById(R.id.pop_add).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    popupWindow.dismiss();
                    BookMarkModel bookMarkModel = new BookMarkModel();
                    if (webViewFragment != null && webViewFragment.mWebView != null) {
                        bookMarkModel.setUrl(webViewFragment.mWebView.getUrl());
                        bookMarkModel.setBookmark_name(webViewFragment.mWebView.getTitle());
                    }
                    createBookMarkDialog(bookMarkModel);
                }
            });

            contentView.findViewById(R.id.pop_share).setOnClickListener(new View.OnClickListener() {
                public String currentUrl;

                @Override
                public void onClick(View view) {
                    if (webViewFragment != null) {
                        currentUrl = webViewFragment.mWebView.getUrl();
                        if (!TextUtils.isEmpty(currentUrl)) {
                            AppUtility.shareWeb(AppUtility.mTargetScene0, webViewFragment.mWebView);
                        }
                    } else {
                        AppUtility.shareApp(context, AppUtility.mTargetScene0);
                    }
                }
            });

            //设置PopupWindow的宽和高,必须设置,否则不显示内容(也可用PopupWindow的构造方法设置宽高)
            popupWindow.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
            popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
            //当需要点击返回键,或者点击空白时,需要设置下面两句代码.
            //如果有背景，则会在contentView外面包一层PopupViewContainer之后作为mPopupView，如果没有背景，则直接用contentView作为mPopupView。
            //而这个PopupViewContainer是一个内部私有类，它继承了FrameLayout，在其中重写了Key和Touch事件的分发处理
            popupWindow.setFocusable(true);
            popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));   //为PopupWindow设置透明背景.
            popupWindow.setOutsideTouchable(false);
            //设置PopupWindow进入和退出动画
            popupWindow.setAnimationStyle(R.style.PopupAnimation);
            //设置PopupWindow显示的位置
            popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
            backgroundAlpha((Activity) context, 0.6f);
            popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    backgroundAlpha((Activity) context, 1f);
                }
            });
        }
    }


    // Activity 参数为了设置添加屏幕的背景透明度
    public void initMultiWindow(final Activity activity, View view) {
        MultiWindowManager multiWindowManager = MultiWindowManager.mulWindowDataList.get(index);
        WebViewFragment webViewFragment = MultiWindowManager.mulWindowBrowserList.get(index);
        if (multiWindowManager != null && !TextUtils.isEmpty(multiWindowManager.getUrl())) {
            if (webViewFragment != null && webViewFragment.mWebView != null) {
                MultiWindowManager.mulWindowDataList.get(index).setUrl(webViewFragment.mWebView.getUrl());
                MultiWindowManager.mulWindowDataList.get(index).setTitle(webViewFragment.mWebView.getTitle());

                //生成缩略图
                Picture snapShot = webViewFragment.mWebView.capturePicture();
                Bitmap bmp = Bitmap.createBitmap(AppUtility.getMetrics(context).widthPixels, AppUtility.getMetrics(context).heightPixels, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bmp);
                snapShot.draw(canvas);
                MultiWindowManager.mulWindowDataList.get(index).setDrawable(new BitmapDrawable(bmp));
            }
        }
        if (view != null) {
            //自定义PopupWindow的布局
            final View contentView = LayoutInflater.from(context).inflate(R.layout.muti_window_content, null);
            //初始化PopupWindow,并为其设置布局文件
            final PopupWindow popupWindow = new PopupWindow(contentView);
            popupWindow.setFocusable(true);
            final GridView gridView = (GridView) contentView.findViewById(R.id.muti_win_panel);
            final ImageView popback = (ImageView) contentView.findViewById(R.id.multi_pop_back);
            popback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                    multiWindowManagers.clear();
                }
            });
            final ImageView imageView = (ImageView) contentView.findViewById(R.id.img_add_window);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (multiWindowManagers.size() < MultiWindowManager.mulWindowDataList.size()) {
                        MultiWindowManager multiWindowManager = new MultiWindowManager();
                        multiWindowManager.setTitle(Constants.HOME_PAGE);
                        multiWindowManager.setDrawable(context.getResources().getDrawable(R.drawable.main_page));
                        MultiWindowManager.mulWindowDataList.set(multiWindowManagers.size(), multiWindowManager);
                        getList();
                        if (adapter != null) {
                            setGridView(gridView, popupWindow);
                        }
                    } else ToastUtils.show(context, "页面数量过多喽！");

                }
            });
            setGridView(gridView, popupWindow);
            //设置PopupWindow的宽和高,必须设置,否则不显示内容(也可用PopupWindow的构造方法设置宽高)
            popupWindow.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
            popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
            //当需要点击返回键,或者点击空白时,需要设置下面两句代码.
            //如果有背景，则会在contentView外面包一层PopupViewContainer之后作为mPopupView，如果没有背景，则直接用contentView作为mPopupView。
            //而这个PopupViewContainer是一个内部私有类，它继承了FrameLayout，在其中重写了Key和Touch事件的分发处理
            popupWindow.setFocusable(true);
            popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));   //为PopupWindow设置透明背景.
            popupWindow.setOutsideTouchable(false);
            //设置PopupWindow进入和退出动画
            popupWindow.setAnimationStyle(R.style.PopupAnimation);
            //设置PopupWindow显示的位置
            backgroundAlpha(activity, 0.6f);
            popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    backgroundAlpha(activity, 1f);
                }
            });
            popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
        }

    }

    public void setGridView(GridView gv, final PopupWindow popupWindow) {
        getList();
        int size = multiWindowManagers.size();
        int length = 300;
        DisplayMetrics dm = new DisplayMetrics();
        ((BrowserBaseActivity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);
        float density = dm.density;
        int gridViewWidth = (int) (size * (length + 4) * density);
        int itemWidth = (int) (length * density) / 3;

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                gridViewWidth, LinearLayout.LayoutParams.MATCH_PARENT);
        gv.setLayoutParams(params);
        gv.setColumnWidth(itemWidth);
        gv.setHorizontalSpacing(5);
        gv.setStretchMode(GridView.NO_STRETCH);
        gv.setNumColumns(size);  //列的个数
        adapter = new MultiAdapter(context, multiWindowManagers);
        gv.setAdapter(adapter);
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Message msg = Message.obtain();
                msg.what = ActivityMsg.SELECT_WINDOW.getValue();
                msg.arg1 = position;
                msg.obj = multiWindowManagers.get(position);
                multiWindowManagers.clear();
                popupWindow.dismiss();
                adapter = null;
                baseFragment.handler.sendMessage(msg);
            }
        });
    }

    public void getList() {
        multiWindowManagers.clear();
        for (MultiWindowManager multi : MultiWindowManager.mulWindowDataList
                ) {
            if (multi != null && null != multi.getTitle()) {
                multiWindowManagers.add(multi);
            }
        }
    }

    /**
     * 设置添加屏幕的背景透明度
     *
     * @param bgAlpha
     */
    public void backgroundAlpha(Activity activity, float bgAlpha) {
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        activity.getWindow().setAttributes(lp);
    }


    public void createBookMarkDialog(final BookMarkModel bookMarkModel) {
        final HBPresenter hbPresenter = new HBPresenter((Activity) context);
        LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.dialog_book_dialog, null);
        final Dialog dialog = DialogUtils.createDialog(context, view);
        final LinearLayout ll_content = (LinearLayout) view.findViewById(R.id.ll);
        final TextView tv_left = (TextView) view.findViewById(R.id.btn_left);
        final TextView tv_right = (TextView) view.findViewById(R.id.btn_right);
        final EditText ed_name = (EditText) view.findViewById(R.id.bookmark_title);
        final EditText ed_url = (EditText) view.findViewById(R.id.bookmark_url);
        if (bookMarkModel != null) {
            ed_name.setText(bookMarkModel.getBookmark_name());
            ed_url.setText(bookMarkModel.getUrl());
        }
        tv_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                dialog.dismiss();
            }
        });
        tv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mark_name = ed_name.getText().toString().trim();
                String mark_url = ed_url.getText().toString().trim();
                if (TextUtils.isEmpty(mark_name) || TextUtils.isEmpty(mark_url)) {
                    ToastUtils.show(context, "书签栏目不能为空！");
                } else {
                    bookMarkModel.setBookmark_name(mark_name);
                    bookMarkModel.setUrl(mark_url);
                    hbPresenter.createBookMark(bookMarkModel);
                    dialog.cancel();
                    dialog.dismiss();
                }
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                closeWindowSoftInput(ll_content);
            }
        });
        dialog.show();
    }

    /**
     * 隐藏软键盘
     *
     * @param view
     */
    protected void closeWindowSoftInput(View view) {
        InputMethodManager imm = (InputMethodManager) context.getApplicationContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        boolean isSoftActive = imm.isActive();
        if (isSoftActive) {
            imm.hideSoftInputFromWindow(
                    view.getApplicationWindowToken(), 0); // 强制隐藏键盘
        }
    }

}
