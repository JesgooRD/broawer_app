package com.mogo.ppaobrowser.member.bean.responseBean;

/**
 * Created by doyer on 2017/7/21.
 */

public class TaskResbean {

    /**
     * date : 1501459200
     * taskId : 1
     * taskNmae : 首次微信登录
     * taskStatus : 1
     * userId : 2
     */

    public long date;
    public int taskId;
    public String taskNmae;
    public int taskStatus;
    public int userId;

}
