package com.mogo.ppaobrowser.member.bean.responseBean;

/**
 * Created by doyer on 2017/7/31.
 * for login & register
 */

public class MemberBean {

    /**
     * "data": {
     * "Sign_count": 0,
     * "age": "0000-00-00",
     * "cash": 0,
     * "gender": 1,
     * "gold": 0,
     * "headUrl": "http://wx.qlogo.cn/mmopen/FhfxXoLj0SZKPEqeOdmVSeoQicbG8r0Xf2GOqBhL75g4SH5ufD7z17HxbeJEbVSRdQyPwWTOrm5gaBtnr9EtZgToFnIKsDoibO/0",
     * "nickName": "DOYER",
     * "openId": "obDFh1DXK1L-KPtMYuxGGnlOF7Lw",
     * "userId": 22
     * }
     */
    public String age;
    public float cash;
    public int gender;
    public int gold;
    public String headUrl;
    public String nickName;
    public String openId;
    public int signCount;
    public int userId;

}
