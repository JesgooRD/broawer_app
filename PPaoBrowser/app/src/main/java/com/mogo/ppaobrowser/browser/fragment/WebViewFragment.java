package com.mogo.ppaobrowser.browser.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.DownloadListener;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mogo.ppaobrowser.Constants;
import com.mogo.ppaobrowser.PPaoApplication;
import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.activity.SearchActivity;
import com.mogo.ppaobrowser.browser.base.BrowserBaseActivity;
import com.mogo.ppaobrowser.browser.bean.DownloadModel;
import com.mogo.ppaobrowser.browser.bean.HistoryModel;
import com.mogo.ppaobrowser.browser.controller.MultiWindowManager;
import com.mogo.ppaobrowser.browser.interface_package.BarController;
import com.mogo.ppaobrowser.gen.DownloadModelDao;
import com.mogo.ppaobrowser.utils.ADFilterTool;
import com.mogo.ppaobrowser.utils.AppUtility;
import com.mogo.ppaobrowser.utils.DensityUtil;
import com.mogo.ppaobrowser.utils.HttpUtil;
import com.mogo.ppaobrowser.utils.LogUtility;
import com.mogo.ppaobrowser.utils.ToastUtils;
import com.mogo.ppaobrowser.utils.UrlUtils;
import com.mogo.ppaobrowser.widget.SearchView;
import com.umeng.analytics.MobclickAgent;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

import butterknife.BindView;

import static com.mogo.ppaobrowser.browser.base.BrowserBaseActivity.PERMISSION_WRITE_EXTERNAL_STORAGE;
import static com.mogo.ppaobrowser.browser.fragment.MainFragment.SEARCH_KEY;


public class WebViewFragment extends WebBottomFragment implements BarController {
    public static final String TAG = "WebViewFragment";
    @BindView(R.id.bar_search)
    SearchView barSearch;
    @BindView(R.id.search_layout)
    LinearLayout searchLayout;
    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.mProgress)
    ProgressBar mProgress;

    public String mUrl;
    public static final String ARG_URL = "url";
    private final Map<String, String> mRequestHeaders = new ArrayMap<>();
    private static final String HEADER_DNT = "DNT";
    private GestureDetector mGestureDetector;
    private float sMaxFling;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE |
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("webView");
        if (mWebView != null) {
            mWebView.getSettings().setBlockNetworkImage(preferenceManager.getBlockImagesEnabled());
            mWebView.onResume();
        }
        MultiWindowManager.mulWindowBrowserList.set(index, this);
        barController = this;
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("webView");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        barController = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mWebView != null) {
            LogUtility.debug(TAG, "onDestroy: ");
            // Check to make sure the WebView has been removed
            // before calling destroy() so that a memory leak is not created
            ViewGroup parent = (ViewGroup) mWebView.getParent();
            if (parent != null) {
                parent.removeView(mWebView);
            }
        }
    }


    @Override
    protected void initViews() {
        Bundle args = getArguments();
        if (args != null) {
            mUrl = args.getString(ARG_URL);
        }
        mWebView = webview;
        sMaxFling = ViewConfiguration.get(this.getActivity()).getScaledMaximumFlingVelocity();

        barSearch.setOnPreFocusListener(new SearchView.PreFocusListener() {
            @Override
            public void onPreFocus() {
                hideSearchBar();
                barSearch.setFocusable(false);
                startActivityForResult(new Intent(mContext, SearchActivity.class), 01);
                getActivity().overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
            }
        });

        if (mWebView != null) {
            setupWebView(mWebView);
        }

        mGestureDetector = new GestureDetector(getContext(), new CustomWebGestureListener());
        mWebView.setOnTouchListener(new TouchListener());
        mWebView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String s, String s1, String s2, String s3, long l) {
                if (preferenceManager.getDownloadLimitkEnabled() && !AppUtility.isWifi(WebViewFragment.this.getContext())) {
                    ToastUtils.show(WebViewFragment.this.getContext(), "由于已经设置”仅WIFI下载“，未能下载~");
                    return;
                } else {
                    if (AppUtility.isMarshmallow() && !AppUtility.isGranted_(mContext, PERMISSION_WRITE_EXTERNAL_STORAGE)) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, 23);
                    } else
                        popUpDownload(mWebView.getRootView(), s, s1, s2, s3);
                }
            }
        });

        if (mWebView != null) {
            if (mUrl != null && !mUrl.isEmpty()) {
                mWebView.loadUrl(mUrl, mRequestHeaders);
                MultiWindowManager.mulWindowDataList.get(index).setTitle(mWebView.getTitle());
                MultiWindowManager.mulWindowDataList.get(index).setUrl(mWebView.getUrl());
            } else {
                ToastUtils.show(mContext, R.string.app_website_error);
            }
        }
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.frag_webview;
    }

    @Override
    protected void updateViews(boolean b) {

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        LogUtility.debug(TAG, "onHiddenChanged" + hidden);
        if (hidden) {
            this.onPause();
        } else {
            this.onResume();
            if (mWebView != null && mWebView.canGoBack()) {
                updateBottomBar(true);
                MultiWindowManager.mulWindowDataList.get(index).setTitle(mWebView.getTitle());
                MultiWindowManager.mulWindowDataList.get(index).setUrl(mWebView.getUrl());
            }
        }
    }


    public static WebViewFragment newInstance(String url, boolean isMain) {
        Bundle args = new Bundle();
        args.putString(ARG_URL, url);
        WebViewFragment fragment = new WebViewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    protected void setupWebView(WebView webView) {
        WebSettings settings = webView.getSettings();
        if (preferenceManager.getDoNotTrackEnabled()) {
            mRequestHeaders.put(HEADER_DNT, "1");
        } else {
            mRequestHeaders.remove(HEADER_DNT);
        }
        if (preferenceManager.getBlockImagesEnabled())
            settings.setBlockNetworkImage(true);
        else
            settings.setBlockNetworkImage(false);
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setBuiltInZoomControls(false);
        settings.setAllowFileAccess(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setDefaultTextEncodingName("utf-8");
        settings.setAllowContentAccess(true);
        //设置 缓存模式
        settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        // 开启 DOM storage API 功能
        settings.setDomStorageEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            settings.setAllowFileAccessFromFileURLs(true);
            settings.setAllowUniversalAccessFromFileURLs(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        settings.setAppCacheEnabled(true);
        settings.setDatabaseEnabled(true);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        webView.setWebViewClient(new WebViewClient() {
                                     //忽略https
                                     @Override
                                     public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                                         handler.proceed();
                                     }

                                     @Override
                                     public void onPageFinished(WebView view, String url) {
                                         super.onPageFinished(view, url);
                                         if (!preferenceManager.getDoNotTrackEnabled()) {
                                             HistoryModel historyModel = new HistoryModel(view.getTitle(), view.getUrl(), System.currentTimeMillis());
                                             browserPresenter.createHistoryItem(historyModel);
                                         }
                                     }

                                     @Override
                                     public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
                                         String url = null;
                                         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                             url = request.getUrl().toString().toLowerCase();
                                         }
                                         if (!url.contains(mUrl)) {
                                             if (preferenceManager.getAdBlockEnabled()) {
                                                 if (!ADFilterTool.hasAd(PPaoApplication.getApplication(), url)) {
                                                     return super.shouldInterceptRequest(view, request);
                                                 } else {
                                                     return new WebResourceResponse(null, null, null);
                                                 }
                                             } else
                                                 return super.shouldInterceptRequest(view, request);
                                         } else {
                                             return super.shouldInterceptRequest(view, request);
                                         }

                                     }
                                 }

        );
        mWebView.setWebChromeClient(new WebChromeClient() {

                                        @Override
                                        public void onProgressChanged(WebView view, int newProgress) {
                                            if (newProgress < 0) {
                                                mProgress.setVisibility(View.VISIBLE);
                                                mProgress.setProgress(0);
                                                return;
                                            }
                                            if (newProgress == 100) {
                                                mProgress.setVisibility(View.GONE);
                                            } else {
                                                mProgress.setVisibility(View.VISIBLE);
                                                mProgress.setProgress(newProgress);
                                            }
                                        }
                                    }

        );

    }

    @Override
    public void forward() {
        if (mWebView != null && mWebView.canGoForward())
            mWebView.goForward();
    }

    @Override
    public void back() {
        if (mWebView != null) {
            if (mWebView.canGoBack()) {
                mWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                mWebView.stopLoading();
                mWebView.goBack();
            } else if (!mWebView.canGoBack()) {
                ((BrowserBaseActivity) mContext).setMainFragment(index);
            }
        }
    }

    //这个地方不去执行 后期需要关注一下这边的逻辑
    @Override
    public void backHome() {
        if (webview != null) {
            webview.destroy();
            webview = null;
            mWebView.destroy();
            mWebView = null;
            MultiWindowManager.mulWindowBrowserList.set(index, null);
        }
        ((BrowserBaseActivity) mContext).setMainFragment(index);
    }


    @Override
    public void stopLoading() {
        if (mWebView != null)
            mWebView.stopLoading();
    }

    @Override
    public void refresh() {
        if (mWebView != null)
            mWebView.reload();
    }

    @Override
    public void showSearchBar() {
        searchLayout.setVisibility(View.VISIBLE);
        barSearch.setText(mWebView.getUrl());
        barSearch.setSelection(0, barSearch.getText().length());
        barSearch.setSelectAllOnFocus(true);
    }

    @Override
    public void hideSearchBar() {
        searchLayout.setVisibility(View.GONE);
    }

    public void update(String urlString) {
        if (mWebView == null)
            mWebView = webview;
        webview.loadUrl(urlString);
        MultiWindowManager.mulWindowDataList.get(index).setTitle(webview.getTitle());
        MultiWindowManager.mulWindowDataList.get(index).setUrl(webview.getUrl());

    }


    private class CustomWebGestureListener implements GestureDetector.OnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            int power = (int) (velocityY * 100 / sMaxFling);
            if (power < -10) {
                hideSearchBar();
            } else if (power > 50) {
                showSearchBar();
            }
            return false;
        }

        private boolean mCanTriggerLongPress = true;

        @Override
        public void onLongPress(MotionEvent e) {
        }

        @Override
        public boolean onDown(MotionEvent motionEvent) {
            return false;
        }

        @Override
        public void onShowPress(MotionEvent e) {
            mCanTriggerLongPress = true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
            return false;
        }
    }

    private class TouchListener implements View.OnTouchListener {

        float mLocation;
        float mY;
        int mAction;
        private int SCROLL_UP_THRESHOLD = DensityUtil.dpToPx(10);

        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouch(@Nullable View view, @NonNull MotionEvent arg1) {
            if (view == null)
                return false;

            if (!view.hasFocus()) {
                view.requestFocus();
            }
            mAction = arg1.getAction();
            mY = arg1.getY();
            if (mAction == MotionEvent.ACTION_DOWN) {
                mLocation = mY;
            } else if (mAction == MotionEvent.ACTION_UP) {
                final float distance = (mY - mLocation);
                if (distance > SCROLL_UP_THRESHOLD && view.getScrollY() < SCROLL_UP_THRESHOLD) {
                    showSearchBar();
                } else if (distance < -SCROLL_UP_THRESHOLD) {
                    hideSearchBar();
                }
                mLocation = 0;
            }
            mGestureDetector.onTouchEvent(arg1);
            return false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtility.debug(TAG, "onActivityResult: " + requestCode);
        if (resultCode == 02) {
            String search_out = data.getExtras().getString(SEARCH_KEY);
            search_out = UrlUtils.smartUrlFilter(search_out, true, Constants.BAIDU_SEARCH + search_out);
            if (barController != null)
                barController.stopLoading();
            ((BrowserBaseActivity) mContext).setBrowserFragment(search_out, index);

        }
    }

}
