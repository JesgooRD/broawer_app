package com.mogo.ppaobrowser.browser.bean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Unique;

/**
 * Created by doyer on 2017/6/29.
 */

@Entity
public class DownloadModel {
    @Id@Unique
    long id;
    String url;
    String path;
    // 0-下载中 1-下载完成 2-暂停状态
    int status = 0;
    String file_name = "";
    int total = 0;
    int sofar = 0;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getSofar() {
        return sofar;
    }

    public void setSofar(int sofar) {
        this.sofar = sofar;
    }

    @Generated(hash = 1167329292)
    public DownloadModel(long id, String url, String path, int status,
            String file_name, int total, int sofar) {
        this.id = id;
        this.url = url;
        this.path = path;
        this.status = status;
        this.file_name = file_name;
        this.total = total;
        this.sofar = sofar;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Generated(hash = 1665448439)
    public DownloadModel() {
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setId(long id) {
        this.id = id;
    }
}
