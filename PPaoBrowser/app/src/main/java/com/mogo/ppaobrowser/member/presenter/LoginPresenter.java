package com.mogo.ppaobrowser.member.presenter;

import com.mogo.ppaobrowser.member.bean.responseBean.LoginResBean;
import com.mogo.ppaobrowser.member.bean.responseBean.MemberBean;
import com.mogo.ppaobrowser.member.contract.LoginContract;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by doyer.du on 2016/10/14.
 */

public class LoginPresenter implements LoginContract.ILoginPresenter {
    private LoginContract.ILoginModel mModel;
    private LoginContract.ILoginView mView;

    public LoginPresenter(LoginContract.ILoginView view) {
        mView = view;
    }

    @Override
    public void login(String type, String userId) {
        mView.showLoadingDialog("登录中...");
        mModel.login(type, userId, new Callback<LoginResBean<MemberBean>>() {
            @Override
            public void onResponse(Call<LoginResBean<MemberBean>> call, Response<LoginResBean<MemberBean>> response) {
                mView.dismissDialog();
                mView.setLoginResult(response.body().data);
            }

            @Override
            public void onFailure(Call<LoginResBean<MemberBean>> call, Throwable t) {
                mView.dismissDialog();
            }
        });
    }
}
