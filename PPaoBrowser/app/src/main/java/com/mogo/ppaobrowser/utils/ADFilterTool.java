package com.mogo.ppaobrowser.utils;

import android.content.Context;
import android.content.res.Resources;

import com.mogo.ppaobrowser.R;

/**
 * Created by doyer on 2017/5/25.
 */

public class ADFilterTool {
    public static boolean hasAd(Context context, String url) {
        Resources res = context.getResources();
        String[] adUrls = res.getStringArray(R.array.adBlockUrl);
        for (String adUrl : adUrls) {
            if (url.contains(adUrl)) {
                return true;
            }
        }
        return false;
    }
}
