package com.mogo.ppaobrowser.browser.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.base.BrowserBaseActivity;
import com.mogo.ppaobrowser.browser.controller.MultiWindowManager;
import com.mogo.ppaobrowser.preference.BrowserCache;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by doyer on 2017/5/25.
 */
public abstract class WebBottomFragment extends WebBaseFragment {
    @BindView(R.id.btn_back)
    ImageView btn_back;
    @BindView(R.id.btn_forward)
    ImageView btn_forward;
    public BrowserCache preferenceManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferenceManager = new BrowserCache(getContext());
    }

    @Nullable
    @OnClick({R.id.bottom_action_back, R.id.bottom_action_forward, R.id.bottom_action_menu, R.id.bottom_action_home, R.id.bottom_page_plus})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bottom_action_back:
                if (barController != null)
                    barController.back();
                break;
            case R.id.bottom_action_forward:
                if (barController != null)
                    barController.forward();
                break;
            case R.id.bottom_action_menu:
                pPaoPresenter.initMenuPopupWindow(view.getRootView());
                break;
            case R.id.bottom_action_home:
                //判断当前为在webview页面还是main页面
                if (!TextUtils.isEmpty(MultiWindowManager.mulWindowDataList.get(index).getUrl())) {
                    ((BrowserBaseActivity) mContext).setMainFragment(index);
                    MultiWindowManager.mulWindowBrowserList.set(index, null);
                } else {
                    barController.backHome();
                }
                break;
            case R.id.bottom_page_plus:
                pPaoPresenter.initMultiWindow(getActivity(),view.getRootView());
                break;
            default:
                break;
        }
    }

    public void updateBottomBar(boolean forwardEnable) {
        if (btn_back.isEnabled()) {
            btn_back.setEnabled(false);
        }
        if (forwardEnable) {
            btn_forward.setEnabled(true);
            btn_forward.setImageResource(R.drawable.bottom_forward);
        } else {
            btn_forward.setEnabled(false);
            btn_forward.setImageResource(R.drawable.bottom_forward);
        }
    }
}
