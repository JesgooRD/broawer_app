package com.mogo.ppaobrowser.browser.presenter;

import android.app.Activity;

import com.mogo.ppaobrowser.Constants;
import com.mogo.ppaobrowser.PPaoApplication;
import com.mogo.ppaobrowser.browser.bean.BookMarkModel;
import com.mogo.ppaobrowser.browser.bean.HistoryModel;
import com.mogo.ppaobrowser.gen.BookMarkModelDao;
import com.mogo.ppaobrowser.gen.DaoSession;
import com.mogo.ppaobrowser.gen.HistoryModelDao;
import com.mogo.ppaobrowser.utils.UrlUtils;

import java.util.List;

/**
 * Created by doyer on 2017/5/24.
 */
public class HBPresenter {
    BookMarkModelDao bookMarkModelDao;
    HistoryModelDao historyModelDao;

    public HBPresenter(Activity activity) {
        initDao();
    }

    private void initDao() {
        DaoSession daoSession = PPaoApplication.getDaoSession();
        bookMarkModelDao = daoSession.getBookMarkModelDao();
        historyModelDao = daoSession.getHistoryModelDao();
    }


    public void createBookMark(BookMarkModel bookMarkModel) {
        String url = bookMarkModel.getUrl();
        if (!UrlUtils.isUrlFormat(url))
            bookMarkModel.setUrl(String.format(Constants.HTTP_FORMAT, url));
        bookMarkModelDao.insert(bookMarkModel);
    }

    public List<BookMarkModel> getAllBookMarks() {
        return bookMarkModelDao.loadAll();
    }

    public void saveBookMark(BookMarkModel bookMarkModel) {
        bookMarkModelDao.update(bookMarkModel);
    }

    public void createBookMarkList(List<BookMarkModel> bookMarkModelList) {
        bookMarkModelDao.insertInTx(bookMarkModelList);
    }

    public void removeBookMark(BookMarkModel bookMarkModel) {
        bookMarkModelDao.delete(bookMarkModel);
    }

    public void removeAllBookMark() {
        bookMarkModelDao.deleteAll();
    }

    public void removeBookMarkList(final List<BookMarkModel> bookMarkModelList) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < bookMarkModelList.size(); i++) {
                    removeBookMark(bookMarkModelList.get(i));
                }
            }
        }).run();
    }

    public void createHistoryItem(HistoryModel historyModel) {
        List<HistoryModel> historyModelList = historyModelDao.loadAll();
        if (historyModelList.size() > 100) {
            historyModelDao.delete(historyModelList.get(0));
        }
        String url = historyModel.getUrl();
        historyModel.setUrl(url);
        historyModelDao.insert(historyModel);
    }

    public List<HistoryModel> getAllHistory() {
        return historyModelDao.loadAll();
    }

    public void removeHistory(HistoryModel historyModel) {
        historyModelDao.delete(historyModel);
    }

    public void removeAllHistory() {
        historyModelDao.deleteAll();
    }

}
