package com.mogo.ppaobrowser.member.bean.responseBean;

/**
 * Created by doyer on 2017/7/21.
 */

public class IncomeResbean {
    /**
     * addCash : 2
     * addGoldCoin : 0
     * eventTime : 1501605057
     * taskId : 1
     * taskName : 首次微信登录
     * userId : 36
     */
    public float addCash;
    public int addGoldCoin;
    public long eventTime;
    public String taskName;
}
