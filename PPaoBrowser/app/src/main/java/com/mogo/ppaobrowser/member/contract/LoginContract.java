package com.mogo.ppaobrowser.member.contract;


import com.mogo.ppaobrowser.member.bean.responseBean.LoginResBean;
import com.mogo.ppaobrowser.member.bean.responseBean.MemberBean;
import com.mogo.ppaobrowser.member.view.IBaseView;

import retrofit2.Callback;

/**
 * Created by Lain on 2016/10/14.
 * 封装登录相关接口的操作
 */

public class LoginContract {
    /**
     * 逻辑处理层
     */
    public interface ILoginModel {
        //登录方法   参数：call
        void login(String type, String name, Callback<LoginResBean<MemberBean>> callback);

        void saveUserInfo(MemberBean user);//登录成功就保存用户信息(暂时没用到)
    }

    /**
     * V视图层
     */
    public interface ILoginView extends IBaseView {
        //对获取到的数据进行操作
        void setLoginResult(MemberBean loginResult);
    }

    /**
     * P视图与逻辑处理的连接层
     */
    public interface ILoginPresenter {
        void login(String type, String openId);//唯一的桥梁就是登录了
    }
}
