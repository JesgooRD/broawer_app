package com.mogo.ppaobrowser.member.bean;

/**
 * Created by doyer on 2017/7/11.
 */

public class WxAccessBean {

    /**
     * access_token : pPaeEozAYHwvTFqGEsRUzALgpg2-HRsSJcMOsYY8cxQif_megkQk_ws--SXbx2pyuGPLPwMeYQCf7ZeqApotVy7I0Rr4_xs8uGZkMnrniT8
     * expires_in : 7200
     * refresh_token : UBrWopMP7KymS6tal84lA65lYgx-k0RI5LSCsCOt-iCBjdRpXwaIbrJ_9xxVvUlU4ixtfc-KChCTC0bkfUP2JfiUrQPP6fVKVf8X-G8GmcM
     * openid : obDFh1DXK1L-KPtMYuxGGnlOF7Lw
     * scope : snsapi_userinfo
     * unionid : osKLt1QOrUtnOv8mwenmUo67NnZI
     */

    private String access_token;
    private int expires_in;
    private String refresh_token;
    private String openid;
    private String scope;
    private String unionid;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }
}
