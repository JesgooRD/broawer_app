package com.mogo.ppaobrowser.utils;

import android.app.Activity;
import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.FileProvider;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Toast;


import com.mogo.ppaobrowser.BuildConfig;
import com.mogo.ppaobrowser.PPaoApplication;
import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.base.AppConst;
import com.mogo.ppaobrowser.browser.activity.BrowserActivity;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by doyer.du on 5/22/17.
 */
public class AppUtility {

    private static final int THUMB_SIZE = 150;
    //发送给聊天好友
    public static int mTargetScene0 = SendMessageToWX.Req.WXSceneSession;
    //分享朋友圈
    public static int mTargetScene1 = SendMessageToWX.Req.WXSceneTimeline;
    public static final int MEMBER_LOGIN_REQUEST = 10;
    public static final String STACK_NAME = "main";
    private static DisplayMetrics displayMetrics = null;


    public static <T> T newInstance(Class<? extends T> classToNew) {
        T newObject = null;
        try {
            newObject = classToNew.newInstance();
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        }
        return newObject;
    }

    public static void pushLocalNotification(Context context, int id, String title, String content) {

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(content)
                        .setGroupSummary(true)
                        .setAutoCancel(true);

        // Create an explicit intent for an Activity in app
        Intent intent = new Intent(context, BrowserActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(BrowserActivity.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent pendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // random Ids allows multiple notifications appear simultaneously in the notification center.
        mNotificationManager.notify(id, builder.build());
    }

    public static String join(String[] array, String token) {
        if (array == null || array.length == 0) {
            return "";
        }
        return join(array, 0, array.length - 1, token);
    }

    public static String join(String[] array, int start, int end, String token) {
        StringBuilder stringBuilder = new StringBuilder("");

        int mStart = start < 0 ? 0 : start;
        int mEnd = end < 0 ? 0 : end;

        if (end < start) {
            mEnd = mStart;
        }

        String mToken = token == null ? "" : token;

        if (array != null) {
            mStart = (mStart >= array.length) ? array.length - 1 : mStart;
            mEnd = (mEnd >= array.length) ? array.length - 1 : mEnd;
        }

        for (int i = mStart; i <= mEnd; i++) {
            stringBuilder.append(array[i]);

            if (i < mEnd) {
                stringBuilder.append(mToken);
            }
        }

        return stringBuilder.toString();
    }

    public static <T> T[] insertAt(T[] base, T objectToInsert, int position) {
        T[] newArray;
        if (base == null && objectToInsert != null) {
            newArray = (T[]) Array.newInstance(objectToInsert.getClass(), 1);
            newArray[0] = objectToInsert;
        } else {
            int mPosition;
            newArray = (T[]) Array.newInstance(objectToInsert.getClass(), base.length + 1);
            if (position >= 0 && position <= base.length) {
                mPosition = position;
            } else {
                mPosition = base.length;
            }

            for (int i = 0; i < newArray.length; i++) {
                if (i < mPosition) {
                    newArray[i] = base[i];
                } else if (i > mPosition) {
                    newArray[i] = base[i - 1];
                } else if (i == mPosition) {
                    newArray[i] = objectToInsert;
                }
            }
        }

        return newArray;
    }

    public static String unicodeEscape(String s) {
        if (s == null || s.isEmpty()) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            sb.append(unicodeEscape(s.charAt(i)));
        }

        return sb.toString();
    }

    public static String unicodeEscape(char ch) {
        if (ch < 0x10) {
            return "\\u000" + Integer.toHexString(ch);
        } else if (ch < 0x100) {
            return "\\u00" + Integer.toHexString(ch);
        } else if (ch < 0x1000) {
            return "\\u0" + Integer.toHexString(ch);
        }
        return "\\u" + Integer.toHexString(ch);
    }


    public static void toggleImmersiveMode(Activity act) {
        int uiOptions = act.getWindow().getDecorView().getSystemUiVisibility();
        int newUiOptions = uiOptions;
        // END_INCLUDE (get_current_ui_flags)
        // BEGIN_INCLUDE (toggle_ui_flags)
        boolean isImmersiveModeEnabled =
                ((uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY) == uiOptions);

        // Navigation bar hiding:  Backwards compatible to ICS.
        if (Build.VERSION.SDK_INT >= 14) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }

        // Status bar hiding: Backwards compatible to Jellybean
        if (Build.VERSION.SDK_INT >= 16) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }

        if (Build.VERSION.SDK_INT >= 18) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        act.getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
        //END_INCLUDE (set_ui_flags)
    }

    public static void collapseViewWithAnimation(final View v) {
        final int initialHeight = v.getHeight();
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        long duringMillis = (long) (initialHeight / v.getContext().getResources().getDisplayMetrics().density);
        a.setDuration(duringMillis);
        v.startAnimation(a);
    }


    public static void hideKeyboard(View v) {
        Application app = PPaoApplication.getApplication();
        InputMethodManager imm = (InputMethodManager)
                app.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static SimpleDateFormat getLocalDateFormat(String dateFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        simpleDateFormat.setTimeZone(TimeZone.getDefault());
        return simpleDateFormat;
    }


    public static String decodeUrl(String url) {
        String decodeUrl = "";
        try {
            decodeUrl = URLDecoder.decode(url, "utf-8");
        } catch (UnsupportedEncodingException ex) {
        }
        return decodeUrl;
    }

    public static String formatDate(Calendar calendar, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(calendar.getTime());
    }

    public static int indexOfArray(String[] array, String elem) {
        if (array == null || elem == null || array.length == 0) {
            return 0;
        }

        int idx = 0;
        boolean found = false;
        for (; idx < array.length; idx++) {
            if (elem.equals(array[idx])) {
                found = true;
                break;
            }
        }

        return found ? idx : 0;
    }

    public static Intent createMailIntent(String mailTo) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", mailTo, null));
        return intent;
    }

    public static String getString(int resId) {
        return PPaoApplication.getApplication().getString(resId);
    }

    public static <T> T[] toArray(List<T> list, T[] array) {
        T[] newArray = null;
        if (list != null && array != null) {
            newArray = list.toArray(array);
        }
        return newArray;
    }


    public static String getAssetContent(Context _context, String _fileName) throws IOException {
        BufferedReader reader = getAssetFileReader(_context, _fileName);
        StringBuilder sb = new StringBuilder();
        try {
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                sb.append(mLine);
            }
        } catch (IOException e) {
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
        }

        return sb.toString();
    }

    public static BufferedReader getAssetFileReader(Context _context, String _fileName) throws IOException {
        AssetManager manager = _context.getAssets();
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(manager.open(_fileName)));
        return reader;
    }

    public static void exitApp(Activity activity) {
        activity.finish();
        System.exit(0);
    }

    public static DisplayMetrics getMetrics(Context context) {
        if (displayMetrics == null) {
            displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getApplicationContext().getSystemService(Context.WINDOW_SERVICE))
                    .getDefaultDisplay().getMetrics(displayMetrics);
        }
        return displayMetrics;
    }

    public static int getStatusBarHeight(Activity activity) {
        Rect rect = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        return rect.top;
    }

    public static boolean isWifi(Context context) {

        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo net = cm.getActiveNetworkInfo();
            if (net != null && net.isConnectedOrConnecting()) {
                if (net.getType() == ConnectivityManager.TYPE_WIFI) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
        }

        return false;
    }

    public static String getDeviceId() {
        return ((TelephonyManager) PPaoApplication.getApplication().getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
    }


    public static String getAppVersion() {
        PackageManager packageManager = PPaoApplication.getApplication().getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(PPaoApplication.getApplication().getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    //版本名
    public static String getVersionName(Context context) {
        return getPackageInfo(context).versionName;
    }

    //版本号
    public static int getVersionCode(Context context) {
        return getPackageInfo(context).versionCode;
    }

    private static PackageInfo getPackageInfo(Context context) {
        PackageInfo pi = null;

        try {
            PackageManager pm = context.getPackageManager();
            pi = pm.getPackageInfo(context.getPackageName(),
                    PackageManager.GET_CONFIGURATIONS);

            return pi;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return pi;
    }

    //微信分享
    public static void shareApp(Context context, int scene) {
        IWXAPI api;
        api = WXAPIFactory.createWXAPI(context, AppConst.WX_APP_ID);
        WXWebpageObject wxWebpageObject = new WXWebpageObject();
        wxWebpageObject.webpageUrl = "http://www.orange1213.cn/";
        WXMediaMessage wxMediaMessage = new WXMediaMessage();
        wxMediaMessage.title = "泡泡，阅读爱好者中毒之选！";
        wxMediaMessage.description = "我正在用泡泡浏览器看小说、刷视频、读资讯，边玩边赚钱，快来一起玩赚泡泡！";
        wxMediaMessage.mediaObject = wxWebpageObject;
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        Bitmap thumbBmp = Bitmap.createScaledBitmap(bitmap, THUMB_SIZE, THUMB_SIZE, true);
        bitmap.recycle();
        wxMediaMessage.thumbData = BitmapLoader.Bitmap2Bytes(thumbBmp);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = wxMediaMessage;
        req.scene = scene;
        api.sendReq(req);
    }

    public static String buildTransaction(final String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
    }

    /**
     * 调用系统应用打开文件
     *
     * @param context
     * @param file
     */
    public static void openFile(Context context, File file) {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //设置intent的Action属性
        intent.setAction(Intent.ACTION_VIEW);
        //获取文件file的MIME类型
        String type = getMIMEType(file);
        //跳转
        //判断是否是AndroidN以及更高的版本
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri contentUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".fileProvider", file);
            intent.setDataAndType(contentUri, type);
        } else {
            //设置intent的data和Type属性。
            intent.setDataAndType(Uri.fromFile(file), type);
        }
        try {
            context.startActivity(intent);
        } catch (Exception e) {
            Log.e("FileUtil", e.getMessage());
            ToastUtils.show(context, "找不到打开此文件的应用！");
        }
    }

    /***根据文件后缀回去MIME类型****/

    private static String getMIMEType(File file) {
        String type = "*/*";
        String fName = file.getName();
        //获取后缀名前的分隔符"."在fName中的位置。
        int dotIndex = fName.lastIndexOf(".");
        if (dotIndex < 0) {
            return type;
        }
        /* 获取文件的后缀名*/
        String end = fName.substring(dotIndex, fName.length()).toLowerCase();
        if (end == "") return type;
        //在MIME和文件类型的匹配表中找到对应的MIME类型。
        for (int i = 0; i < MIME_MapTable.length; i++) { //MIME_MapTable??在这里你一定有疑问，这个MIME_MapTable是什么？
            if (end.equals(MIME_MapTable[i][0]))
                type = MIME_MapTable[i][1];
        }
        return type;
    }

    public static boolean isGranted_(Context context, String permission) {
        int checkSelfPermission = ActivityCompat.checkSelfPermission(context, permission);
        return checkSelfPermission == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isDebug() {
        try {
            ApplicationInfo info = PPaoApplication.getApplication().getApplicationInfo();
            return (info.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isMarshmallow() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    /**
     * 跳转到权限设置界面
     */
    public static void getAppDetailSettingIntent(Context context) {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= 9) {
            intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(Uri.fromParts("package", context.getPackageName(), null));
        } else if (Build.VERSION.SDK_INT <= 8) {
            intent.setAction(Intent.ACTION_VIEW);
            intent.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            intent.putExtra("com.android.settings.ApplicationPkgName", context.getPackageName());
        }
        context.startActivity(intent);
    }

    private static final String[][] MIME_MapTable = {
            // {后缀名，MIME类型}
            {".3gp", "video/3gpp"},
            {".apk", "application/vnd.android.package-archive"},
            {".asf", "video/x-ms-asf"},
            {".avi", "video/x-msvideo"},
            {".bin", "application/octet-stream"},
            {".bmp", "image/bmp"},
            {".c", "text/plain"},
            {".class", "application/octet-stream"},
            {".conf", "text/plain"},
            {".cpp", "text/plain"},
            {".doc", "application/msword"},
            {".docx",
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
            {".xls", "application/vnd.ms-excel"},
            {".xlsx",
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
            {".exe", "application/octet-stream"},
            {".gif", "image/gif"},
            {".gtar", "application/x-gtar"},
            {".gz", "application/x-gzip"},
            {".h", "text/plain"},
            {".htm", "text/html"},
            {".html", "text/html"},
            {".jar", "application/java-archive"},
            {".java", "text/plain"},
            {".jpeg", "image/jpeg"},
            {".jpg", "image/jpeg"},
            {".js", "application/x-javascript"},
            {".log", "text/plain"},
            {".m3u", "audio/x-mpegurl"},
            {".m4a", "audio/mp4a-latm"},
            {".m4b", "audio/mp4a-latm"},
            {".m4p", "audio/mp4a-latm"},
            {".m4u", "video/vnd.mpegurl"},
            {".m4v", "video/x-m4v"},
            {".mov", "video/quicktime"},
            {".mp2", "audio/x-mpeg"},
            {".mp3", "audio/x-mpeg"},
            {".mp4", "video/mp4"},
            {".mpc", "application/vnd.mpohun.certificate"},
            {".mpe", "video/mpeg"},
            {".mpeg", "video/mpeg"},
            {".mpg", "video/mpeg"},
            {".mpg4", "video/mp4"},
            {".mpga", "audio/mpeg"},
            {".msg", "application/vnd.ms-outlook"},
            {".ogg", "audio/ogg"},
            {".pdf", "application/pdf"},
            {".png", "image/png"},
            {".pps", "application/vnd.ms-powerpoint"},
            {".ppt", "application/vnd.ms-powerpoint"},
            {".pptx",
                    "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
            {".prop", "text/plain"}, {".rc", "text/plain"},
            {".rmvb", "audio/x-pn-realaudio"}, {".rtf", "application/rtf"},
            {".sh", "text/plain"}, {".tar", "application/x-tar"},
            {".tgz", "application/x-compressed"}, {".txt", "text/plain"},
            {".wav", "audio/x-wav"}, {".wma", "audio/x-ms-wma"},
            {".wmv", "audio/x-ms-wmv"},
            {".wps", "application/vnd.ms-works"}, {".xml", "text/plain"},
            {".z", "application/x-compress"},
            {".zip", "application/x-zip-compressed"}, {"", "*/*"}};


    public static void shareWeb(int mTargetScene, WebView webView) {
        IWXAPI api;
        api = WXAPIFactory.createWXAPI(PPaoApplication.getApplication(), AppConst.WX_APP_ID);
        WXWebpageObject wxWebpageObject = new WXWebpageObject();
        wxWebpageObject.webpageUrl = webView.getUrl();
        WXMediaMessage wxMediaMessage = new WXMediaMessage();
        wxMediaMessage.title = webView.getTitle();
        wxMediaMessage.description = "我正在看【" + webView.getTitle() + "】,为你分享，来看看呗！";
        wxMediaMessage.mediaObject = wxWebpageObject;
        Bitmap bitmap = webView.getDrawingCache();
        if (bitmap == null)
            bitmap = captureWebView(webView);
        if (bitmap == null)
            bitmap = BitmapFactory.decodeResource(PPaoApplication.getApplication().getResources(), R.drawable.share_link_default);
        if (bitmap != null) {
            Bitmap thumbBmp = Bitmap.createScaledBitmap(bitmap, 150, 150, true);
            bitmap.recycle();
            wxMediaMessage.thumbData = BitmapLoader.Bitmap2Bytes(thumbBmp);
        }

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = AppUtility.buildTransaction("webpage");
        req.message = wxMediaMessage;
        req.scene = mTargetScene;
        api.sendReq(req);
    }

    private static Bitmap captureWebView(WebView webView) {
        Picture snapShot = webView.capturePicture();
        Bitmap bmp = Bitmap.createBitmap(snapShot.getWidth(), snapShot.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmp);
        snapShot.draw(canvas);
        return bmp;
    }
}
