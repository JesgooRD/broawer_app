package com.mogo.ppaobrowser.member.bean.responseBean;

/**
 * Created by doyer on 2017/7/17.
 */

public class LoginResBean<T> {

    /**
     * code : 200
     * data : {"age":0,"cash":2,"gender":1,"gold":0,"headUrl":"http://127.0.0.1:9091","nickName":"chenxun","oepnId":"sfa12","signCount":0,"userId":27}
     * message : success
     */

    public int code;
    public T data;
    public String message;

}
