package com.mogo.ppaobrowser.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mogo.ppaobrowser.PPaoApplication;
import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.activity.SettingActivity;
import com.mogo.ppaobrowser.browser.bean.SearchHistoryModel;
import com.mogo.ppaobrowser.browser.interface_package.SearchListener;
import com.mogo.ppaobrowser.gen.DaoSession;
import com.mogo.ppaobrowser.gen.SearchHistoryModelDao;
import com.mogo.ppaobrowser.preference.BrowserCache;
import com.mogo.ppaobrowser.update.ApkUpdateUtils;
import com.mogo.ppaobrowser.utils.DialogUtils;
import com.mogo.ppaobrowser.utils.LogUtility;

import org.greenrobot.greendao.query.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by doyer.du on 16/11/15.
 */
public class Search_View extends LinearLayout {

    private static final String TAG = "Search_View";
    public Context context;
    /*UI组件*/
    private TextView tv_clear;
    private EditText et_search;
    private TextView tv_tip;
    private TextView action;
    private ImageView iv_search;

    /*列表及其适配器*/
    private Search_ListView listView;
    private BaseAdapter adapter;

    /*数据库变量*/
    SearchHistoryModelDao searchHistoryModelDao;

    //提供给外部的回调
    SearchListener searchListener;

    /*三个构造函数*/
    //在构造函数里直接对搜索框进行初始化 - init()
    public Search_View(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public Search_View(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public Search_View(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
    }


    /*初始化搜索框*/
    private void init() {
        //初始化UI组件
        initView();
        //实例化数据库SQLiteOpenHelper子类对象
        DaoSession daoSession = PPaoApplication.getDaoSession();
        searchHistoryModelDao = daoSession.getSearchHistoryModelDao();
        // 第一次进入时查询所有的历史记录
        queryData("");
        //"清空搜索历史"按钮
        tv_clear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                createDeleteNoteDialog();
            }
        });

        action.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (action.getText().toString().equals(getResources().getString(R.string.action_cancel)))
                    ((Activity) context).finish();
                else {
                    searchContent();
                }

            }
        });

        //搜索框的文本变化实时监听
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!TextUtils.isEmpty(s)) {
                    action.setText(getResources().getString(R.string.action_search));
                } else {
                    action.setText(getResources().getString(R.string.action_cancel));
                }
                LogUtility.debug(TAG, "onTextChanged:  s" + s + " start" + start + "before" + before + "count" + count);
            }

            //输入后调用该方法
            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() == 0) {
                    //若搜索框为空,则模糊搜索空字符,即显示所有的搜索历史
                    tv_tip.setText("搜索历史");
                } else {
                    tv_tip.setText("搜索结果");
                }
                //每次输入后都查询数据库并显示
                //根据输入的值去模糊查询数据库中有没有数据
                String tempName = et_search.getText().toString();
                queryData(tempName);

            }
        });


        // 搜索框的键盘搜索键
        // 点击回调
        et_search.setOnKeyListener(new OnKeyListener() {// 输入完后按键盘上的搜索键


            // 修改回车键功能
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
                    // 隐藏键盘，这里getCurrentFocus()需要传入Activity对象，如果实际不需要的话就不用隐藏键盘了，免得传入Activity对象，这里就先不实现了
//                    ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
//                            getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    searchContent();
                }
                return false;
            }
        });


        //列表监听
        //即当用户点击搜索历史里的字段后,会直接将结果当作搜索字段进行搜索
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //获取到用户点击列表里的文字,并自动填充到搜索框内
                TextView textView = (TextView) view.findViewById(R.id.tv_search_content);
                String name = textView.getText().toString();
                et_search.setText(name);
                if (searchListener != null)
                    searchListener.search_out(name);

            }
        });

//        // 插入数据，便于测试，否则第一次进入没有数据怎么测试呀？
//        Date date = new Date();
//        long time = date.getTime();
//        insertData("Leo" + time);

        //点击搜索按钮后的事件
        iv_search.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                searchContent();
            }
        });


    }

    private void searchContent() {
        // 按完搜索键后将当前查询的关键字保存起来,如果该关键字已经存在就不执行保存
        String search_content = et_search.getText().toString().trim();
        boolean hasData = hasData(search_content);
        if (!hasData) {
            insertData(et_search.getText().toString().trim());
            queryData("");
        }
        //根据输入的内容模糊查询商品，并跳转到另一个界面，这个需要根据需求实现
        if (searchListener != null)
            searchListener.search_out(search_content);
    }


    /**
     * 封装的函数
     */

    /*初始化组件*/
    private void initView() {
        LayoutInflater.from(context).inflate(R.layout.search_layout, this);
        et_search = (EditText) findViewById(R.id.et_search);
        tv_clear = (TextView) findViewById(R.id.tv_clear);
        tv_tip = (TextView) findViewById(R.id.tv_tip);
        listView = (Search_ListView) findViewById(R.id.listView);
        action = (TextView) findViewById(R.id.tv_action);
        iv_search = (ImageView) findViewById(R.id.iv_title_search);
    }

    /*插入数据*/
    private void insertData(String tempName) {
        BrowserCache browserCache = new BrowserCache(context);
        if (!TextUtils.isEmpty(tempName) && !browserCache.getDoNotTrackEnabled()) {
            SearchHistoryModel searchHistoryModel = new SearchHistoryModel();
            searchHistoryModel.setSearch_content(tempName);
            searchHistoryModel.setTime(System.currentTimeMillis());
            searchHistoryModelDao.insertOrReplace(searchHistoryModel);
        }
    }

    /*模糊查询数据 并显示在ListView列表上*/
    private void queryData(String tempName) {
        //模糊搜索
        Query<SearchHistoryModel> query = searchHistoryModelDao.queryBuilder().where(SearchHistoryModelDao.Properties.Search_content.like("%" + tempName + "%")).build();
        List<String> query_List = new ArrayList<>();
        // 创建adapter适配器对象,装入模糊搜索的结果
        for (SearchHistoryModel search : query.list()
                ) {
            query_List.add(search.getSearch_content());
        }
        adapter = new SearchAdapter(context, query_List);
        // 设置适配器
        listView.setAdapter(adapter);
    }

    /*检查数据库中是否已经有该条记录*/
    private boolean hasData(String tempName) {
        Query<SearchHistoryModel> query = searchHistoryModelDao.queryBuilder().where(SearchHistoryModelDao.Properties.Search_content.eq(tempName)).build();
        if (query.list().size() > 0) return true;
        else return false;
    }

    /*清空数据*/
    private void deleteData() {
        searchHistoryModelDao.deleteAll();
    }

    public void setSearchListener(SearchListener searchListener) {
        this.searchListener = searchListener;
    }

    public void removeSearchListener() {
        this.searchListener = null;
    }

    public static class SearchAdapter extends BaseAdapter {
        List<String> list = new ArrayList<>();
        private Context context;

        public SearchAdapter(Context context, List<String> query_list) {
            if (query_list != null && query_list.size() > 0)
                this.list = query_list;
            this.context = context;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.search_list, null);
                holder = new ViewHolder();
                holder.icon_left = (ImageView) convertView.findViewById(R.id.icon_left);
                holder.icon_right = (ImageView) convertView.findViewById(R.id.icon_right);
                holder.text = (TextView) convertView.findViewById(R.id.tv_search_content);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            String content = list.get(position);
            holder.icon_left.setImageResource(R.drawable.icon_search);
            holder.text.setText(content);
           /* if (UrlUtils.isUrlFormat(String.format(Constants.HTTP_FORMAT, content))) {
                //if (UrlUtils.isUrlFormat(content)) {
                holder.icon_left.setImageResource(R.drawable.icon_search_left);
                holder.text.setText(content);
            } else
            {
                holder.icon_left.setImageResource(R.drawable.icon_search);
                holder.text.setText(content);
            }*/
            return convertView;
        }

        public static class ViewHolder {
            ImageView icon_left;
            ImageView icon_right;
            TextView text;
        }
    }

    public void createDeleteNoteDialog() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_setting_update, null);
        final Dialog dialog = DialogUtils.createDialog(context, view);
        final TextView tv_title = (TextView) view.findViewById(R.id.title);
        final TextView tv_update_next = (TextView) view.findViewById(R.id.btn_left);
        final TextView tv_update_now = (TextView) view.findViewById(R.id.btn_right);
        tv_title.setText("清空搜索历史？");
        tv_update_next.setText("取消");
        tv_update_now.setText("清空");
        tv_update_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                dialog.dismiss();
            }
        });
        tv_update_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //清空数据库
                deleteData();
                queryData("");
                dialog.cancel();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
