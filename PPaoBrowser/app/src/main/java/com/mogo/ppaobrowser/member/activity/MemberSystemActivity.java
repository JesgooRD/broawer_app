package com.mogo.ppaobrowser.member.activity;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.activity.PPaoBaseActivity;
import com.mogo.ppaobrowser.browser.interface_package.DataAccessListener;
import com.mogo.ppaobrowser.member.bean.responseBean.MemberBean;
import com.mogo.ppaobrowser.member.biz.UserBiz;
import com.mogo.ppaobrowser.preference.MemberCache;
import com.umeng.analytics.MobclickAgent;

import butterknife.BindView;
import butterknife.OnClick;

public class MemberSystemActivity extends PPaoBaseActivity {

    private static final String TAG = "MemberSystemActivity";
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.member_photo)
    SimpleDraweeView memberPhoto;
    @BindView(R.id.tvAccount)
    TextView tvAccount;
    @BindView(R.id.tv_gold)
    TextView tvGold;
    @BindView(R.id.tv_cash)
    TextView tvCash;
    @BindView(R.id.item_book_rack)
    RelativeLayout itemBookRack;
    @BindView(R.id.item_task)
    RelativeLayout itemTask;
    @BindView(R.id.item_conversion)
    RelativeLayout itemConversion;
    @BindView(R.id.item_income)
    RelativeLayout itemIncome;
    @BindView(R.id.item_rule)
    RelativeLayout itemRule;
    @BindView(R.id.item_message)
    RelativeLayout itemMessage;

    UserBiz userBiz = new UserBiz();


    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("个人中心");
        Log.d(TAG, "onResume: ");
        getUserInfo();
        memberPhoto.setImageURI(MemberCache.getMemHeadUrl());
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("个人中心");
    }

    @Override
    protected void updateViews(boolean b) {

    }

    @Override
    protected void initViews() {
        setSupportActionBar(toolbar);
    }

    private void getUserInfo() {
        userBiz.fetchUserInfo(new DataAccessListener<MemberBean>() {
            @Override
            public void requestSuccess(MemberBean ob) {
                if (ob != null) {
                    Log.d(TAG, "gold " + ob.gold + " cash " + ob.gold);
                    memberPhoto.setImageURI(ob.headUrl);
                    tvCash.setText(ob.cash + "");
                    tvGold.setText(ob.gold + "");
                    toolbarTitle.setText(ob.nickName);
                }
            }

            @Override
            public void requestFail(String error) {

            }
        });
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_member_system;
    }

    @OnClick({R.id.member_photo, R.id.tvAccount, R.id.item_book_rack, R.id.item_task, R.id.item_conversion, R.id.item_income, R.id.item_rule, R.id.item_message})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.member_photo:
            case R.id.tvAccount:
                census("mem_person");
                startActivityWithZoom(new Intent(MemberSystemActivity.this, PersonInfoActivity.class));
                break;
            case R.id.item_book_rack:
                break;
            case R.id.item_task:
                census("mem_task");
                startActivityWithZoom(new Intent(MemberSystemActivity.this, TaskHallActivity.class));
                break;
            case R.id.item_conversion:
                census("mem_conversion");
                startActivityWithZoom(new Intent(MemberSystemActivity.this, PayMoneyActivity.class));
                break;
            case R.id.item_income:
                census("mem_income");
                startActivityWithZoom(new Intent(MemberSystemActivity.this, IncomeDetailActivity.class));
                break;
            case R.id.item_rule:
                census("mem_rule");
                startActivityWithZoom(new Intent(MemberSystemActivity.this, CashRuleActivity.class));
                break;
            case R.id.item_message:
                break;
        }
    }

    private void census(String tag) {
        MobclickAgent.onEvent(this, tag);
    }
}
