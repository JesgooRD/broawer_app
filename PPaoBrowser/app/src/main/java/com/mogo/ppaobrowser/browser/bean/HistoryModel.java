package com.mogo.ppaobrowser.browser.bean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * Created by doyer on 2017/5/23.
 */

@Entity
public class HistoryModel {
    @Id(autoincrement = true)
    private Long id;
    String name;
    String url;
    long addtime;

    public HistoryModel(String name, String url, long addtime) {
        this.name = name;
        this.url = url;
        this.addtime = addtime;
    }

    public HistoryModel() {

    }

    @Generated(hash = 1183746184)
    public HistoryModel(Long id, String name, String url, long addtime) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.addtime = addtime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getAddtime() {
        return addtime;
    }

    public void setAddtime(long addtime) {
        this.addtime = addtime;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
