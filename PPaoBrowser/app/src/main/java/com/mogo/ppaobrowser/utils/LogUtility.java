package com.mogo.ppaobrowser.utils;

import android.util.Log;

/**
 *
 */
public class LogUtility {

    private final static String TAG = "ppao";

    public static void setEnable(boolean enable) {
        isEnabled = enable;
    }

    private static boolean isEnabled = false;

    public static void debugFormat(String format, Object... args) {
        if (isEnabled && format != null) {
            if (args == null || args.length == 0) {
                Log.d(TAG, format);
            } else {
                Log.d(TAG, String.format(format, args));
            }
        }
    }

    public static void debug(String TAG, String args) {
        if (isEnabled && args != null) {
            if (args == null || args.length() == 0) {
                Log.d(TAG, args);
            }
        }
    }

    public static void info(String format, Object... args) {
        if (format != null) {
            if (args == null || args.length == 0) {
                Log.d(TAG, format);
            } else {
                Log.d(TAG, String.format(format, args));
            }
        }
    }

    public static void e(String format, Object... args) {
        Log.e(TAG, String.format(format, args));
    }

    public static void e(Throwable error) {
        if (error != null && error.getMessage() != null) {
            Log.e(TAG, error.getMessage());
        }
    }
}
