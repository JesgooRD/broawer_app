package com.mogo.ppaobrowser.browser.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.mogo.ppaobrowser.BuildConfig;
import com.mogo.ppaobrowser.Constants;
import com.mogo.ppaobrowser.PPaoApplication;
import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.interface_package.DataAccessListener;
import com.mogo.ppaobrowser.preference.BrowserCache;
import com.mogo.ppaobrowser.update.ApkUpdateUtils;
import com.mogo.ppaobrowser.utils.AppUtility;
import com.mogo.ppaobrowser.utils.HttpUtil;
import com.mogo.ppaobrowser.utils.ToastUtils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingActivity extends PPaoBaseActivity {

    private static final String TAG = "SettingActivity";
    private static final int NO_NEED_UPDATE = 0x0024;
    private static final int NEED_UPDATE = 0x0025;
    @BindView(R.id.block_image)
    SwitchCompat blockImage;
    @BindView(R.id.block_ad)
    SwitchCompat blockAd;
    @BindView(R.id.block_trace)
    SwitchCompat blockTrace;
    @BindView(R.id.block_download)
    SwitchCompat blockDownload;
    @BindView(R.id.tv_feed_back)
    TextView tvFeedBack;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_check_version)
    TextView tvCheckVersion;
    @BindView(R.id.tv_version)
    TextView tvVersion;

    BrowserCache preferenceManager;
    Handler handler = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
            if (msg.what == NO_NEED_UPDATE) {
                ToastUtils.show(SettingActivity.this, "当前已是最新版本！");
            } else if (msg.what == NEED_UPDATE) {
                String update_url = (String) msg.obj;
                BrowserCache browserCache = new BrowserCache(PPaoApplication.getApplication());
                if (!browserCache.getDownloadLimitkEnabled()) {
                    createUpdateAppDialog(update_url,true);
                } else {
                    if (AppUtility.isWifi(PPaoApplication.getApplication())) {
                        ApkUpdateUtils.download(SettingActivity.this, update_url, "泡泡浏览器");
                    } else {
                        createUpdateAppDialog(update_url,false);
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    protected void updateViews(boolean b) {

    }

    @Override
    protected void initViews() {
        preferenceManager = new BrowserCache(this);
        initSettingView();
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_setting;
    }

    private void initSettingView() {
        setSupportActionBar(toolbar);
        toolbarTitle.setText(getResources().getString(R.string.action_setting));
        blockImage.setChecked(preferenceManager.getBlockImagesEnabled());
        blockAd.setChecked(preferenceManager.getAdBlockEnabled());
        blockTrace.setChecked(preferenceManager.getDoNotTrackEnabled());
        blockDownload.setChecked(preferenceManager.getDownloadLimitkEnabled());
        tvVersion.setText("当前版本号： " + BuildConfig.VERSION_NAME);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        exitSetting();
    }

    private void exitSetting() {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putBoolean("image", blockImage.isChecked());
        intent.putExtras(bundle);
        setResult(0, intent);
        finish();
    }

    @OnClick({/*R.id.title_back,*/ R.id.block_image, R.id.block_ad, R.id.block_trace, R.id.block_download, R.id.tv_feed_back, R.id.tv_check_version})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            case R.id.title_back:
//                exitSetting();
//                break;
            case R.id.block_image:
                preferenceManager.setBlockImagesEnabled(blockImage.isChecked());
                break;
            case R.id.block_ad:
                preferenceManager.setAdBlockEnabled(blockAd.isChecked());
                break;
            case R.id.block_trace:
                preferenceManager.setDoNotTrackEnabled(blockTrace.isChecked());
                break;
            case R.id.block_download:
                preferenceManager.setDownloadLimitEnabled(blockDownload.isChecked());
                break;
            case R.id.tv_feed_back:
                startActivity(new Intent(SettingActivity.this, FeedBackActivity.class));
                overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
                break;
            case R.id.tv_check_version:
                HttpUtil.HttpGet(Constants.GET_VERSION, new DataAccessListener() {
                    @Override
                    public void requestSuccess(Object ob) {
                        try {
                            JSONObject jsonObject = new JSONObject(ob.toString());
                            int version = AppUtility.getVersionCode(SettingActivity.this);
//                            int version = 0;
                            int update_version = jsonObject.optInt("version_code", version);
                            Log.d(TAG, "本地version " + version + " 最新version" + update_version);
                            if (version < update_version) {
                                Message message = Message.obtain();
                                String update_url = jsonObject.optString("app_url");
                                Log.d(TAG, "requestSuccess: " + update_url);
                                if (!TextUtils.isEmpty(update_url)) {
                                    message.what = NEED_UPDATE;
                                    message.obj = update_url;
                                    handler.sendMessage(message);
                                }
                            } else {
                                Message message = Message.obtain();
                                message.what = NO_NEED_UPDATE;
                                handler.sendMessage(message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void requestFail(String error) {

                    }
                });
                break;
        }
    }

}
