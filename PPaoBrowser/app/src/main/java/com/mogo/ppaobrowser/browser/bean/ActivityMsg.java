package com.mogo.ppaobrowser.browser.bean;

/**
 * Created by doyer on 17/5/31.
 */
public enum ActivityMsg {

    BACK(0),
    BROWSER2MAIN(1),
    SELECT_WINDOW(2),
    UPDATE_APK(3),
    CLOSE_WINDOW(4);



    int value;

    ActivityMsg(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ActivityMsg getActivityMsg(int value) {
        ActivityMsg[] values = ActivityMsg.values();
        for (int i = 0; i < values.length; i++) {
            if (values[i].getValue() == value) {
                return values[i];
            }
        }
        return null;
    }
}
