package com.mogo.ppaobrowser.browser.controller;

import android.os.Message;
import android.support.annotation.Nullable;

/**
 * Created by doyer on 2017/6/21.
 */

public interface UIController {

    void updateUrl(@Nullable String title, boolean shortUrl);

    void onCreateWindow(Message resultMsg);

}
