package com.mogo.ppaobrowser.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by doyer on 2017/5/25.
 */
public class WebViewPager extends ViewPager {

    //左右滑动flag
    private boolean enabled = true;

    public WebViewPager(Context context) {
        super(context);
    }

    public WebViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean getScrollable() {
        return enabled;
    }

    public void setScrollable(boolean scrollable) {
        this.enabled = scrollable;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // 触摸事件不触发
        if (this.enabled) {
            return super.onTouchEvent(event);
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // 不处理触摸拦截事件
        if (this.enabled) {
            return super.onInterceptTouchEvent(event);
        }
        return false;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        // 分发事件，这个是必须要的，如果把这个方法覆盖了，那么ViewPager的子View就接收不到事件了
        if (this.enabled) {
            return super.dispatchTouchEvent(event);
        }
        return super.dispatchTouchEvent(event);
    }

    public void setPagingEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
