package com.mogo.ppaobrowser.browser.interface_package;


/**
 * Created by doyer on 2017/5/24.
 */
public interface SaveDataListener {

    void createSuccess(Object object);

    void editSuccess(Object object);

    void saveCancel();
}
