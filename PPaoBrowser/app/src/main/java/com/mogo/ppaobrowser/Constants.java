package com.mogo.ppaobrowser;

import android.os.Environment;

/**
 * Created by doyer on 2017/5/24.
 */

public final class Constants {
    // Search query URLs
    public static final String YAHOO_SEARCH = "https://search.yahoo.com/search?p=";
    public static final String GOOGLE_SEARCH = "https://www.google.com/search?client=lightning&ie=UTF-8&oe=UTF-8&q=";
    public static final String BING_SEARCH = "https://www.bing.com/search?q=";
    public static final String DUCK_SEARCH = "https://duckduckgo.com/?t=lightning&q=";
    public static final String DUCK_LITE_SEARCH = "https://duckduckgo.com/lite/?t=lightning&q=";
    public static final String STARTPAGE_MOBILE_SEARCH = "https://startpage.com/do/m/mobilesearch?language=english&query=";
    public static final String STARTPAGE_SEARCH = "https://startpage.com/do/search?language=english&query=";
    public static final String ASK_SEARCH = "http://www.ask.com/web?qsrc=0&o=0&l=dir&qo=LightningBrowser&q=";
    public static final String BAIDU_SEARCH = "https://www.baidu.com/s?wd=";
    public static final String YANDEX_SEARCH = "https://yandex.ru/yandsearch?lr=21411&text=";
    public static final String HTTP_FORMAT = "http://%s/";
    // Custom local page schemes
    public static final String SCHEME_HOMEPAGE = "about:home";
    // Default text encoding we will use
    public static final String DEFAULT_ENCODING = "UTF-8";

    public static String JS_FORMAT = "javascript:%s";
    public static String JS_STR = "javascript:%s";

    //public static String FEED_BACK = "http://rcv.moogos.com/newsdk?t=%s&h1=%s&user=%s&n1=%s&pkg=%s&op1=%s&op2=%s&c=%s&op3=%s&ts=%s&v=%s&n2=%s&h2=%s";
    public static String PPAO = "paopao";
    public static String BLOG_VALUE = "blog";
    public static String APP_CACAHE_DIRNAME = "ppao_cache";
    public static String GET_VERSION = "http://static.moogos.com/paopao/v.json";
    public static String HOME_PAGE = "首页";
    public static String DOWNLOAD_PATH = Environment.getExternalStorageDirectory().getPath() + "/Download";


}
