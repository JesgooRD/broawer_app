package com.mogo.ppaobrowser.member.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.activity.PPaoBaseActivity;
import com.mogo.ppaobrowser.browser.interface_package.DataAccessListener;
import com.mogo.ppaobrowser.member.bean.responseBean.IncomeResbean;
import com.mogo.ppaobrowser.member.bean.responseBean.MemberBean;
import com.mogo.ppaobrowser.member.biz.UserBiz;
import com.mogo.ppaobrowser.member.fragment.TabIncomeFragment;
import com.mogo.ppaobrowser.utils.DateTimeUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;

/**
 * Created by doyer on 2017/7/19.
 */

public class IncomeDetailActivity extends PPaoBaseActivity {

    private static final String TAG = "IncomeDetailActivity";
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_right_image)
    ImageView toolbarRightImage;
    @BindView(R.id.income_tab)
    TabLayout incomeTab;
    @BindView(R.id.income_viewpager)
    ViewPager incomePager;
    @BindView(R.id.tv_today_gold)
    TextView tvTodayGold;
    @BindView(R.id.tv_yesterday_cash)
    TextView tvYesterdayCash;
    @BindView(R.id.tv_remain_cash)
    TextView tvRemainCash;

    private List<String> tabIndicators = new ArrayList<>();
    private List<Fragment> tabFragments = new ArrayList<>();
    private ContentPagerAdapter contentAdapter;
    UserBiz userBiz = new UserBiz();
    List<IncomeResbean> list = new ArrayList<>();
    int today_gold;
    float yesterday_cash;
    TabIncomeFragment cashIncomeFragment;
    TabIncomeFragment goldIncomeFragment;
    MemberBean member;

    @Override
    protected void updateViews(boolean b) {

    }

    @Override
    protected void initViews() {
        setSupportActionBar(toolbar);
        toolbarTitle.setText(getString(R.string.income_detail));
        if (userBiz.getDefault() != null) {
            member = userBiz.getDefault();
        }
        if (member != null)
            tvRemainCash.setText("¥ " + member.cash);
        initTab();
        initContent();
    }

    private void initTab() {
        tabIndicators.add(getString(R.string.income_gold));
        tabIndicators.add(getString(R.string.income_cash));
        incomeTab.setupWithViewPager(incomePager);
        cashIncomeFragment = (TabIncomeFragment) TabIncomeFragment.newInstance(getString(R.string.income_cash));
        goldIncomeFragment = (TabIncomeFragment) TabIncomeFragment.newInstance(getString(R.string.income_gold));
        tabFragments.add(goldIncomeFragment);
        tabFragments.add(cashIncomeFragment);
    }


    private void initContent() {
        contentAdapter = new ContentPagerAdapter(getSupportFragmentManager());
        incomePager.setAdapter(contentAdapter);
        userBiz.getIncome(new DataAccessListener<List<IncomeResbean>>() {
            @Override
            public void requestSuccess(List<IncomeResbean> ob) {
                list = ob;
                if (list != null && list.size() > 0) {
                    cashIncomeFragment.setData(list);
                    goldIncomeFragment.setData(list);
                    for (IncomeResbean income : ob
                            ) {
                        if (!(income.addGoldCoin < 0 && income.addCash > 0)) {
                            if (DateTimeUtils.getDisNow(income.eventTime * 1000) == 0) {
                                today_gold = today_gold + income.addGoldCoin;
                            }
                            if (DateTimeUtils.getDisNow(income.eventTime * 1000) == 1) {
                                yesterday_cash = yesterday_cash + income.addCash;
                            }
                        } else {
                            yesterday_cash = yesterday_cash + income.addCash;
                        }
                    }
                    tvTodayGold.setText(today_gold + "");
                    tvYesterdayCash.setText("¥ " + yesterday_cash);
                }
            }

            @Override
            public void requestFail(String error) {

            }
        });
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.income_layout;
    }

    private class ContentPagerAdapter extends FragmentPagerAdapter {
        public ContentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return tabFragments.get(position);
        }

        @Override
        public int getCount() {
            return tabFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabIndicators.get(position);
        }

    }
}
