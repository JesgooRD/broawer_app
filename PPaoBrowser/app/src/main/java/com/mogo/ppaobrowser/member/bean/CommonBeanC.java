package com.mogo.ppaobrowser.member.bean;

/**
 * Created by doyer on 2017/7/17.
 */

public class CommonBeanC {

    /**
     * code : 200
     * data : 0
     * message : shared
     */

    private int code;
    private int data;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
