package com.mogo.ppaobrowser.member.api;


import com.mogo.ppaobrowser.member.bean.CommonBean;
import com.mogo.ppaobrowser.member.bean.CommonBeanB;
import com.mogo.ppaobrowser.member.bean.TransferResNote;
import com.mogo.ppaobrowser.member.bean.responseBean.ComGoldResBean;
import com.mogo.ppaobrowser.member.bean.responseBean.IconResBean;
import com.mogo.ppaobrowser.member.bean.responseBean.IncomeResbean;
import com.mogo.ppaobrowser.member.bean.responseBean.LoginResBean;
import com.mogo.ppaobrowser.member.bean.responseBean.MemberBean;
import com.mogo.ppaobrowser.member.bean.responseBean.RegisterBean;
import com.mogo.ppaobrowser.member.bean.responseBean.ResponseBean;
import com.mogo.ppaobrowser.member.bean.responseBean.SignResBean;
import com.mogo.ppaobrowser.member.bean.responseBean.TaskResbean;
import com.mogo.ppaobrowser.member.constant.HttpConstant;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by doyer.du on 2016/10/14.
 * 接口服务类 可以将所有接口都写在该类中
 */

public interface ApiService {

    @GET(HttpConstant.WEB_ACTION)
    Call<LoginResBean<MemberBean>> login(@Query("type") String type, @Query("userId") int userId);

    @POST(HttpConstant.WEB_ACTION)
    Call<RegisterBean<MemberBean>> register(@Query("type") String type, @Body RequestBody body);

    @GET(HttpConstant.TYPE_SIGN_IN)
    Call<ResponseBean<SignResBean>> signIn(@Query("userId") int userId);

    @GET(HttpConstant.TYPE_SHARE)
    Call<ComGoldResBean> shareAction(@Query("userId") int userId);

    @POST(HttpConstant.TYPE_UPDATE)
    Call<ComGoldResBean> updateAction(@Body RequestBody body);

    @POST(HttpConstant.TYPE_TRANSFER)
    Call<ResponseBean> transferCash(@Body RequestBody body);

    @GET(HttpConstant.TYPE_PAY_NOTE)
    Call<List<TransferResNote>> transferHistory(@Query("userId") int userId);

    @GET(HttpConstant.TYPE_INCOME)
    Call<List<IncomeResbean>> getIncome(@Query("userId") int userId);

    @GET(HttpConstant.TYPE_READ)
    Call<ComGoldResBean> readAward(@Query("userId") int userId, @Query("type") int readId);

    @GET(HttpConstant.TASK_STATE)
    Call<List<TaskResbean>> getTaskState(@Query("userId") int userId);

    @POST(HttpConstant.TASK_COMPLAIN)
    Call<ResponseBean> complainApp(@Body RequestBody body);

    @GET(HttpConstant.ICON)
    Call<IconResBean> getIcon();
}
