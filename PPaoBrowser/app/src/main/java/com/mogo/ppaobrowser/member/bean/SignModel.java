package com.mogo.ppaobrowser.member.bean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by doyer on 2017/7/21.
 */

@Entity
public class SignModel {

    @Id(autoincrement = true)
    private Long id;
    //用户ID
    int userId;
    //存储的时间
    long time;
    //当钱签到天数
    int count;
    @Generated(hash = 608285264)
    public SignModel(Long id, int userId, long time, int count) {
        this.id = id;
        this.userId = userId;
        this.time = time;
        this.count = count;
    }
    @Generated(hash = 561380475)
    public SignModel() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public int getUserId() {
        return this.userId;
    }
    public void setUserId(int userId) {
        this.userId = userId;
    }
    public long getTime() {
        return this.time;
    }
    public void setTime(long time) {
        this.time = time;
    }
    public int getCount() {
        return this.count;
    }
    public void setCount(int count) {
        this.count = count;
    }

}
