package com.mogo.ppaobrowser.browser.base;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mogo.ppaobrowser.Constants;
import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.activity.PPaoBaseActivity;
import com.mogo.ppaobrowser.browser.controller.MultiWindowManager;
import com.mogo.ppaobrowser.browser.fragment.MainFragment;
import com.mogo.ppaobrowser.browser.fragment.WebViewFragment;
import com.mogo.ppaobrowser.browser.interface_package.FragmentBackListener;
import com.mogo.ppaobrowser.update.ApkUpdateUtils;
import com.mogo.ppaobrowser.utils.AppUtility;
import com.mogo.ppaobrowser.utils.DialogUtils;
import com.umeng.analytics.MobclickAgent;

import butterknife.ButterKnife;

/**
 * Created by doyer on 2017/5/22.
 */
public abstract class BrowserBaseActivity<T extends IBasePresenter> extends FragmentActivity {
    private static final String TAG = "BaseActivity";

    public FragmentBackListener backListener;
    private boolean isInterception = false;
    protected long exitTime = 0;
    public BaseFragment baseFragment;

    public static final String PERMISSION_READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    public static final String PERMISSION_WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public static final String PERMISSION_READ_PHONE_STATE = Manifest.permission.READ_PHONE_STATE;
    public static final int request_code0 = 0x2032;
    public static final int request_code1 = 0x2033;


    private static final String[] requestPermissions = {
            PERMISSION_READ_EXTERNAL_STORAGE,
            PERMISSION_WRITE_EXTERNAL_STORAGE
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Log.d(TAG, "onCreate: ");
        MobclickAgent.setDebugMode(true);
        setContentView(attachLayoutRes());
        ButterKnife.bind(this);
        initViews();
        updateViews(false);
        if (AppUtility.isMarshmallow() && !AppUtility.isGranted_(this, PERMISSION_WRITE_EXTERNAL_STORAGE)) {
            requestPermission(requestPermissions, request_code0);
        }

    }

    protected abstract void updateViews(boolean b);

    protected abstract void initViews();

    protected abstract int attachLayoutRes();

    public FragmentBackListener getBackListener() {
        return backListener;
    }

    public void setBackListener(FragmentBackListener backListener) {
        this.backListener = backListener;
    }

    public boolean isInterception() {
        return isInterception;
    }

    public void setInterception(boolean isInterception) {
        this.isInterception = isInterception;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isInterception()) {
                if (backListener != null) {
                    backListener.onbackForward();
                    return false;
                }
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public void setBrowserFragment(String url, int index) {
        WebViewFragment fragment = MultiWindowManager.mulWindowBrowserList.get(index);
        if (fragment == null) {
            fragment = WebViewFragment.newInstance(url, false);
            MultiWindowManager.mulWindowBrowserList.set(index, fragment);
        } else {
            fragment.update(url);
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.activity_fade_in, R.anim.activity_fade_out);
        //我之前把hide注释掉，导致多页面webviewfragment跳转时之前的页面无法隐藏,但是做hide操作的话又使得fragment变形
        hideAllFragment(transaction, fragment);
        transaction.commit();
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        if (fragment.isAdded()) {
            transaction.show(fragment);
        } else {
            transaction.add(R.id.frame_layout, fragment, WebViewFragment.TAG + index);
        }
        baseFragment = fragment;
    }

    /**
     * 我这边排除了很久的原因 并不确定具体情况是什么 但是这个方法导致了fragment与顶部有一个空白的空间
     * 然后顶部有时会出现变形的情况！！！
     */
    private void hideAllFragment(FragmentTransaction transaction, Fragment fragment) {

        for (int i = 0; i < MultiWindowManager.mulWindowBrowserList.size(); i++) {
            Fragment f = MultiWindowManager.mulWindowBrowserList.get(i);
            if (f != null && f.isAdded() && !f.isHidden() && f != fragment) {
                transaction.hide(f);
            }

            Fragment f2 = MultiWindowManager.mulWindowMainList.get(i);
            if (f2 != null && f2.isAdded() && !f2.isHidden() && f2 != fragment) {
                transaction.hide(f2);
            }
        }
    }

    public void setMainFragment(int index) {
        //url将会成为之后判断的一个key值
        MultiWindowManager multiWindowManager = MultiWindowManager.mulWindowDataList.get(index);
        if (multiWindowManager == null)
            multiWindowManager = new MultiWindowManager();

        multiWindowManager.setUrl(null);
        multiWindowManager.setTitle(Constants.HOME_PAGE);
        multiWindowManager.setDrawable(getResources().getDrawable(R.drawable.main_page));

        MainFragment fragment = MultiWindowManager.mulWindowMainList.get(index);
        if (fragment == null) {
            fragment = MainFragment.newInstance();
            MultiWindowManager.mulWindowMainList.set(index, fragment);
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.activity_fade_in, R.anim.activity_fade_out);
        hideAllFragment(transaction, fragment);
        if (fragment.isAdded()) {
            transaction.show(fragment);
        } else {
            transaction.add(R.id.frame_layout, fragment, MainFragment.TAG + index);
        }
        transaction.commit();
        baseFragment = fragment;
    }

    private void requestPermission(String[] permissions, int requestCode) {
        ActivityCompat.requestPermissions(this, requestPermissions, requestCode);
    }

    protected void startActivityWithZoom(Intent intent) {
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == request_code1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //已获取权限

                //响应事件
            } else {
                //权限被拒绝
                final AlertDialog.Builder normalDialog =
                        new AlertDialog.Builder(BrowserBaseActivity.this);
                normalDialog.setTitle("下载文件需要存储权限，去往设置界面授权？");
                normalDialog.setPositiveButton("确定",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                AppUtility.getAppDetailSettingIntent(BrowserBaseActivity.this);
                            }
                        });
                normalDialog.setNegativeButton("关闭",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                // 显示
                normalDialog.show();
            }
        }
    }

    public void createUpdateAppDialog(final String url, final boolean flag) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.dialog_setting_update, null);
        final Dialog dialog = DialogUtils.createDialog(this, view);
        final LinearLayout ll_content = (LinearLayout) view.findViewById(R.id.ll_content);
        final TextView tv_update_next = (TextView) view.findViewById(R.id.btn_left);
        final TextView tv_update_now = (TextView) view.findViewById(R.id.btn_right);
        tv_update_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                dialog.dismiss();
            }
        });
        tv_update_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag) {
                    ApkUpdateUtils.download(BrowserBaseActivity.this, url, "泡泡浏览器");
                } else createWifiDownDialog(url);
                dialog.cancel();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void createWifiDownDialog(final String url) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.dialog_setting_update, null);
        final Dialog dialog = DialogUtils.createDialog(this, view);
        final LinearLayout ll_content = (LinearLayout) view.findViewById(R.id.ll_content);
        final TextView tv_left = (TextView) view.findViewById(R.id.btn_left);
        final TextView tv_right = (TextView) view.findViewById(R.id.btn_right);
        final TextView tv_title = (TextView) view.findViewById(R.id.title);
        tv_title.setText(getString(R.string.update_wifi_warning));
        tv_left.setText(getString(R.string.action_cancel));
        tv_right.setText(getString(R.string.update_ensure_download));
        tv_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                dialog.dismiss();
            }
        });
        tv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApkUpdateUtils.download(BrowserBaseActivity.this, url, "泡泡浏览器");
                dialog.cancel();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /**
     * 隐藏软键盘
     *
     * @param view
     */
    protected void closeWindowSoftInput(View view) {
        InputMethodManager imm = (InputMethodManager) getApplicationContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        boolean isSoftActive = imm.isActive();
        if (isSoftActive) {
            imm.hideSoftInputFromWindow(
                    view.getApplicationWindowToken(), 0); // 强制隐藏键盘
        }
    }


}
