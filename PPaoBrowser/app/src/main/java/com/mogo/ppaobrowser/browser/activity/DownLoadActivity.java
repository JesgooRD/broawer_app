package com.mogo.ppaobrowser.browser.activity;

import android.app.DownloadManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mogo.ppaobrowser.PPaoApplication;
import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.bean.DownloadModel;
import com.mogo.ppaobrowser.gen.DownloadModelDao;
import com.mogo.ppaobrowser.utils.AppUtility;
import com.mogo.ppaobrowser.widget.OwnItemDecoration;

import org.greenrobot.greendao.query.Query;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by doyer on 2017/6/29.
 */

/**
 * Created by doyer on 2017/6/29.
 */

public class DownLoadActivity extends PPaoBaseActivity {

    private static final String TAG = "DownLoadActivity";
    @BindView(R.id.download_history)
    RecyclerView downHistory_Recycler;
    @BindView(R.id.empty_layout)
    RelativeLayout emptyLayout;
    @BindView(R.id.download_edit)
    Button downloadEdit;
    @BindView(R.id.download_remove)
    Button downloadRemove;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_memory)
    TextView tvMemory;

    /*download*/
    private List<Boolean> checkList = new ArrayList<>();
    List<DownloadModel> list = new ArrayList<>();
    private DownloadManager mDownloadManager;
    private Handler mHandler = new Handler();
    boolean check_visible;
    DownloadAdapter adapter;
    DownloadModelDao downloadModelDao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @Override
    protected void updateViews(boolean b) {

    }

    @Override
    protected void initViews() {
        setSupportActionBar(toolbar);
        toolbarTitle.setText(getString(R.string.action_download));
        downloadModelDao = PPaoApplication.getDaoSession().getDownloadModelDao();
        mDownloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        Query<DownloadModel> downloadModelQuery = downloadModelDao.queryBuilder().build();
        list = downloadModelQuery.list();
        initCheckList();
        if (list.size() == 0) {
            emptyLayout.setVisibility(View.VISIBLE);
            downHistory_Recycler.setVisibility(View.GONE);
            return;
        } else {
            //创建默认的线性LayoutManager
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
            downHistory_Recycler.setLayoutManager(mLayoutManager);
            adapter = new DownloadAdapter();
            downHistory_Recycler.addItemDecoration(new OwnItemDecoration());
            downHistory_Recycler.setAdapter(adapter);
        }
        // 获得手机内部存储控件的状态
        File dataFileDir = Environment.getDataDirectory();
        String dataMemory = getMemoryInfo(dataFileDir);
        tvMemory.setText(dataMemory);
    }

    private void initCheckList() {
        checkList.clear();
        for (int i = 0; i < list.size(); i++) {
            checkList.add(false);
        }
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.download_history;
    }


    @OnClick({R.id.download_edit, R.id.download_remove/*, R.id.title_back*/})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.download_edit:
                if (check_visible) {
                    DownloadManager downloadmanager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                    int length = checkList.size();
                    for (int i = 0; i < length; i++) {
                        if (checkList.get(i)) {
                            downloadModelDao.delete(list.get(i));
                            deleteDownloadFile(list.get(i).getPath());
                            downloadmanager.remove(list.get(i).getId());
                        }
                    }
                    list.clear();
                    list = downloadModelDao.queryBuilder().build().list();
                    if (list == null)
                        list = new ArrayList<>();
                    initCheckList();
                    downloadEdit.setClickable(true);
                    downloadEdit.setTextColor(// 同时兼容高、低版本
                            ContextCompat.getColor(this, R.color.black));
                    if (adapter != null)
                        adapter.notifyDataSetChanged();
                } else {
                    check_visible = !check_visible;
                    if (adapter != null) {
                        adapter.notifyDataSetChanged();
                        updateBottomState(false);
                    }
                }
                break;
            case R.id.download_remove:
                if (check_visible) {
                    updateBottomState(true);
                    initCheckList();
                    check_visible = !check_visible;
                    if (adapter != null)
                        adapter.notifyDataSetChanged();
                } else {
                    downloadModelDao.deleteAll();
                    list.clear();
                    if (list.size() == 0) {
                        emptyLayout.setVisibility(View.VISIBLE);
                        downHistory_Recycler.setVisibility(View.GONE);
                        return;
                    }
                    adapter.notifyDataSetChanged();
                }
                break;
        }
    }

    private void deleteDownloadFile(String path) {
        File file = new File(path);
        file.delete();
    }

    private class DownloadAdapter extends RecyclerView.Adapter<DownloadAdapter.DownloadHolder> {

        @Override
        public DownloadHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.download_item, parent, false);
            DownloadHolder downloadHolder = new DownloadHolder(view);
            return downloadHolder;
        }

        @Override
        public void onBindViewHolder(final DownloadHolder holder, int position) {
            final DownloadModel downloadModel = list.get(position);
            holder.downloadModel = downloadModel;
            holder.checkBox.setChecked(checkList.get(position));
            if (check_visible) {
                holder.status.setVisibility(View.GONE);
                holder.checkBox.setVisibility(View.VISIBLE);
                initCheckList();
            } else {
                holder.checkBox.setVisibility(View.GONE);
                holder.status.setVisibility(View.VISIBLE);
            }

            if (downloadModel.getStatus() == 0) {
                holder.mDownloadId = downloadModel.getId();
                mHandler.postDelayed(holder.mRefresh, 1000);
            } else if (downloadModel.getStatus() == 1) {
                holder.title.setText(downloadModel.getFile_name());
                holder.content.setText(getFileSize(downloadModel.getTotal()));
                holder.status.setText("打开");
                holder.status.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AppUtility.openFile(DownLoadActivity.this, new File(downloadModel.getPath()));
                    }
                });
            } else {
                holder.title.setText("暂停下载" + downloadModel.getFile_name());
//                holder.status.setText("继续");
                holder.content.setText(getFileSize(downloadModel.getTotal()));
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class DownloadHolder extends RecyclerView.ViewHolder {
            TextView title;
            TextView content;
            ImageView imageView;
            CheckBox checkBox;
            TextView status;
            long mDownloadId;
            DownloadModel downloadModel;
            Runnable mRefresh = new Runnable() {
                @Override
                public void run() {
                    boolean bFinish = false;
                    DownloadManager.Query down_query = new DownloadManager.Query();
                    down_query.setFilterById(mDownloadId);
                    Cursor cursor = mDownloadManager.query(down_query);
                    String file_name = downloadModel.getFile_name();
                    if (cursor.moveToFirst()) {
                        for (; ; cursor.moveToNext()) {
                            int mediaTypeIdx = cursor.getColumnIndex(DownloadManager.COLUMN_MEDIA_TYPE);
                            int totalSizeIdx = cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES);
                            int nowSizeIdx = cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR);
                            int statusIdx = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                            int progress = (int) (100 *
                                    cursor.getLong(nowSizeIdx) / cursor.getLong(totalSizeIdx));
                            String desc = "";
                            desc = String.format("%s媒体类型：%s\n", desc, cursor.getString(mediaTypeIdx));
                            desc = String.format("%s文件总大小：%d\n", desc, cursor.getLong(totalSizeIdx));
                            desc = String.format("%s已下载大小：%d\n", desc, cursor.getLong(nowSizeIdx));
                            desc = String.format("%s下载进度：%d%%\n", desc, progress);
                            if (progress == 100) {
                                title.setText("" + file_name);
                                content.setText(getFileSize((int) cursor.getLong(totalSizeIdx)));
                                status.setText("打开");
                            } else if (progress < 100) {
                                title.setText("正在下载 " + file_name);
                                status.setText("");
                                content.setText(getFileSize((int) cursor.getLong(nowSizeIdx)) + "/" + getFileSize((int) cursor.getLong(totalSizeIdx)));
                            } else if (statusIdx == DownloadManager.STATUS_PAUSED) {
                                title.setText("暂停 " + file_name);
//                                status.setText("继续");
                                content.setText(getFileSize((int) cursor.getLong(nowSizeIdx)) + "/" + getFileSize((int) cursor.getLong(totalSizeIdx)));
                            }
                            Log.d(TAG, "run: " + desc);
                            if (progress == 100) {
                                bFinish = true;
                                downloadModel.setStatus(1);
                                downloadModel.setFile_name(downloadModel.getFile_name());
                                downloadModel.setTotal((int) cursor.getLong(totalSizeIdx));
                                downloadModel.setSofar((int) cursor.getLong(nowSizeIdx));
                                downloadModelDao.insertOrReplace(downloadModel);
                            }
                            if (cursor.isLast() == true) {
                                break;
                            }
                        }
                    }
                    cursor.close();
                    if (bFinish != true) {
                        mHandler.postDelayed(this, 1000);
                    }
                }
            };

            public DownloadHolder(View itemView) {
                super(itemView);
                title = (TextView) itemView.findViewById(R.id.app_status);
                content = (TextView) itemView.findViewById(R.id.app_content);
                imageView = (ImageView) itemView.findViewById(R.id.app_icon);
                checkBox = (CheckBox) itemView.findViewById(R.id.check_select);
                status = (TextView) itemView.findViewById(R.id.download_status);
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        checkList.set(getAdapterPosition(), isChecked);
                        updateDownState();
                        Log.d(TAG, "AdapterPosition: " + getAdapterPosition());
                    }
                });
            }
        }
    }

    private void updateBottomState(boolean init) {
        if (init) {
            downloadRemove.setText("清空");
            downloadEdit.setText("编辑");
            downloadEdit.setClickable(true);
            downloadEdit.setTextColor(// 同时兼容高、低版本
                    ContextCompat.getColor(this, R.color.black));
        } else {
            downloadEdit.setText("删除文件及源任务");
            downloadEdit.setTextColor(// 同时兼容高、低版本
                    ContextCompat.getColor(this, R.color.divider_light));
            downloadEdit.setClickable(false);
            downloadRemove.setText("取消");
        }
    }

    private void updateDownState() {
        boolean flag = false;
        for (int i = 0; i < checkList.size(); i++) {
            if (checkList.get(i)) {
                flag = true;
            }
        }
        if (flag) {
            downloadEdit.setTextColor(// 同时兼容高、低版本
                    ContextCompat.getColor(DownLoadActivity.this, R.color.black));
            downloadEdit.setClickable(true);
        } else {
            downloadEdit.setTextColor(// 同时兼容高、低版本
                    ContextCompat.getColor(DownLoadActivity.this, R.color.divider_light));
            downloadEdit.setClickable(true);
        }
    }

    private String getFileSize(int total) {
        DecimalFormat df = new DecimalFormat("#.00");
        double g = ((double) total) / 1024 / 1024 / 1024;
        double m = ((double) total) / 1024 / 1024;
        double k = ((double) total) / 1024;
        if (g > 0.1) {
            return df.format(g) + "G";
        } else if (m > 0.1) {
            return df.format(m) + "M";
        } else {
            return df.format(k) + "K";
        }
    }

    /**
     * 根据路径获取内存状态
     *
     * @param path
     * @return
     */
    private String getMemoryInfo(File path) {
        // 获得一个磁盘状态对象
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();   // 获得一个扇区的大小
        long totalBlocks = stat.getBlockCount();    // 获得扇区的总数
        long availableBlocks = stat.getAvailableBlocks();   // 获得可用的扇区数量
        // 总空间
        String totalMemory = Formatter.formatFileSize(this, totalBlocks * blockSize);
        // 可用空间
        String availableMemory = Formatter.formatFileSize(this, availableBlocks * blockSize);
        return "可用空间: " + availableMemory + "/总空间: " + totalMemory;
    }

    @Override
    public void onBackPressed() {
        if (check_visible) {
            check_visible = !check_visible;
            initCheckList();
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
            updateBottomState(true);
            return;
        } else super.onBackPressed();

    }
}