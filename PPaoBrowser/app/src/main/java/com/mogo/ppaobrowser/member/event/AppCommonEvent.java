package com.mogo.ppaobrowser.member.event;

import android.os.Bundle;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by doyer.du on 2016-8-18.
 */
public class AppCommonEvent {

    public static final String WECHAT_SHARE = "wechatShare";
    public static final String TASK_NOVEL = "task_novel";
    public static final String TASK_CARTOON = "task_cartoon";
    public static final String TASK_NEWS = "task_news";
    public static final String TASK_VIDEO = "task_video";
    public static final String RIGISTER = "register";
    public static final String UPDATE = "update";
    public static final String LOGOUT = "logout";
    public static final String EVENT_KEY = "EVENT_KEY";
    public static final String EVENT_KEY2 = "EVENT_KEY2";

    public static final String TYPE_NOVEL = "type_novel";
    public static final String TYPE_CARTOON = "type_cartoon";
    public static final String TYPE_VIDEO = "type_video";
    public static final String TYPE_NEWS = "type_news";
    public static final String TYPE_READ = "type_read";


    public static final String AUTH_CANCEL = "authCancel";
    public static final String AUTH_FAIL = "authFail";

    public final String type;
    /**
     * Bundle object can carry more extra info in event.
     */
    private Bundle _extras;

    public AppCommonEvent(String type) {
        this.type = type;
    }

    public AppCommonEvent putExtra(String name, boolean value) {
        _extras = _extras == null ? new Bundle() : _extras;
        _extras.putBoolean(name, value);
        return this;
    }

    public AppCommonEvent putExtra(String name, int value) {
        _extras = _extras == null ? new Bundle() : _extras;
        _extras.putInt(name, value);
        return this;
    }

    public AppCommonEvent putExtra(String name, String value) {
        _extras = _extras == null ? new Bundle() : _extras;
        _extras.putString(name, value);
        return this;
    }

    public AppCommonEvent putExtra(String name, float value) {
        _extras = _extras == null ? new Bundle() : _extras;
        _extras.putFloat(name, value);
        return this;
    }

    public boolean containsKey(String key) {
        return _extras != null && _extras.containsKey(key);
    }

    public boolean getBooleanExtra(String name, boolean defaultValue) {
        return _extras == null ? defaultValue : _extras.getBoolean(name, defaultValue);
    }

    public int getIntExtra(String name, int defaultValue) {
        return _extras == null ? defaultValue : _extras.getInt(name, defaultValue);
    }

    public String getStringExtra(String name, String defaultValue) {
        return _extras == null ? defaultValue : (_extras.containsKey(name) ? _extras.getString(name) : defaultValue);
    }

    public float getFloatExtra(String name, float defaultValue) {
        return _extras == null ? defaultValue : _extras.getFloat(name, defaultValue);
    }

    /**
     * helper method that post this event by EventBus.
     */
    public void postEvent() {
        EventBus.getDefault().post(this);
    }
}
