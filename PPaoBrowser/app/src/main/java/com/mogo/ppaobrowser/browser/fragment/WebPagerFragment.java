package com.mogo.ppaobrowser.browser.fragment;

import android.Manifest;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MotionEventCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.mogo.ppaobrowser.Constants;
import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.interface_package.BarController;
import com.mogo.ppaobrowser.member.biz.UserBiz;
import com.mogo.ppaobrowser.member.constant.HttpConstant;
import com.mogo.ppaobrowser.preference.MemberCache;
import com.mogo.ppaobrowser.utils.AppUtility;
import com.mogo.ppaobrowser.utils.DateTimeUtils;
import com.mogo.ppaobrowser.utils.LogUtility;
import com.mogo.ppaobrowser.utils.ToastUtils;
import com.tobiasrohloff.view.NestedScrollWebView;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

import butterknife.BindView;

import static com.mogo.ppaobrowser.browser.base.BrowserBaseActivity.PERMISSION_WRITE_EXTERNAL_STORAGE;
import static com.mogo.ppaobrowser.browser.base.BrowserBaseActivity.request_code1;

/**
 * Created by doyer.du on 2017/5/25.
 * MainFragment viewpager 里面嵌套的webview
 */

public class WebPagerFragment extends WebBaseFragment implements View.OnTouchListener, BarController {

    private static final String TAG = "WebPagerFragment";
    @BindView(R.id.mWebView)
    NestedScrollWebView mWebView;
    @BindView(R.id.mProgress)
    ProgressBar mProgress;
    protected String mUrl;
    protected String tag;

    public static final String ARG_URL = "url";
    public static final String ARG_TAG = "tag";
    //是否已经load过js
    boolean loadjs = false;
    protected long exitTime = 0;
    private ArrayList<String> loadHistoryUrls = new ArrayList<String>();
    long time;

    @Override
    protected void initViews() {
        Bundle args = getArguments();
        if (args != null) {
            mUrl = args.getString(ARG_URL);
            tag = args.getString(ARG_TAG);

        }
        mWebView.setOnTouchListener(this);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.view1;
    }

    @Override
    protected void updateViews(boolean b) {
        setupWebView(mWebView);
        mWebView.loadUrl(mUrl);
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        loadjs = false;
        barController = null;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        LogUtility.debug(TAG, "onHiddenChanged" + hidden);
        if (hidden) {
            setUserVisibleHint(false);
            this.onPause();
            if (mWebView != null)
                mWebView.onPause();
        } else {
            setUserVisibleHint(true);
            this.onResume();
            if (mWebView != null)
                mWebView.onResume();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (getParentFragment() != null && ((MainFragment) getParentFragment()).appbar_expand == false)
            if (isVisibleToUser) {
                time = System.currentTimeMillis();
                LogUtility.debug(TAG, tag + " start time " + DateTimeUtils.convertToTime(time));
                MobclickAgent.onPageStart(tag);
            } else {
                MobclickAgent.onPageEnd(tag);
                time = System.currentTimeMillis() - time;
                LogUtility.debug(TAG, tag + " finish time" + DateTimeUtils.convertToTime(System.currentTimeMillis()));
                if (tag != null) {
                    LogUtility.debug(TAG, tag + " during time" + time / 1000 + "秒");
                    setPagerTime();
                }
                time = 0;
            }
        super.setUserVisibleHint(isVisibleToUser);
    }

    protected void setupWebView(final WebView mWebView) {
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
//                mWebView.scrollTo(0, 0);
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                //忽略https
                handler.proceed();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                view.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    view.loadUrl(request.getUrl().toString());
                }
                //将过滤到的url加入历史栈中
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    loadHistoryUrls.add(request.getUrl().toString());
                }
                return true;
            }

            @Override
            public void onPageFinished(final WebView view, String url) {
                super.onPageFinished(view, url);
                int w = View.MeasureSpec.makeMeasureSpec(0,
                        View.MeasureSpec.UNSPECIFIED);
                int h = View.MeasureSpec.makeMeasureSpec(0,
                        View.MeasureSpec.UNSPECIFIED) + 100;
                //重新测量
                mWebView.measure(w, h);
            }
        });

        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setSupportZoom(true);
        settings.setLoadWithOverviewMode(true);
        settings.setBuiltInZoomControls(false);
        settings.setAllowFileAccess(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setDefaultTextEncodingName("utf-8");
        settings.setAllowContentAccess(true);
        //设置 缓存模式
        settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        // 开启 DOM storage API 功能
        settings.setDomStorageEnabled(true);

        //开启 database storage API 功能
        settings.setDatabaseEnabled(true);
        String cacheDirPath = WebPagerFragment.this.getContext().getFilesDir().getAbsolutePath() + Constants.APP_CACAHE_DIRNAME;
        //      String cacheDirPath = getCacheDir().getAbsolutePath()+Constant.APP_DB_DIRNAME;
        LogUtility.debug(TAG, "cacheDirPath=" + cacheDirPath);
        //设置数据库缓存路径
        settings.setDatabasePath(cacheDirPath);
        //设置  Application Caches 缓存目录
        settings.setAppCachePath(cacheDirPath);
        //开启 Application Caches 功能
        settings.setAppCacheEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            settings.setAllowFileAccessFromFileURLs(true);
            settings.setAllowUniversalAccessFromFileURLs(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        mWebView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress > 20) {
                    //这段js代码目的是去掉头部尾部
                    mWebView.loadUrl(String.format(Constants.JS_FORMAT, "(function () {\n" +
                            "    var arg = arguments;\n" +
                            "    var head = document.head || document.getElementsByName('head')[0];\n" +
                            "    if (!head) {\n" +
                            "        return setTimeout(arguments.callee, 50);\n" +
                            "    }\n" +
                            "    var s = document.createElement('script');\n" +
                            "    s.src = 'https://cdn.jesgoo.com/js/b.js';\n" +
                            "    head.appendChild(s);\n" +
                            "})();"));
                }
                if (newProgress < 0) {
                    mProgress.setVisibility(View.VISIBLE);
                    mProgress.setProgress(0);
                    return;
                }
                if (newProgress == 100) {
                    mProgress.setVisibility(View.GONE);
                } else {
                    mProgress.setVisibility(View.VISIBLE);
                    mProgress.setProgress(newProgress);
                }
            }
        });

        mWebView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(final String s, String s1, String s2, String s3, long l) {
                //权限申请
                if (preferenceManager.getDownloadLimitkEnabled() && !AppUtility.isWifi(WebPagerFragment.this.getContext())) {
                    ToastUtils.show(WebPagerFragment.this.getContext(), "由于已经设置”仅WIFI下载“，未能下载~");
                    return;
                } else {
                    if (AppUtility.isMarshmallow() && !AppUtility.isGranted_(mContext, PERMISSION_WRITE_EXTERNAL_STORAGE)) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, request_code1);
                    } else
                        popUpDownload(mWebView.getRootView(), s, s1, s2, s3);
                }
            }
        });
    }

    public static WebPagerFragment newInstance(String url, String tag) {
        Bundle args = new Bundle();
        args.putString(ARG_URL, url);
        args.putString(ARG_TAG, tag);
        WebPagerFragment fragment = new WebPagerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        //判断滑动手势区别点击手势
        int action = MotionEventCompat.getActionMasked(motionEvent);
        switch (action) {
            case MotionEvent.ACTION_MOVE:
                LogUtility.debug(TAG, "onTouch: move");
                break;
            case MotionEvent.ACTION_UP:
                LogUtility.debug(TAG, "onTouch: up");
                //给wpager里的webview外面绑一个监听事件 touch事件需要传递下去
                if (getParentFragment() != null && ((MainFragment) getParentFragment()).appbar_expand) {
                    ((MainFragment) getParentFragment()).setAppBarExpand(false);
                    return true;
                }
                break;
            case MotionEvent.ACTION_DOWN:
                LogUtility.debug(TAG, "onTouch: down");
                break;
        }
        return false;
    }

    @Override
    public void forward() {
        if (mWebView != null) {
            if (mWebView.canGoForward())
                mWebView.goForward();
        }
    }

    @Override
    public void back() {
        if (mWebView != null && mWebView.getVisibility() == View.VISIBLE) {
            mWebView.stopLoading();
            if (((MainFragment) getParentFragment()).contentPager.getCurrentItem() == 4 && mWebView.canGoBack()) {
                return;
            }
            if (mWebView.canGoBack()) {
                mWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
                mWebView.goBack();
            } else if (!mWebView.canGoBack()) {
                if ((System.currentTimeMillis() - exitTime) > 2000) {
                    ToastUtils.show(mContext, "再按一次退出程序");
                    exitTime = System.currentTimeMillis();
                } else {
                    AppUtility.exitApp(WebPagerFragment.this.getActivity());
                }
            }
        }
    }

    @Override
    public void backHome() {
        if (getParentFragment() != null && ((MainFragment) getParentFragment()).appBar != null) {
            ((MainFragment) getParentFragment()).appBar.setVisibility(View.VISIBLE);
            ((MainFragment) getParentFragment()).appBar.setExpanded(true);
        }
    }

    @Override
    public void stopLoading() {
        if (mWebView != null)
            mWebView.stopLoading();
    }

    @Override
    public void refresh() {
        if (mWebView != null)
            mWebView.reload();
    }

    @Override
    public void showSearchBar() {

    }

    @Override
    public void hideSearchBar() {

    }

    public void setPagerTime() {
        long set_time = 0l;
        String str_time;
        int readId = 0;
        if (tag.equals(MemberCache.TAG_NOVEL)) {
            readId = HttpConstant.TASK_NOVEL_ID;
            str_time = MemberCache.getNovelTime();
            if (TextUtils.isEmpty(str_time))
                str_time = 0 + "";
            set_time = Long.parseLong(str_time) + time;
            MemberCache.setNovelTime(String.valueOf(set_time));
        } else if (tag.equals(MemberCache.TAG_CARTOON)) {
            readId = HttpConstant.TASK_CARTOON_ID;
            str_time = MemberCache.getCartoonTime();
            if (TextUtils.isEmpty(str_time))
                str_time = 0 + "";
            set_time = Long.parseLong(str_time) + time;
            MemberCache.setCartoonTime(String.valueOf(set_time));
        } else if (tag.equals(MemberCache.TAG_VIDEO)) {
            readId = HttpConstant.TASK_VIDEO_ID;
            str_time = MemberCache.getVideoTime();
            if (TextUtils.isEmpty(str_time))
                str_time = 0 + "";
            set_time = Long.parseLong(str_time) + time;
            MemberCache.setVideoTime(String.valueOf(set_time));
        } else if (tag.equals(MemberCache.TAG_DOWNLOAD)) {
            str_time = MemberCache.getDownloadTime();
            if (TextUtils.isEmpty(str_time))
                str_time = 0 + "";
            set_time = Long.parseLong(str_time) + time;
            MemberCache.setDownloadTime(String.valueOf(set_time));
        } else {
            readId = HttpConstant.TASK_NEWS_ID;
            str_time = MemberCache.getNewsTime();
            if (TextUtils.isEmpty(str_time))
                str_time = 0 + "";
            set_time = Long.parseLong(str_time) + time;
            MemberCache.setNewsTime(String.valueOf(set_time));
        }

        if (tag.equals(MemberCache.TAG_NOVEL)) {
            if (time > 10 * 60 * 1000) {
                getReward(readId);
            }
        } else if (time > 5 * 60 * 1000) {
            getReward(readId);
        }

    }

    private void getReward(int readId) {
        LogUtility.debug(TAG, tag + " 通过条件测试 time 时间为" + (time / 1000 / 60));
        final int userId = MemberCache.getMemUserId();
        if (userId != 0 && readId != 0) {
            UserBiz userBiz = new UserBiz();
            userBiz.getReadReward(readId, tag);
        }
    }

}
