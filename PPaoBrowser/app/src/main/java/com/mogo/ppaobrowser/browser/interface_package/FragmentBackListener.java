package com.mogo.ppaobrowser.browser.interface_package;

/**
 * Created by doyer on 2017/5/22.
 */

public interface FragmentBackListener {

    void onbackForward();
}
