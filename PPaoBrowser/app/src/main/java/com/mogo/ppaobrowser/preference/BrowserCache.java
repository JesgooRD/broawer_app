package com.mogo.ppaobrowser.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


public class BrowserCache {

    private static class Name {
        static final String BLOCK_ADS = "AdBlock";
        static final String BLOCK_IMAGES = "blockimages";
        static final String HOMEPAGE = "home";
        static final String TEXT_SIZE = "textsize";
        static final String DO_NOT_TRACK = "doNotTrack";
        static final String DOWNLOAD_LIMIT = "download_limit";
        static final String ICON_VERSION = "icon_version";
        static final String PREFERENCES = "ppao_settings";

    }

    @NonNull
    private final SharedPreferences mPrefs;

    public BrowserCache(@NonNull final Context context) {
        mPrefs = context.getSharedPreferences(Name.PREFERENCES, 0);
    }


    public boolean getAdBlockEnabled() {
        return mPrefs.getBoolean(Name.BLOCK_ADS, false);
    }

    public boolean getBlockImagesEnabled() {
        return mPrefs.getBoolean(Name.BLOCK_IMAGES, false);
    }

    public boolean getDoNotTrackEnabled() {
        return mPrefs.getBoolean(Name.DO_NOT_TRACK, false);
    }

    public boolean getDownloadLimitkEnabled() {
        return mPrefs.getBoolean(Name.DOWNLOAD_LIMIT, false);
    }

    private void putBoolean(@NonNull String name, boolean value) {
        mPrefs.edit().putBoolean(name, value).apply();
    }

    private void putInt(@NonNull String name, int value) {
        mPrefs.edit().putInt(name, value).apply();
    }

    private void putString(@NonNull String name, @Nullable String value) {
        mPrefs.edit().putString(name, value).apply();
    }

    private String getString(@NonNull String name, @Nullable String value) {
        return mPrefs.getString(name, value);
    }

    public void setDoNotTrackEnabled(boolean doNotTrack) {
        putBoolean(Name.DO_NOT_TRACK, doNotTrack);
    }

    public void setDownloadLimitEnabled(boolean downloadLimitEnabled) {
        putBoolean(Name.DOWNLOAD_LIMIT, downloadLimitEnabled);
    }

    public void setAdBlockEnabled(boolean enable) {
        putBoolean(Name.BLOCK_ADS, enable);
    }

    public void setBlockImagesEnabled(boolean enable) {
        putBoolean(Name.BLOCK_IMAGES, enable);
    }

    public void setHomepage(@NonNull String homepage) {
        putString(Name.HOMEPAGE, homepage);
    }

    public void setTextSize(int size) {
        putInt(Name.TEXT_SIZE, size);
    }

    public void setIconVersion(String icon_version) {
        putString(Name.ICON_VERSION, icon_version);
    }

    public String getIconVersion() {
        return getString(Name.ICON_VERSION, "");
    }
}
