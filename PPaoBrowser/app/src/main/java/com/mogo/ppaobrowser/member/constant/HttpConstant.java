package com.mogo.ppaobrowser.member.constant;

/**
 * Created by Lain on 2016/10/14.
 */

public class HttpConstant {
    //release
    public static final String API_BASE_URL = "http://ppapi.orange1213.cn:9091/";
    //stage
    //public static final String API_BASE_URL = "http://120.26.36.243:9091/";
    //debug
    //public static final String API_BASE_URL = "http://192.168.2.114:9091/";
    public static final String WEB_ACTION = "loginAndOut";// 登录
    public static final String TYPE_LOGIN = "login";// 类型
    public static final String TYPE_REGISTER = "register";// 注册
    public static final String TYPE_SIGN_IN = "userSignIn";// 签到
    public static final String TYPE_SHARE = "shareApp";// 分享
    public static final String TYPE_UPDATE = "updateUserData";// 完善个人资料
    public static final String TYPE_TRANSFER = "withdrawal";// 类型
    public static final String TYPE_PAY_NOTE = "withdrawalRcord";//提现记录
    public static final String TYPE_INCOME = "incomeList";// 获得收入明细
    public static final String TYPE_READ = "readAward";//阅读tab
    public static final String TASK_STATE = "getTaskStatus";//获得任务状态
    public static final String TASK_COMPLAIN = "feedback";//吐槽
    public static final String ICON = "icon";//图标

    public static final int TASK_INFO_ID = 2;//完善个人信息
    public static final int TASK_SIGN_ID = 3;//签到
    public static final int TASK_SHARE_ID = 4;//分享
    public static final int TASK_NOVEL_ID = 5;//小说
    public static final int TASK_CARTOON_ID = 6;//漫画
    public static final int TASK_VIDEO_ID = 7;//视屏
    public static final int TASK_NEWS_ID = 8;//资讯

}
