package com.mogo.ppaobrowser.browser.bean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * Created by doyer on 2017/5/22.
 */

@Entity
public class BookMarkModel {
    @Id(autoincrement = true)
    private Long id;
    String bookmark_name = "";
    String url = "";

    public BookMarkModel(String bookmark_name, String url) {
        this.bookmark_name = bookmark_name;
        this.url = url;
    }

    public BookMarkModel() {
    }

    @Generated(hash = 1199163876)
    public BookMarkModel(Long id, String bookmark_name, String url) {
        this.id = id;
        this.bookmark_name = bookmark_name;
        this.url = url;
    }
    

    public String getBookmark_name() {
        return bookmark_name;
    }

    public void setBookmark_name(String bookmark_name) {
        this.bookmark_name = bookmark_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
