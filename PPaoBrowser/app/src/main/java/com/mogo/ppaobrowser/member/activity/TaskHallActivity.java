package com.mogo.ppaobrowser.member.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.SimpleDraweeView;
import com.mogo.ppaobrowser.PPaoApplication;
import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.activity.BrowserActivity;
import com.mogo.ppaobrowser.browser.activity.FeedBackActivity;
import com.mogo.ppaobrowser.browser.activity.PPaoBaseActivity;
import com.mogo.ppaobrowser.browser.fragment.MainFragment;
import com.mogo.ppaobrowser.browser.interface_package.DataAccessListener;
import com.mogo.ppaobrowser.gen.DaoSession;
import com.mogo.ppaobrowser.gen.SignModelDao;
import com.mogo.ppaobrowser.member.bean.SignModel;
import com.mogo.ppaobrowser.member.bean.responseBean.MemberBean;
import com.mogo.ppaobrowser.member.bean.responseBean.SignResBean;
import com.mogo.ppaobrowser.member.bean.responseBean.TaskResbean;
import com.mogo.ppaobrowser.member.biz.UserBiz;
import com.mogo.ppaobrowser.member.event.AppCommonEvent;
import com.mogo.ppaobrowser.preference.MemberCache;
import com.mogo.ppaobrowser.utils.AppUtility;
import com.mogo.ppaobrowser.utils.DateTimeUtils;
import com.mogo.ppaobrowser.utils.ToastUtils;
import com.mogo.ppaobrowser.widget.HorizontalStepView;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TaskHallActivity extends PPaoBaseActivity {

    private static final String TAG = "TaskHallActivity";
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.step_view)
    HorizontalStepView stepView;
    @BindView(R.id.task_sign)
    SimpleDraweeView taskSignImage;
    @BindView(R.id.relative_info)
    RelativeLayout relativeInfo;
    @BindView(R.id.tv_info)
    TextView tvInfo;
    @BindView(R.id.relative_sign)
    RelativeLayout relativeSign;
    @BindView(R.id.tv_sign)
    TextView tvSign;
    @BindView(R.id.relative_share)
    RelativeLayout relativeShare;
    @BindView(R.id.tv_share)
    TextView tvShare;
    @BindView(R.id.relative_novel)
    RelativeLayout relativeNovel;
    @BindView(R.id.tv_novel)
    TextView tvNovel;
    @BindView(R.id.relative_cartoon)
    RelativeLayout relativeCartoon;
    @BindView(R.id.tv_cartoon)
    TextView tvCartoon;
    @BindView(R.id.relative_news)
    RelativeLayout relativeNews;
    @BindView(R.id.tv_news)
    TextView tvNews;
    @BindView(R.id.relative_video)
    RelativeLayout relativeVideo;
    @BindView(R.id.tv_video)
    TextView tvVideo;
    @BindView(R.id.item_video)
    RelativeLayout itemVideo;
    @BindView(R.id.relative_complain)
    RelativeLayout relativeComplain;
    @BindView(R.id.tv_complain)
    TextView tvComplain;
    @BindView(R.id.item_complain)
    RelativeLayout itemComplain;

    private SignModelDao signDao;
    private SignModel signModel;
    MemberBean memberBean;
    int sign_count;
    UserBiz userBiz = new UserBiz();
    private boolean signed;//当天是否签到


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Override
    protected void updateViews(boolean b) {

    }

    @Override
    protected void initViews() {
        setSupportActionBar(toolbar);
        toolbarTitle.setText(getString(R.string.task_hall));
//        initDao();
//        getSignModel();
//        if (signModel != null)
//            sign_count = signModel.getCount();
        if (userBiz.getDefault() != null)
            sign_count = userBiz.getDefault().signCount;
    }

    @Override
    protected void onResume() {
        super.onResume();
        initState();
        MobclickAgent.onPageStart("任务大厅");
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("任务大厅");
    }

    private void initState() {
//        List<TaskResbean> list = userBiz.getDefaultTasks();
        userBiz.getTaskState(new DataAccessListener<List<TaskResbean>>() {
            @Override
            public void requestSuccess(List<TaskResbean> list) {

                if (list != null && list.size() > 0) {
                    for (TaskResbean taskState : list
                            ) {
                        if (taskState.taskId == 2 && taskState.taskStatus == 1) {
                            updateItem(0, true);
                        } else if (taskState.taskId == 3) {
                            if (taskState.taskStatus == 1) {
                                updateItem(1, true);
                                signed = true;
                                setTaskIcon(signed);
                                if (!signed)
                                    sign_count = sign_count - 1;
                            }
                            showStepView();
                        } else if (taskState.taskId == 3 && taskState.taskStatus == 0) {
                            showStepView();
                        } else if (taskState.taskId == 4 && taskState.taskStatus == 1) {
                            updateItem(2, true);
                        } else if (taskState.taskId == 5 && taskState.taskStatus == 1) {
                            updateItem(3, true);
                        } else if (taskState.taskId == 6 && taskState.taskStatus == 1) {
                            updateItem(4, true);
                        } else if (taskState.taskId == 7 && taskState.taskStatus == 1) {
                            updateItem(6, true);
                        } else if (taskState.taskId == 8 && taskState.taskStatus == 1) {
                            updateItem(5, true);
                        } else if (taskState.taskId == 9 && taskState.taskStatus == 1) {
                            updateItem(7, true);
                        }
                    }
                }

            }

            @Override
            public void requestFail(String error) {

            }
        }, false);
    }

    //今天是否已经签到
    private void setTaskIcon(boolean isSigned) {
        GenericDraweeHierarchy hierarchy = taskSignImage.getHierarchy();
        if (isSigned) {
            hierarchy.setPlaceholderImage(R.drawable.task_signed);
            taskSignImage.setClickable(false);
        } else {
            hierarchy.setPlaceholderImage(R.drawable.task_unsign);
            taskSignImage.setClickable(true);
        }
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_task_hall;
    }

    private void showStepView() {
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        map.put("1天", "+10金币");
        map.put("2天", "+15金币");
        map.put("3天", "+20金币");
        map.put("4天", "+25金币");
        map.put("5天", "+30金币");
        map.put("6天", "+35金币");
        map.put("7天", "+40金币");
        stepView.setStepsViewIndicatorComplectingPosition(sign_count)//设置完成的步数
                .setStepViewTexts(map)//总步骤
                .setStepsViewIndicatorCompletedLineColor(ContextCompat.getColor(this, android.R.color.white))//设置StepsViewIndicator完成线的颜色
                .setStepsViewIndicatorUnCompletedLineColor(ContextCompat.getColor(this, R.color.uncompleted_text_color))//设置StepsViewIndicator未完成线的颜色
                .setStepViewComplectedTextColor(ContextCompat.getColor(this, android.R.color.white))//设置StepsView text完成线的颜色
                .setStepViewUnComplectedTextColor(ContextCompat.getColor(this, R.color.uncompleted_text_color))//设置StepsView text未完成线的颜色
                .setStepsViewIndicatorCompleteIcon(ContextCompat.getDrawable(this, R.drawable.task_step_signed))//设置StepsViewIndicator CompleteIcon
                .setStepsViewIndicatorDefaultIcon(ContextCompat.getDrawable(this, R.drawable.task_step_unsign))//设置StepsViewIndicator DefaultIcon
                .setStepsViewIndicatorAttentionIcon(ContextCompat.getDrawable(this, R.drawable.task_step_unsign));//设置StepsViewIndicator AttentionIcon
    }

    @OnClick({R.id.task_sign, R.id.item_share, R.id.item_complete_info, R.id.item_sign_in, R.id.item_novel, R.id.item_cartoon, R.id.item_news, R.id.item_video, R.id.item_complain})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.item_sign_in:
            case R.id.task_sign:
                census("task_sign");
                signAction();
                break;
            case R.id.item_share:
                census("task_share");
                popShareAction(view);
                break;
            case R.id.item_complete_info:
                census("task_info");
                startActivityWithZoom(new Intent(TaskHallActivity.this, PersonInfoActivity.class));
                break;
            case R.id.item_novel:
                census("task_novel");
                Intent intent = new Intent(TaskHallActivity.this, BrowserActivity.class);
                startActivityWithZoom(intent);
                new AppCommonEvent(AppCommonEvent.TASK_NOVEL).postEvent();
                TaskHallActivity.this.finish();
                break;
            case R.id.item_cartoon:
                census("task_cartoon");
                startActivityWithZoom(new Intent(TaskHallActivity.this, BrowserActivity.class));
                new AppCommonEvent(AppCommonEvent.TASK_CARTOON).postEvent();
                finish();
                break;
            case R.id.item_news:
                census("task_news");
                startActivityWithZoom(new Intent(TaskHallActivity.this, BrowserActivity.class));
                new AppCommonEvent(AppCommonEvent.TASK_NEWS).postEvent();
                finish();
                break;
            case R.id.item_video:
                census("task_video");
                startActivityWithZoom(new Intent(TaskHallActivity.this, BrowserActivity.class));
                new AppCommonEvent(AppCommonEvent.TASK_VIDEO).postEvent();
                finish();
                break;
            case R.id.item_complain:
                census("task_complain");
                startActivityWithZoom(new Intent(TaskHallActivity.this, FeedBackActivity.class));
                break;
        }

    }

    private void census(String tag) {
        MobclickAgent.onEvent(this, tag);
    }

    private void signAction() {
        //完成签到操作 后台api
        userBiz.signIn(new DataAccessListener<SignResBean>() {
            @Override
            public void requestSuccess(SignResBean ob) {
                signed = true;
                setTaskIcon(signed);
                updateItem(1, true);
                sign_count = sign_count + 1;
                showStepView();
            }

            @Override
            public void requestFail(String error) {
                if (error.equals("300")) {
                    signed = true;
                    setTaskIcon(signed);
                    ToastUtils.show(TaskHallActivity.this, "今日已签到");
                }
            }
        });
    }

    private void popShareAction(View view) {
        //自定义PopupWindow的布局
        final View contentView = LayoutInflater.from(TaskHallActivity.this).inflate(R.layout.share_platform_layout, null);
        //初始化PopupWindow,并为其设置布局文件
        final PopupWindow popupWindow = new PopupWindow(contentView);
        ImageView img_circle = (ImageView) contentView.findViewById(R.id.img_share_circle);
        ImageView img_friend = (ImageView) contentView.findViewById(R.id.img_share_friend);
        img_circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                AppUtility.shareApp(TaskHallActivity.this, AppUtility.mTargetScene1);
            }
        });
        img_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                AppUtility.shareApp(TaskHallActivity.this, AppUtility.mTargetScene0);
            }
        });
        popupWindow.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popupWindow.setFocusable(true);
//                popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));   //为PopupWindow设置透明背景.
        popupWindow.setOutsideTouchable(false);
        //设置PopupWindow进入和退出动画
        popupWindow.setAnimationStyle(R.style.PopupAnimation);
        //设置PopupWindow显示的位置
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
        backgroundAlpha(0.6f);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                backgroundAlpha(1f);
            }
        });
    }

    @Subscribe
    public void onEvent(AppCommonEvent event) {
        if (event.type.equals(AppCommonEvent.WECHAT_SHARE)) {
            String share_message = event.getStringExtra(AppCommonEvent.EVENT_KEY, "");
            int status = event.getIntExtra(AppCommonEvent.EVENT_KEY2, 0);
            if (status == 200) {
                ToastUtils.show(TaskHallActivity.this, share_message);
                updateItem(2, true);
            } else if (status == 300) {
                ToastUtils.show(TaskHallActivity.this, "已经分享过了哦");
                updateItem(2, true);
            } else ToastUtils.show(TaskHallActivity.this, "获取金币失败");

        } else if (event.type.equals(AppCommonEvent.TYPE_READ)) {
            String page_type = event.getStringExtra(AppCommonEvent.EVENT_KEY2, null);
            int add_gold = event.getIntExtra(AppCommonEvent.EVENT_KEY, 0);
            if (!TextUtils.isEmpty(page_type)) {
                if (page_type.equals(MemberCache.TAG_NOVEL)) {
                    updateItem(3, true);
                    MemberCache.setTaskNovelState(true);
                } else if (page_type.equals(MemberCache.TAG_CARTOON)) {
                    updateItem(4, true);
                    MemberCache.setTaskCartoonState(true);
                } else if (page_type.equals(MemberCache.TAG_NEWS)) {
                    updateItem(5, true);
                    MemberCache.setTaskNewsState(true);
                }
            }
        }
    }

    private void getSignModel() {
        memberBean = userBiz.getDefault();
        if (memberBean != null) {
            int userId = memberBean.userId;
            if (signModel == null) {
                QueryBuilder<SignModel> query = signDao.queryBuilder().where(SignModelDao.Properties.UserId.eq(userId));
                List<SignModel> list = query.list();
                if (list.size() > 0)
                    signModel = list.get(0);
                else {
                    signModel = new SignModel();
                    signModel.setUserId(userId);
                }
            }
        }
    }

    @Nullable
    //是否已经签到
    private boolean getSignState() {
        memberBean = userBiz.getDefault();
        int userId = memberBean.userId;
        QueryBuilder<SignModel> query = signDao.queryBuilder().where(SignModelDao.Properties.UserId.eq(userId));
        List<SignModel> list = query.list();
        if (list.size() > 0) {
            signModel = list.get(0);
            if (signModel.getCount() < memberBean.signCount)
                return false;
            else {
                //判断相隔天数
                int distanceDay = DateTimeUtils.getDisDay(signModel.getTime(), System.currentTimeMillis());
                if (distanceDay == 1) {
                    return false;
                } else if (distanceDay == 0) {
                    //避免第一次
                    if (MemberCache.getIsFirst()) {
                        //发卡量
                        return false;
                    }
                    return true;
                } else {
                    signModel.setTime(System.currentTimeMillis());
                    signModel.setCount(0);
                    signModel.setUserId(userId);
                    signDao.insertOrReplace(signModel);
                    return false;
                }
            }
        } else {
            getSignModel();
            signModel.setTime(System.currentTimeMillis());
            signModel.setCount(0);
            signModel.setUserId(userId);
            signDao.insertOrReplace(signModel);
            return false;
        }
    }

    public void updateItem(int i, boolean finished) {
        switch (i) {
            case 0:
                //完善个人信息
                if (finished) {
                    tvInfo.setVisibility(View.VISIBLE);
                    relativeInfo.setVisibility(View.GONE);
                } else {
                    tvInfo.setVisibility(View.GONE);
                    relativeInfo.setVisibility(View.VISIBLE);
                }
                break;
            case 1:
                //签到
                if (finished) {
                    tvSign.setVisibility(View.VISIBLE);
                    relativeSign.setVisibility(View.GONE);
                } else {
                    tvSign.setVisibility(View.GONE);
                    relativeSign.setVisibility(View.VISIBLE);
                }
                break;
            case 2:
                //分享app
                if (finished) {
                    tvShare.setVisibility(View.VISIBLE);
                    relativeShare.setVisibility(View.GONE);
                } else {
                    tvShare.setVisibility(View.GONE);
                    relativeShare.setVisibility(View.VISIBLE);
                }
                break;
            case 3:
                //阅读小说
                if (finished) {
                    tvNovel.setVisibility(View.VISIBLE);
                    relativeNovel.setVisibility(View.GONE);
                } else {
                    tvNovel.setVisibility(View.GONE);
                    relativeNovel.setVisibility(View.VISIBLE);
                }
                break;
            case 4:
                //阅读漫画
                if (finished) {
                    tvCartoon.setVisibility(View.VISIBLE);
                    relativeCartoon.setVisibility(View.GONE);
                } else {
                    tvCartoon.setVisibility(View.GONE);
                    relativeCartoon.setVisibility(View.VISIBLE);
                }
                break;
            case 5:
                //阅读资讯
                if (finished) {
                    tvNews.setVisibility(View.VISIBLE);
                    relativeNews.setVisibility(View.GONE);
                } else {
                    tvNews.setVisibility(View.GONE);
                    relativeNews.setVisibility(View.VISIBLE);
                }
                break;
            case 6:
                //阅读视频
                if (finished) {
                    tvVideo.setVisibility(View.VISIBLE);
                    relativeVideo.setVisibility(View.GONE);
                } else {
                    tvVideo.setVisibility(View.GONE);
                    relativeVideo.setVisibility(View.VISIBLE);
                }
                break;
            case 7:
                //有效反馈
                if (finished) {
                    tvComplain.setVisibility(View.VISIBLE);
                    relativeComplain.setVisibility(View.GONE);
                } else {
                    tvComplain.setVisibility(View.GONE);
                    relativeComplain.setVisibility(View.VISIBLE);
                }
                break;
        }
    }
}
