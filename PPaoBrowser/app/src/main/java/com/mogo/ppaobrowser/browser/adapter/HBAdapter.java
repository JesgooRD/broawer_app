package com.mogo.ppaobrowser.browser.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.base.BaseFragment;
import com.mogo.ppaobrowser.browser.interface_package.SaveDataListener;
import com.mogo.ppaobrowser.browser.bean.BookMarkModel;
import com.mogo.ppaobrowser.browser.bean.HistoryModel;
import com.mogo.ppaobrowser.browser.presenter.HBPresenter;
import com.mogo.ppaobrowser.utils.DialogUtils;
import com.mogo.ppaobrowser.utils.ToastUtils;
import com.mogo.ppaobrowser.widget.CustomDialog;

import java.util.List;

/**
 * Created by doyer on 2017/5/22.
 * history&bookmark
 */
public class HBAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int BOOKMARK_TYPE = 0;
    private static final int HISTORY_TYPE = 1;
    List<Object> dataList;
    private List<Boolean> checkList;
    public boolean selected_show = false;
    Context mContext;
    SaveDataListener saveDataListener;

    private OnItemClickListener onItemClickListener;


    public HBAdapter(Context context, List<Object> dataList, List<Boolean> listCheck, SaveDataListener saveDataListener) {
        this.dataList = dataList;
        this.checkList = listCheck;
        mContext = context;
        this.saveDataListener = saveDataListener;
    }


    @Override
    public int getItemViewType(int position) {
        if (dataList.get(position) instanceof BookMarkModel)
            return BOOKMARK_TYPE;
        else
            return HISTORY_TYPE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hb_item, parent, false);
        RecyclerView.ViewHolder vh = null;
        if (viewType == HISTORY_TYPE) {
            vh = new HistoryViewHolder(view);
        } else
            vh = new BookMarkViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        Object model = dataList.get(position);
        if (model instanceof BookMarkModel) {
            ((BookMarkViewHolder) holder).site_name.setText(((BookMarkModel) model).getBookmark_name());
            ((BookMarkViewHolder) holder).site_url.setText(((BookMarkModel) model).getUrl());
            ((BookMarkViewHolder) holder).site_select.setChecked(checkList.get(position));
            ((BookMarkViewHolder) holder).site_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    createBookMarkDialog((BookMarkModel) dataList.get(position), saveDataListener);
                }
            });
            if (selected_show) {
                ((BookMarkViewHolder) holder).site_select.setVisibility(View.VISIBLE);
                ((BookMarkViewHolder) holder).site_edit.setVisibility(View.VISIBLE);
            } else {
                ((BookMarkViewHolder) holder).site_select.setVisibility(View.GONE);
                ((BookMarkViewHolder) holder).site_edit.setVisibility(View.GONE);
            }
        } else if (model instanceof HistoryModel) {
            ((HistoryViewHolder) holder).site_name.setText(((HistoryModel) model).getName());
            ((HistoryViewHolder) holder).site_url.setText(((HistoryModel) model).getUrl());
            ((HistoryViewHolder) holder).site_select.setChecked(checkList.get(position));
            ((HistoryViewHolder) holder).site_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CustomDialog customDialog = new CustomDialog();
                    customDialog.showEditHistoryDialog(mContext, (HistoryModel) dataList.get(position));
                }
            });
            if (selected_show) {
                ((HistoryViewHolder) holder).site_select.setVisibility(View.VISIBLE);
            } else {
                ((HistoryViewHolder) holder).site_select.setVisibility(View.GONE);
                ((HistoryViewHolder) holder).site_edit.setVisibility(View.GONE);
            }
        }

    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }

    //自定义的ViewHolder，持有每个Item的的所有界面元素
    public class BookMarkViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener, CompoundButton.OnCheckedChangeListener {
        public TextView site_name;
        public TextView site_url;
        public TextView site_edit;
        public CheckBox site_select;
        public ImageView site_logo;

        public BookMarkViewHolder(View view) {
            super(view);
            site_name = (TextView) view.findViewById(R.id.tv_site_name);
            site_url = (TextView) view.findViewById(R.id.tv_site_url);
            site_select = (CheckBox) view.findViewById(R.id.site_select);
            site_logo = (ImageView) view.findViewById(R.id.site_logo);
            site_edit = (TextView) view.findViewById(R.id.site_edit);
            view.setOnLongClickListener(this);
            view.setOnClickListener(this);
            site_select.setOnClickListener(this);
            site_select.setOnCheckedChangeListener(this);

        }

        @Override
        public void onClick(View view) {
            if (onItemClickListener != null) {
                if (view.getId() == R.id.site_select) {
                    if (site_select.isChecked()) {
                        site_select.setChecked(true);
                    } else {
                        site_select.setChecked(false);
                    }
                } else if (!selected_show) {
                    onItemClickListener.setOnItemClickListener(getAdapterPosition());
                }
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if (onItemClickListener != null) {
                return onItemClickListener.setOnItemLongClick(getAdapterPosition());
            }
            return false;
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (onItemClickListener != null) {
                onItemClickListener.setOnItemCheckedChanged(getAdapterPosition(), b);
            }
        }
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener, CompoundButton.OnCheckedChangeListener {
        public TextView site_name;
        public TextView site_url;
        public TextView site_edit;
        public CheckBox site_select;
        public ImageView site_logo;

        public HistoryViewHolder(View view) {
            super(view);
            site_name = (TextView) view.findViewById(R.id.tv_site_name);
            site_url = (TextView) view.findViewById(R.id.tv_site_url);
            site_select = (CheckBox) view.findViewById(R.id.site_select);
            site_logo = (ImageView) view.findViewById(R.id.site_logo);
            site_edit = (TextView) view.findViewById(R.id.site_edit);
            view.setOnLongClickListener(this);
            view.setOnClickListener(this);
            site_select.setOnClickListener(this);
            site_select.setOnCheckedChangeListener(this);

        }

        @Override
        public void onClick(View view) {
            if (onItemClickListener != null) {
                if (view.getId() == R.id.site_select) {
                    if (site_select.isChecked()) {
                        site_select.setChecked(true);
                    } else {
                        site_select.setChecked(false);
                    }
                } else if (!selected_show) {
                    onItemClickListener.setOnItemClickListener(getAdapterPosition());
                }
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if (onItemClickListener != null) {
                return onItemClickListener.setOnItemLongClick(getAdapterPosition());
            }
            return false;
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (onItemClickListener != null) {
                onItemClickListener.setOnItemCheckedChanged(getAdapterPosition(), b);
            }
        }
    }

    public interface OnItemClickListener {

        boolean setOnItemLongClick(int position);

        void setOnItemCheckedChanged(int position, boolean isCheck);

        void setOnItemClickListener(int positiong);
    }

    public void setOnItemListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void createBookMarkDialog(final BookMarkModel bookMarkModel, final SaveDataListener saveDataListener) {
        final HBPresenter hbPresenter = new HBPresenter((Activity) mContext);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.dialog_book_dialog, null);
        final Dialog dialog = DialogUtils.createDialog(mContext, view);
        final LinearLayout ll_content = (LinearLayout) view.findViewById(R.id.ll);
        final TextView tv_left = (TextView) view.findViewById(R.id.btn_left);
        final TextView tv_right = (TextView) view.findViewById(R.id.btn_right);
        final EditText ed_name = (EditText) view.findViewById(R.id.bookmark_title);
        final EditText ed_url = (EditText) view.findViewById(R.id.bookmark_url);
        if (bookMarkModel != null) {
            ed_name.setText(bookMarkModel.getBookmark_name());
            ed_url.setText(bookMarkModel.getUrl());
        }
        tv_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                dialog.dismiss();
            }
        });
        tv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mark_name = ed_name.getText().toString().trim();
                String mark_url = ed_url.getText().toString().trim();
                if (TextUtils.isEmpty(mark_name) || TextUtils.isEmpty(mark_url)) {
                    ToastUtils.show(mContext, "书签栏目不能为空！");
                } else {
                    bookMarkModel.setBookmark_name(mark_name);
                    bookMarkModel.setUrl(mark_url);
                    saveDataListener.editSuccess(bookMarkModel);
                    dialog.cancel();
                    dialog.dismiss();
                }
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                closeWindowSoftInput(ll_content);
            }
        });
        dialog.show();
    }

    /**
     * 隐藏软键盘
     *
     * @param view
     */
    protected void closeWindowSoftInput(View view) {
        InputMethodManager imm = (InputMethodManager) mContext.getApplicationContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        boolean isSoftActive = imm.isActive();
        if (isSoftActive) {
            imm.hideSoftInputFromWindow(
                    view.getApplicationWindowToken(), 0); // 强制隐藏键盘
        }
    }


}
