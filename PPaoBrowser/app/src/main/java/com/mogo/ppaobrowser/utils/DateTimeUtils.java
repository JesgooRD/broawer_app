package com.mogo.ppaobrowser.utils;

import android.app.FragmentManager;
import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import mirko.android.datetimepicker.date.DatePickerDialog;
import mirko.android.datetimepicker.time.TimePickerDialog;

/**
 * Created by Mikes on 2016-8-23.
 */
public class DateTimeUtils {

    private static final Calendar mCalendar = Calendar.getInstance();
    public static int hourOfDay = mCalendar.get(Calendar.HOUR_OF_DAY);
    public static int minute = mCalendar.get(Calendar.MINUTE);
    public static int day = mCalendar.get(Calendar.DAY_OF_MONTH);
    public static int month = mCalendar.get(Calendar.MONTH);
    public static int year = mCalendar.get(Calendar.YEAR);

    public static void resetDate(Context context, TextView tv) {
        tv.setText(new StringBuilder().append(pad(day))
                .append(" ").append(pad(month + 1)).append(" ").append(pad(year)));
        tv.setTextColor(context.getResources().getColor(android.R.color.darker_gray));
    }

    public static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    public static String pad2(int c) {
        if (c == 12)
            return String.valueOf(c);
        if (c == 00)
            return String.valueOf(c + 12);
        if (c > 12)
            return String.valueOf(c - 12);
        else
            return String.valueOf(c);
    }

    public static String pad3(int c) {
        if (c == 12)
            return " PM";
        if (c == 00)
            return " AM";
        if (c > 12)
            return " PM";
        else
            return " AM";
    }

    public static void showDatePickerDialog(final Context context, FragmentManager fragmentManager, String tag, DatePickerDialog.OnDateSetListener onDateSetListener) {
        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(onDateSetListener, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show(fragmentManager, tag);
    }

    public static void showTimePickerDialog24h(final Context context, FragmentManager fragmentManager, String tag, TimePickerDialog.OnTimeSetListener onTimeSetListener) {
        TimePickerDialog timePickerDialog24h = TimePickerDialog.newInstance(onTimeSetListener, mCalendar.get(Calendar.HOUR_OF_DAY), mCalendar.get(Calendar.MINUTE), true);
        timePickerDialog24h.setCancelable(false);
        timePickerDialog24h.show(fragmentManager, tag);
    }

    public static void showTimePickerDialog12h(final Context context, FragmentManager fragmentManager, String tag, TimePickerDialog.OnTimeSetListener onTimeSetListener) {
        TimePickerDialog timePickerDialog12h = TimePickerDialog.newInstance(onTimeSetListener, mCalendar.get(Calendar.HOUR_OF_DAY), mCalendar.get(Calendar.MINUTE), false);
        timePickerDialog12h.setCancelable(false);
        timePickerDialog12h.show(fragmentManager, tag);
    }

    /**
     * 与当前时间比较
     *
     * @param inputYear  年
     * @param inputMonth 月
     * @param inputDay   日
     * @return 1 --大于当前日期,  -1 --小于当前日期, 0 ==等于当前时间
     */
    public static int compareDate(int inputYear, int inputMonth, int inputDay) {
        String inputDate = "" + inputYear + DateTimeUtils.pad(inputMonth) + DateTimeUtils.pad(inputDay);
        String currentDate = "" + year + DateTimeUtils.pad(month) + DateTimeUtils.pad(day);
        if (Integer.parseInt(inputDate) < Integer.parseInt(currentDate)) {
            return -1;
        } else if (Integer.parseInt(inputDate) == Integer.parseInt(currentDate)) {
            return 0;
        } else {
            return 1;
        }
    }

    public static int compareTime(int inputHour, int inputMinute) {
        String inputTime = "" + DateTimeUtils.pad(inputHour) + DateTimeUtils.pad(inputMinute);
        String currentTime = "" + DateTimeUtils.pad(hourOfDay) + DateTimeUtils.pad(minute);
        if (Integer.parseInt(inputTime) < Integer.parseInt(currentTime)) {
            return -1;
        } else if (Integer.parseInt(inputTime) == Integer.parseInt(currentTime)) {
            return 0;
        } else {
            return 1;
        }
    }

    public static Calendar parseCalendar(String s, String dateFormat) {
        return parseCalendar(s, dateFormat, TimeZone.getDefault());
    }

    public static Calendar parseCalendar(String dateStr, String dateFormat, TimeZone timeZone) {
        Calendar calendar = null;
        Date date = parseDate(dateStr, dateFormat, timeZone);
        if (date != null) {
            calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.setTimeZone(timeZone);
        }
        return calendar;
    }

    public static Date parseDate(String s, String format, TimeZone timeZone) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setTimeZone(timeZone);
        return parseDate(s, dateFormat);
    }

    public static Date parseDate(String s, String format) {
        return parseDate(s, format, TimeZone.getDefault());
    }

    public static Date parseDate(String s, DateFormat inFormat) {
        Date date = null;

        try {
            date = inFormat.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String convertDate(String s, DateFormat inFormat, DateFormat outFormat) {
        if (s == null) {
            return "";
        }

        Date date = null;
        String outputString = "";
        try {
            date = inFormat.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date != null) {
            outputString = outFormat.format(date);
        }

        return outputString;
    }

    public static String convertToCRMUpdateDate(String s) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyyMMdd");
        return convertDate(s, inputFormat, outputFormat);
    }

    public static String convertToTime(long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String times = format.format(new Date(time));
        times = times.substring(times.indexOf(" ") + 1);
        return times;
    }

    public static String convertToDate(long s) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(s);
        return calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH);
    }

    public static String convertToDate(String s) {
        long source = Long.parseLong(s);
        return convertToDate(source);
    }

    public static String convertToDisplayDate(String s) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy/MM/dd");
        return convertDate(s, inputFormat, outputFormat);
    }

    /**
     * 描述：计算两个日期所差的天数.
     *
     * @param date 第一个时间的毫秒表示
     * @return int 所差的天数
     */
    public static int getDisNow(long date) {
        return getDisDay(date, System.currentTimeMillis());
    }

    /**
     * 描述：计算两个日期所差的天数.
     *
     * @param date1 第一个时间的毫秒表示
     * @param date2 第二个时间的毫秒表示
     * @return int 所差的天数
     */
    public static int getDisDay(long date1, long date2) {
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTimeInMillis(date1);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTimeInMillis(date2);
        //先判断是否同年
        int y1 = calendar1.get(Calendar.YEAR);
        int y2 = calendar2.get(Calendar.YEAR);
        int d1 = calendar1.get(Calendar.DAY_OF_YEAR);
        int d2 = calendar2.get(Calendar.DAY_OF_YEAR);
        int maxDays = 0;
        int day = 0;
        if (y1 - y2 > 0) {
            maxDays = calendar2.getActualMaximum(Calendar.DAY_OF_YEAR);
            day = d1 - d2 + maxDays;
        } else if (y1 - y2 < 0) {
            maxDays = calendar1.getActualMaximum(Calendar.DAY_OF_YEAR);
            day = d1 - d2 - maxDays;
        } else {
            day = d1 - d2;
        }
        return Math.abs(day);
    }

}
