package com.mogo.ppaobrowser.browser.base;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.controller.MultiWindowManager;
import com.mogo.ppaobrowser.browser.fragment.WebViewFragment;
import com.mogo.ppaobrowser.browser.interface_package.BarController;
import com.mogo.ppaobrowser.browser.bean.ActivityMsg;
import com.mogo.ppaobrowser.browser.presenter.HBPresenter;
import com.mogo.ppaobrowser.update.ApkUpdateUtils;
import com.mogo.ppaobrowser.utils.AppUtility;
import com.mogo.ppaobrowser.utils.LogUtility;
import com.mogo.ppaobrowser.widget.EmptyLayout;

import java.util.List;

import butterknife.ButterKnife;


/**
 * Created by doyer on 2017/5/23.
 */
public abstract class BaseFragment extends Fragment implements IBaseView, EmptyLayout.OnRetryListener {

    //缓存Fragment view
    private View mRootView;

    public Context mContext;
    public BarController barController;
    public static int index = 0;
    public PPaoPresenter pPaoPresenter;
    public HBPresenter browserPresenter;
    //multi
    private List<MultiWindowManager> mData;
    private MultiGridAdapter adapter;
    public FrameLayout frameLayout;
    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (ActivityMsg.getActivityMsg(msg.what)) {
                case SELECT_WINDOW:
                    if (msg.arg1 != index) {
                        index = msg.arg1;
                        MultiWindowManager data = (MultiWindowManager) msg.obj;
                        if (data.mulWindowBrowserList.get(index) != null && !data.getTitle().equals("首页")) {
                            ((BrowserBaseActivity) mContext).setBrowserFragment(data.getUrl(), index);
                        } else {
                            ((BrowserBaseActivity) mContext).setMainFragment(index);
                        }
                    }
                    break;
                case CLOSE_WINDOW:
                    if (msg.arg1 != index) {
                        index = msg.arg1;
                        ((BrowserBaseActivity) mContext).setMainFragment(index);
                    }
                    break;
                case UPDATE_APK:
                    String url = (String) msg.obj;
                    updateApkVersion(url, "");
                    break;
                default:
                    break;
            }

        }
    };
    private boolean mIsMulti;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        browserPresenter = new HBPresenter(this.getActivity());
        pPaoPresenter = new PPaoPresenter(mContext, this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(attachLayoutRes(), null);
            ButterKnife.bind(this, mRootView);
            initViews();
        }
        ViewGroup parent = (ViewGroup) mRootView.getParent();
        if (parent != null) {
            parent.removeView(mRootView);
        }
        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getUserVisibleHint() && mRootView != null && !mIsMulti) {
            mIsMulti = true;
            updateViews(false);
        }
    }

    protected abstract void initViews();

    protected abstract int attachLayoutRes();

    protected abstract void updateViews(boolean b);

    protected void multiWin() {
        if (frameLayout != null && frameLayout.getParent() != null) {
            ViewGroup vg = (ViewGroup) frameLayout.getParent();
            vg.removeView(frameLayout);
        }
        MultiWindowManager multiWindowManager = MultiWindowManager.mulWindowDataList.get(index);
        WebViewFragment webViewFragment = MultiWindowManager.mulWindowBrowserList.get(index);
        if (!TextUtils.isEmpty(multiWindowManager.getUrl())) {
            MultiWindowManager.mulWindowDataList.get(index).setUrl(webViewFragment.mWebView.getUrl());
            MultiWindowManager.mulWindowDataList.get(index).setTitle(webViewFragment.mWebView.getTitle());
            //生成缩略图
            Picture snapShot = webViewFragment.mWebView.capturePicture();
            Bitmap bmp = Bitmap.createBitmap(AppUtility.getMetrics(mContext).widthPixels, AppUtility.getMetrics(mContext).heightPixels, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bmp);
            snapShot.draw(canvas);
            MultiWindowManager.mulWindowDataList.get(index).setDrawable(new BitmapDrawable(bmp));
        }
        mData = MultiWindowManager.mulWindowDataList;
        ViewGroup decorView = (ViewGroup) ((Activity) mContext).getWindow().getDecorView();

        frameLayout = new FrameLayout(mContext);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(-1, AppUtility.getMetrics(mContext).heightPixels - AppUtility.getStatusBarHeight((Activity) mContext));
        lp.topMargin = AppUtility.getStatusBarHeight(((Activity) mContext));
        decorView.addView(frameLayout, lp);
        ((Activity) mContext).getLayoutInflater().inflate(R.layout.muti_window_content, frameLayout);

        GridView gv = (GridView) frameLayout.findViewById(R.id.muti_win_panel);

        setGridView(gv);

        frameLayout.setOnClickListener(null);
        frameLayout.requestFocus();
    }

    public void setGridView(GridView gv) {
        int size = index + 1;
        int length = 100;
        DisplayMetrics dm = new DisplayMetrics();
        ((BrowserBaseActivity) mContext).getWindowManager().getDefaultDisplay().getMetrics(dm);
        float density = dm.density;
        int gridviewWidth = (int) (size * (length + 4) * density);
        int itemWidth = (int) (length * density);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                gridviewWidth, LinearLayout.LayoutParams.MATCH_PARENT);
        gv.setLayoutParams(params);
        gv.setColumnWidth(itemWidth);
        gv.setHorizontalSpacing(5);
        gv.setStretchMode(GridView.NO_STRETCH);
        gv.setNumColumns(size);

        adapter = new MultiGridAdapter(mContext);
        gv.setAdapter(adapter);
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (frameLayout != null && frameLayout.getParent() != null) {
                    ViewGroup vg = (ViewGroup) frameLayout.getParent();
                    vg.removeView(frameLayout);
                }
                Message msg = Message.obtain();
                msg.what = ActivityMsg.SELECT_WINDOW.getValue();
                msg.arg1 = position;
                msg.obj = mData.get(position);
                handler.sendMessage(msg);
            }
        });
    }


    private void updateApkVersion(final String url, String content) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(mContext);
        builder.setTitle("发现新版本");
        builder.setMessage("有新内容发布，立刻更新？");
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                LogUtility.debug("doyer", "onClick: " + url);
                ApkUpdateUtils.download(mContext, url, getResources().getString(R.string.app_name));

            }
        });
        builder.show();
    }


    class MultiGridAdapter extends BaseAdapter {
        private LayoutInflater mInflater;


        public MultiGridAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return index + 2;
        }

        @Override
        public Object getItem(int arg0) {
            return mData.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            if (convertView == null) {

                holder = new ViewHolder();

                convertView = mInflater.inflate(R.layout.muti_window_item, null);
                holder.bgimg = (ImageView) convertView.findViewById(R.id.background);
                holder.closeimg = (ImageView) convertView.findViewById(R.id.close);
                holder.contimg = (ImageView) convertView.findViewById(R.id.preview);
                holder.title = (TextView) convertView.findViewById(R.id.shortcutTitle);
                holder.title.setSingleLine(true);

                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            MultiWindowManager data = mData.get(position);


            if (data != null && data.toString() != null) {
                holder.bgimg.setBackgroundResource(R.drawable.tab_thumbnail_selected);
                holder.title.setText(data.getTitle());
                holder.contimg.setImageDrawable(data.getDrawable());
                holder.contimg.setScaleType(ImageView.ScaleType.FIT_XY);
                holder.closeimg.setBackgroundResource(R.drawable.close_window);

                final ViewHolder h = holder;
                holder.closeimg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (position != 0) {
                            MultiWindowManager.mulWindowDataList.set(position, new MultiWindowManager());
                            h.title.setText("新建窗口");
                            h.bgimg.setBackgroundResource(R.drawable.tab_thumbnail_add);
                        }
                        if (frameLayout != null && frameLayout.getParent() != null) {
                            ViewGroup vg = (ViewGroup) frameLayout.getParent();
                            vg.removeView(frameLayout);
                        }
                        Message msg = Message.obtain();
                        msg.what = ActivityMsg.CLOSE_WINDOW.getValue();
                        msg.arg1 = 0;
                        msg.obj = mData.get(position);
                        MultiWindowManager.mulWindowDataList.set(position, new MultiWindowManager());
                        handler.sendMessage(msg);
                        adapter.notifyDataSetChanged();
                    }
                });
            } else {
                holder.title.setText("新建窗口");
                holder.bgimg.setBackgroundResource(R.drawable.tab_thumbnail_add);
            }


            return convertView;
        }

        class ViewHolder {
            public ImageView bgimg;
            public ImageView contimg;
            public ImageView closeimg;
            public TextView title;
        }

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showNetError() {
    }

    @Override
    public void onRetry() {
        updateViews(false);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        LogUtility.debug("doyer", "setUserVisibleHint: ");
        if (isVisibleToUser && isVisible() && mRootView != null && !mIsMulti) {
            mIsMulti = true;
            updateViews(false);
        } else {
            super.setUserVisibleHint(isVisibleToUser);
        }
    }

    /**
     * 隐藏软键盘
     *
     * @param view
     */
    protected void closeWindowSoftInput(View view) {
        InputMethodManager imm = (InputMethodManager) mContext.getApplicationContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        boolean isSoftActive = imm.isActive();
        if (isSoftActive) {
            imm.hideSoftInputFromWindow(
                    view.getApplicationWindowToken(), 0); // 强制隐藏键盘
        }
    }

}
