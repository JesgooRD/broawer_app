package com.mogo.ppaobrowser.member.bean.responseBean;

import java.util.List;

/**
 * Created by doyer on 2017/8/3.
 */

public class IconResBean {

    /**
     * version : 1.0.1
     * data : [{"Image_url":"http://static.moogos.com/resource/hao_123_1500883433_cd57f7b0ab213e8495e8a3b0ee968c2e_eec492ee.png","Icon_name":"好123","Url":"http://m.hao123.com/"},{"Image_url":"http://static.moogos.com/resource/hao_123_1500883433_cd57f7b0ab213e8495e8a3b0ee968c2e_eec492ee.png","Icon_name":"好123","Url":"http://m.hao123.com/"},{"Image_url":"http://static.moogos.com/resource/xiaoshuo_1500883484_9ef6f25103510cb0bb6c1da60305cf77_794a2ab0.png","Icon_name":"小说","Url":""},{"Image_url":"http://static.moogos.com/resource/baidu_1500883504_c1ad4fd9b2ccb9833cbfaa2ed67e5bec_3eb0be43.png","Icon_name":"百度","Url":"http://www.baidu.com/"},{"Image_url":"http://static.moogos.com/resource/tencent_video_1500883866_e3f45a9dd4883f407ca323e395c4268d_5e07e438.png","Icon_name":"腾讯视频","Url":"http://m.v.qq.com/"},{"Image_url":"http://static.moogos.com/resource/Bzhan_1500883896_70cd6d27e2f61a01f1b0610416d6d462_efa2a76c.png","Icon_name":"B站","Url":"http://m.bilibili.com/index.html"},{"Image_url":"http://static.moogos.com/resource/A_zhan_1500883934_7f08a96b10072441bb1359d50860532c_a0de31ad.png","Icon_name":"A站","Url":"http://m.acfun.cn/"},{"Image_url":"http://static.moogos.com/resource/qiushibaike_1500883961_1162e4f2b4232193890a3b8144a6fca2_af2cee31.png","Icon_name":"糗事百科","Url":"http://www.qiushibaike.com/"},{"Image_url":"http://static.moogos.com/resource/lengtu_1500883985_54653a3c8524d20c0b82b77fec78e15f_c00d1884.png","Icon_name":"冷兔","Url":"http://m.lengxiaohua.com/new"},{"Image_url":"http://static.moogos.com/resource/taobao_1500884014_8e6a9c21f63f45a9729db18110b775c1_c6ff829f.png","Icon_name":"淘宝","Url":"https://m.taobao.com/#index"},{"Image_url":"http://static.moogos.com/resource/jingdong_1500884038_df9668fc58ab1a81990883927f0a5e21_80616096.png","Icon_name":"京东","Url":"https://m.jd.com/"},{"Image_url":"http://static.moogos.com/resource/fenghuang_1500884069_7d12e57812c737033a854afca942c409_fcc3afe8.png","Icon_name":"凤凰","Url":"http://i.ifeng.com/?ch=ifengweb_2014"},{"Image_url":"http://static.moogos.com/resource/58_1500884096_f54fd4c10b9aaa879d383261b457a230_e78a80fa.png","Icon_name":"58","Url":"http://m.58.com"},{"Image_url":"http://static.moogos.com/resource/ganji_1500884114_3a6f859080bcdd12bfc40fcdf035acac_6049d16d.png","Icon_name":"赶集","Url":"https://3g.ganji.com/"}]
     */

    public String version;
    public List<IconBean> data;

    public static class IconBean {
        /**
         * Image_url : http://static.moogos.com/resource/hao_123_1500883433_cd57f7b0ab213e8495e8a3b0ee968c2e_eec492ee.png
         * Icon_name : 好123
         * Url : http://m.hao123.com/
         */

        private String Image_url;
        private String Icon_name;
        private String Url;

        public String getImage_url() {
            return Image_url;
        }

        public void setImage_url(String Image_url) {
            this.Image_url = Image_url;
        }

        public String getIcon_name() {
            return Icon_name;
        }

        public void setIcon_name(String Icon_name) {
            this.Icon_name = Icon_name;
        }

        public String getUrl() {
            return Url;
        }

        public void setUrl(String Url) {
            this.Url = Url;
        }
    }
}
