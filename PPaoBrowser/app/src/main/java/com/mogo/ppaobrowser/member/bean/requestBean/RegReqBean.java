package com.mogo.ppaobrowser.member.bean.requestBean;

/**
 * Created by doyer on 2017/7/31.
 */

public class RegReqBean {


    /**
     * OpenId : chen
     * NickName : chenxun
     * HeadUrl : http://127.0.0.1:9091
     * Gender : 1
     * Age : 2017-05-04
     */

    private String OpenId;
    private String NickName;
    private String HeadUrl;
    private int Gender;
    private String Age;

    public String getOpenId() {
        return OpenId;
    }

    public void setOpenId(String OpenId) {
        this.OpenId = OpenId;
    }

    public String getNickName() {
        return NickName;
    }

    public void setNickName(String NickName) {
        this.NickName = NickName;
    }

    public String getHeadUrl() {
        return HeadUrl;
    }

    public void setHeadUrl(String HeadUrl) {
        this.HeadUrl = HeadUrl;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int Gender) {
        this.Gender = Gender;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String Age) {
        this.Age = Age;
    }
}
