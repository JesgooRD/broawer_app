package com.mogo.ppaobrowser.member.activity;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.base.AppConst;
import com.mogo.ppaobrowser.browser.activity.PPaoBaseActivity;
import com.mogo.ppaobrowser.member.bean.responseBean.MemberBean;
import com.mogo.ppaobrowser.member.contract.LoginContract;
import com.mogo.ppaobrowser.member.event.AppCommonEvent;
import com.mogo.ppaobrowser.member.presenter.LoginPresenter;
import com.mogo.ppaobrowser.preference.MemberCache;
import com.mogo.ppaobrowser.utils.ToastUtils;
import com.mogo.ppaobrowser.widget.ProgressView;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends PPaoBaseActivity implements LoginContract.ILoginView {

    private static final String TAG = "LoginActivity";
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private IWXAPI api;
    private LoginPresenter mLoginPresenter;

    ProgressView progressView;

    @Override
    protected void updateViews(boolean b) {

    }

    @Override
    protected void initViews() {
        setSupportActionBar(toolbar);
        toolbarTitle.setText(getString(R.string.login));
        //mLoginPresenter = new LoginPresenter(this);
        progressView = new ProgressView(this);
        api = WXAPIFactory.createWXAPI(LoginActivity.this, AppConst.WX_APP_ID, true);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_login;
    }


    @OnClick(R.id.btn_wx_login)
    public void onViewClicked() {
//            mLoginPresenter.login(HttpConstant.TYPE_LOAGIN, open_id);
        wxLogin();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (progressView != null) {
            progressView.hideProgress();
            progressView.dismiss();
        }
    }

    private void wxLogin() {
        progressView.showProgress();
        // send oauth request
        final SendAuth.Req req = new SendAuth.Req();
        req.scope = "snsapi_userinfo";
        req.state = "wechat_ppao";
        api.sendReq(req);
    }

    @Override
    public void showToast(String str) {
        Log.d(TAG, "showToast: " + str);
        showToast(str);
    }

    @Override
    public void showLoadingDialog(String string) {
        Log.d(TAG, "showLoadingDialog: ");
        progressView.showProgress();
    }

    @Override
    public void dismissDialog() {
        if (progressView != null) {
            progressView.hideProgress();
            progressView.dismiss();
        }
    }

    @Override
    public void setLoginResult(MemberBean loginResult) {
        Log.d(TAG, "setLoginResult: " + loginResult.nickName);
        if (loginResult != null)
            startActivityWithZoom(new Intent(this, MemberSystemActivity.class));
    }

    @Subscribe
    public void onEvent(AppCommonEvent event) {
        if (event.type.equals(AppCommonEvent.RIGISTER) && MemberCache.getMemUserId() != 0) {
            startActivityWithZoom(new Intent(LoginActivity.this, MemberSystemActivity.class));
            finish();
        } else {
            ToastUtils.show(LoginActivity.this, "登陆失败");
            finish();
        }
    }

}
