package com.mogo.ppaobrowser.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.mogo.ppaobrowser.PPaoApplication;

import java.util.HashMap;
import java.util.Map;


public class MemberCache {

    private static final String PREF_MEM = "mem_preference";
    private static final String MEM_OPENID = "openId";
    private static final String MEM_NICK_NAME = "nick_name";
    private static final String MEM_HEAD_URL = "head_url";
    private static final String MEM_CASH = "cash";
    private static final String MEM_GOLD = "gold";
    private static final String MEM_AGE = "age";
    private static final String MEM_GENDER = "gender";
    private static final String MEM_USERID = "userId";
    private static final String MEM_SIGN_COUNT = "sign_count";
    private static final String MEMBER = "member";
    //记录任务状态 需要本地去记录
    private static final String TASK_INFO = "task_info";
    private static final String TASK_NOVEL = "task_novel";
    private static final String TASK_SIGN = "task_sign";
    private static final String TASK_SHARE = "task_share";
    private static final String TASK_CARTOON = "task_cartoon";
    private static final String TASK_NEWS = "task_news";
    private static final String TASK_TIME = "task_time";

    public static String TAG_NOVEL = "novel";
    public static String TAG_CARTOON = "cartoon";
    public static String TAG_VIDEO = "video";
    public static String TAG_NEWS = "news";
    public static String TAG_DOWNLOAD = "download";

    public static final String FIRST = "first";

    private static SharedPreferences getSharedPreference() {
        return PPaoApplication.getApplication().getSharedPreferences(PREF_MEM, Context.MODE_PRIVATE);
    }

    public static boolean getBooleanPreference(String key, boolean defaultValue) {
        SharedPreferences preferences = getSharedPreference();
        return preferences.getBoolean(key, defaultValue);
    }

    private static void putBooleanPreference(String key, boolean value) {
        SharedPreferences preferences = getSharedPreference();
        preferences.edit().putBoolean(key, value).apply();
    }


    public static String getStringPreference(String key, String defaultValue) {
        SharedPreferences preferences = getSharedPreference();
        return preferences.getString(key, defaultValue);
    }

    private static void putStringPreference(String key, String value) {
        SharedPreferences preferences = getSharedPreference();
        preferences.edit().putString(key, value).apply();
    }

    private static void putIntPreference(String key, int value) {
        SharedPreferences preferences = getSharedPreference();
        preferences.edit().putInt(key, value).apply();
    }

    private static int getIntPreference(String key, int defalutValue) {
        SharedPreferences preferences = getSharedPreference();
        return preferences.getInt(key, defalutValue);
    }

    private static void putFloatPreference(String key, float value) {
        SharedPreferences preferences = getSharedPreference();
        preferences.edit().putFloat(key, value).apply();
    }

    private static float getFloatPreference(String key, long defalutValue) {
        SharedPreferences preferences = getSharedPreference();
        return preferences.getFloat(key, defalutValue);
    }

    public static String getMemOpenid() {
        return getStringPreference(MEM_OPENID, null);
    }

    public static void setMemOpenid(String openId) {
        putStringPreference(MEM_OPENID, openId);
    }

    public static String getMemNickName() {
        return getStringPreference(MEM_NICK_NAME, null);
    }

    public static void setMemNickName(String nickName) {
        putStringPreference(MEM_NICK_NAME, nickName);
    }

    public static String getMemHeadUrl() {
        return getStringPreference(MEM_HEAD_URL, null);
    }

    public static void setMemHeadUrl(String headUrl) {
        putStringPreference(MEM_HEAD_URL, headUrl);
    }

    public static float getMemCash() {
        return getFloatPreference(MEM_CASH, 0);
    }

    public static void setMemCash(float cash) {
        putFloatPreference(MEM_CASH, cash);
    }

    public static String getMemAge() {
        return getStringPreference(MEM_AGE, null);
    }

    public static void setMemAge(String age) {
        putStringPreference(MEM_AGE, age);
    }

    public static int getMemGold() {
        return getIntPreference(MEM_GOLD, 0);
    }

    public static void setMemGold(int gold) {
        putIntPreference(MEM_GOLD, gold);
    }

    public static int getMemGender() {
        return getIntPreference(MEM_GENDER, 0);
    }

    public static void setMemGender(int gender) {
        putIntPreference(MEM_GENDER, gender);
    }

    public static int getMemSignCount() {
        return getIntPreference(MEM_SIGN_COUNT, 0);
    }

    public static void setMemSignCount(int count) {
        putIntPreference(MEM_SIGN_COUNT, count);
    }

    public static int getMemUserId() {
        return getIntPreference(MEM_USERID, 0);
    }

    public static void setMemUserId(int userId) {
        putIntPreference(MEM_USERID, userId);
    }

    public static boolean getTaskShareState() {
        return getBooleanPreference(TASK_SHARE, false);
    }

    public static void setTaskShareState(boolean taskShareState) {
        putBooleanPreference(TASK_SHARE, taskShareState);
    }

    public static boolean getTaskNovelState() {
        return getBooleanPreference(TASK_NOVEL, false);
    }

    public static void setTaskNovelState(boolean taskNovelState) {
        putBooleanPreference(TASK_NOVEL, taskNovelState);
    }

    public static boolean getTaskNewsState() {
        return getBooleanPreference(TASK_NEWS, false);
    }

    public static void setTaskNewsState(boolean taskNewsState) {
        putBooleanPreference(TASK_NEWS, taskNewsState);
    }

    public static boolean getTaskCartoonState() {
        return getBooleanPreference(TASK_CARTOON, false);
    }

    public static void setTaskCartoonState(boolean taskNovelState) {
        putBooleanPreference(TASK_CARTOON, taskNovelState);
    }

    public static boolean getTaskSignState() {
        return getBooleanPreference(TASK_SIGN, false);
    }

    public static void setTaskSignState(boolean taskSignState) {
        putBooleanPreference(TASK_SIGN, taskSignState);
    }

    public static boolean getTaskInfoState() {
        return getBooleanPreference(TASK_INFO, false);
    }

    public static void setTaskInfoState(boolean taskInfoState) {
        putBooleanPreference(TASK_INFO, taskInfoState);
    }

    public static String getNewsTime() {
        return getStringPreference(TAG_NEWS, null);
    }

    public static void setNewsTime(String time) {
        putStringPreference(TAG_NEWS, time);
    }

    public static String getDownloadTime() {
        return getStringPreference(TAG_DOWNLOAD, null);
    }

    public static void setDownloadTime(String time) {
        putStringPreference(TAG_DOWNLOAD, time);
    }

    public static String getVideoTime() {
        return getStringPreference(TAG_VIDEO, null);
    }

    public static void setVideoTime(String time) {
        putStringPreference(TAG_VIDEO, time);
    }

    public static String getCartoonTime() {
        return getStringPreference(TAG_CARTOON, null);
    }

    public static void setCartoonTime(String time) {
        putStringPreference(TAG_CARTOON, time);
    }

    public static String getNovelTime() {
        return getStringPreference(TAG_NOVEL, null);
    }

    public static void setNovelTime(String time) {
        putStringPreference(TAG_NOVEL, time);
    }

    public static String getTaskTime() {
        return getStringPreference(TASK_TIME, null);
    }

    public static void setTaskTime(String time) {
        putStringPreference(TASK_TIME, time);
    }

    public static boolean getIsFirst() {
        return getBooleanPreference(FIRST, true);
    }

    public static void setIsFirst(boolean isfirst) {
        putBooleanPreference(FIRST, isfirst);
    }

    private static Map<String, Object> mCache = new HashMap<String, Object>();

    public static void put(String key, Object value) {
        mCache.put(key, value);
    }

    public static Object get(String key) {
        return mCache.get(key);
    }

    public static void initTaskState(boolean b) {
        setTaskCartoonState(b);
        setTaskShareState(b);
        setTaskNovelState(b);
        setTaskSignState(b);
        setTaskNewsState(b);
        if (!b) {
            setNovelTime(null);
            setCartoonTime(null);
            setNewsTime(null);
            setDownloadTime(null);
            setVideoTime(null);
        }
        setTaskTime(System.currentTimeMillis() + "");
    }
}
