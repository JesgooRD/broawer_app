package com.mogo.ppaobrowser.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Patterns;
import android.webkit.URLUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by doyer on 2017/5/23.
 */
public class UrlUtils {
    private static final Pattern ACCEPTED_URI_SCHEMA = Pattern.compile(
            "(?i)" + // switch on case insensitive matching
                    '(' +    // begin group for schema
                    "(?:http|https|file)://" +
                    "|(?:inline|data|about|javascript):" +
                    "|(?:.*:.*@)" +
                    ')' +
                    "(.*)");
    // Google search
    public final static String QUERY_PLACE_HOLDER = "%s";

    public static String smartUrlFilter(@NonNull String url, boolean canBeSearch, String searchUrl) {
        String inUrl = url.trim();
        boolean hasSpace = inUrl.indexOf(' ') != -1;
        Matcher matcher = ACCEPTED_URI_SCHEMA.matcher(inUrl);
        if (matcher.matches()) {
            // force scheme to lowercase
            String scheme = matcher.group(1);
            String lcScheme = scheme.toLowerCase();
            if (!lcScheme.equals(scheme)) {
                inUrl = lcScheme + matcher.group(2);
            }
            if (hasSpace && Patterns.WEB_URL.matcher(inUrl).matches()) {
                inUrl = inUrl.replace(" ", "%20");
            }
            return inUrl;
        }
        if (!hasSpace) {
            if (Patterns.WEB_URL.matcher(inUrl).matches()) {
                return URLUtil.guessUrl(inUrl);
            }
        }
        if (canBeSearch) {
            return searchUrl;
        } else
            return "";
    }

    public static boolean isUrlFormat(@NonNull String url) {
        String inUrl = url.trim();
        Matcher matcher = ACCEPTED_URI_SCHEMA.matcher(inUrl);
        if (matcher.matches()) {
            return true;
        } else return false;
    }

    /**
     * Returns whether the given url is the bookmarks/history page or a normal website
     */
    public static boolean isSpecialUrl(@Nullable String url) {
        return true;
    }
}
