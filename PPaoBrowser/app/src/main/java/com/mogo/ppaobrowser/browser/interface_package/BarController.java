package com.mogo.ppaobrowser.browser.interface_package;

public interface BarController {

    void forward();

    void back();

    void backHome();

    void stopLoading();

    void refresh();

    void showSearchBar();

    void hideSearchBar();

}
