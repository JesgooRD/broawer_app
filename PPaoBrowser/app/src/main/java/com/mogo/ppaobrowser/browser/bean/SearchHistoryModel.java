package com.mogo.ppaobrowser.browser.bean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by doyer on 2017/6/28.
 */

@Entity
public class SearchHistoryModel {
    //搜索时间
    long time;
    //搜索字段
    String search_content;
//    int status;

    @Generated(hash = 1148528146)
    public SearchHistoryModel(long time, String search_content) {
        this.time = time;
        this.search_content = search_content;
    }

    @Generated(hash = 2050687136)
    public SearchHistoryModel() {
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getSearch_content() {
        return this.search_content;
    }

    public void setSearch_content(String search_content) {
        this.search_content = search_content;
    }
}
