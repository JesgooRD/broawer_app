package com.mogo.ppaobrowser.member.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.mogo.ppaobrowser.R;
import com.mogo.ppaobrowser.browser.activity.BrowserActivity;
import com.mogo.ppaobrowser.browser.activity.PPaoBaseActivity;
import com.mogo.ppaobrowser.browser.interface_package.DataAccessListener;
import com.mogo.ppaobrowser.member.bean.responseBean.ComGoldResBean;
import com.mogo.ppaobrowser.member.bean.responseBean.MemberBean;
import com.mogo.ppaobrowser.member.biz.UserBiz;
import com.mogo.ppaobrowser.utils.BitmapLoader;
import com.mogo.ppaobrowser.utils.CameraUtils;
import com.mogo.ppaobrowser.utils.DialogUtils;
import com.mogo.ppaobrowser.utils.FileUtils;
import com.mogo.ppaobrowser.utils.ScreenUtils;
import com.mogo.ppaobrowser.utils.ToastUtils;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

import static com.mogo.ppaobrowser.R.id.tv_gender;


public class PersonInfoActivity extends PPaoBaseActivity {


    private static final String TAG = "PersonInfoActivity";
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.simple_photo)
    SimpleDraweeView simplePhoto;
    @BindView(R.id.tv_nickname)
    TextView tvNickname;
    @BindView(tv_gender)
    TextView tvGender;
    @BindView(R.id.tv_city)
    TextView tvCity;
    @BindView(R.id.tv_birthday)
    TextView tvBirthday;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    @BindView(R.id.refresh_layout)
    PtrClassicFrameLayout refreshLayout;

    private static final int REQUEST_CHOOSE_CAMERA_AND_CROP = 0xac23;
    private static final int REQUEST_CHOOSE_PICTURE_AND_CROP = 0xac22;
    private static final int REQUEST_SELECT_CITY = 0xac24;

    private static String mPhotoPath;
    private String cityId;

    UserBiz userBiz = new UserBiz();
    MemberBean member;


    @Override
    protected void updateViews(boolean b) {
        if (b) {
            simplePhoto.setImageURI(member.headUrl);
            tvNickname.setText(member.nickName);
            tvGender.setText(member.gender == 1 ? "男" : "女");
            tvBirthday.setText(member.age);
        }
    }

    @Override
    protected void initViews() {
        setSupportActionBar(toolbar);
        toolbarTitle.setText(getString(R.string.mine_info_title));
        initRefresh();
        userBiz.fetchUserInfo(new DataAccessListener<MemberBean>() {
            @Override
            public void requestSuccess(MemberBean ob) {
                if (ob != null) {
                    member = ob;
                    simplePhoto.setImageURI(ob.headUrl);
                    tvNickname.setText(ob.nickName);
                    tvGender.setText(ob.gender == 1 ? "男" : "女");
                    tvBirthday.setText(ob.age);
                }
            }

            @Override
            public void requestFail(String error) {

            }
        });
    }

    private void initRefresh() {
        refreshLayout.setPullToRefresh(false);
        // the following are default settings
        refreshLayout.setResistance(1.7f);
        refreshLayout.setRatioOfHeaderHeightToRefresh(1.2f);
        refreshLayout.setDurationToClose(200);
        refreshLayout.setDurationToCloseHeader(1000);
        // default is false
        refreshLayout.setPullToRefresh(false);
        // default is true
        refreshLayout.setKeepHeaderWhenRefresh(true);
        refreshLayout.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, refreshLayout, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                refreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        refreshLayout.refreshComplete();
                    }
                }, 100);
            }
        });
    }

    @Override
    protected int attachLayoutRes() {
        return R.layout.activity_person_info;
    }

    @OnClick({R.id.item_portrait, R.id.item_update, R.id.item_nick_name, R.id.item_gender, R.id.item_city, R.id.item_birthday, R.id.tv_birthday, R.id.item_setting})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.item_portrait:
//                createSelectPictureDialog();
                break;
            case R.id.item_nick_name:
//                createUpdateNicknameDialog();
                break;
            case R.id.item_gender:
//                createSelectGenderDialog();
                break;
            case R.id.item_city:
                /*Intent intent = new Intent(this, CitySelectActivity.class);
                intent.putExtra(CitySelectActivity.TAG_CONTANT_ALL, false);
                startActivityForResult(intent, REQUEST_SELECT_CITY);*/
                break;
            case R.id.item_birthday:
                createDateDialog();
                break;
            case R.id.item_setting:
                //自定义PopupWindow的布局
                final View contentView = LayoutInflater.from(PersonInfoActivity.this).inflate(R.layout.logout_layout, null);
                //初始化PopupWindow,并为其设置布局文件
                final PopupWindow popupWindow = new PopupWindow(contentView);
                TextView tv_logout = (TextView) contentView.findViewById(R.id.tv_logout);
                contentView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        userBiz.removeData();
                        startActivityWithZoom(new Intent(PersonInfoActivity.this, BrowserActivity.class));
                        PersonInfoActivity.this.finish();
                    }
                });
                popupWindow.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
                popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
                popupWindow.setFocusable(true);
                popupWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));   //为PopupWindow设置透明背景.
                popupWindow.setOutsideTouchable(false);
                //设置PopupWindow进入和退出动画
                popupWindow.setAnimationStyle(R.style.PopupAnimation);
                //设置PopupWindow显示的位置
                popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
                backgroundAlpha(0.6f);
                popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        backgroundAlpha(1f);
                    }
                });
                break;
            case R.id.item_update:
                final MemberBean memberBean = member;
                // 等陈老板那边接口写好了再改
                if (memberBean != null) {
                    memberBean.age = tvBirthday.getText().toString().trim();
                    memberBean.gender = member.gender;
                    memberBean.headUrl = member.headUrl;
                    memberBean.nickName = tvNickname.getText().toString();
                    memberBean.userId = member.userId;
                    userBiz.update(memberBean, new DataAccessListener<ComGoldResBean>() {
                        @Override
                        public void requestSuccess(ComGoldResBean ob) {
                            Log.d(TAG, "requestSuccess: " + ob.toString());
                            member = memberBean;
                            userBiz.setDefault(member);
                            updateViews(true);
                            ToastUtils.show(PersonInfoActivity.this, "更新成功！");
                        }

                        @Override
                        public void requestFail(String error) {
                            Log.d(TAG, "requestFail: ");

                        }
                    });
                }
                break;
            default:
                break;
        }
    }

    private void createDateDialog() {
        /*DateTimeUtils.showDatePickerDialog(this, getFragmentManager(), "birthday", new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
                if (DateTimeUtils.compareDate(year, monthOfYear, dayOfMonth) == 1) {
                    ToastUtils.show(PersonInfoActivity.this, R.string.time_error_tip2);
                    return;
                }
                tvBirthday.setText(new StringBuilder()
                        .append(DateTimeUtils.pad(year))
                        .append("-")
                        .append(DateTimeUtils.pad(monthOfYear + 1))
                        .append("-")
                        .append(DateTimeUtils.pad(dayOfMonth)));
                tvBirthday.setTextColor(PersonInfoActivity.this.getResources().getColor(R.color.global_text_hint_color));
            }
        });*/

        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                //修改日历控件的年，月，日
                //这里的year,monthOfYear,dayOfMonth的值与DatePickerDialog控件设置的最新值一致
                tvBirthday.setText(new StringBuilder()
                        .append(year)
                        .append("-")
                        .append(monthOfYear + 1)
                        .append("-")
                        .append(dayOfMonth));
                tvBirthday.setTextColor(PersonInfoActivity.this.getResources().getColor(R.color.global_text_hint_color));
            }
        };

        Calendar dateAndTime = Calendar.getInstance(Locale.CHINA);
        //当点击DatePickerDialog控件的设置按钮时，调用该方法
        DatePickerDialog dateDlg = new DatePickerDialog(PersonInfoActivity.this,
                listener,
                dateAndTime.get(Calendar.YEAR),
                dateAndTime.get(Calendar.MONTH),
                dateAndTime.get(Calendar.DAY_OF_MONTH));
        dateDlg.show();

    }

    public void createSelectPictureDialog() {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.dialog_select_picture, null);
        final Dialog dialog = DialogUtils.createDialog(this, view);
        TextView camera = (TextView) view.findViewById(R.id.dialog_pick_camera);
        TextView picture = (TextView) view.findViewById(R.id.dialog_pick_picture);
        dialog.setCanceledOnTouchOutside(true);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                takePhoto();
            }
        });
        picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                CameraUtils.openPhotos(PersonInfoActivity.this, REQUEST_CHOOSE_PICTURE_AND_CROP);
            }
        });

        dialog.show();
    }

    private void takePhoto() {
        if (Environment.MEDIA_MOUNTED.equals(Environment
                .getExternalStorageState())) {
            String dir = Environment.getExternalStorageDirectory() + "/sheying/Camera/";
            File destDir = new File(dir);
            if (!destDir.exists()) {
                destDir.mkdirs();
            }
            File file = new File(dir, new DateFormat().format(
                    "yyyy_MMdd_hhmmss", Calendar.getInstance(Locale.CHINA))
                    + ".jpg");
            mPhotoPath = file.getAbsolutePath();
            try {
                CameraUtils.openCamera(PersonInfoActivity.this, REQUEST_CHOOSE_CAMERA_AND_CROP, file);
            } catch (ActivityNotFoundException anf) {
                anf.printStackTrace();
                ToastUtils.show(this, R.string.camera_not_prepared);
            }
        } else {
            ToastUtils.show(this, R.string.sdcard_not_exist_toast);
        }
    }

    public void createUpdateNicknameDialog() {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.dialog_mine_update_input, null);
        final Dialog dialog = DialogUtils.createDialog(this, view);
        final LinearLayout ll_content = (LinearLayout) view.findViewById(R.id.ll_content);
        DialogUtils.setDialogSize(this, 0.8f, -0.5f, ll_content);
        TextView btn_male = (TextView) view.findViewById(R.id.tv_title);
        final EditText et_content = (EditText) view.findViewById(R.id.et_content);
        TextView tv_explain = (TextView) view.findViewById(R.id.tv_explain);
        btn_male.setText(R.string.mine_update_dialog_nickname);
        tv_explain.setText(R.string.mine_update_dialog_nickname_explain);
        if (member != null)
            et_content.setText(member.nickName);
        Button btn_confirm = (Button) view.findViewById(R.id.btn_confirm);
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeWindowSoftInput(ll_content);
                dialog.dismiss();
                String newName = et_content.getText().toString().trim();
                if (TextUtils.isEmpty(newName)) {
                    showToast(R.string.mine_update_error_hint1);
                } else {
                    tvNickname.setText(newName);
                }
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                closeWindowSoftInput(ll_content);
            }
        });
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    public void createSelectGenderDialog() {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.dialog_select_gender, null);
        final Dialog dialog = DialogUtils.createDialog(this, view);
        TextView btn_male = (TextView) view.findViewById(R.id.dialog_pick_male);
        TextView btn_female = (TextView) view.findViewById(R.id.dialog_pick_female);
        dialog.setCanceledOnTouchOutside(true);
        btn_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                tvGender.setText(R.string.mine_update_gender_male);
            }
        });
        btn_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                tvGender.setText(R.string.mine_update_gender_female);
            }
        });

        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CHOOSE_CAMERA_AND_CROP:
                    if (TextUtils.isEmpty(mPhotoPath)) {
                        ToastUtils.show(this, R.string.camera_not_prepared);
                    } else {
                        Uri source = Uri.parse("file://" + mPhotoPath);
                        Uri destination = Uri.fromFile(new File(getCacheDir(), "croppedAvatar"));
                        Crop.of(source, destination)
                                .withMaxSize(ScreenUtils.getScreenWidth(this), ScreenUtils.getScreenHeight(this))
                                .start(this);
                    }
                    break;
                case REQUEST_CHOOSE_PICTURE_AND_CROP:
                    Uri source = data.getData();
                    String original = FileUtils.getPath(this, source);
                    if (original != null) {
                        source = Uri.parse("file://" + original);
                    }
                    Uri destination = Uri.fromFile(new File(getCacheDir(), "croppedAvatar"));
                    Crop.of(source, destination)
                            .withMaxSize(ScreenUtils.getScreenWidth(this), ScreenUtils.getScreenHeight(this))
                            .start(this);
                    break;
                case Crop.REQUEST_CROP:
                    Uri avatarUri = Uri.fromFile(new File(getCacheDir(), "croppedAvatar"));
                    String photoPath = FileUtils.getPath(this, avatarUri);
                    Bitmap bitmap = BitmapLoader.getBitmapFromFile(photoPath, 720,
                            1280);
                    String thumbnailPath = BitmapLoader.saveBitmapToLocal(this, bitmap);
//                    ServerApis.uploadPicture(new File(thumbnailPath));
                    break;
                case REQUEST_SELECT_CITY:
                    String cityName = data.getStringExtra("cityName");
                    cityId = data.getStringExtra("cityId");
                    tvCity.setText(cityName);
                    break;
                default:
                    break;
            }
        } else {
            if (requestCode == Crop.REQUEST_CROP && data != null) {
                Throwable throwable = Crop.getError(data);
                if (throwable != null && throwable instanceof OutOfMemoryError) {
                    showToast(R.string.selection_too_large);
                }
            }
        }
    }

}
