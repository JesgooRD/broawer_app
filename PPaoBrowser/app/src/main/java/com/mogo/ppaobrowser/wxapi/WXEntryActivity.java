package com.mogo.ppaobrowser.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.mogo.ppaobrowser.base.AppConst;
import com.mogo.ppaobrowser.browser.interface_package.DataAccessListener;
import com.mogo.ppaobrowser.browser.interface_package.RequestListener;
import com.mogo.ppaobrowser.member.api.ApiService;
import com.mogo.ppaobrowser.member.bean.responseBean.RegisterBean;
import com.mogo.ppaobrowser.member.bean.WxAccessBean;
import com.mogo.ppaobrowser.member.bean.WxUserBean;
import com.mogo.ppaobrowser.member.bean.responseBean.ComGoldResBean;
import com.mogo.ppaobrowser.member.bean.responseBean.MemberBean;
import com.mogo.ppaobrowser.member.biz.UserBiz;
import com.mogo.ppaobrowser.member.constant.HttpConstant;
import com.mogo.ppaobrowser.member.event.AppCommonEvent;
import com.mogo.ppaobrowser.member.utils.RetrofitUtils;
import com.mogo.ppaobrowser.preference.MemberCache;
import com.mogo.ppaobrowser.utils.HttpUtil;
import com.mogo.ppaobrowser.utils.ToastUtils;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jie.du on 16/10/24.
 */

public class WXEntryActivity extends Activity implements IWXAPIEventHandler, RequestListener {

    String wx_base_url = "https://api.weixin.qq.com/sns/";

    public static final int GET_ACCESS_TOKEN = 0x15;
    public static final int GET_USER_INFO = 0x16;
    public static final int LOGIN = 0x17;
    public static final int REGISTER = 0x18;
    private IWXAPI api;
    private String TAG = "ppao_wx";
    WxAccessBean accessBean;
    WxUserBean wxUserBean;
    RegisterBean loginBean;
    UserBiz userBiz = new UserBiz();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        api = WXAPIFactory.createWXAPI(this, AppConst.WX_APP_ID, false);
        api.handleIntent(getIntent(), this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq req) {
        String result = req.openId;
        ToastUtils.show(this, result);
    }

    @Override
    public void onResp(BaseResp baseResp) {
        String result = "";
        if (baseResp != null) {

            switch (baseResp.errCode) {
                case BaseResp.ErrCode.ERR_OK:
                    result = "发送成功";
                    // 登陆返回
                    if (baseResp instanceof SendAuth.Resp) {
                        String code = ((SendAuth.Resp) baseResp).code;
                        getAccessToken(code);
                        finish();
                    } else if (baseResp.transaction.contains("webpage"))// 分享返回
                    {
                        userBiz.share(new DataAccessListener<ComGoldResBean>() {
                            @Override
                            public void requestSuccess(ComGoldResBean ob) {
                                int add_gold = ob.gold;
                                new AppCommonEvent(AppCommonEvent.WECHAT_SHARE).putExtra(AppCommonEvent.EVENT_KEY, "分享成功，获得" + add_gold + "金币").putExtra(AppCommonEvent.EVENT_KEY2, 200).postEvent();
                            }

                            @Override
                            public void requestFail(String error) {
                                if (error.equals("300"))
                                    new AppCommonEvent(AppCommonEvent.WECHAT_SHARE).putExtra(AppCommonEvent.EVENT_KEY, "已经分享过了！").putExtra(AppCommonEvent.EVENT_KEY2, 300).postEvent();
                                if (error.equals("400")) {
                                    new AppCommonEvent(AppCommonEvent.WECHAT_SHARE).putExtra(AppCommonEvent.EVENT_KEY, "用户不存在！").putExtra(AppCommonEvent.EVENT_KEY2, 400).postEvent();
                                }

                            }
                        });
                        finish();
                    }
                    break;
                case BaseResp.ErrCode.ERR_USER_CANCEL:
                    result = "发送取消";
                    Log.d(TAG, "onResp: " + result);
                    finish();
                    break;
                case BaseResp.ErrCode.ERR_AUTH_DENIED:
                    result = "发送被拒绝";
                    Log.d(TAG, "onResp: " + result);
                    finish();
                    break;
                default:
                    result = "发送返回";
                    Log.d(TAG, "onResp: " + result);
                    finish();
                    break;
            }
        }

    }


    /**
     * 获取openid 等值用于后期操作
     *
     * @param code 请求码
     */

    public void getAccessToken(final String code) {
        String path = wx_base_url + "oauth2/access_token?appid="
                + AppConst.WX_APP_ID
                + "&secret="
                + AppConst.WX_APP_SECRET
                + "&code="
                + code
                + "&grant_type=authorization_code";
        HttpUtil.HttpGet(path, GET_ACCESS_TOKEN, this);
    }

    @Override
    public void requestSuccess(final int request_code, Object ob) {
        final Gson gson = new Gson();
        switch (request_code) {
            case GET_ACCESS_TOKEN:
                accessBean = gson.fromJson(ob.toString(), WxAccessBean.class);
                String path = wx_base_url + "userinfo?access_token=" + accessBean.getAccess_token() + "&openid=" + accessBean.getOpenid();
                //获得微信用户信息
                HttpUtil.HttpGet(path, GET_USER_INFO, this);

                break;
            case GET_USER_INFO:
                //获得微信个人信息作为注册信息
                wxUserBean = gson.fromJson(ob.toString(), WxUserBean.class);
                Log.d(TAG, "requestSuccess: " + wxUserBean.getNickname());
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("OpenId", wxUserBean.getOpenid());
                    jsonObject.put("NickName", wxUserBean.getNickname());
                    jsonObject.put("HeadUrl", wxUserBean.getHeadimgurl());
                    jsonObject.put("Gender", wxUserBean.getSex());
                    jsonObject.put("Age", "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json;charset=UTF-8"), jsonObject.toString());
                RetrofitUtils.createService(ApiService.class)//实例化Retrofit对象
                        .register(HttpConstant.TYPE_REGISTER, body)
                        .enqueue(new Callback<RegisterBean<MemberBean>>() {
                            @Override
                            public void onResponse(Call<RegisterBean<MemberBean>> call, Response<RegisterBean<MemberBean>> response) {
                                Log.d(TAG, "onResponse: " + response.message());
                                if (response != null && response.body() != null && response.body().code == 200) {
                                    //注册成功
                                    if (response.body().data.userId != 0) {
                                        MemberCache.setMemUserId(response.body().data.userId);
//                                        userBiz.setDefault(response.body().data);
                                        new AppCommonEvent(AppCommonEvent.RIGISTER).postEvent();
                                    }
                                } else if (response != null && response.body() != null && response.body().code == 300) {
                                    //注册过
                                    int userId = response.body().userId;
                                    MemberCache.setMemUserId(userId);
                                    new AppCommonEvent(AppCommonEvent.RIGISTER).putExtra(AppCommonEvent.EVENT_KEY, userId).
                                            postEvent();
                                }
                            }

                            @Override
                            public void onFailure(Call<RegisterBean<MemberBean>> call, Throwable t) {
                                Log.d(TAG, "onFailure: " + t.getMessage());
                            }
                        });
                break;
        }
    }

    @Override
    public void requestFail(String error) {
        Log.d(TAG, "requestFail: " + error);
    }
}
